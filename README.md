# FS-Infrastructure
This repository is comprised of several projects.


## API Server
API server is the container where all API endpoints are located. It is the only place where databases are accessed.

### Prerequisite
  1. php version(>=5.6) is installed;
  2. Composer is installed (Refer to Proxy readme for more detailed composer installation instructions);

### Configuration  
  1. deploy all these folders & files in web server;
  2. configure 'public' folder as web server's document root, or change 'public' folder name to web server's document root;
  3. go to the folder where composer.json locates, and run 'php composer install' (or, 'php composer.phar install' based on composer's name);
  4. rename all configuration files under /config folder to the name without '_example', e.g. change common_example.php to common.php.
  5. run 'php /your-path-to-database.php/database.php' to create essential databases and tables;
              
## Crontab Server
A single crontab entry to run every minute via php cli, which polls the database for jobs to run.

### Prerequisite
 1. php version(>=5.6) is installed;

### configuration  
  1. copy all these folders & files to the server (/user/home/directory/bin/);
  2. rename all configuration files to the name without '_example', e.g. change common_example.php to common.php.
  3. config crontab to run the single entry job_scheduler.php every 30 minutes;
              
## Proxy Server
A reverse proxy server.

### Prerequisite
  1. php version(>=5.6) is installed;

### configuration  
  1. copy all these folders & files to the server (/user/home/directory/bin/);
  2. rename all configuration files to the name without '_example', e.g. change common_example.php to common.php.
