<?php

namespace FS\Common;

class IMAPHelper
{
    public static function message($msg, $object = null, $die = false)
    {
        echo '[' .  date('Y-m-d H:i:s') . ']' . $msg . PHP_EOL;
        
        if (!empty($object)) {
            print_r($object);
            echo PHP_EOL;
        }

        if ($die) {
            exit();
        }
    }

    public static function retrieveAttachments($conn, $msgNo)
    {
        $columns   = [];
        $fields    = [];
        $col2field = [
            'message_id'    => 'Message-ID',
            'from_address'  => 'From',
            'to_address'    => 'To',
            'subject'       => 'Subject',
            'time_received' => 'Date',
            'reason'        => 'Status'
        ];

        // Fetch body parts
        $fetchTextContent     = imap_fetchbody($conn, $msgNo, '1');
        $fetchAll             = imap_fetchbody($conn, $msgNo, '');
        $needle               = 'Delivery to the following recipients failed.';
        $attachments          = substr($fetchAll, strpos($fetchAll, $needle));
        $lines                = explode(PHP_EOL, $attachments);
        $items                = ['Message-ID', 'From', 'Subject', 'Date', 'Status', 'Diagnostic-Code'];
        $diagnosticCodePrefix = '{FS-PREFIX}';

        foreach ($lines as $line) {
            foreach ($items as $item) {
                if (strpos($line, $item) === 0) {
                    $fields[$item] = trim(iconv_mime_decode(substr($line, strlen($item) + 1)));

                    if ($item == 'Diagnostic-Code') {
                        if (strpos($fields[$item], 'smtp;') === 0) {
                            $fields[$item] = substr($fields[$item], 5);
                        }

                        $diagnosticCodePrefix = explode(' ', $fields[$item])[0];
                    }
                }
            }

            if (strpos($line, $diagnosticCodePrefix) === 0) {
                $fields['Diagnostic-Code'] .= substr($line, strlen($diagnosticCodePrefix));
            }
        }

        // Get email address that failed to sent
        $fields['To'] = substr($fetchTextContent, strpos($fetchTextContent, $needle) + strlen($needle));
        $fields['To'] = str_replace(' ', '', $fields['To']);
        $arr          = [];

        foreach (explode(PHP_EOL, $fields['To']) as $element) {
            if (!empty(trim($element, PHP_EOL))) {
                array_push($arr, trim($element, PHP_EOL));
            }
        }

        $fields['To'] = implode(', ', $arr);

        foreach ($col2field as $col => $field) {
            if (!isset($fields[$field])) {
                throw new \Exception('Failed to extract information about {' . $field . '}');
            }

            $columns[$col] = $fields[$field];
        }

        if (isset($fields['Diagnostic-Code'])) {
            $columns['reason'] = $fields['Diagnostic-Code'];
        }

        $columns['status']        = 'failed';
        $columns['message_id']    = trim($columns['message_id'], '<>');
        $columns['time_received'] = (new \DateTime($columns['time_received']))->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');

        return $columns;
    }
}
