<?php

use FS\Common\IMAPHelper;
use FS\Common\CURL;

require __DIR__ . '/config/common.php';
require __DIR__ . '/autoload.php';

// Check constants defined in config file
if (!defined('IMAP_SERVER') || 
    !defined('IMAP_USERNAME') || 
    !defined('IMAP_PASSWORD') ||
    !defined('INFRASTRUCTURE_SERVICE_LOCATION') || 
    !defined('INFRASTRUCTURE_SERVICE_TOKEN')
) {
    IMAPHelper::message('Error: The configuration file is not set up correctly', null, true);
}

// Connect to email
IMAPHelper::message('Connecting to imap server...');

$conn = imap_open(IMAP_SERVER, IMAP_USERNAME, IMAP_PASSWORD);

if ($conn === false) {
    IMAPHelper::message('Error: Failed to create connection to mailbox.', null, true);
}

IMAPHelper::message('Successfully connected to imap server.');

// Search unread emails
IMAPHelper::message('Searching emails that need to be handled...');

$searchDate     = (new \DateTime())->sub(new \DateInterval('P1D'))->format('d-M-Y');
$searchCriteria = 'SINCE "' . $searchDate . '"';

$search = imap_search($conn, $searchCriteria);

if ($search === false) {
    IMAPHelper::message('There is no searched email', null, true);
}

IMAPHelper::message('Successfully got emails.');

// Save to db
try {
    $curl = CURL::getCurl();
    
    $url = INFRASTRUCTURE_SERVICE_LOCATION . 'log/email/create';

    foreach ($search as $msgNo) {
        IMAPHelper::message('Processing email numbered {' . $msgNo . '}');

        try {
            $columns = IMAPHelper::retrieveAttachments($conn, $msgNo);

            $columns['token']  = INFRASTRUCTURE_SERVICE_TOKEN;
                                
            $data = http_build_query($columns);
            $opts = [
                CURLOPT_URL             => $url,
                CURLOPT_FOLLOWLOCATION  => true,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_POST            => true,
                CURLOPT_POSTFIELDS      => $data
            ];

            curl_setopt_array($curl, $opts);

            $responseJson = CURL::getResult($curl);

            $response = json_decode($responseJson, true);
            
            if ($response['response']['status'] == 'error') {
                IMAPHelper::message('Warning: ' . $response['response']['data'] . ', ignored.');
                continue;
            }
        } catch (\Exception $e) {
            IMAPHelper::message('Error: ' . $e->getMessage() . ', skipped.');
            continue;
        }

        IMAPHelper::message('Successfully processed email numbered {' . $msgNo . '}');
    }

    CURL::closeCurl($curl);
} catch (\Exception $e) {
    IMAPHelper::message('Error: ' . $e->getMessage() . ', terminated...');
}

imap_expunge($conn);
imap_close($conn);
