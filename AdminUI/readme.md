# Setting up Gulp task runner in Phpstorm

Go to the Node.js website, download and Install the current version of Node.js

In PhpStorm, go to File, Settings

In Settings go to Plugin
- Click on Install JetBrainsPlugin near the bottom of the window
- In the search box, type NodeJS and click in the Install button
- If the Install button is not displayed, you already have the plugin installed

In Settings go to 'Languages & Frameworks', 'Node.js and NPM'

- At the top of the window, click on the '...' button to set the Node interpreter
	- By default it should be installed at "C:\Program Files\nodejs\node.exe"
	- Set the Npm package to "C:\Program Files\nodejs\node_modules\npm" by clicking on the '...' button
- Click on Enable Node.js Code library
- In the packages view port click on the '+' button on the right and Install the packages gulp, gulp-concat, google-closure-compiler-js, vinyl

In Settings go to Tools, Startup Tasks

- Add new Gulp.js configuration
    - Gulp should be the gulpfile.js in the root of the current working project
    - Gulp package should be the Gulp folder under node_modules folder in the current working project

Close and reopen PhpStorm

Gulp should automatically start running when the project is opened

You can manually create the concatenated and minified files by:
- Right clicking on the gulpfile.js
- Select 'Show Gulp Tasks' in the context menu
- In the opened tool window, select 'concat-compile' and press Enter on your keyboard


Gulp is set up so that when any javascript files defined in the files array in gulpfile.js is saved, it will concatinate and minify the javascript files
It may take several seconds for the files to be generated

If any changes are made to gulpfile.js, then Gulp needs to be rerun.
- This can be done through either the Run tool window or the configurations drop down at the top right of PhpStorm.