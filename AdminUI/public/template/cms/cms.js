angular.module('app').controller('CMSCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    var search = '';
    vm.filter = {};
    vm.pageNo = 1;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.style = {
        'width': screen.width + 'px !important'
    };

    vm.list = function () {
        var data = {
            'name': vm.filter.name,
            'liveDomain': vm.filter.liveDomain,
            'liveTunnelIp': vm.filter.liveTunnelIp,
            'liveTunnelEndpoint': vm.filter.liveTunnelEndpoint,
            'limit': 20,
            'offset': (vm.pageNo - 1) * 20
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }

        if (vm.filter.liveDomain) {
            searchParams = searchParams === '' ? 'liveDomain=' + vm.filter.liveDomain : searchParams + '&liveDomain=' + vm.filter.liveDomain;
        }

        if (vm.filter.liveTunnelIp) {
            searchParams = searchParams === '' ? 'liveTunnelIp=' + vm.filter.liveTunnelIp : searchParams + '&liveTunnelIp=' + vm.filter.liveTunnelIp;
        }
        if (vm.filter.liveTunnelEndpoint) {
            searchParams = searchParams === '' ?  'liveTunnelEndpoint=' + vm.filter.liveTunnelEndpoint : searchParams + '&liveTunnelEndpoint=' + vm.filter.liveTunnelEndpoint;
        }

        $location.search(searchParams);

        $http.post('api/cms/customer/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers = response.data.response.data;
                for (var i = 0; i < vm.customers.rows.length; i++) {
                    vm.customers.rows[i].availableOptions = [];

                    if (vm.customers.rows[i]['processing'] == 1) {
                        vm.customers.rows[i].availableOptions.push({key: 'p-p-p', value: 'Under Processing'});
                    } else {
                        switch (vm.customers.rows[i].stage) {
                            case 'DEV':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'p-d-s', value: 'Promote: DEV to STAGING'}
                                );
                                break;
                            case 'STAGING':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'p-s-l', value: 'Promote: STAGING to LIVE'},
                                    {key: 'r-d-c', value: 'Reset: DEV code'}
                                );
                                break;
                            case 'LIVE':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'r-d-c', value: 'Reset: DEV code'},
                                    {key: 'r-d-d', value: 'Reset: DEV content'},
                                    {key: 'r-s-c', value: 'Reset: STAGING code'},
                                    {key: 'r-s-d', value: 'Reset: STAGING content'}
                                );
                                break;
                            default:
                                break;
                        }
                        vm.customers.rows[i].option = vm.customers.rows[i]['availableOptions'][0];
                    }
                    vm.customers.rows[i].enabled = vm.customers.rows[i].enabled == '1' ? true : false;
                }
                vm.customers.rows.sort(function (item1, item2) {
                    var item1Up = item1.name.toUpperCase(), item2Up = item2.name.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
                createPager();
            }
        });
    };

    vm.search = function () {
        vm.pageNo = 1;
        vm.list();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.list();
    };

    vm.change = function(customer) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Do you want to  update status of ' + customer.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function (){
            customer.enabled = !customer.enabled;
            var updateCustomer;
            if (customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    name: customer.name,
                    enabled: 1
                }
            } else if (!customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    name: customer.name,
                    enabled: 0
                }
            }

            $http.post('api/cms/customer/update', updateCustomer).then(function (response) {
                if (hasError(response.data)) {
                    customer.enabled = !customer.enabled;
                }
            });
        })
    };

    vm.addClick = function () {
        vm.modalCMSEditTitle = 'Add';
        vm.modalCMSEditCustomer = {
            'name': '',
            'devBranch': 'master',
            'devAliases': '',
            'stagingBranch': 'master',
            'liveBranch': 'master',
            'liveDomain': '',
            'liveAliases': '',
            'liveTunnelEndpoint': '',
            'liveTunnelIp': '',
            'repository': '',
            'directory': '',
            'backupFrequency': 'Daily'
        };
        vm.showDevPassword = false;
        vm.showStagingPassword = false;
        vm.showLivePassword = false;
        vm.edit = false;
        vm.modalCMSEditShow = true;
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;
    };

    vm.editClick = function (customer) {
        vm.modalCMSEditTitle = 'Edit';
        vm.modalCMSEditCustomer = angular.copy(customer);
        vm.showDevPassword = false;
        vm.showStagingPassword = false;
        vm.showLivePassword = false;
        vm.modalCMSEditShow = true;
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;
        vm.edit = true;
    };

    vm.deleteClick = function (customer) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/cms/customer/delete/' + customer.id).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                }
            });
        });
    };

    vm.saveClick = function (customer) {
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;

        var pattern = new RegExp("^([a-z0-9]+([-a-z0-9:]+)*\.)+[a-z]{2,}(,([a-z0-9]+([-a-z0-9]+)*\.)+[a-z]{2,})*$");

        if (customer.devAliases !== null && customer.devAliases !== '') {
            vm.devValidation = pattern.test(customer.devAliases);
        }

        if (customer.liveAliases !== null && customer.liveAliases !== '') {
            vm.liveValidation = pattern.test(customer.liveAliases);
        }

        if (customer.liveTunnelEndpoint !== null && customer.liveTunnelEndpoint !== '') {
            var liveTunnelAliasPattern = new RegExp("^([a-z0-9]+([-a-z0-9]+)*\.)+[a-z]{2,}$");
            vm.liveTunnelAliasValidation = liveTunnelAliasPattern.test(customer.liveTunnelEndpoint);
        }

        if (vm.devValidation !== false && vm.liveValidation !== false && vm.liveTunnelAliasValidation !== false) {
            var url = 'api/cms/customer/';

            if (vm.edit) {
                url += 'update';
            } else {
                url += 'create';
            }

            $http.post(url, customer).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                    vm.modalCMSEditShow = false;
                    successNotify();
                }
            });
        }
    };

    vm.backClick = function () {
        vm.container = 0;
    };

    vm.processClick = function (customer) {
        vm.modalCMSProcessTitle = 'Process';
        vm.modalCMSProcessCustomer = customer;
        vm.modalCMSProcessShow = true;
    };

    vm.processConfirm = function (customer) {
        var endpoint = '';

        switch (customer.option.key) {
            case 'p-d-s':
                endpoint = 'promote-dev-to-staging';
                break;
            case 'p-s-l':
                endpoint = 'promote-staging-to-live';
                break;
            case 'r-d-c':
                endpoint = 'reset-dev-code';
                break;
            case 'r-d-d':
                endpoint = 'reset-dev-data';
                break;
            case 'r-s-c':
                endpoint = 'reset-staging-code';
                break;
            case 'r-s-d':
                endpoint = 'reset-staging-data';
                break;
            default:
                alert('The selected action is unknown');
                return;
        }

        var data = {
            'id': customer.id,
            'function': endpoint
        };

        $http.get('api/cms/config').then(function (response) {
            data.name = response.data.userName;
            data.email = response.data.userEmail;
            $http.post('api/cms/queue/create', data).then(function (response) {
                if (!hasError(response.data)) {
                    vm.list();
                    vm.modalCMSProcessShow = false;
                    alert('Job queued, a confirmation email will be sent to you when it has finished.');
                }
            });
        });
    };

    vm.sharedDirCheck = function(current, originalOption){
        var customers = this.customers.rows;
        var sharedCustomers = [];
        var modalData = {};

        for(var key in customers){
            switch (current.option.key) {
                case 'p-d-s':
                case 'r-s-c':
                case 'r-s-d':
                    stage = 'STAGING';
                    modalData.branch = current.stagingBranch;
                    break;
                case 'p-s-l':
                    stage = 'LIVE';
                    modalData.branch = current.liveBranch;
                    break;
                case 'r-d-c':
                case 'r-d-d':
                    stage = 'DEV';
                    modalData.branch = current.devBranch;
                    break;
                default:
                    stage = '';
                    return;
            }
            if(customers[key].id != current.id){
                if(customers[key].stage != stage){
                    continue;
                }

                if(current.directory === customers[key].directory){
                    sharedCustomers.push(customers[key]);
                }
            }
        }

        if(sharedCustomers.length > 0){
            vm.modalOriginalOption = originalOption;
            vm.modalSharedDirCustomer = current;
            vm.modalSharedDirShow = true;
            modalData.name = current.name;
            modalData.directory = current.directory;
            modalData.repository = current.directory;
            modalData.environment = stage;
            vm.modalData = modalData;
        }
    };

    function createPager() {
        var pager = [];
        var pageCount = Math.ceil(vm.customers.totalRowCount / 20);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalCmsProcess', {
    bindings: {
        show: '=',
        customer: '<',
        save: '<',
        header: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/cms/process.html'

}).component('modalCmsEdit', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        save: '<',
        showsp: '=',
        showdp: '=',
        showlp: '=',
        devvalidation: '=',
        livevalidation: '=',
        tunnelvalidation: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        };

    },
    templateUrl: 'template/cms/edit.html'

}).component('modalSharedDirectory', {
    bindings: {
        show: '=',
        customer: '<',
        originalOption: '=',
        data: '<'
    },
    controller: function () {
        var vm = this;

        vm.close = function () {
            vm.customer.option = vm.originalOption;
            vm.show = false;
        };
        vm.confirm = function () {
            vm.show = false;
        };
    },
    templateUrl: 'template/cms/shared-dir-modal.html'
});
