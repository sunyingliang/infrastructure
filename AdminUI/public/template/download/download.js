angular.module('app').controller('DownloadCtrl', ['$http', 'errorNotify', '$location', 'notificationService', 'successNotify', function ($http, errorNotify, $location, notificationService, successNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    var search = '';

    vm.addClick = function () {
        vm.modalDownloadEditTitle = 'Add';
        vm.modalDownloadEditMap = {notes:''};
        vm.modalDownloadEditShow = true;
        vm.saveClick = add;
    };

    vm.editClick = function (component) {
        vm.modalDownloadEditTitle = 'Edit';
        vm.modalDownloadEditMap = {
            id: component.id,
            name: component.name,
            release_notes_url: component.release_notes_url,
            notes: component.notes
        };
        vm.modalDownloadLoading = true;
        $http.post('api/download/version/' + component.id).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDownloadEditMap.versions = response.data.response.data;
                vm.modalDownloadLoading = false;
                successNotify();
            }
        });
        vm.modalDownloadEditShow = true;
        vm.saveClick = edit;
    };

    vm.deleteClick = function (component) {
        vm.pnotifyHash = {
            title: 'Delete',
            title_escape: false,
            text: 'Are you sure you want to delete "' + component.name + '"?<br>Warning: This operation is irreversible',
            text_escape: false,
            styling: "bootstrap3",
            width: "450px",
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        };

        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/download/component/delete', component).then(function (response) {
                if (!hasError(response.data)) {
                    vm.refresh();
                }
            });
        });
    };

    function add(component) {
        save('api/download/component/create', component);
    }
    function edit(component) {
        save('api/download/component/update', component);
    }
    function save(url, component) {
        $http.post(url, component).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDownloadEditShow = false;
                successNotify();
                vm.refresh();
            }
        });
    }
    vm.refresh = function () {
        var data = {
            'name': vm.filter.name,
            'release_notes_url': vm.filter.release_notes_url,
            'notes': vm.filter.notes,
            'version': vm.filter.version,
            'release_date': vm.filter.release_date
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.release_notes_url) {
            searchParams = searchParams === '' ? 'release_notes_url=' + vm.filter.release_notes_url : searchParams + '&release_notes_url=' + vm.filter.release_notes_url;
        }
        if (vm.filter.notes) {
            searchParams = searchParams === '' ? 'notes=' + vm.filter.notes : searchParams + '&notes=' + vm.filter.notes;
        }

        if (vm.filter.version) {
            searchParams = searchParams === '' ? 'version=' + vm.filter.version : searchParams + '&version=' + vm.filter.version;
        }

        if (vm.filter.release_date) {
            searchParams = searchParams === '' ? 'release_date=' + vm.filter.release_date : searchParams + '&release_date=' + vm.filter.release_date;
        }

        $location.search(searchParams);

        $http.post('api/download/component/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.components = response.data.response.data;
            }
        });
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.refresh();

}]).component('modalDownloadEdit', {
    bindings: {
        header: '<',
        show: '=',
        map: '<',
        save: '<',
        loading: '<'
    },
    controller: ['$http', function ($http) {
        var vm = this;
        vm.order = {};
        vm.add = function () {
            if (!vm.map.versions) {
                vm.map.versions = [];
            }
            vm.map.versions.push({public:1});
        };
        vm.remove = function(index) {
            var version = vm.map.versions[index];
            if (version.id) {
                version.delete = true;
                vm.form.$setDirty();
            } else {
                vm.map.versions.splice(index, 1);
            }
        };
        vm.close = function () {
            vm.show = false;
            vm.form.$setPristine();
        };
    }],
    templateUrl: 'template/download/edit.html'
});