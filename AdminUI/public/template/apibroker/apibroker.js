angular.module('app').controller('APIBrokerCtrl', ['$http', 'notificationService', 'errorNotify', function ($http, notificationService, errorNotify) {
    var vm = this;

    $http.post('api/apibroker/remap/list').then(function (response) {
      if (!hasError(response.data)){
            vm.server = response.data.response.data;
            vm.broker = {
                'server': vm.server[0]
            }
        }
    });

    $http.post('api/apibroker/remap/host').then(function (response) {
       if (!hasError(response.data)) {
            vm.host = response.data.response.data;
            vm.db = {
                'host': vm.host[0]
            }
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    vm.process = function (database, server, host) {
        var text = [];

        if (!angular.isUndefined(database)) {
            $http.post('api/apibroker/remap/process', {'database': database, 'host': host}).then(function (response) {
                var result = response.data.response;
                var values = [];
                if (!hasError(response.data)) {
                    if (result.data.length < 1 || result.data == undefined) {
                        notificationService.notify({
                            title: 'Notice',
                            styling: "bootstrap3",
                            text: 'No token matches the database.'
                        });
                        return;
                    }
                }

                for (var i = 0; i < result.data.length; i++) {
                    values.push('ID=' + result.data[i]['ID'] + ";" + result.data[i]['Value']);
                }

                for (var j = 0; j < values.length; j++) {
                    var value = values[j].split(";");
                    var databaseName = value[2].replace("Database=", "");
                    var oldServer = value[1].replace("Server=", "");

                    if (oldServer !== server) {
                        text.push('<li><strong>' + databaseName + "</strong> from <strong>" + oldServer + "</strong> to <strong>" + server + "</strong>?</li><br/>");
                    }
                }

                text = text.join('');

                if (text.length < 1) {
                    notificationService.notify({
                        title: 'Notice',
                        styling: "bootstrap3",
                        text: 'No token found that needs updating.'
                    })
                } else {
                    notificationService.notify({
                        title: 'Do you want to update host <strong>' + host + '</strong>:',
                        title_escape: false,
                        text: "<br/>" + text,
                        text_escape: false,
                        styling: "bootstrap3",
                        hide: false,
                        width: "700px",
                        confirm: {
                            confirm: true
                        },
                        buttons: {
                            closer: false,
                            sticker: false
                        },
                        history: {
                            history: false
                        }
                    }).get().on('pnotify.confirm', function () {
                        $http.post('api/apibroker/remap/update', {
                            'newServer': server,
                            'result': values.toString(),
                            'host': host
                        }).then(function (response) {
                            var result = response.data;
                            if (!hasError(response.data)){
                                notificationService.notify({
                                    styling: "bootstrap3",
                                    title: result.response.status
                                });
                            }
                        });
                    });
                }
            });
        }
    }
}]);
