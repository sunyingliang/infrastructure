angular.module('app').controller('RRCServiceCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order  = {};

    vm.search = function () {
        var data = {
            'name': vm.filter.name,
            'url': vm.filter.url,
            'version': vm.filter.version
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.url) {
            searchParams = searchParams === '' ? 'url=' + vm.filter.url : searchParams + '&url=' + vm.filter.url;
        }
        if (vm.filter.version) {
            searchParams = searchParams === '' ? 'version=' + vm.filter.version : searchParams + '&version=' + vm.filter.version;
        }

        $location.search(searchParams);

        $http.post('api/rrcservice/list', data).then(function (response) {
            if (!hasError(response.data)) {
               vm.rrcservice = response.data.response.data;
            }
        });
    };

    vm.addClick = function (){
        vm.modalRRCEdit = {
            name: '',
            url: ''
        };
        vm.modalRRCEditShow = true;
        vm.modalRRCEditTitle = 'Add';
        vm.edit = false;
    };

    vm.editClick = function(rrc) {
        vm.modalRRCEdit = angular.copy(rrc);
        vm.modalRRCEditShow = true;
        vm.modalRRCEditTitle = 'Edit';
        vm.edit = true;
    };

    vm.saveClick = function (rrc) {
        var requestUrl = 'api/rrcservice/';
        if (vm.edit) {
            requestUrl += 'edit';
        } else {
            requestUrl += 'add';
        }

        $http.post(requestUrl, rrc).then(function (response) {
            if(!hasError(response.data)) {
                successNotify();
                vm.search();
            }
            vm.modalRRCEditShow = false;
        });
    };

    vm.checkAll = function () {
        for (var i=0; vm.rrcservice.length < i; i++) {
            vm.check(vm.rrcservice[i]);
        }
    };

    vm.check = function (rrc) {
        var postData = rrc === 'all' ? {'data': 'all'} : {'id': rrc.id, 'url': rrc.url};
        $http.post('api/rrcservice/check', postData).then(function (response) {
            if (!hasError(response.data)) {
                successNotify();
            }
            vm.search();
        })
    };

    vm.deleteClick = function (rrc) {
        vm.pnotifyHash = {
            title: 'Delete',
            title_escape: false,
            text: 'Are you sure you want to delete "' + rrc.name + '"?<br>Warning: This operation is irreversible',
            text_escape: false,
            styling: "bootstrap3",
            width: "450px",
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        };

        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/rrcservice/delete', rrc).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                    vm.search();
                }
            });
        });
    };

    vm.change_enable_status = function (rrc) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to update status of ' + rrc.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            rrc.enabled = !rrc.enabled;
            var updateRRC;
            if (rrc.enabled) {
                updateRRC = {
                    id: rrc.id,
                    enabled: 1
                }
            } else if (!rrc.enabled) {
                updateRRC = {
                    id: rrc.id,
                    enabled: 0
                }
            }

            $http.post('/api/rrcservice/edit', updateRRC).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                } else {
                    rrc.enabled = !rrc.enabled;
                }
            });
        })
    };


    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalRrcEdit', {
    bindings: {
        header: '<',
        show: '=',
        rrc: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/rrcservice/edit.html'
});
