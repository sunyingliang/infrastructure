angular.module('app').controller('GlobalAdminCtrl', ['$http', 'errorNotify', 'successNotify', function ($http, errorNotify, successNotify) {
    var vm = this;

    vm.signUp = function () {
        $http.post('api/sitemaster/globaladmin/create', vm.account).then(function (response) {
            if (!hasError(response.data)) {
                vm.passwordLink = response.data.response.data;
                vm.container = 1;
                successNotify();
            }
        }, function (error) {
            errorNotify(error.data);
        });
    };

    vm.reset = function() {
        vm.container = 0;
        vm.account = {
            'sitename': vm.sites[0],
            'fullname': vm.response['fullname'],
            'email'   : vm.response['email']
        };
    };

    $http.post('api/sitemaster/globaladmin/list').then(function (response) {
        if (!hasError(response.data)) {
            vm.response = response.data;
            vm.sites = vm.response['response']['data'];
            vm.reset();
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);
