angular.module('app').controller('CollectionsCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order = {};

    vm.editClick = function (row) {
        vm.addshow = false;
        vm.modalCollectionsEditTitle = 'Edit';
        vm.modalCollectionsEditSite = angular.copy(row);
        vm.modalCollectionsEditShow = true;
        vm.modalstyle = 'modal-dialog collection-edit-dialog';
        vm.cssstyle = 'modal-body collection-edit-body';
    };

    vm.addClick = function () {
        vm.addshow = true;
        vm.modalCollectionsEditTitle = 'Add';
        vm.modalCollectionsEditShow = true;
        vm.modalstyle = 'modal-dialog collection-dialog';
        vm.cssstyle = 'modal-body collection-body';
    };

    vm.saveClick = function(site) {
        var url = 'api/sitemaster/collections/';
        if (vm.addshow) {
            url += 'create';
            data = { name: site.newdbname };
        } else if(vm.addshow==false) {
            url += 'update';
            var data = {
                siteName: site.name,
                additionalIPs: site.additionalips,
                authSiteHosts: site.authsitehosts,
                anonSiteHosts: site.anonsitehosts
            };
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCollectionsEditShow = false;
                successNotify();
                refresh();
            }
        });
    };

    vm.deleteClick = function(site) {
        notificationService.notify({
            title: 'Delete',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to delete ' + site.name + '?' +
            '<br>Note: This action cannot be undone and will delete the master record, child site databases, and database user.',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            $http.post('api/sitemaster/collections/delete', {siteName: site.name}).then(function (response) {
                if (!hasError(response.data)) {
                    refresh();
                }
            })
        });
    };

    vm.search = function () {
        refresh();
    };

    vm.info = function (name, auth) {
        if (auth) {
            window.open('https://' + name + '-auth.collections.firmsteptest.com/admin.html', '_blank');
        } else {
            window.open('https://' + name + '.collections.firmsteptest.com/admin.html', '_blank');
        }
    };

    function refresh() {
        var data = {
            host: vm.filter.host,
            name: vm.filter.name,
            dbserver: vm.filter.dbserver
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.host) {
            searchParams = searchParams === '' ? 'host=' + vm.filter.host : searchParams + '&host=' + vm.filter.host;
        }
        if (vm.filter.dbserver) {
            searchParams = searchParams === '' ? 'dbserver=' + vm.filter.dbserver : searchParams + '&dbserver=' + vm.filter.dbserver;
        }

        $location.search(searchParams);

        $http.post('api/sitemaster/collections/list', data).then(function (response) {
            if (!hasError(response.data)){
                vm.collections = response.data.response.data;
            }
        });
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    refresh();
}]).component('modalCollectionsEdit', {
    bindings: {
        show: '=',
        site: '<',
        save: '<',
        title: '<',
        edit: '=',
        cssstyle: '<',
        modalstyle: '<'
    },
    controller: function() {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/collections/edit.html'
}).directive('toggle', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            if (attrs.toggle=="popover"){
                $(element).popover();
            }
        }
    };
});
