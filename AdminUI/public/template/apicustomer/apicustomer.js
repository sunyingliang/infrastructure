angular.module('app').controller('APICustomerCtrl', ['$http', 'notificationService', 'errorNotify', '$location', function ($http, notificationService, errorNotify, $location) {
    var vm = this;
    vm.display = 'customer';
    vm.filter = {};
    vm.order = {};

    vm.search = function () {
        data = vm.filter;

        var searchParams = '';

        if (vm.filter.customer_name) {
            searchParams = searchParams === '' ? 'customer_name=' + vm.filter.customer_name : searchParams + '&customer_name=' + vm.filter.customer_name;
        }
        if (vm.filter.db_host) {
            searchParams = searchParams === '' ? 'db_host=' + vm.filter.db_host : searchParams + '&db_host=' + vm.filter.db_host;
        }
        if (vm.filter.environment) {
            searchParams = searchParams === '' ? 'environment=' + vm.filter.environment : searchParams + '&environment=' + vm.filter.environment;
        }
        if (vm.display) {
            searchParams = searchParams === '' ? 'display=' + vm.display : searchParams + '&display=' + vm.display;
        }

        $location.search(searchParams);

        if (vm.display == 'customer') {
            $http.post('api/apibroker/customer/list', data).then(function (response) {
                if (!hasError(response.data)) {
                    vm.customers = response.data.response.data;
                }
            });
        } else if (vm.display == 'host') {
           vm.searchByHost();
        }
    };

    vm.searchByHost = function () {
        $http.post('api/apibroker/customer/listbyhost', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers_host = response.data.response.data;
            }
        });
    };

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug' && keyValue[0] !== 'display') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            } else if (keyValue[0] == 'display') {
                vm.display = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
    vm.searchByHost();

    $http.post('api/apibroker/customer/environment').then(function (response) {
        if (!hasError(response.data)) {
            vm.environments = response.data.response.data;
            vm.filter.environment = vm.environments[0];
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);
