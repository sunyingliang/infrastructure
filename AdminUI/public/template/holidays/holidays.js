angular.module('app').controller('HolidaysCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    var date = new Date();
    var currentYear = date.getFullYear().toString();
    vm.filter = {};
    var current = false;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.getYear = function (calendar, newHoliday) {
        $http.post('api/holiday/list-year', {
            'calendar': calendar
        }).then(function (response) {
            if (!hasError(response.data)) {
                var result = response.data.response.data;

                checkYearInHash();

                for (var i = 0; i < result.length; i++) {
                    if (result[i].year == vm.yearInHash) {
                        var check = true;
                    }
                    if (result[i].year == currentYear) {
                        current = true;
                    }
                }
                if (result.length) {
                    if (newHoliday) {
                        vm.filter.holiday = newHoliday.substring(0, 4);
                    } else if (vm.yearInHash && check) {
                        vm.filter.holiday = vm.yearInHash;
                    } else if (!vm.yearInHashChecker && current) {
                        vm.filter.holiday = currentYear;
                    } else {
                        vm.filter.holiday = result[0].year;
                    }
                }
                vm.year = result;
                vm.search();
            }
        });
    };

    vm.search = function () {
        if (vm.filter.calendar && vm.filter.holiday) {
            $location.search('calendar=' + vm.filter.calendar + '&year=' + vm.filter.holiday);
            $http.post('api/holiday/list', {
                'calendar': vm.filter.calendar,
                'year': vm.filter.holiday
            }).then(function (response) {
                if (!hasError(response.data)) {
                    vm.holiday_list = response.data.response.data;
                }
            });
        } else {
            refresh();
        }
    };

    vm.addClick = function () {
        vm.modalHolidaysEditTitle = 'Add';
        if (vm.calendars == null || vm.calendars == '') {
            vm.modalHolidaysEditMap = {'calendar': ''};
        } else {
            vm.modalHolidaysEditMap = {'calendar': vm.filter.calendar};
        }
        vm.modalHolidaysEditShow = true;
        vm.addshow = true;
    };

    vm.saveClick = function (holidays) {
        var url = 'api/holiday/';
        if (vm.addshow) {
            url += 'create';
            var date = holidays.holiday.toISODateString();
            var data = {
                'calendar': holidays.calendar,
                'holiday': date,
                'notes': holidays.notes ? holidays.notes : ''
            };
        } else {
            url += 'edit';
            data = {
                'holiday': vm.filter.calendar,
                'id': holidays.id,
                'notes': holidays.notes ? holidays.notes : ''
            };
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                refresh(holidays.calendar, date);
                vm.search();
                successNotify();
                vm.modalHolidaysEditShow = false;
            }
        });
    };

    vm.editClick = function (row) {
        vm.modalHolidaysEditTitle = 'Edit';
        vm.modalHolidaysEditMap = {'calendar': vm.filter.calendar, 'holiday': row.holiday, 'notes':row.notes, 'id':row.id};
        vm.modalHolidaysEditShow = true;
        vm.addshow = false;
    };

    vm.deleteClick = function (holiday) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/holiday/delete', {id: holiday.id}).then(function (response) {
                if (!hasError(response.data)) {
                    vm.getYear(vm.filter.calendar);
                    vm.search();
                }
            });
        });
    };

    function refresh(newCalendar, newHoliday) {
        $http.post('api/holiday/list-calendar').then(function (response) {
            if (!hasError(response.data)) {
                vm.calendars = response.data.response.data;
                if (typeof vm.calendars[0] !== 'undefined' && vm.calendars[0] !== null) {
                    if (newCalendar && newHoliday) {
                        vm.filter.calendar = newCalendar;
                        vm.getYear(vm.filter.calendar, newHoliday);
                    } else if (vm.calendarInHash == true) {
                        vm.getYear(vm.filter.calendar);
                    } else {
                        vm.filter.calendar = vm.calendars[0].calendar;
                        vm.getYear(vm.filter.calendar)
                    }
                } else {
                    vm.filter.calendar = '';
                }
            }
        });
    }

    refresh();

    function checkYearInHash() {
        vm.yearInHashChecker = false;
        var calendarInHash = '';
        if (location.search) {
            if (location.search.includes('&')) {
                var parts = location.search.substr(1).split('&');
                for (var i = 0; i < parts.length; i++) {
                    var keyValue = parts[i].split('=');
                    if (keyValue[0] == 'year') {
                        vm.yearInHashChecker = true;
                        vm.yearInHash = keyValue[1];
                    }
                    if (keyValue[0] == 'calendar') {
                        calendarInHash = keyValue[1];
                    }
                }
                if (calendarInHash != vm.filter.calendar) {

                    vm.yearInHashChecker = false;
                }
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        if (location.search.includes('&')) {
            var parts = location.search.substr(1).split('&');
            for (var i = 0; i < parts.length; i++) {
                var keyValue = parts[i].split('=');
                if (keyValue[0] !== 'debug') {
                    vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
                }
                if (keyValue[0] == 'calendar') {
                    vm.calendarInHash = true;
                }
            }

        }
    }
}]).component('modalHolidaysEdit', {
    bindings: {
        title: '<',
        show: '=',
        holidays: '<',
        save: '<',
        add: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/holidays/edit.html'
});