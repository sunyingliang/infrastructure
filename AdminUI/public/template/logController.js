angular.module('app').controller('LogCtrl', ['$http', '$location', 'errorNotify', 'params', function ($http, $location, errorNotify, params) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.pageNo = 1;
    vm.showSummary = false;

    angular.element(document).ready(function () {
        vm.timeToPicker = angular.element('#timeToPicker').datetimepicker({
            defaultDate: null,
            format: 'HH:mm:ss'
        });
        vm.timeFromPicker = angular.element('#timeFromPicker').datetimepicker({
            defaultDate: null,
            format: 'HH:mm:ss'
        });
    });

    vm.toggle = function (row, isError) {
        if (isError) {
            if (row.showException) {
                row.showException = false;
                row.toggleException = 'Show Full';
            } else {
                row.showException = true;
                row.toggleException = 'Show Less';
            }
        } else {
            if (row.showMessage) {
                row.showMessage = false;
                row.toggleMessage = !row.Response ? 'Show Full' : 'Show';
            } else {
                row.showMessage = true;
                row.toggleMessage = !row.Response ? 'Show Less' : 'Hide';
            }
        }
    };

    vm.search = function () {
        vm.pageNo = 1;
        vm.filter.page = '';

        if (params.url == 'api/log/paymentConnectors/list') {
            vm.filter.timeTo = vm.timeToPicker.data().date;
            vm.filter.timeFrom = vm.timeFromPicker.data().date;
        }

        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.filter.page = '';
        filter();
    };

    vm.summary = function () {
        vm.showSummary = !vm.showSummary;
        $http.post('api/requestlog/getsummary').then(function (response) {
            if (!hasError(response.data)) {
                vm.requests = response.data.response.data;
            }
        });
    };

    function filter() {
        var url = searchURL();
        var urlArr = url.split('?'), searchItems = urlArr[1].split('&'), data = {};
        url = urlArr[0];
        searchItems.forEach(function (element, index, array) {
            var item = element.split('=');
            data[item[0]] = item[1];
        });
        if (vm.order.property && vm.order.sortOrder) {
            data['sort'] = vm.order.property;
            data['sortOrder'] = vm.order.sortOrder;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.logs = response.data.response.data;
                truncateMessage(vm.logs.rows);
                if (url.substring(0, url.indexOf('?')) !== 'api/lim/monitoring/list' || url.substring(0, url.indexOf('?')) !== 'api/lim/monitoring/resultcodes') {
                    createPager();
                }
            }
        });
    }

    function searchURL() {
        var search = '';
        var url = '';

        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    var value = vm.filter[key];
                    if (value instanceof Date) {
                        value = value.toISODateString();
                    }
                    if (key == 'page') {
                        vm.pageNo = vm.filter[key];
                    } else {
                        search += '&' + key + '=' + value;
                    }
                }
            }
        }

        var offset = (vm.pageNo - 1) * 20;

        $location.search(vm.pageNo == 1 ? search.substr(1) : 'page=' + vm.pageNo + search);
        if (params.url.indexOf('?') > -1) {
            url = params.url + '&offset=' + offset + '&limit=20' + search;
        } else {
            url = params.url + '?offset=' + offset + '&limit=20' + search;
        }
        return url;
    }

    function truncateMessage(logs) {
        if (logs) {
            for (var i = 0, len = logs.length; i < len; i++) {
                var log = logs[i];

                if (params.truncate) {
                    params.truncate(log);
                } else {
                    /**** KEY OF MESSAGE TYPES ****
                     *
                     log.Data = page
                     log.data = auditlog page
                     log.message = collectionlog page
                     log.Message = filltasklog, limlog, paymentlog, agilisyslog, rrclog page
                     log.response = ftplog page
                     log.Response = lim page
                     */
                    //
                    var message ='';
                    var truncateLength = 0;
                    switch (params.url) {
                        case 'api/log/paymentConnectors/list':
                            message = log.Message;
                            truncateLength =50;
                            break;
                        case 'api/log/lim/list':
                            message = log.Message;
                            truncateLength =140;
                            break;
                        case 'api/log/rrc/list':
                            message = log.Message;
                            truncateLength =90;
                            break;
                        case 'api/log/agilisys/list':
                            message = log.Message;
                            truncateLength =80;
                            break;
                        case 'api/log/ftp/list':
                            message = log.response;
                            truncateLength =80;
                            break;
                        case 'api/log/fillTask/list':
                            truncateLength =140;
                            message = log.Message;
                            break;
                        case 'api/lim/monitoring/list':
                            message =log.Response;
                            truncateLength =100;
                            break;
                        case 'api/adminlog':
                            message = log.data;
                            truncateLength =100;
                            break;
                        case 'api/log/collections/list':
                            message = log.message;
                            truncateLength =80;
                            break;
                        case 'api/log/email/list?status=sent':
                        case 'api/log/email/list?status=failed':
                            message = log.to_address;
                            truncateLength =80;
                            break;
                    }

                    if (message) {
                        if (message.length > truncateLength) {
                            log.truncateMessage = message.substring(0, truncateLength) + '...';
                            log.toggleMessage = !log.Response ? 'Show Full' : 'Show';
                        } else {
                            log.truncateMessage = message;
                        }
                    }
                }
            }
        }
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.logs.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }
    if (params.init) {
        params.init(vm, $http, errorNotify);
    }

    filter();
}]);
