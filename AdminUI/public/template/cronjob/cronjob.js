angular.module('app').controller('CronJobCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify,successNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    function searchURL() {
        var search = '';

        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    var value = vm.filter[key];
                    search += '&' + key + '=' + value;
                }
            }
        }

        $location.search(search);
        return 'api/cron/cronjob/list' + '?' + search;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
        }
    }

    vm.search = function () {
        var url = searchURL();

        var data = {
            name: vm.filter.name,
            frequency: vm.filter.frequency,
            url: vm.filter.url,
            command: vm.filter.command,
            concurrent: vm.filter.concurrent,
            started: vm.filter.started,
            enabled: vm.filter.enabled,
            delay: vm.filter.delay
        };

        if (vm.filter.execution_time !== null) {
            data.execution_time = vm.filter.execution_time;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.cronJobs = response.data.response;
                vm.cronJobs.data.sort(function (item1, item2) {
                    var item1Up = item1.name.toUpperCase(), item2Up = item2.name.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
            }
        });
    };

    vm.addClick = function () {
        vm.modalCronjobEditTitle = 'Add';
        vm.modalCronjobEditSite = {
            concurrent: 0,
            enabled: 0,
            delay: 1,
            frequency: '0 * * * * ',
            execution_time: 60
        };
        vm.modalCronjobEditShow = true;
        vm.edit = false;
    };

    vm.editClick = function (row) {
        vm.modalCronjobEditTitle = 'Edit';
        vm.modalCronjobEditSite = angular.copy(row);
        vm.modalCronjobEditShow = true;
        vm.edit = true;
    };

    vm.deleteClick = function (cronJob) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/cron/cronjob/delete', {id: cronJob.id}).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                }
            });
        });
    };

    vm.saveClick = function (cronJob) {
        var url = 'api/cron/cronjob/';
        if (vm.edit) {
            url += 'update';
        } else {
            url += 'create';
        }

        $http.post(url, cronJob).then(function (response) {
            if (!hasError(response.data)) {
                vm.search();
                vm.modalCronjobEditShow = false;
                successNotify();
            }
        });
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    vm.search();
}]).component('modalCronjobEdit', {
    bindings: {
        header: '<',
        show: '=',
        cronJob: '<',
        save: '<',
        message: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/cronjob/edit.html'
});
