angular.module('app').controller('ReverseProxyCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order  = {};
    vm.pageNo = 1;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.list = function () {
        var data = {
            'path': vm.filter.path,
            'url': vm.filter.url,
            'comment': vm.filter.comment,
            'keep_alive': vm.filter.keep_alive,
            'http_downgrade': vm.filter.chunked,
            'limit': 20,
            'offset': (vm.pageNo - 1) * 20,
            'sort': vm.order.property,
            'sortOrder' : vm.order.sortOrder
        };

        var searchParams = '';

        if (vm.filter.path) {
            searchParams = searchParams === '' ? 'path=' + vm.filter.path : searchParams + '&path=' + vm.filter.path;
        }
        if (vm.filter.url) {
            searchParams = searchParams === '' ? 'url=' + vm.filter.url : searchParams + '&url=' + vm.filter.url;
        }
        if (vm.filter.comment) {
            searchParams = searchParams === '' ? 'comment=' + vm.filter.comment : searchParams + '&comment=' + vm.filter.comment;
        }

        if (vm.filter.chunked) {
            searchParams = searchParams === '' ? 'chunked=' + vm.filter.chunked : searchParams + '&chunked=' + vm.filter.chunked;
        }

        if (vm.filter.keep_alive) {
            searchParams = searchParams === '' ? 'keep_alive=' + vm.filter.keep_alive : searchParams + '&keep_alive=' + vm.filter.keep_alive;
        }

        $location.search(searchParams);

        $http.post('api/proxy/mapping/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.mapping = response.data.response.data;
                vm.mapping.rows.sort(function (item1, item2) {
                    var item1Up = item1.path.toUpperCase(), item2Up = item2.path.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
                createPager();
            }
        });
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.list();
    };

    vm.search = function () {
       vm.pageNo = 1;
       vm.list();
    };

    vm.addClick = function () {
        vm.modalProxyEditTitle = 'Add';
        vm.modalProxyEditMap = {
            test_url: '',
            comment: '',
            http_downgrade: 0,
            keep_alive: 0,
            pooled: 1
        };
        vm.modalProxyEditShow = true;
        vm.edit = false;
    };

    vm.editClick = function (map) {
        vm.modalProxyEditTitle = 'Edit';
        vm.modalProxyEditMap = angular.copy(map);
        vm.modalProxyEditShow = true;
        vm.edit = true;
    };

    vm.saveClick = function (map) {
        var url = 'api/proxy/mapping/';
        if (vm.edit) {
            url += 'update';
            data = {'id': map.id}
        } else {
            url += 'create';
        }
        $http.post(url, map).then(function (response) {
            if (!hasError(response.data)) {
                successNotify();
                vm.search();
            }
            vm.modalProxyEditShow = false;
        });
    };

    vm.deleteClick = function (map) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/proxy/mapping/delete', {
                'path': map.path
            }).then(function (response) {
                if (!hasError(response.data)) {
                    vm.offline = false;
                    vm.search();
                }
            });
        });
    };

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.mapping.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalProxyEdit', {
    bindings: {
        title: '<',
        show: '=',
        map: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/reverseproxy/edit.html'
});
