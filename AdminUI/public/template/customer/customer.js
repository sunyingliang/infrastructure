angular.module('app').controller('CustomerCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.pageNo = 1;
    vm.order = {};
    vm.btnEnabled = true;

    vm.toggle = function (customer) {
        if (customer) {
            var notes = customer.notes;
            if (notes) {
                if (customer.showMessage) {
                    customer.showMessage = false;
                    customer.toggleMessage = 'Show';
                } else {
                    customer.showMessage = true;
                    customer.toggleMessage = 'Hide';
                }
            }
        }
    };

    function filter() {
        var url = searchURL();
        var urlArr = url.split('?'), searchItems = urlArr[1].split('&'), data = {};
        url = urlArr[0];
        searchItems.forEach(function (element, index, array) {
            var item = element.split('=');
            data[item[0]] = item[1];
        });

        if (vm.order.property && vm.order.sortOrder) {
            data['sort'] = vm.order.property;
            data['sortOrder'] = vm.order.sortOrder;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers = response.data.response.data;
                for (var i = 0; i < vm.customers.rows.length; i++) {
                    vm.customers.rows[i].showMessage = false;
                    vm.customers.rows[i].toggleMessage = 'Show';
                }
                createPager();
            }
        });
    }

    vm.search = function () {
        vm.pageNo = 1;
        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        filter();
    };

    vm.editClick = function (row, mode) {
        $http.post('api/solutions/customer/masters', {"id": row.id}).then(function (response) {
            if (!hasError(response.data)) {
                $http.post('api/solutions/customer/list-feature', {"id": row.id}).then(function (response) {
                    if (!hasError(response.data)) {
                        vm.modalCustomersEditNotes.available_features = response.data.response.data.available_features;
                        vm.modalCustomersEditNotes.enabled_features = response.data.response.data.enabled_features;
                        if (vm.modalCustomersEditNotes.available_features.length > 0) {
                            vm.modalCustomersEditNotes.selectedAvailableFeature = vm.modalCustomersEditNotes.available_features[0];
                        }
                        if (vm.modalCustomersEditNotes.enabled_features.length > 0) {
                            vm.modalCustomersEditNotes.selectedEnabledFeature = vm.modalCustomersEditNotes.enabled_features[0];
                        }
                    }
                });

                if (mode === 'edit'){
                    vm.modalCustomersEditTitle = 'Edit';
                } else if (mode === 'view') {
                    vm.modalCustomersEditTitle = 'View';
                }

                vm.modalCustomersEditNotes = angular.copy(row);
                vm.modalCustomersEditNotes.master_pool = [{'id': 0, 'name': ''}].concat(response.data.response.data);
                vm.modalCustomersEditNotes.master = {'id': 0, 'name': ''};
                for (var m in vm.modalCustomersEditNotes.master_pool) {
                    if (vm.modalCustomersEditNotes.master_id == vm.modalCustomersEditNotes.master_pool[m].id) {
                        vm.modalCustomersEditNotes.master = vm.modalCustomersEditNotes.master_pool[m];
                    }
                }
                vm.modalCustomersEditShow = true;
            }
        });
    };

    vm.addNewClick = function () {
        vm.modalCustomersAddNewTitle = 'Add New';
        vm.modalCustomersAddNewShow = true;
    };

    vm.addExistingClick = function () {
        vm.modalCustomersAddTitle = 'Add Existing';
        vm.modalCustomersAdd = {method: 'clone'};
        vm.modalCustomersAddShow = true;
    };

    vm.syncClick = function (row) {
        vm.modalDbSyncTitle = 'Re-sync Database';
        vm.modalCustomer = angular.copy(row);
        vm.modalDbSyncShow = true;
    };

    vm.change_enable_status = function (customer) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Do you want to  update enabled status of ' + customer.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            customer.enabled = !customer.enabled;
            var updateCustomer;
            if (customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    enabled: 1
                }
            } else if (!customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    enabled: 0
                }
            }

            $http.post('api/solutions/customer/update-enabled', updateCustomer).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                } else {
                    customer.enabled = !customer.enabled;
                }
            });
        })
    };

    vm.saveSyncClick = function (customer) {
        vm.btnEnabled = false;
        console.log(customer);
        //return false;
        var syncCustomer = {
            'id': customer.id,
            'name': customer.name,
            'alias': customer.alias,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbPasswordRead': customer.db_password_read,
            'dbUserRead': customer.db_user_read,
            'ftpUser': customer.ftp_user,
            'ftpPassword': customer.ftp_pass_raw,
            'httpUser': customer.http_user,
            'httpPassword': customer.http_pass,
            'homeFolder': customer.home_folder,
            'enabled': customer.enabled,
            'notes': customer.notes,
            'master_id': customer.master_id,
            'syncHost': customer.syncHost,
            'syncDbName': customer.syncDbName,
            'syncUserName': customer.syncUserName,
            'syncPassword': customer.syncPassword
        };

        $http.post('api/solutions/customer/sync', syncCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDbSyncShow = false;
                successNotify();
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.saveEditClick = function (customer) {
        vm.btnEnabled = false;
        var updateCustomer = {
            'id': customer.id,
            'name': customer.name,
            'alias': customer.alias,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbPasswordRead': customer.db_password_read,
            'dbUserRead': customer.db_user_read,
            'ftpUser': customer.ftp_user,
            'ftpPassword': customer.ftp_pass_raw,
            'httpUser': customer.http_user,
            'httpPassword': customer.http_pass,
            'homeFolder': customer.home_folder,
            'enabled': customer.enabled,
            'notes': customer.notes,
            'master_id': customer.master_id,
            'available_features': customer.available_features,
            'enabled_features': customer.enabled_features
        };
        $http.post('api/solutions/customer/update', updateCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCustomersEditShow = false;
                successNotify();
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.saveAddClick = function (customer) {
        var addCustomer;
        vm.btnEnabled = false;

        if (!customer.db_host) {
            addCustomer = customer;
        } else {
            addCustomer = {
                'name': customer.name,
                'alias': customer.alias,
                'dbHost': customer.db_host,
                'dbDatabase': customer.db_database,
                'dbUser': customer.db_user,
                'dbPassword': customer.db_password,
                'dbUserRead': customer.db_user_read,
                'dbPasswordRead': customer.db_password_read,
                'method': customer.method,
                'notes': customer.notes
            };
        }

        $http.post('api/solutions/customer/create', addCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCustomersAddShow = false;
                vm.modalCustomersAddNewShow = false;
                successNotify();
                vm.modalCustomersAdd = {};
                vm.modalCustomersAddNew = {};
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.deleteClick = function (customer) {
        notificationService.notify({
            title: 'Delete',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to delete ' + customer.name + '?<br>Note: This action cannot be undone and will only remove the master record, no customer data or databases will be deleted.',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            $http.post('api/solutions/customer/delete', {id: customer.id}).then(function (response) {
                if (!hasError(response.data)) {
                    filter();
                }
            });
        });
    };

    vm.connection = function (customer) {
        data = {
            'id': customer.id,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbUserRead': customer.db_user_read,
            'dbPasswordRead': customer.db_password_read
        };
        $http.post('api/solutions/customer/test', data).then(function (response) {
            if (!hasError(response.data)) {
                successNotify('Connection test for read and write user successful');
            }

            if (response.data) {
                filter();
            }
        });
    };

    function searchURL() {
        var offset = (vm.pageNo - 1) * 20;
        var search = '';
        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    search += '&' + key + '=' + vm.filter[key];
                }
            }
        }

        $location.search(vm.pageNo == 1 ? search.substr(1) : 'page=' + vm.pageNo + search);

        return 'api/solutions/customer/list' + '?offset=' + offset + '&limit=20' + search;
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.customers.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    filter();
}]).component('modalCustomersEdit', {
    templateUrl: 'template/customer/edit.html',
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        domain: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        };

        vm.addFeatureClick = function () {
            var feature = vm.customer.selectedAvailableFeature;
            if (feature) {
                for (var i = 0; i < vm.customer.available_features.length; i++) {
                    if (vm.customer.available_features[i].id == feature.id) {
                        vm.customer.available_features.splice(i, 1);
                        break;
                    }
                }
                vm.customer.enabled_features.push(feature);
                selectDefaultFeatures();
            }
        };
        vm.removeFeatureClick = function () {
            var feature = vm.customer.selectedEnabledFeature;
            if (feature) {
                for (var i = 0; i < vm.customer.enabled_features.length; i++) {
                    if (vm.customer.enabled_features[i].id == feature.id) {
                        vm.customer.enabled_features.splice(i, 1);
                        break;
                    }
                }
                vm.customer.available_features.push(feature);
                selectDefaultFeatures();
            }
        };
        function selectDefaultFeatures() {
            if (vm.customer.available_features.length > 0) {
                vm.customer.selectedAvailableFeature = vm.customer.available_features[0];
            }
            if (vm.customer.enabled_features.length > 0) {
                vm.customer.selectedEnabledFeature = vm.customer.enabled_features[0];
            }
        }
    }
}).component('modalCustomersAddNew', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/addnew.html'
}).component('modalCustomersAdd', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/add.html'
}).directive('toggle', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (attrs.toggle == "popover") {
                $(element).popover();
            }
        }
    };
}).component('modalDbSync', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/dbSync.html'
});