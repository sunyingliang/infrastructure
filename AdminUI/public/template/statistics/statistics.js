angular.module('app').controller('StatisticsCtrl', ['$http', '$location', 'errorNotify', function ($http, $location, errorNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.date = [];
    vm.year = [];
    vm.month = [];
    vm.day = [];
    vm.display = '';
    var dateExist = false;

    vm.search = function () {
        var search = '';
        var id = 0;

        if (vm.filter) {
            search = 'period=' + vm.filter.period + '&date=' + vm.filter.year + '-' + vm.filter.month + '-' + vm.filter.day;
            id = vm.date[vm.filter.year][vm.filter.month][vm.filter.day];
        }

        $location.search(search);
        $http.post('api/oaf/statistics/report', {id: id}).then(function (response) {
            if (!hasError(response.data)) {
                if (vm.display === '') {
                    vm.display = 'Summary';
                }
                vm.statistics = response.data.response.data;
            }
        });
    };

    vm.refresh = function () {
        $http.post('api/oaf/statistics/list').then(function (response) {
            vm.date = [];
            if (!hasError(response.data)) {
                if (response.data.response.data.json) {
                    var data = response.data.response.data.json;

                    if (!vm.filter.period) {
                        vm.filter.period = data.all ? 'all' : 'year';
                    }

                    if (data[vm.filter.period]) {
                        data[vm.filter.period].forEach(function (item) {
                            var year = item.date.substr(0, 4);
                            var month = item.date.substr(5, 2);
                            var day = item.date.substr(8, 2);
                            var id = item.id;
                            if (vm.filter.year && vm.filter.year == year && vm.filter.month && vm.filter.month == month && vm.filter.day && vm.filter.day == day) {
                                dateExist = true;
                            }
                            if (!vm.date[year]) vm.date[year] = [];
                            if (!vm.date[year][month]) vm.date[year][month] = [];
                            vm.date[year][month][day] = id;
                        });
                        vm.checkShow();
                        vm.getYear();
                    }
                }
            }
        });
    };

    vm.refresh();

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                if (keyValue[0] == 'date') {
                    var value = decodeURIComponent(keyValue[1]);
                    vm.filter.year = value.substr(0,4);
                    vm.filter.month = value.substr(5,2);
                    vm.filter.day = value.substr(8,2);
                } else {
                    vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
                }

            }
        }
    }

    vm.getYear = function () {
        vm.year = [];
        for (var key in vm.date) {
            if (vm.date.hasOwnProperty(key)) {
                vm.year.push(key);
            }
        }
        vm.year.sort();
        vm.year.reverse();

        if (!vm.filter.year || dateExist === false || vm.yearOption == false) vm.filter.year = vm.year[0];
        vm.getMonth();
    };

    vm.getMonth = function () {
        vm.month = [];
        for (var key in vm.date[vm.filter.year]) {
            if (vm.date[vm.filter.year].hasOwnProperty(key)) {
                vm.month.push(key);
            }
        }
        vm.month.sort();
        vm.month.reverse();
        if (!vm.filter.month || dateExist == false || vm.monthOption == false) vm.filter.month = vm.month[0];
        vm.getDay();
    };

    vm.getDay = function () {
        vm.day = [];
        for (var key in vm.date[vm.filter.year][vm.filter.month]) {
            if (vm.date[vm.filter.year][vm.filter.month].hasOwnProperty(key)) {
                vm.day.push(key);
            }
        }
        vm.day.sort();
        vm.day.reverse();
        if (!vm.filter.day || dateExist == false || vm.dayOption == false) vm.filter.day = vm.day[0];
    };

    vm.checkShow = function () {
        vm.yearOption = true;
        vm.monthOption = true;
        vm.dayOption = true;
        if (vm.filter.period == 'all') {
            vm.yearOption = false;
            vm.monthOption = false;
            vm.dayOption = false;
        } else if (vm.filter.period == 'year') {
            vm.monthOption = false;
            vm.dayOption = false;
        } else if (vm.filter.period == 'month') {
            vm.dayOption = false;
        }
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

}]);