angular.module('app').controller('CronLogCtrl', ['$http', '$location', 'notificationService', 'errorNotify', function ($http, $location, notificationService, errorNotify) {
    var vm = this;
    vm.filter = {};
    vm.pageNo = 1;

    vm.toggle = function (row, isError) {
        if (isError) {
            if (row.showException) {
                row.showException = false;
                row.toggleException = 'Show Full';
            } else {
                row.showException = true;
                row.toggleException = 'Show Less';
            }
        } else {
            if (row.showMessage) {
                row.showMessage = false;
                row.toggleMessage = 'Show Full';
            } else {
                row.showMessage = true;
                row.toggleMessage = 'Show Less';
            }
        }
    };

    vm.search = function () {
        vm.pageNo = 1;
        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        filter();
    };

    vm.getPeriods = function () {
        $http.post('api/log/cron/period', {
            job_id: vm.filter.job_id
        }).then(function (response) {
            if (!hasError(response.data)) {
                var periods = response.data.response.data;
                if (periods.length && !vm.filter.date) {
                    vm.filter.date = periods[0].start_time;
                }
                vm.periods = periods;
                vm.search();
            }
        });

    };

    function filter() {
        var filter = {
            offset: (vm.pageNo - 1) * 20,
            limit: 20,
            job_id: vm.filter.job_id,
            date: vm.filter.date
        };

        for (var i = 0; i < vm.cronJobs.length; i++) {
            if (filter.job_id == vm.cronJobs[i].id) {
                var name = vm.cronJobs[i].name;
                break;
            }
        }

        var jobString = typeof name !== 'undefined' ? '&job=' + name : '';
        var dateString = typeof filter.date !== 'undefined' ? '&date=' + filter.date : '';
        $location.search('page=' + vm.pageNo + jobString + dateString);

        if (filter.date && filter.date !== 'undefined') {
            $http.post('api/log/cron/list', filter).then(function (response) {
                if (!hasError(response.data)) {
                    vm.logs = response.data.response.data;
                    truncateMessage(vm.logs.data);
                    createPager();
                }
            });
        }
    }

    function truncateMessage(logs) {
        if (logs) {
            for (var i = 0, len = logs.length; i < len; i++) {
                var log = logs[i];
                var message = log.response;
                if (message) {
                    if (message.length > 170) {
                        log.truncateMessage = message.substring(0, 170) + '...';
                        log.toggleMessage = 'Show Full';
                    } else {
                        log.truncateMessage = message;
                    }
                }
            }
        }
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.ceil(vm.logs.totalRowCount / 20);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    $http.post('api/cron/cronjob/list').then(function (response) {
        if (!hasError(response.data)) {
            vm.cronJobs = response.data.response.data;
            if (location.search && vm.filter.job) {
                for (var i = 0; i < vm.cronJobs.length; i++) {
                    if (vm.filter.job == vm.cronJobs[i].name) {
                        vm.filter.job_id = vm.cronJobs[i].id;
                        break;
                    }
                }
            } else {
                vm.filter.job_id = vm.cronJobs[0].id;
            }
            vm.getPeriods();
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);
