<?php

session_start();

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Authentication/FAM.php';

$fam = new FS\Authentication\FAM(FAM_KEY, FAM_IV, FAM_CIPHER);

if (isset($_POST['AuthToken'])) {
    $user = $fam->decodeResponse($_POST['AuthToken']);

    $_SESSION['user'] = $user;

    header('Location: ' . str_replace('ReturnURL=', '', $_SERVER['QUERY_STRING']));
} else {
    $requestUri = urldecode($_SERVER['REQUEST_URI']);

    if (preg_match('#^http[s]?://#', $requestUri) === 1) {
        $returnURL = $requestUri;
    } else {
        $returnURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $requestUri;
    }

    $authToken = $fam->generateRequest();
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Connecting to authentication server...</title>
    </head>
    <body>
    <form action="<?php echo FAM_URL; ?>" method="post">
        <div>
            Connecting to authentication server...
            <input type="hidden" name="AuthToken" id="AuthToken" value="<?php echo $authToken; ?>"/>
            <input type="hidden" name="DestPage" id="DestPage" value="<?php echo $returnURL; ?>"/ >
            <noscript><input type="submit" value="Press here to continue"/></noscript>
        </div>
    </form>
    <script>document.forms[0].submit();</script>
    </body>
    </html>
    <?php
}

session_write_close();
?>
