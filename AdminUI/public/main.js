(function () {
    Date.prototype.toISODateString = function () {
        return this.getFullYear() + '-' + ('0' + (this.getMonth() + 1)).slice(-2) + '-' + ('0' + this.getDate()).slice(-2);
    };
    var dateTest = document.createElement('input');
    try {
        dateTest.type = 'date';
    } catch (err) {
    }
    if (dateTest.type != 'date') {
        var css = document.createElement('link');
        css.setAttribute('rel', 'stylesheet');
        css.setAttribute('href', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css');
        document.body.appendChild(css);
        var j = document.createElement('script');
        j.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
        document.body.appendChild(j);
        var jui = document.createElement('script');
        jui.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
        document.body.appendChild(jui);
    }
    setInterval(function () {
        $.ajax({
            'url': '/api/session/refresh',
            'type': 'get',
            'complete': function (data, status, xhr) {
                console.log('Session refreshed...');
            }
        });
    }, 300000);
})();
angular.module('app', ['ngRoute', 'jlareau.pnotify', 'ui.toggle']).directive('input', function () {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function (scope, elm, attrs, ngModelCtrl) {
            var type = attrs.type;
            if (type == 'time') {
                ngModelCtrl.$parsers.push(function (value) {
                    if (value && value instanceof Date) {
                        return ('0' + value.getHours()).slice(-2) + ':' + ('0' + value.getMinutes()).slice(-2);
                    }
                    return value;
                });
                ngModelCtrl.$formatters.push(function (value) {
                    if (value && typeof value == 'string') {
                        var time = value.split(':');
                        return new Date(1970, 0, 1, time[0], time[1]);
                    }
                    return value;
                });
            } else if (type == 'date') {
                ngModelCtrl.$formatters.push(function (value) {
                    if (value && typeof value == 'string') {
                        return new Date(value);
                    }
                    return value;
                });
                var dateTest = document.createElement('input');
                try {
                    dateTest.type = 'date';
                } catch (err) {
                }
                if (dateTest.type != 'date') {
                    elm[0].style.width = '90px';
                    $(elm).datepicker({
                        dateFormat: "yy-mm-dd",
                        onSelect: function (date) {
                            ngModelCtrl.$setViewValue(date);
                        }
                    });
                }
            }
        }
    }
}).component('sortButton', {
    bindings: {
        property: '@',
        order: '='
    },
    template: '<input class="sort-btn btn btn-xs btn-default" type="button" ng-value="$ctrl.orderValue()" ng-click="$ctrl.orderBy()" ng-class="$ctrl.orderClass()"/>',
    controller: function () {
        var vm = this;
        vm.orderBy = function () {
            if (vm.order.property == vm.property) {
                if (vm.order.reverse) {
                    vm.order.property = '';
                    vm.order.sortOrder = '';
                }
                else {
                    vm.order.sortOrder = 'ASC';
                }
                vm.order.reverse = !vm.order.reverse;
            } else {
                vm.order.property = vm.property;
                vm.order.sortOrder = 'DESC';
            }
        };
        vm.orderValue = function () {
            if (vm.order.property == vm.property) {
                return vm.order.reverse ? '▲' : '▼';
            }
            return '▬';
        };
        vm.orderClass = function () {
            return vm.order.property == vm.property ? 'black' : 'lite';
        }
    }
}).factory('AuthInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    // redirect to login page when getting unauthorised error
    return {
        request: function (config) {
            var root = $rootScope.$$childHead.root;
            root.loading++;
            return config;
        },
        response: function (response) {
            var root = $rootScope.$$childHead.root;
            if (root.loading > 0) {
                root.loading--;
            }
            return response;
        },
        responseError: function responseError(rejection) {
            var root = $rootScope.$$childHead.root;
            if (root.loading > 0) {
                root.loading--;
            }
            if (rejection.status == 401) {
                location.href = 'famlogin.php?ReturnURL=' + encodeURIComponent(location.href);
            } else if (rejection.status == 403) {
                location.href = 'noaccess';
            } else if (rejection.status == 503) {
                location.href = '/';
            } else {
                return $q.reject(rejection);
            }
        }
    };
}]).factory('errorNotify', ['notificationService', function (notificationService) {
    return function (errorMessage) {
        notificationService.notify({
            styling: "bootstrap3",
            title: "Error",
            type: 'error',
            width: "600px",
            text: errorMessage
        })
    }
}]).factory('successNotify', ['notificationService', function (notificationService) {
    return function (successMessage) {
        if (typeof(successMessage) == 'undefined') {
            successMessage = '';
        }
        notificationService.notify({
            styling: "bootstrap3",
            title: "Success",
            type: 'success',
            text: successMessage
        })
    }
}]).factory('Auth', ['$http', function ($http) {
    var auth;
    return {
        setAuth: function (permission) {
            auth = permission;
        },
        isLogin: function () {
            return auth ? auth : false;
        }
    };
}]).filter('trim', function () {
    return function (value) {
        if (!angular.isString(value)) {
            return value;
        }
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
}).config(['$httpProvider', '$locationProvider', '$routeProvider', '$qProvider', function ($httpProvider, $locationProvider, $routeProvider, $qProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
    $qProvider.errorOnUnhandledRejections(false);
    $routeProvider.when('/', {
        templateUrl: 'template/home.html'
    }).when('/paymentconnector/', {
        templateUrl: 'template/paymentlog/paymentlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Payment Connectors',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/paymentConnectors/list',
                    init: function (vm, $http) {
                        $http.post('api/log/paymentConnectors/source').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.paymentSource = response.data.response.data;
                                if (!vm.filter.log) {
                                    vm.filter.log = vm.paymentSource[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    },
                    truncate: function (log) {
                        if (log.Message) {
                            if (log.Message.length > 40) {
                                log.truncateMessage = log.Message.substring(0, 40) + '...';
                                log.toggleMessage = 'Show Full';
                            } else {
                                log.truncateMessage = log.Message;
                            }
                        }
                        if (log.Exception) {
                            if (log.Exception.length > 40) {
                                log.truncateException = log.Exception.substring(0, 40) + '...';
                                log.toggleException = 'Show Full';
                            } else {
                                log.truncateException = log.Exception;
                            }
                        }
                    }
                }
            }
        }
    }).when('/limlog/', {
        templateUrl: 'template/limlog/limlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/LIM',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/lim/list'
                }
            }
        }
    }).when('/rrclog/', {
        templateUrl: 'template/rrclog/rrclog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/RRC',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/rrc/list',
                    init: function (vm, $http) {
                        $http.post('api/log/rrc/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.sites = response.data.response.data;
                                if (!vm.filter.site) {
                                    vm.filter.site = vm.sites[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/agilisyslog/', {
        templateUrl: 'template/agilisyslog/agilisyslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Agilisys Portal',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/agilisys/list',
                    init: function (vm, $http) {
                        $http.post('api/log/agilisys/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.sites = response.data.response.data;
                                if (!vm.filter.log) {
                                    vm.filter.log = vm.sites[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/log/agilisys/environment').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.environments = response.data.response.data;
                                if (!vm.filter.environment) {
                                    vm.filter.environment = vm.environments[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/ftplog/', {
        templateUrl: 'template/ftplog/ftplog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Solutions/FTP Log',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/ftp/list',
                    init: function (vm, $http) {
                        $http.post('api/ftp/log/customer').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.customer = response.data.response.data.map(function(value){
                                    return value.name;
                                });
                                if (!vm.filter.customer) {
                                    vm.filter.customer = vm.customer[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/filltasklog/', {
        templateUrl: 'template/filltasklog/filltasklog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/FillTask',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/fillTask/list'
                }
            }
        }
    }).when('/integrationlog/', {
        templateUrl: 'template/integrationlog/integrationlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Integration',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/integration/list',
                    init: function (vm, $http) {
                        $http.post('api/log/integration/source').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.loggers = response.data.response.data.logger;
                                vm.customers = response.data.response.data.customer;
                                if (!vm.filter.logger) {
                                    vm.filter.log = vm.loggers[0];
                                }
                                if (!vm.filter.customer) {
                                    vm.filter.customer = vm.customers[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    },
                    truncate: function (log) {
                        if (log.message) {
                            if (log.message.length > 40) {
                                log.truncateMessage = log.message.substring(0, 40) + '...';
                                log.toggleMessage = 'Show Full';
                            } else {
                                log.truncateMessage = log.message;
                            }
                        }
                        if (log.exception) {
                            if (log.exception.length > 35) {
                                log.truncateException = log.exception.substring(0, 35) + '...';
                                log.toggleException = 'Show Full';
                            } else {
                                log.truncateException = log.exception;
                            }
                        }
                    }
                }
            }
        }
    }).when('/componentslog/', {
        templateUrl: 'template/componentslog/componentslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Components',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/components/list',
                    init: function (vm, $http) {
                        $http.post('api/log/components/status').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.status = response.data.response.data;
                                if (!vm.filter.status) {
                                    vm.filter.status = vm.status[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/collectionslog/', {
        templateUrl: 'template/collectionslog/collectionslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Collections',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/collections/list'
                }
            }
        }
    }).when('/downloadlog/', {
        templateUrl: 'template/downloadlog/downloadlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Download',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/download/list'
                }
            }
        }
    }).when('/backgroundlog/', {
        templateUrl: 'template/backgroundlog/backgroundlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Background Log',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/background/list',
                    init: function (vm, $http) {
                        $http.post('api/oaf/background/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.site = response.data.response.data;
                                if (!vm.filter.site) {
                                    vm.filter.site = vm.site[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/oaf/background/server').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.server = response.data.response.data;
                                if (!vm.filter.server) {
                                    vm.filter.server = vm.server[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/oaf/background/operation').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.operation = response.data.response.data;
                                if (!vm.filter.operation) {
                                    vm.filter.operation = vm.operation[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/limmonitor/', {
        templateUrl: 'template/limmonitor/limmonitor.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/LIM',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/lim/monitoring/list',
                    init: function (vm, $http, errorNotify) {
                        $http.post('api/lim/monitoring/resultcodes').then(function (response) {
                            if (response.data.status !== 'error') {
                                vm.resultCodes = response.data.response.data;
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                    }
                }
            }
        }
    }).when('/platformerrors/', {
        templateUrl: 'template/platformerrors/platformerrors.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Errors',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/error/log'
                }
            }
        }
    }).when('/featureusage/', {
        templateUrl: 'template/featureusage/featureusage.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Feature Usage',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/feature/log'
                }
            }
        }
    }).when('/customer/', {
        templateUrl: 'template/customer/customer.html',
        controller: 'CustomerCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Customer',
        reloadOnSearch: false
    }).when('/globaladmin/', {
        templateUrl: 'template/globaladmin/globaladmin.html',
        controller: 'GlobalAdminCtrl',
        controllerAs: 'vm',
        title: 'OAF/Global Admin'
    }).when('/reverseproxy/', {
        templateUrl: 'template/reverseproxy/reverseproxy.html',
        controller: 'ReverseProxyCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Proxy',
        reloadOnSearch: false
    }).when('/cms/', {
        templateUrl: 'template/cms/cms.html',
        controller: 'CMSCtrl',
        controllerAs: 'vm',
        title: 'CMS/Customer',
        reloadOnSearch: false
    }).when('/apibroker/', {
        templateUrl: 'template/apibroker/apibroker.html',
        controller: 'APIBrokerCtrl',
        controllerAs: 'vm',
        title: 'API Broker/Remap',
        reloadOnSearch: false
    }).when('/apicustomer/', {
        templateUrl: 'template/apicustomer/apicustomer.html',
        controller: 'APICustomerCtrl',
        controllerAs: 'vm',
        title: 'API Broker/Customer',
        reloadOnSearch: false
    }).when('/holidays/', {
        templateUrl: 'template/holidays/holidays.html',
        controller: 'HolidaysCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Holidays',
        reloadOnSearch: false
    }).when('/collections/', {
        templateUrl: 'template/collections/collections.html',
        controller: 'CollectionsCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Collections',
        reloadOnSearch: false
    }).when('/statistics/', {
        templateUrl: 'template/statistics/statistics.html',
        controller: 'StatisticsCtrl',
        controllerAs: 'vm',
        title: 'OAF/Statistics',
        reloadOnSearch: false
    }).when('/cronjob/', {
        templateUrl: 'template/cronjob/cronjob.html',
        controller: 'CronJobCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Crontab',
        reloadOnSearch: false
    }).when('/cronlog/', {
        templateUrl: 'template/cronlog/cronlog.html',
        controller: 'CronLogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Crontab',
        reloadOnSearch: false
    }).when('/download/', {
        templateUrl: 'template/download/download.html',
        controller: 'DownloadCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Download',
        reloadOnSearch: false
    }).when('/auditlog/', {
        templateUrl: 'template/auditlog/auditlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Audit',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/adminlog'
                }
            }
        }
    }).when('/requestlog/', {
        templateUrl: 'template/requestlog/requestlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Request',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/requestlog/list',
                    init: function (vm, $http, errorNotify) {
                        $http.post('api/requestlog/endpoint').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.endpoints = response.data.response.data;
                                if (!vm.filter.endpoint) {
                                    vm.filter.endpoint = vm.endpoints[0];
                                }
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                        $http.post('api/requestlog/tabpage').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.tab_pages = response.data.response.data;
                                if (!vm.filter.tab_page) {
                                    vm.filter.tab_page = vm.tab_pages[0];
                                }
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                    }
                }
            }
        }
    }).when('/emailsentlog/', {
        templateUrl: 'template/email/emailsent.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Email Sent',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/email/list?status=sent'
                }
            }
        }
    }).when('/emailfailedlog/', {
        templateUrl: 'template/email/emailfailed.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Email Failed',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/email/list?status=failed'
                }
            }
        }
    }).when('/rrcservice/', {
        templateUrl: 'template/rrcservice/rrcservice.html',
        controller: 'RRCServiceCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/RRC Service',
        reloadOnSearch: false
    }).when('/rrcstatistics/', {
        templateUrl: 'template/rrcstatistics/rrcstatistics.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/RRC Statistics',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/rrcstatistics/list'
                }
            }
        }
    }).when('/noaccess/', {
        templateUrl: 'noaccess.html'
    }).when('/monitoring/', {
        redirectTo: '/api/config'
    }).otherwise({
        redirectTo: '/'
    });
}]).controller('AppCtrl', ['$http', '$scope', '$window', 'errorNotify', '$location', 'Auth', function ($http, $scope, $window, errorNotify, $location, auth) {
    var vm = this, path = $window.location.pathname.substr(-1) == '/' ? $window.location.pathname.slice(0, -1) : $window.location.pathname;
    if (path == '/noaccess') {
        return;
    }
    vm.loading = 0;

    vm.tabActive = function (tab) {
        for (var i=0; i < vm.auth.permissions[tab].length; i++) {
            if (vm.auth.permissions[tab][i]['path'] + '/' === location.pathname) {
                return true;
            }
        }
    };

    vm.hasWritePermission = function (tab, page) {
        if (vm.auth) {
            for (var key in vm.auth.permissions[tab]) {
                if (vm.auth.permissions[tab][key]['page'] == page && vm.auth.permissions[tab][key]['permission'] == 'write') {
                    return true;
                }
            }
        }
        return false;
    };

    vm.isActive = function (viewLocation) {
        return viewLocation === location.pathname;
    };

    $http.get('api/login').then(function (response) {
        if (response.data.message) {
            errorNotify(response.data.message);
        } else {
            auth.setAuth(response.data);
            vm.auth = response.data;
        }
    }, function (data) {
        console.log(data)
    });

    $scope.$on('$routeChangeSuccess', function (event, data) {
        //Change page title, based on Route information
        document.title = 'Admin' + (data.title ? ' - ' + data.title : '');
    });
}]).run(['$rootScope', '$location', '$window', '$http', 'Auth', function ($rootScope, $location, $window, $http, auth) {
    $rootScope.$on('$routeChangeStart', function (event) {
        if (!auth.isLogin()) {
            var st = Math.floor(Date.now() / 1000);
            while (Math.floor(Date.now() / 1000) - st < 2) {;}
        }
    });
}]);

angular.module('app').controller('LogCtrl', ['$http', '$location', 'errorNotify', 'params', function ($http, $location, errorNotify, params) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.pageNo = 1;
    vm.showSummary = false;

    angular.element(document).ready(function () {
        vm.timeToPicker = angular.element('#timeToPicker').datetimepicker({
            defaultDate: null,
            format: 'HH:mm:ss'
        });
        vm.timeFromPicker = angular.element('#timeFromPicker').datetimepicker({
            defaultDate: null,
            format: 'HH:mm:ss'
        });
    });

    vm.toggle = function (row, isError) {
        if (isError) {
            if (row.showException) {
                row.showException = false;
                row.toggleException = 'Show Full';
            } else {
                row.showException = true;
                row.toggleException = 'Show Less';
            }
        } else {
            if (row.showMessage) {
                row.showMessage = false;
                row.toggleMessage = !row.Response ? 'Show Full' : 'Show';
            } else {
                row.showMessage = true;
                row.toggleMessage = !row.Response ? 'Show Less' : 'Hide';
            }
        }
    };

    vm.search = function () {
        vm.pageNo = 1;
        vm.filter.page = '';

        if (params.url == 'api/log/paymentConnectors/list') {
            vm.filter.timeTo = vm.timeToPicker.data().date;
            vm.filter.timeFrom = vm.timeFromPicker.data().date;
        }

        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.filter.page = '';
        filter();
    };

    vm.summary = function () {
        vm.showSummary = !vm.showSummary;
        $http.post('api/requestlog/getsummary').then(function (response) {
            if (!hasError(response.data)) {
                vm.requests = response.data.response.data;
            }
        });
    };

    function filter() {
        var url = searchURL();
        var urlArr = url.split('?'), searchItems = urlArr[1].split('&'), data = {};
        url = urlArr[0];
        searchItems.forEach(function (element, index, array) {
            var item = element.split('=');
            data[item[0]] = item[1];
        });
        if (vm.order.property && vm.order.sortOrder) {
            data['sort'] = vm.order.property;
            data['sortOrder'] = vm.order.sortOrder;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.logs = response.data.response.data;
                truncateMessage(vm.logs.rows);
                if (url.substring(0, url.indexOf('?')) !== 'api/lim/monitoring/list' || url.substring(0, url.indexOf('?')) !== 'api/lim/monitoring/resultcodes') {
                    createPager();
                }
            }
        });
    }

    function searchURL() {
        var search = '';
        var url = '';

        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    var value = vm.filter[key];
                    if (value instanceof Date) {
                        value = value.toISODateString();
                    }
                    if (key == 'page') {
                        vm.pageNo = vm.filter[key];
                    } else {
                        search += '&' + key + '=' + value;
                    }
                }
            }
        }

        var offset = (vm.pageNo - 1) * 20;

        $location.search(vm.pageNo == 1 ? search.substr(1) : 'page=' + vm.pageNo + search);
        if (params.url.indexOf('?') > -1) {
            url = params.url + '&offset=' + offset + '&limit=20' + search;
        } else {
            url = params.url + '?offset=' + offset + '&limit=20' + search;
        }
        return url;
    }

    function truncateMessage(logs) {
        if (logs) {
            for (var i = 0, len = logs.length; i < len; i++) {
                var log = logs[i];

                if (params.truncate) {
                    params.truncate(log);
                } else {
                    /**** KEY OF MESSAGE TYPES ****
                     *
                     log.Data = page
                     log.data = auditlog page
                     log.message = collectionlog page
                     log.Message = filltasklog, limlog, paymentlog, agilisyslog, rrclog page
                     log.response = ftplog page
                     log.Response = lim page
                     */
                    //
                    var message ='';
                    var truncateLength = 0;
                    switch (params.url) {
                        case 'api/log/paymentConnectors/list':
                            message = log.Message;
                            truncateLength =50;
                            break;
                        case 'api/log/lim/list':
                            message = log.Message;
                            truncateLength =140;
                            break;
                        case 'api/log/rrc/list':
                            message = log.Message;
                            truncateLength =90;
                            break;
                        case 'api/log/agilisys/list':
                            message = log.Message;
                            truncateLength =80;
                            break;
                        case 'api/log/ftp/list':
                            message = log.response;
                            truncateLength =80;
                            break;
                        case 'api/log/fillTask/list':
                            truncateLength =140;
                            message = log.Message;
                            break;
                        case 'api/lim/monitoring/list':
                            message =log.Response;
                            truncateLength =100;
                            break;
                        case 'api/adminlog':
                            message = log.data;
                            truncateLength =100;
                            break;
                        case 'api/log/collections/list':
                            message = log.message;
                            truncateLength =80;
                            break;
                        case 'api/log/email/list?status=sent':
                        case 'api/log/email/list?status=failed':
                            message = log.to_address;
                            truncateLength =80;
                            break;
                    }

                    if (message) {
                        if (message.length > truncateLength) {
                            log.truncateMessage = message.substring(0, truncateLength) + '...';
                            log.toggleMessage = !log.Response ? 'Show Full' : 'Show';
                        } else {
                            log.truncateMessage = message;
                        }
                    }
                }
            }
        }
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.logs.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }
    if (params.init) {
        params.init(vm, $http, errorNotify);
    }

    filter();
}]);

angular.module('app').controller('CollectionsCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order = {};

    vm.editClick = function (row) {
        vm.addshow = false;
        vm.modalCollectionsEditTitle = 'Edit';
        vm.modalCollectionsEditSite = angular.copy(row);
        vm.modalCollectionsEditShow = true;
        vm.modalstyle = 'modal-dialog collection-edit-dialog';
        vm.cssstyle = 'modal-body collection-edit-body';
    };

    vm.addClick = function () {
        vm.addshow = true;
        vm.modalCollectionsEditTitle = 'Add';
        vm.modalCollectionsEditShow = true;
        vm.modalstyle = 'modal-dialog collection-dialog';
        vm.cssstyle = 'modal-body collection-body';
    };

    vm.saveClick = function(site) {
        var url = 'api/sitemaster/collections/';
        if (vm.addshow) {
            url += 'create';
            data = { name: site.newdbname };
        } else if(vm.addshow==false) {
            url += 'update';
            var data = {
                siteName: site.name,
                additionalIPs: site.additionalips,
                authSiteHosts: site.authsitehosts,
                anonSiteHosts: site.anonsitehosts
            };
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCollectionsEditShow = false;
                successNotify();
                refresh();
            }
        });
    };

    vm.deleteClick = function(site) {
        notificationService.notify({
            title: 'Delete',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to delete ' + site.name + '?' +
            '<br>Note: This action cannot be undone and will delete the master record, child site databases, and database user.',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            $http.post('api/sitemaster/collections/delete', {siteName: site.name}).then(function (response) {
                if (!hasError(response.data)) {
                    refresh();
                }
            })
        });
    };

    vm.search = function () {
        refresh();
    };

    vm.info = function (name, auth) {
        if (auth) {
            window.open('https://' + name + '-auth.collections.firmsteptest.com/admin.html', '_blank');
        } else {
            window.open('https://' + name + '.collections.firmsteptest.com/admin.html', '_blank');
        }
    };

    function refresh() {
        var data = {
            host: vm.filter.host,
            name: vm.filter.name,
            dbserver: vm.filter.dbserver
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.host) {
            searchParams = searchParams === '' ? 'host=' + vm.filter.host : searchParams + '&host=' + vm.filter.host;
        }
        if (vm.filter.dbserver) {
            searchParams = searchParams === '' ? 'dbserver=' + vm.filter.dbserver : searchParams + '&dbserver=' + vm.filter.dbserver;
        }

        $location.search(searchParams);

        $http.post('api/sitemaster/collections/list', data).then(function (response) {
            if (!hasError(response.data)){
                vm.collections = response.data.response.data;
            }
        });
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    refresh();
}]).component('modalCollectionsEdit', {
    bindings: {
        show: '=',
        site: '<',
        save: '<',
        title: '<',
        edit: '=',
        cssstyle: '<',
        modalstyle: '<'
    },
    controller: function() {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/collections/edit.html'
}).directive('toggle', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            if (attrs.toggle=="popover"){
                $(element).popover();
            }
        }
    };
});

angular.module('app').controller('CronLogCtrl', ['$http', '$location', 'notificationService', 'errorNotify', function ($http, $location, notificationService, errorNotify) {
    var vm = this;
    vm.filter = {};
    vm.pageNo = 1;

    vm.toggle = function (row, isError) {
        if (isError) {
            if (row.showException) {
                row.showException = false;
                row.toggleException = 'Show Full';
            } else {
                row.showException = true;
                row.toggleException = 'Show Less';
            }
        } else {
            if (row.showMessage) {
                row.showMessage = false;
                row.toggleMessage = 'Show Full';
            } else {
                row.showMessage = true;
                row.toggleMessage = 'Show Less';
            }
        }
    };

    vm.search = function () {
        vm.pageNo = 1;
        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        filter();
    };

    vm.getPeriods = function () {
        $http.post('api/log/cron/period', {
            job_id: vm.filter.job_id
        }).then(function (response) {
            if (!hasError(response.data)) {
                var periods = response.data.response.data;
                if (periods.length && !vm.filter.date) {
                    vm.filter.date = periods[0].start_time;
                }
                vm.periods = periods;
                vm.search();
            }
        });

    };

    function filter() {
        var filter = {
            offset: (vm.pageNo - 1) * 20,
            limit: 20,
            job_id: vm.filter.job_id,
            date: vm.filter.date
        };

        for (var i = 0; i < vm.cronJobs.length; i++) {
            if (filter.job_id == vm.cronJobs[i].id) {
                var name = vm.cronJobs[i].name;
                break;
            }
        }

        var jobString = typeof name !== 'undefined' ? '&job=' + name : '';
        var dateString = typeof filter.date !== 'undefined' ? '&date=' + filter.date : '';
        $location.search('page=' + vm.pageNo + jobString + dateString);

        if (filter.date && filter.date !== 'undefined') {
            $http.post('api/log/cron/list', filter).then(function (response) {
                if (!hasError(response.data)) {
                    vm.logs = response.data.response.data;
                    truncateMessage(vm.logs.data);
                    createPager();
                }
            });
        }
    }

    function truncateMessage(logs) {
        if (logs) {
            for (var i = 0, len = logs.length; i < len; i++) {
                var log = logs[i];
                var message = log.response;
                if (message) {
                    if (message.length > 170) {
                        log.truncateMessage = message.substring(0, 170) + '...';
                        log.toggleMessage = 'Show Full';
                    } else {
                        log.truncateMessage = message;
                    }
                }
            }
        }
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.ceil(vm.logs.totalRowCount / 20);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    $http.post('api/cron/cronjob/list').then(function (response) {
        if (!hasError(response.data)) {
            vm.cronJobs = response.data.response.data;
            if (location.search && vm.filter.job) {
                for (var i = 0; i < vm.cronJobs.length; i++) {
                    if (vm.filter.job == vm.cronJobs[i].name) {
                        vm.filter.job_id = vm.cronJobs[i].id;
                        break;
                    }
                }
            } else {
                vm.filter.job_id = vm.cronJobs[0].id;
            }
            vm.getPeriods();
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);

angular.module('app').controller('CronJobCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify,successNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    function searchURL() {
        var search = '';

        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    var value = vm.filter[key];
                    search += '&' + key + '=' + value;
                }
            }
        }

        $location.search(search);
        return 'api/cron/cronjob/list' + '?' + search;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
        }
    }

    vm.search = function () {
        var url = searchURL();

        var data = {
            name: vm.filter.name,
            frequency: vm.filter.frequency,
            url: vm.filter.url,
            command: vm.filter.command,
            concurrent: vm.filter.concurrent,
            started: vm.filter.started,
            enabled: vm.filter.enabled,
            delay: vm.filter.delay
        };

        if (vm.filter.execution_time !== null) {
            data.execution_time = vm.filter.execution_time;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.cronJobs = response.data.response;
                vm.cronJobs.data.sort(function (item1, item2) {
                    var item1Up = item1.name.toUpperCase(), item2Up = item2.name.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
            }
        });
    };

    vm.addClick = function () {
        vm.modalCronjobEditTitle = 'Add';
        vm.modalCronjobEditSite = {
            concurrent: 0,
            enabled: 0,
            delay: 1,
            frequency: '0 * * * * ',
            execution_time: 60
        };
        vm.modalCronjobEditShow = true;
        vm.edit = false;
    };

    vm.editClick = function (row) {
        vm.modalCronjobEditTitle = 'Edit';
        vm.modalCronjobEditSite = angular.copy(row);
        vm.modalCronjobEditShow = true;
        vm.edit = true;
    };

    vm.deleteClick = function (cronJob) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/cron/cronjob/delete', {id: cronJob.id}).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                }
            });
        });
    };

    vm.saveClick = function (cronJob) {
        var url = 'api/cron/cronjob/';
        if (vm.edit) {
            url += 'update';
        } else {
            url += 'create';
        }

        $http.post(url, cronJob).then(function (response) {
            if (!hasError(response.data)) {
                vm.search();
                vm.modalCronjobEditShow = false;
                successNotify();
            }
        });
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    vm.search();
}]).component('modalCronjobEdit', {
    bindings: {
        header: '<',
        show: '=',
        cronJob: '<',
        save: '<',
        message: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/cronjob/edit.html'
});

angular.module('app').controller('GlobalAdminCtrl', ['$http', 'errorNotify', 'successNotify', function ($http, errorNotify, successNotify) {
    var vm = this;

    vm.signUp = function () {
        $http.post('api/sitemaster/globaladmin/create', vm.account).then(function (response) {
            if (!hasError(response.data)) {
                vm.passwordLink = response.data.response.data;
                vm.container = 1;
                successNotify();
            }
        }, function (error) {
            errorNotify(error.data);
        });
    };

    vm.reset = function() {
        vm.container = 0;
        vm.account = {
            'sitename': vm.sites[0],
            'fullname': vm.response['fullname'],
            'email'   : vm.response['email']
        };
    };

    $http.post('api/sitemaster/globaladmin/list').then(function (response) {
        if (!hasError(response.data)) {
            vm.response = response.data;
            vm.sites = vm.response['response']['data'];
            vm.reset();
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);

angular.module('app').controller('ReverseProxyCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order  = {};
    vm.pageNo = 1;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.list = function () {
        var data = {
            'path': vm.filter.path,
            'url': vm.filter.url,
            'comment': vm.filter.comment,
            'keep_alive': vm.filter.keep_alive,
            'http_downgrade': vm.filter.chunked,
            'limit': 20,
            'offset': (vm.pageNo - 1) * 20,
            'sort': vm.order.property,
            'sortOrder' : vm.order.sortOrder
        };

        var searchParams = '';

        if (vm.filter.path) {
            searchParams = searchParams === '' ? 'path=' + vm.filter.path : searchParams + '&path=' + vm.filter.path;
        }
        if (vm.filter.url) {
            searchParams = searchParams === '' ? 'url=' + vm.filter.url : searchParams + '&url=' + vm.filter.url;
        }
        if (vm.filter.comment) {
            searchParams = searchParams === '' ? 'comment=' + vm.filter.comment : searchParams + '&comment=' + vm.filter.comment;
        }

        if (vm.filter.chunked) {
            searchParams = searchParams === '' ? 'chunked=' + vm.filter.chunked : searchParams + '&chunked=' + vm.filter.chunked;
        }

        if (vm.filter.keep_alive) {
            searchParams = searchParams === '' ? 'keep_alive=' + vm.filter.keep_alive : searchParams + '&keep_alive=' + vm.filter.keep_alive;
        }

        $location.search(searchParams);

        $http.post('api/proxy/mapping/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.mapping = response.data.response.data;
                vm.mapping.rows.sort(function (item1, item2) {
                    var item1Up = item1.path.toUpperCase(), item2Up = item2.path.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
                createPager();
            }
        });
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.list();
    };

    vm.search = function () {
       vm.pageNo = 1;
       vm.list();
    };

    vm.addClick = function () {
        vm.modalProxyEditTitle = 'Add';
        vm.modalProxyEditMap = {
            test_url: '',
            comment: '',
            http_downgrade: 0,
            keep_alive: 0,
            pooled: 1
        };
        vm.modalProxyEditShow = true;
        vm.edit = false;
    };

    vm.editClick = function (map) {
        vm.modalProxyEditTitle = 'Edit';
        vm.modalProxyEditMap = angular.copy(map);
        vm.modalProxyEditShow = true;
        vm.edit = true;
    };

    vm.saveClick = function (map) {
        var url = 'api/proxy/mapping/';
        if (vm.edit) {
            url += 'update';
            data = {'id': map.id}
        } else {
            url += 'create';
        }
        $http.post(url, map).then(function (response) {
            if (!hasError(response.data)) {
                successNotify();
                vm.search();
            }
            vm.modalProxyEditShow = false;
        });
    };

    vm.deleteClick = function (map) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/proxy/mapping/delete', {
                'path': map.path
            }).then(function (response) {
                if (!hasError(response.data)) {
                    vm.offline = false;
                    vm.search();
                }
            });
        });
    };

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.mapping.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalProxyEdit', {
    bindings: {
        title: '<',
        show: '=',
        map: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/reverseproxy/edit.html'
});

angular.module('app').controller('StatisticsCtrl', ['$http', '$location', 'errorNotify', function ($http, $location, errorNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    vm.date = [];
    vm.year = [];
    vm.month = [];
    vm.day = [];
    vm.display = '';
    var dateExist = false;

    vm.search = function () {
        var search = '';
        var id = 0;

        if (vm.filter) {
            search = 'period=' + vm.filter.period + '&date=' + vm.filter.year + '-' + vm.filter.month + '-' + vm.filter.day;
            id = vm.date[vm.filter.year][vm.filter.month][vm.filter.day];
        }

        $location.search(search);
        $http.post('api/oaf/statistics/report', {id: id}).then(function (response) {
            if (!hasError(response.data)) {
                if (vm.display === '') {
                    vm.display = 'Summary';
                }
                vm.statistics = response.data.response.data;
            }
        });
    };

    vm.refresh = function () {
        $http.post('api/oaf/statistics/list').then(function (response) {
            vm.date = [];
            if (!hasError(response.data)) {
                if (response.data.response.data.json) {
                    var data = response.data.response.data.json;

                    if (!vm.filter.period) {
                        vm.filter.period = data.all ? 'all' : 'year';
                    }

                    if (data[vm.filter.period]) {
                        data[vm.filter.period].forEach(function (item) {
                            var year = item.date.substr(0, 4);
                            var month = item.date.substr(5, 2);
                            var day = item.date.substr(8, 2);
                            var id = item.id;
                            if (vm.filter.year && vm.filter.year == year && vm.filter.month && vm.filter.month == month && vm.filter.day && vm.filter.day == day) {
                                dateExist = true;
                            }
                            if (!vm.date[year]) vm.date[year] = [];
                            if (!vm.date[year][month]) vm.date[year][month] = [];
                            vm.date[year][month][day] = id;
                        });
                        vm.checkShow();
                        vm.getYear();
                    }
                }
            }
        });
    };

    vm.refresh();

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                if (keyValue[0] == 'date') {
                    var value = decodeURIComponent(keyValue[1]);
                    vm.filter.year = value.substr(0,4);
                    vm.filter.month = value.substr(5,2);
                    vm.filter.day = value.substr(8,2);
                } else {
                    vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
                }

            }
        }
    }

    vm.getYear = function () {
        vm.year = [];
        for (var key in vm.date) {
            if (vm.date.hasOwnProperty(key)) {
                vm.year.push(key);
            }
        }
        vm.year.sort();
        vm.year.reverse();

        if (!vm.filter.year || dateExist === false || vm.yearOption == false) vm.filter.year = vm.year[0];
        vm.getMonth();
    };

    vm.getMonth = function () {
        vm.month = [];
        for (var key in vm.date[vm.filter.year]) {
            if (vm.date[vm.filter.year].hasOwnProperty(key)) {
                vm.month.push(key);
            }
        }
        vm.month.sort();
        vm.month.reverse();
        if (!vm.filter.month || dateExist == false || vm.monthOption == false) vm.filter.month = vm.month[0];
        vm.getDay();
    };

    vm.getDay = function () {
        vm.day = [];
        for (var key in vm.date[vm.filter.year][vm.filter.month]) {
            if (vm.date[vm.filter.year][vm.filter.month].hasOwnProperty(key)) {
                vm.day.push(key);
            }
        }
        vm.day.sort();
        vm.day.reverse();
        if (!vm.filter.day || dateExist == false || vm.dayOption == false) vm.filter.day = vm.day[0];
    };

    vm.checkShow = function () {
        vm.yearOption = true;
        vm.monthOption = true;
        vm.dayOption = true;
        if (vm.filter.period == 'all') {
            vm.yearOption = false;
            vm.monthOption = false;
            vm.dayOption = false;
        } else if (vm.filter.period == 'year') {
            vm.monthOption = false;
            vm.dayOption = false;
        } else if (vm.filter.period == 'month') {
            vm.dayOption = false;
        }
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

}]);
angular.module('app').controller('CMSCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    var search = '';
    vm.filter = {};
    vm.pageNo = 1;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.style = {
        'width': screen.width + 'px !important'
    };

    vm.list = function () {
        var data = {
            'name': vm.filter.name,
            'liveDomain': vm.filter.liveDomain,
            'liveTunnelIp': vm.filter.liveTunnelIp,
            'liveTunnelEndpoint': vm.filter.liveTunnelEndpoint,
            'limit': 20,
            'offset': (vm.pageNo - 1) * 20
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }

        if (vm.filter.liveDomain) {
            searchParams = searchParams === '' ? 'liveDomain=' + vm.filter.liveDomain : searchParams + '&liveDomain=' + vm.filter.liveDomain;
        }

        if (vm.filter.liveTunnelIp) {
            searchParams = searchParams === '' ? 'liveTunnelIp=' + vm.filter.liveTunnelIp : searchParams + '&liveTunnelIp=' + vm.filter.liveTunnelIp;
        }
        if (vm.filter.liveTunnelEndpoint) {
            searchParams = searchParams === '' ?  'liveTunnelEndpoint=' + vm.filter.liveTunnelEndpoint : searchParams + '&liveTunnelEndpoint=' + vm.filter.liveTunnelEndpoint;
        }

        $location.search(searchParams);

        $http.post('api/cms/customer/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers = response.data.response.data;
                for (var i = 0; i < vm.customers.rows.length; i++) {
                    vm.customers.rows[i].availableOptions = [];

                    if (vm.customers.rows[i]['processing'] == 1) {
                        vm.customers.rows[i].availableOptions.push({key: 'p-p-p', value: 'Under Processing'});
                    } else {
                        switch (vm.customers.rows[i].stage) {
                            case 'DEV':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'p-d-s', value: 'Promote: DEV to STAGING'}
                                );
                                break;
                            case 'STAGING':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'p-s-l', value: 'Promote: STAGING to LIVE'},
                                    {key: 'r-d-c', value: 'Reset: DEV code'}
                                );
                                break;
                            case 'LIVE':
                                vm.customers.rows[i].availableOptions.push(
                                    {key: 'r-d-c', value: 'Reset: DEV code'},
                                    {key: 'r-d-d', value: 'Reset: DEV content'},
                                    {key: 'r-s-c', value: 'Reset: STAGING code'},
                                    {key: 'r-s-d', value: 'Reset: STAGING content'}
                                );
                                break;
                            default:
                                break;
                        }
                        vm.customers.rows[i].option = vm.customers.rows[i]['availableOptions'][0];
                    }
                    vm.customers.rows[i].enabled = vm.customers.rows[i].enabled == '1' ? true : false;
                }
                vm.customers.rows.sort(function (item1, item2) {
                    var item1Up = item1.name.toUpperCase(), item2Up = item2.name.toUpperCase();
                    return item1Up < item2Up ? -1 : (item1Up > item2Up ? 1 : 0);
                });
                createPager();
            }
        });
    };

    vm.search = function () {
        vm.pageNo = 1;
        vm.list();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        vm.list();
    };

    vm.change = function(customer) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Do you want to  update status of ' + customer.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function (){
            customer.enabled = !customer.enabled;
            var updateCustomer;
            if (customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    name: customer.name,
                    enabled: 1
                }
            } else if (!customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    name: customer.name,
                    enabled: 0
                }
            }

            $http.post('api/cms/customer/update', updateCustomer).then(function (response) {
                if (hasError(response.data)) {
                    customer.enabled = !customer.enabled;
                }
            });
        })
    };

    vm.addClick = function () {
        vm.modalCMSEditTitle = 'Add';
        vm.modalCMSEditCustomer = {
            'name': '',
            'devBranch': 'master',
            'devAliases': '',
            'stagingBranch': 'master',
            'liveBranch': 'master',
            'liveDomain': '',
            'liveAliases': '',
            'liveTunnelEndpoint': '',
            'liveTunnelIp': '',
            'repository': '',
            'directory': '',
            'backupFrequency': 'Daily'
        };
        vm.showDevPassword = false;
        vm.showStagingPassword = false;
        vm.showLivePassword = false;
        vm.edit = false;
        vm.modalCMSEditShow = true;
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;
    };

    vm.editClick = function (customer) {
        vm.modalCMSEditTitle = 'Edit';
        vm.modalCMSEditCustomer = angular.copy(customer);
        vm.showDevPassword = false;
        vm.showStagingPassword = false;
        vm.showLivePassword = false;
        vm.modalCMSEditShow = true;
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;
        vm.edit = true;
    };

    vm.deleteClick = function (customer) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/cms/customer/delete/' + customer.id).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                }
            });
        });
    };

    vm.saveClick = function (customer) {
        vm.devValidation = true;
        vm.liveValidation = true;
        vm.liveTunnelAliasValidation = true;

        var pattern = new RegExp("^([a-z0-9]+([-a-z0-9:]+)*\.)+[a-z]{2,}(,([a-z0-9]+([-a-z0-9]+)*\.)+[a-z]{2,})*$");

        if (customer.devAliases !== null && customer.devAliases !== '') {
            vm.devValidation = pattern.test(customer.devAliases);
        }

        if (customer.liveAliases !== null && customer.liveAliases !== '') {
            vm.liveValidation = pattern.test(customer.liveAliases);
        }

        if (customer.liveTunnelEndpoint !== null && customer.liveTunnelEndpoint !== '') {
            var liveTunnelAliasPattern = new RegExp("^([a-z0-9]+([-a-z0-9]+)*\.)+[a-z]{2,}$");
            vm.liveTunnelAliasValidation = liveTunnelAliasPattern.test(customer.liveTunnelEndpoint);
        }

        if (vm.devValidation !== false && vm.liveValidation !== false && vm.liveTunnelAliasValidation !== false) {
            var url = 'api/cms/customer/';

            if (vm.edit) {
                url += 'update';
            } else {
                url += 'create';
            }

            $http.post(url, customer).then(function (response) {
                if (!hasError(response.data)) {
                    vm.search();
                    vm.modalCMSEditShow = false;
                    successNotify();
                }
            });
        }
    };

    vm.backClick = function () {
        vm.container = 0;
    };

    vm.processClick = function (customer) {
        vm.modalCMSProcessTitle = 'Process';
        vm.modalCMSProcessCustomer = customer;
        vm.modalCMSProcessShow = true;
    };

    vm.processConfirm = function (customer) {
        var endpoint = '';

        switch (customer.option.key) {
            case 'p-d-s':
                endpoint = 'promote-dev-to-staging';
                break;
            case 'p-s-l':
                endpoint = 'promote-staging-to-live';
                break;
            case 'r-d-c':
                endpoint = 'reset-dev-code';
                break;
            case 'r-d-d':
                endpoint = 'reset-dev-data';
                break;
            case 'r-s-c':
                endpoint = 'reset-staging-code';
                break;
            case 'r-s-d':
                endpoint = 'reset-staging-data';
                break;
            default:
                alert('The selected action is unknown');
                return;
        }

        var data = {
            'id': customer.id,
            'function': endpoint
        };

        $http.get('api/cms/config').then(function (response) {
            data.name = response.data.userName;
            data.email = response.data.userEmail;
            $http.post('api/cms/queue/create', data).then(function (response) {
                if (!hasError(response.data)) {
                    vm.list();
                    vm.modalCMSProcessShow = false;
                    alert('Job queued, a confirmation email will be sent to you when it has finished.');
                }
            });
        });
    };

    vm.sharedDirCheck = function(current, originalOption){
        var customers = this.customers.rows;
        var sharedCustomers = [];
        var modalData = {};

        for(var key in customers){
            switch (current.option.key) {
                case 'p-d-s':
                case 'r-s-c':
                case 'r-s-d':
                    stage = 'STAGING';
                    modalData.branch = current.stagingBranch;
                    break;
                case 'p-s-l':
                    stage = 'LIVE';
                    modalData.branch = current.liveBranch;
                    break;
                case 'r-d-c':
                case 'r-d-d':
                    stage = 'DEV';
                    modalData.branch = current.devBranch;
                    break;
                default:
                    stage = '';
                    return;
            }
            if(customers[key].id != current.id){
                if(customers[key].stage != stage){
                    continue;
                }

                if(current.directory === customers[key].directory){
                    sharedCustomers.push(customers[key]);
                }
            }
        }

        if(sharedCustomers.length > 0){
            vm.modalOriginalOption = originalOption;
            vm.modalSharedDirCustomer = current;
            vm.modalSharedDirShow = true;
            modalData.name = current.name;
            modalData.directory = current.directory;
            modalData.repository = current.directory;
            modalData.environment = stage;
            vm.modalData = modalData;
        }
    };

    function createPager() {
        var pager = [];
        var pageCount = Math.ceil(vm.customers.totalRowCount / 20);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalCmsProcess', {
    bindings: {
        show: '=',
        customer: '<',
        save: '<',
        header: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/cms/process.html'

}).component('modalCmsEdit', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        save: '<',
        showsp: '=',
        showdp: '=',
        showlp: '=',
        devvalidation: '=',
        livevalidation: '=',
        tunnelvalidation: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        };

    },
    templateUrl: 'template/cms/edit.html'

}).component('modalSharedDirectory', {
    bindings: {
        show: '=',
        customer: '<',
        originalOption: '=',
        data: '<'
    },
    controller: function () {
        var vm = this;

        vm.close = function () {
            vm.customer.option = vm.originalOption;
            vm.show = false;
        };
        vm.confirm = function () {
            vm.show = false;
        };
    },
    templateUrl: 'template/cms/shared-dir-modal.html'
});

angular.module('app').controller('APIBrokerCtrl', ['$http', 'notificationService', 'errorNotify', function ($http, notificationService, errorNotify) {
    var vm = this;

    $http.post('api/apibroker/remap/list').then(function (response) {
      if (!hasError(response.data)){
            vm.server = response.data.response.data;
            vm.broker = {
                'server': vm.server[0]
            }
        }
    });

    $http.post('api/apibroker/remap/host').then(function (response) {
       if (!hasError(response.data)) {
            vm.host = response.data.response.data;
            vm.db = {
                'host': vm.host[0]
            }
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    vm.process = function (database, server, host) {
        var text = [];

        if (!angular.isUndefined(database)) {
            $http.post('api/apibroker/remap/process', {'database': database, 'host': host}).then(function (response) {
                var result = response.data.response;
                var values = [];
                if (!hasError(response.data)) {
                    if (result.data.length < 1 || result.data == undefined) {
                        notificationService.notify({
                            title: 'Notice',
                            styling: "bootstrap3",
                            text: 'No token matches the database.'
                        });
                        return;
                    }
                }

                for (var i = 0; i < result.data.length; i++) {
                    values.push('ID=' + result.data[i]['ID'] + ";" + result.data[i]['Value']);
                }

                for (var j = 0; j < values.length; j++) {
                    var value = values[j].split(";");
                    var databaseName = value[2].replace("Database=", "");
                    var oldServer = value[1].replace("Server=", "");

                    if (oldServer !== server) {
                        text.push('<li><strong>' + databaseName + "</strong> from <strong>" + oldServer + "</strong> to <strong>" + server + "</strong>?</li><br/>");
                    }
                }

                text = text.join('');

                if (text.length < 1) {
                    notificationService.notify({
                        title: 'Notice',
                        styling: "bootstrap3",
                        text: 'No token found that needs updating.'
                    })
                } else {
                    notificationService.notify({
                        title: 'Do you want to update host <strong>' + host + '</strong>:',
                        title_escape: false,
                        text: "<br/>" + text,
                        text_escape: false,
                        styling: "bootstrap3",
                        hide: false,
                        width: "700px",
                        confirm: {
                            confirm: true
                        },
                        buttons: {
                            closer: false,
                            sticker: false
                        },
                        history: {
                            history: false
                        }
                    }).get().on('pnotify.confirm', function () {
                        $http.post('api/apibroker/remap/update', {
                            'newServer': server,
                            'result': values.toString(),
                            'host': host
                        }).then(function (response) {
                            var result = response.data;
                            if (!hasError(response.data)){
                                notificationService.notify({
                                    styling: "bootstrap3",
                                    title: result.response.status
                                });
                            }
                        });
                    });
                }
            });
        }
    }
}]);

angular.module('app').controller('APICustomerCtrl', ['$http', 'notificationService', 'errorNotify', '$location', function ($http, notificationService, errorNotify, $location) {
    var vm = this;
    vm.display = 'customer';
    vm.filter = {};
    vm.order = {};

    vm.search = function () {
        data = vm.filter;

        var searchParams = '';

        if (vm.filter.customer_name) {
            searchParams = searchParams === '' ? 'customer_name=' + vm.filter.customer_name : searchParams + '&customer_name=' + vm.filter.customer_name;
        }
        if (vm.filter.db_host) {
            searchParams = searchParams === '' ? 'db_host=' + vm.filter.db_host : searchParams + '&db_host=' + vm.filter.db_host;
        }
        if (vm.filter.environment) {
            searchParams = searchParams === '' ? 'environment=' + vm.filter.environment : searchParams + '&environment=' + vm.filter.environment;
        }
        if (vm.display) {
            searchParams = searchParams === '' ? 'display=' + vm.display : searchParams + '&display=' + vm.display;
        }

        $location.search(searchParams);

        if (vm.display == 'customer') {
            $http.post('api/apibroker/customer/list', data).then(function (response) {
                if (!hasError(response.data)) {
                    vm.customers = response.data.response.data;
                }
            });
        } else if (vm.display == 'host') {
           vm.searchByHost();
        }
    };

    vm.searchByHost = function () {
        $http.post('api/apibroker/customer/listbyhost', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers_host = response.data.response.data;
            }
        });
    };

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug' && keyValue[0] !== 'display') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            } else if (keyValue[0] == 'display') {
                vm.display = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
    vm.searchByHost();

    $http.post('api/apibroker/customer/environment').then(function (response) {
        if (!hasError(response.data)) {
            vm.environments = response.data.response.data;
            vm.filter.environment = vm.environments[0];
        }
    });

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }
}]);

angular.module('app').controller('HolidaysCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    var date = new Date();
    var currentYear = date.getFullYear().toString();
    vm.filter = {};
    var current = false;

    vm.pnotifyHash = {
        title: 'Delete',
        title_escape: false,
        text: 'Are you sure you want to delete this?<br>Warning: This operation is irreversible',
        text_escape: false,
        styling: "bootstrap3",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    };

    vm.getYear = function (calendar, newHoliday) {
        $http.post('api/holiday/list-year', {
            'calendar': calendar
        }).then(function (response) {
            if (!hasError(response.data)) {
                var result = response.data.response.data;

                checkYearInHash();

                for (var i = 0; i < result.length; i++) {
                    if (result[i].year == vm.yearInHash) {
                        var check = true;
                    }
                    if (result[i].year == currentYear) {
                        current = true;
                    }
                }
                if (result.length) {
                    if (newHoliday) {
                        vm.filter.holiday = newHoliday.substring(0, 4);
                    } else if (vm.yearInHash && check) {
                        vm.filter.holiday = vm.yearInHash;
                    } else if (!vm.yearInHashChecker && current) {
                        vm.filter.holiday = currentYear;
                    } else {
                        vm.filter.holiday = result[0].year;
                    }
                }
                vm.year = result;
                vm.search();
            }
        });
    };

    vm.search = function () {
        if (vm.filter.calendar && vm.filter.holiday) {
            $location.search('calendar=' + vm.filter.calendar + '&year=' + vm.filter.holiday);
            $http.post('api/holiday/list', {
                'calendar': vm.filter.calendar,
                'year': vm.filter.holiday
            }).then(function (response) {
                if (!hasError(response.data)) {
                    vm.holiday_list = response.data.response.data;
                }
            });
        } else {
            refresh();
        }
    };

    vm.addClick = function () {
        vm.modalHolidaysEditTitle = 'Add';
        if (vm.calendars == null || vm.calendars == '') {
            vm.modalHolidaysEditMap = {'calendar': ''};
        } else {
            vm.modalHolidaysEditMap = {'calendar': vm.filter.calendar};
        }
        vm.modalHolidaysEditShow = true;
        vm.addshow = true;
    };

    vm.saveClick = function (holidays) {
        var url = 'api/holiday/';
        if (vm.addshow) {
            url += 'create';
            var date = holidays.holiday.toISODateString();
            var data = {
                'calendar': holidays.calendar,
                'holiday': date,
                'notes': holidays.notes ? holidays.notes : ''
            };
        } else {
            url += 'edit';
            data = {
                'holiday': vm.filter.calendar,
                'id': holidays.id,
                'notes': holidays.notes ? holidays.notes : ''
            };
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                refresh(holidays.calendar, date);
                vm.search();
                successNotify();
                vm.modalHolidaysEditShow = false;
            }
        });
    };

    vm.editClick = function (row) {
        vm.modalHolidaysEditTitle = 'Edit';
        vm.modalHolidaysEditMap = {'calendar': vm.filter.calendar, 'holiday': row.holiday, 'notes':row.notes, 'id':row.id};
        vm.modalHolidaysEditShow = true;
        vm.addshow = false;
    };

    vm.deleteClick = function (holiday) {
        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/holiday/delete', {id: holiday.id}).then(function (response) {
                if (!hasError(response.data)) {
                    vm.getYear(vm.filter.calendar);
                    vm.search();
                }
            });
        });
    };

    function refresh(newCalendar, newHoliday) {
        $http.post('api/holiday/list-calendar').then(function (response) {
            if (!hasError(response.data)) {
                vm.calendars = response.data.response.data;
                if (typeof vm.calendars[0] !== 'undefined' && vm.calendars[0] !== null) {
                    if (newCalendar && newHoliday) {
                        vm.filter.calendar = newCalendar;
                        vm.getYear(vm.filter.calendar, newHoliday);
                    } else if (vm.calendarInHash == true) {
                        vm.getYear(vm.filter.calendar);
                    } else {
                        vm.filter.calendar = vm.calendars[0].calendar;
                        vm.getYear(vm.filter.calendar)
                    }
                } else {
                    vm.filter.calendar = '';
                }
            }
        });
    }

    refresh();

    function checkYearInHash() {
        vm.yearInHashChecker = false;
        var calendarInHash = '';
        if (location.search) {
            if (location.search.includes('&')) {
                var parts = location.search.substr(1).split('&');
                for (var i = 0; i < parts.length; i++) {
                    var keyValue = parts[i].split('=');
                    if (keyValue[0] == 'year') {
                        vm.yearInHashChecker = true;
                        vm.yearInHash = keyValue[1];
                    }
                    if (keyValue[0] == 'calendar') {
                        calendarInHash = keyValue[1];
                    }
                }
                if (calendarInHash != vm.filter.calendar) {

                    vm.yearInHashChecker = false;
                }
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        if (location.search.includes('&')) {
            var parts = location.search.substr(1).split('&');
            for (var i = 0; i < parts.length; i++) {
                var keyValue = parts[i].split('=');
                if (keyValue[0] !== 'debug') {
                    vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
                }
                if (keyValue[0] == 'calendar') {
                    vm.calendarInHash = true;
                }
            }

        }
    }
}]).component('modalHolidaysEdit', {
    bindings: {
        title: '<',
        show: '=',
        holidays: '<',
        save: '<',
        add: '='
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/holidays/edit.html'
});
angular.module('app').controller('CustomerCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.pageNo = 1;
    vm.order = {};
    vm.btnEnabled = true;

    vm.toggle = function (customer) {
        if (customer) {
            var notes = customer.notes;
            if (notes) {
                if (customer.showMessage) {
                    customer.showMessage = false;
                    customer.toggleMessage = 'Show';
                } else {
                    customer.showMessage = true;
                    customer.toggleMessage = 'Hide';
                }
            }
        }
    };

    function filter() {
        var url = searchURL();
        var urlArr = url.split('?'), searchItems = urlArr[1].split('&'), data = {};
        url = urlArr[0];
        searchItems.forEach(function (element, index, array) {
            var item = element.split('=');
            data[item[0]] = item[1];
        });

        if (vm.order.property && vm.order.sortOrder) {
            data['sort'] = vm.order.property;
            data['sortOrder'] = vm.order.sortOrder;
        }

        $http.post(url, data).then(function (response) {
            if (!hasError(response.data)) {
                vm.customers = response.data.response.data;
                for (var i = 0; i < vm.customers.rows.length; i++) {
                    vm.customers.rows[i].showMessage = false;
                    vm.customers.rows[i].toggleMessage = 'Show';
                }
                createPager();
            }
        });
    }

    vm.search = function () {
        vm.pageNo = 1;
        filter();
    };

    vm.page = function (page) {
        vm.pageNo = page;
        filter();
    };

    vm.editClick = function (row, mode) {
        $http.post('api/solutions/customer/masters', {"id": row.id}).then(function (response) {
            if (!hasError(response.data)) {
                $http.post('api/solutions/customer/list-feature', {"id": row.id}).then(function (response) {
                    if (!hasError(response.data)) {
                        vm.modalCustomersEditNotes.available_features = response.data.response.data.available_features;
                        vm.modalCustomersEditNotes.enabled_features = response.data.response.data.enabled_features;
                        if (vm.modalCustomersEditNotes.available_features.length > 0) {
                            vm.modalCustomersEditNotes.selectedAvailableFeature = vm.modalCustomersEditNotes.available_features[0];
                        }
                        if (vm.modalCustomersEditNotes.enabled_features.length > 0) {
                            vm.modalCustomersEditNotes.selectedEnabledFeature = vm.modalCustomersEditNotes.enabled_features[0];
                        }
                    }
                });

                if (mode === 'edit'){
                    vm.modalCustomersEditTitle = 'Edit';
                } else if (mode === 'view') {
                    vm.modalCustomersEditTitle = 'View';
                }

                vm.modalCustomersEditNotes = angular.copy(row);
                vm.modalCustomersEditNotes.master_pool = [{'id': 0, 'name': ''}].concat(response.data.response.data);
                vm.modalCustomersEditNotes.master = {'id': 0, 'name': ''};
                for (var m in vm.modalCustomersEditNotes.master_pool) {
                    if (vm.modalCustomersEditNotes.master_id == vm.modalCustomersEditNotes.master_pool[m].id) {
                        vm.modalCustomersEditNotes.master = vm.modalCustomersEditNotes.master_pool[m];
                    }
                }
                vm.modalCustomersEditShow = true;
            }
        });
    };

    vm.addNewClick = function () {
        vm.modalCustomersAddNewTitle = 'Add New';
        vm.modalCustomersAddNewShow = true;
    };

    vm.addExistingClick = function () {
        vm.modalCustomersAddTitle = 'Add Existing';
        vm.modalCustomersAdd = {method: 'clone'};
        vm.modalCustomersAddShow = true;
    };

    vm.syncClick = function (row) {
        vm.modalDbSyncTitle = 'Re-sync Database';
        vm.modalCustomer = angular.copy(row);
        vm.modalDbSyncShow = true;
    };

    vm.change_enable_status = function (customer) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Do you want to  update enabled status of ' + customer.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            customer.enabled = !customer.enabled;
            var updateCustomer;
            if (customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    enabled: 1
                }
            } else if (!customer.enabled) {
                updateCustomer = {
                    id: customer.id,
                    enabled: 0
                }
            }

            $http.post('api/solutions/customer/update-enabled', updateCustomer).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                } else {
                    customer.enabled = !customer.enabled;
                }
            });
        })
    };

    vm.saveSyncClick = function (customer) {
        vm.btnEnabled = false;
        console.log(customer);
        //return false;
        var syncCustomer = {
            'id': customer.id,
            'name': customer.name,
            'alias': customer.alias,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbPasswordRead': customer.db_password_read,
            'dbUserRead': customer.db_user_read,
            'ftpUser': customer.ftp_user,
            'ftpPassword': customer.ftp_pass_raw,
            'httpUser': customer.http_user,
            'httpPassword': customer.http_pass,
            'homeFolder': customer.home_folder,
            'enabled': customer.enabled,
            'notes': customer.notes,
            'master_id': customer.master_id,
            'syncHost': customer.syncHost,
            'syncDbName': customer.syncDbName,
            'syncUserName': customer.syncUserName,
            'syncPassword': customer.syncPassword
        };

        $http.post('api/solutions/customer/sync', syncCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDbSyncShow = false;
                successNotify();
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.saveEditClick = function (customer) {
        vm.btnEnabled = false;
        var updateCustomer = {
            'id': customer.id,
            'name': customer.name,
            'alias': customer.alias,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbPasswordRead': customer.db_password_read,
            'dbUserRead': customer.db_user_read,
            'ftpUser': customer.ftp_user,
            'ftpPassword': customer.ftp_pass_raw,
            'httpUser': customer.http_user,
            'httpPassword': customer.http_pass,
            'homeFolder': customer.home_folder,
            'enabled': customer.enabled,
            'notes': customer.notes,
            'master_id': customer.master_id,
            'available_features': customer.available_features,
            'enabled_features': customer.enabled_features
        };
        $http.post('api/solutions/customer/update', updateCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCustomersEditShow = false;
                successNotify();
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.saveAddClick = function (customer) {
        var addCustomer;
        vm.btnEnabled = false;

        if (!customer.db_host) {
            addCustomer = customer;
        } else {
            addCustomer = {
                'name': customer.name,
                'alias': customer.alias,
                'dbHost': customer.db_host,
                'dbDatabase': customer.db_database,
                'dbUser': customer.db_user,
                'dbPassword': customer.db_password,
                'dbUserRead': customer.db_user_read,
                'dbPasswordRead': customer.db_password_read,
                'method': customer.method,
                'notes': customer.notes
            };
        }

        $http.post('api/solutions/customer/create', addCustomer).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalCustomersAddShow = false;
                vm.modalCustomersAddNewShow = false;
                successNotify();
                vm.modalCustomersAdd = {};
                vm.modalCustomersAddNew = {};
            }
            filter();
            vm.btnEnabled = true;
        })
    };

    vm.deleteClick = function (customer) {
        notificationService.notify({
            title: 'Delete',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to delete ' + customer.name + '?<br>Note: This action cannot be undone and will only remove the master record, no customer data or databases will be deleted.',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            $http.post('api/solutions/customer/delete', {id: customer.id}).then(function (response) {
                if (!hasError(response.data)) {
                    filter();
                }
            });
        });
    };

    vm.connection = function (customer) {
        data = {
            'id': customer.id,
            'dbHost': customer.db_host,
            'dbDatabase': customer.db_database,
            'dbUser': customer.db_user,
            'dbPassword': customer.db_password,
            'dbUserRead': customer.db_user_read,
            'dbPasswordRead': customer.db_password_read
        };
        $http.post('api/solutions/customer/test', data).then(function (response) {
            if (!hasError(response.data)) {
                successNotify('Connection test for read and write user successful');
            }

            if (response.data) {
                filter();
            }
        });
    };

    function searchURL() {
        var offset = (vm.pageNo - 1) * 20;
        var search = '';
        if (vm.filter) {
            for (var key in vm.filter) {
                if (vm.filter.hasOwnProperty(key) && vm.filter[key]) {
                    search += '&' + key + '=' + vm.filter[key];
                }
            }
        }

        $location.search(vm.pageNo == 1 ? search.substr(1) : 'page=' + vm.pageNo + search);

        return 'api/solutions/customer/list' + '?offset=' + offset + '&limit=20' + search;
    }

    function createPager() {
        var pager = [];
        var pageCount = Math.min(Math.ceil(vm.customers.totalRowCount / 20), 30);
        if (pageCount > 1) {
            var pageIndex = vm.pageNo - 1;
            for (var i = 1; i <= pageCount; i++) {
                if ((i == 1 || (i <= 5) || ((i == pageIndex + 1) || (i < (pageIndex + 6) && (i > pageIndex))) || ((i == pageCount) || (i > (pageCount - 5) && (i < pageCount)))))
                    pager.push({'text': i, 'value': i});
                if (((i == (pageIndex - 5) && (pageIndex - 5) > 5)) || ((i == (pageIndex + 5) && (pageIndex + 5) < (pageCount - 5))))
                    pager.push({'text': '...........', 'value': i});
                if (i == 6 && pageIndex >= 6 && pageIndex < 11)
                    pager.push({'text': '...........', 'value': i});
            }
        }
        vm.pager = pager;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0, len = parts.length; i < len; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    filter();
}]).component('modalCustomersEdit', {
    templateUrl: 'template/customer/edit.html',
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        domain: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        };

        vm.addFeatureClick = function () {
            var feature = vm.customer.selectedAvailableFeature;
            if (feature) {
                for (var i = 0; i < vm.customer.available_features.length; i++) {
                    if (vm.customer.available_features[i].id == feature.id) {
                        vm.customer.available_features.splice(i, 1);
                        break;
                    }
                }
                vm.customer.enabled_features.push(feature);
                selectDefaultFeatures();
            }
        };
        vm.removeFeatureClick = function () {
            var feature = vm.customer.selectedEnabledFeature;
            if (feature) {
                for (var i = 0; i < vm.customer.enabled_features.length; i++) {
                    if (vm.customer.enabled_features[i].id == feature.id) {
                        vm.customer.enabled_features.splice(i, 1);
                        break;
                    }
                }
                vm.customer.available_features.push(feature);
                selectDefaultFeatures();
            }
        };
        function selectDefaultFeatures() {
            if (vm.customer.available_features.length > 0) {
                vm.customer.selectedAvailableFeature = vm.customer.available_features[0];
            }
            if (vm.customer.enabled_features.length > 0) {
                vm.customer.selectedEnabledFeature = vm.customer.enabled_features[0];
            }
        }
    }
}).component('modalCustomersAddNew', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/addnew.html'
}).component('modalCustomersAdd', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/add.html'
}).directive('toggle', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (attrs.toggle == "popover") {
                $(element).popover();
            }
        }
    };
}).component('modalDbSync', {
    bindings: {
        header: '<',
        show: '=',
        customer: '<',
        button: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
            vm.customer = {};
        }
    },
    templateUrl: 'template/customer/dbSync.html'
});
angular.module('app').controller('DownloadCtrl', ['$http', 'errorNotify', '$location', 'notificationService', 'successNotify', function ($http, errorNotify, $location, notificationService, successNotify) {
    var vm = this;
    vm.order = {};
    vm.filter = {};
    var search = '';

    vm.addClick = function () {
        vm.modalDownloadEditTitle = 'Add';
        vm.modalDownloadEditMap = {notes:''};
        vm.modalDownloadEditShow = true;
        vm.saveClick = add;
    };

    vm.editClick = function (component) {
        vm.modalDownloadEditTitle = 'Edit';
        vm.modalDownloadEditMap = {
            id: component.id,
            name: component.name,
            release_notes_url: component.release_notes_url,
            notes: component.notes
        };
        vm.modalDownloadLoading = true;
        $http.post('api/download/version/' + component.id).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDownloadEditMap.versions = response.data.response.data;
                vm.modalDownloadLoading = false;
                successNotify();
            }
        });
        vm.modalDownloadEditShow = true;
        vm.saveClick = edit;
    };

    vm.deleteClick = function (component) {
        vm.pnotifyHash = {
            title: 'Delete',
            title_escape: false,
            text: 'Are you sure you want to delete "' + component.name + '"?<br>Warning: This operation is irreversible',
            text_escape: false,
            styling: "bootstrap3",
            width: "450px",
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        };

        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/download/component/delete', component).then(function (response) {
                if (!hasError(response.data)) {
                    vm.refresh();
                }
            });
        });
    };

    function add(component) {
        save('api/download/component/create', component);
    }
    function edit(component) {
        save('api/download/component/update', component);
    }
    function save(url, component) {
        $http.post(url, component).then(function (response) {
            if (!hasError(response.data)) {
                vm.modalDownloadEditShow = false;
                successNotify();
                vm.refresh();
            }
        });
    }
    vm.refresh = function () {
        var data = {
            'name': vm.filter.name,
            'release_notes_url': vm.filter.release_notes_url,
            'notes': vm.filter.notes,
            'version': vm.filter.version,
            'release_date': vm.filter.release_date
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.release_notes_url) {
            searchParams = searchParams === '' ? 'release_notes_url=' + vm.filter.release_notes_url : searchParams + '&release_notes_url=' + vm.filter.release_notes_url;
        }
        if (vm.filter.notes) {
            searchParams = searchParams === '' ? 'notes=' + vm.filter.notes : searchParams + '&notes=' + vm.filter.notes;
        }

        if (vm.filter.version) {
            searchParams = searchParams === '' ? 'version=' + vm.filter.version : searchParams + '&version=' + vm.filter.version;
        }

        if (vm.filter.release_date) {
            searchParams = searchParams === '' ? 'release_date=' + vm.filter.release_date : searchParams + '&release_date=' + vm.filter.release_date;
        }

        $location.search(searchParams);

        $http.post('api/download/component/list', data).then(function (response) {
            if (!hasError(response.data)) {
                vm.components = response.data.response.data;
            }
        });
    };

    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.refresh();

}]).component('modalDownloadEdit', {
    bindings: {
        header: '<',
        show: '=',
        map: '<',
        save: '<',
        loading: '<'
    },
    controller: ['$http', function ($http) {
        var vm = this;
        vm.order = {};
        vm.add = function () {
            if (!vm.map.versions) {
                vm.map.versions = [];
            }
            vm.map.versions.push({public:1});
        };
        vm.remove = function(index) {
            var version = vm.map.versions[index];
            if (version.id) {
                version.delete = true;
                vm.form.$setDirty();
            } else {
                vm.map.versions.splice(index, 1);
            }
        };
        vm.close = function () {
            vm.show = false;
            vm.form.$setPristine();
        };
    }],
    templateUrl: 'template/download/edit.html'
});
angular.module('app').controller('RRCServiceCtrl', ['$http', '$location', 'notificationService', 'errorNotify', 'successNotify', function ($http, $location, notificationService, errorNotify, successNotify) {
    var vm = this;
    vm.filter = {};
    vm.order  = {};

    vm.search = function () {
        var data = {
            'name': vm.filter.name,
            'url': vm.filter.url,
            'version': vm.filter.version
        };

        var searchParams = '';

        if (vm.filter.name) {
            searchParams = searchParams === '' ? 'name=' + vm.filter.name : searchParams + '&name=' + vm.filter.name;
        }
        if (vm.filter.url) {
            searchParams = searchParams === '' ? 'url=' + vm.filter.url : searchParams + '&url=' + vm.filter.url;
        }
        if (vm.filter.version) {
            searchParams = searchParams === '' ? 'version=' + vm.filter.version : searchParams + '&version=' + vm.filter.version;
        }

        $location.search(searchParams);

        $http.post('api/rrcservice/list', data).then(function (response) {
            if (!hasError(response.data)) {
               vm.rrcservice = response.data.response.data;
            }
        });
    };

    vm.addClick = function (){
        vm.modalRRCEdit = {
            name: '',
            url: ''
        };
        vm.modalRRCEditShow = true;
        vm.modalRRCEditTitle = 'Add';
        vm.edit = false;
    };

    vm.editClick = function(rrc) {
        vm.modalRRCEdit = angular.copy(rrc);
        vm.modalRRCEditShow = true;
        vm.modalRRCEditTitle = 'Edit';
        vm.edit = true;
    };

    vm.saveClick = function (rrc) {
        var requestUrl = 'api/rrcservice/';
        if (vm.edit) {
            requestUrl += 'edit';
        } else {
            requestUrl += 'add';
        }

        $http.post(requestUrl, rrc).then(function (response) {
            if(!hasError(response.data)) {
                successNotify();
                vm.search();
            }
            vm.modalRRCEditShow = false;
        });
    };

    vm.checkAll = function () {
        for (var i=0; vm.rrcservice.length < i; i++) {
            vm.check(vm.rrcservice[i]);
        }
    };

    vm.check = function (rrc) {
        var postData = rrc === 'all' ? {'data': 'all'} : {'id': rrc.id, 'url': rrc.url};
        $http.post('api/rrcservice/check', postData).then(function (response) {
            if (!hasError(response.data)) {
                successNotify();
            }
            vm.search();
        })
    };

    vm.deleteClick = function (rrc) {
        vm.pnotifyHash = {
            title: 'Delete',
            title_escape: false,
            text: 'Are you sure you want to delete "' + rrc.name + '"?<br>Warning: This operation is irreversible',
            text_escape: false,
            styling: "bootstrap3",
            width: "450px",
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        };

        notificationService.notify(vm.pnotifyHash).get().on('pnotify.confirm', function () {
            $http.post('api/rrcservice/delete', rrc).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                    vm.search();
                }
            });
        });
    };

    vm.change_enable_status = function (rrc) {
        notificationService.notify({
            title: 'Update',
            styling: "bootstrap3",
            width: '500px',
            text: 'Are you sure you want to update status of ' + rrc.name + '?',
            confirm: {
                confirm: true
            }
        }).get().on('pnotify.confirm', function () {
            rrc.enabled = !rrc.enabled;
            var updateRRC;
            if (rrc.enabled) {
                updateRRC = {
                    id: rrc.id,
                    enabled: 1
                }
            } else if (!rrc.enabled) {
                updateRRC = {
                    id: rrc.id,
                    enabled: 0
                }
            }

            $http.post('/api/rrcservice/edit', updateRRC).then(function (response) {
                if (!hasError(response.data)) {
                    successNotify();
                } else {
                    rrc.enabled = !rrc.enabled;
                }
            });
        })
    };


    function hasError(response) {
        if (response.status == 'offline') {
            vm.offline = true;
            vm.offlineError = response.message;
            return true;
        } else if (response.status == 'error') {
            errorNotify(response.message);
            return true;
        }
        return false;
    }

    if (location.search) {
        var parts = location.search.substr(1).split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyValue = parts[i].split('=');
            if (keyValue[0] !== 'debug') {
                vm.filter[keyValue[0]] = decodeURIComponent(keyValue[1]);
            }
        }
    }

    vm.search();
}]).component('modalRrcEdit', {
    bindings: {
        header: '<',
        show: '=',
        rrc: '<',
        save: '<'
    },
    controller: function () {
        var vm = this;
        vm.close = function () {
            vm.show = false;
        }
    },
    templateUrl: 'template/rrcservice/edit.html'
});
