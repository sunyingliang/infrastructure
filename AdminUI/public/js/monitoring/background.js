$(function () {
    var server = $('#hdn_server').val();
    var url = '/api/background/list', type = 'post', data = {'server': server}, cbSuccess = actionSuccess, cbError = actionError;
    $.ajax({
        url: url,
        type: type,
        data: data,
        success: cbSuccess,
        error: cbError
    });
});

function actionSuccess(data, status, xhr) {
    var responseJson = data;
    if(responseJson.response.status == 'success'){
        // Fill data to web page
        var tbody = [], rows = responseJson.response.data;
        for(var i = 0; i < rows.length; i++){
            tbody.push('<tr>');
            tbody.push('<td>', rows[i]['timestamp'], '<td>');
            tbody.push('<td>', rows[i]['server_id'], '<td>');
            tbody.push('<td>', rows[i]['action'], '&emsp;site', rows[i]['site'], '<td>');
            tbody.push('</tr>');
        }
        $('#tbl_body').html(tbody.join(''));
    }else{
        $('#content').html(responseJson.response.message);
    }
}

function actionError(xhr, status, error) {
    $('#content').html('Failed to send request.');
}