$(function () {
    // Fetch default content
    fetchDefaultContent();
    setInterval(function(){
        $.ajax({
            'url': '/api/session/refresh',
            'type': 'get',
            'complete': function (data, status, xhr) {
                console.log('Session refreshed...');
            }
        });
    }, 300000);
});

var htmlDelimiter = 'FS_DELIMITER_HTML';

function fetchDefaultContent() {
    // Connect to local api to transfer it to infrastructure api
    var url = '/api/monitoring/default', type = 'post', data = {}, cbSuccess = fetchDefaultContentSuccess, cbError = fetchDefaultContentError;
    $('#hdn_mute') && (data['mute'] = $('#hdn_mute').val());
    $('#hdn_mutebird') && (data['mutebird'] = $('#hdn_mutebird').val());
    $('#hdn_fam') && (data['fam'] = $('#hdn_fam').val());
    $('#hdn_famtoken') && (data['famToken'] = $('#hdn_famtoken').val());
    doAjax(url, type, data, cbSuccess, cbError);
}

function fetchDefaultContentSuccess(data, status, xhr) {
    if (data.response) {
        var dataArr = data.response.data.split(htmlDelimiter);
        $('#header').html(dataArr[0]);
        $('#main').html(dataArr[1]);
        $('#tag').html(dataArr[2]);
    } else {
        $('#container').html(data);
    }

    if ($('#hdn_interval').length > 0) {
        setTimeout(function () {
            fetchDefaultContent();
        }, +$('#hdn_interval').val() * 1000);
    }
}

function fetchDefaultContentError(xhr, status, error) {
    $('#header').html('Failed to send request.');

    if ($('#hdn_interval').length > 0) {
        setTimeout(function () {
            fetchDefaultContent();
        }, +$('#hdn_interval').val() * 1000);
    }
}

function showOptions() {
    var x = document.getElementById("features");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("showButton").style.display = "none";
        document.getElementById("hideButton").style.display = "inline-block";
    } else {
        x.style.display = "none";
        document.getElementById("showButton").style.display = "inline-block";
        document.getElementById("hideButton").style.display = "none";
    }
}

function features(key) {
    var element = document.getElementById(key);
    var action;
    if (element.value === "on") {
        element.style.backgroundColor = '#f0ad4e';
        element.value = "off";
        action = 'remove';
    } else {
        element.style.backgroundColor = '#5cb85c';
        element.value = "on";
        action = 'insert';
    }
    changeQueryString(key, action);
}

function changeQueryString(key, action) {
    var urlQueryString = window.location.search;
    var queryString = '', paramsArr = [], param;
    if (action === 'remove') {
        if (urlQueryString !== "") {
            queryString = urlQueryString.substr(1);
            paramsArr = queryString.split("&");
            for (var i = paramsArr.length - 1; i >= 0; i -= 1) {
                param = paramsArr[i].split("=")[0];
                if (param === key) {
                    paramsArr.splice(i, 1);
                }
            }
            queryString = paramsArr.join("&");
        }
        if (queryString === '') {
            window.location.href = window.location.href.substr(0, window.location.href.indexOf("?"));
        } else {
            document.location.search = queryString;
        }
    }
    if (action === 'insert') {
        if (urlQueryString !== "") {
            document.location.search = urlQueryString + '&' + key + "=1";
        } else {
            document.location.search = key + "=1";
        }
    }
}


// Utils
function doAjax(url, type, data, cbSuccess, cbError) {
    $.ajax({
        url: url,
        type: type,
        data: data,
        success: cbSuccess,
        error: cbError
    });
}