<?php

require __DIR__ . '/../../autoload.php';
require __DIR__ . '/../../config/common.php';

ini_set('max_execution_time', CURL_TIMEOUT);

session_start(['read_and_close' => true]);

$router = new FS\Common\Router();

$router->get('/session/refresh', function () { return \FS\Authentication\Session::refresh(); });

if (strpos($_SERVER['REQUEST_URI'], '/api/monitoring/') !== false) {
    $router->route('/(monitoring/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService())->getMonitoringResult($query); });

    $router->execute();
} else if (isset($_SESSION['user'])) {
    $router->get('/login', function () { return (new FS\Authentication\Login())->login(); });

    $router->post('/(log/\S+/\S+)', function ($query) { return (new FS\Services\InfrastructureService())->getLogs($query); });

    $router->post('/(ftp/\S+)', function ($query) { return (new FS\Services\SolutionsAdminService('Solutions', 'Customer'))->getResult($query); });

    $router->group('/(sitemaster/', function () {
        $this->route('globaladmin/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('OAF', 'Global Admin'))->mapGlobalAdminPath($query); });
        $this->route('collections/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('Solutions', 'Collections'))->getCollectionsResult($query); });
    });

    $router->group('/(apibroker/', function () {
        $this->route('remap/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('API Broker', 'Remap'))->mapAPIBrokerPath($query); });
        $this->route('customer/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('API Broker', 'Customer'))->mapAPIBrokerPath($query); });
    });

    $router->route('/(holiday/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\SolutionsAdminService('Solutions', 'Holidays'))->getResult($query); });

    $router->route('/(solutions/customer/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\SolutionsAdminService('Solutions', 'Customer'))->getResult($query); });

    $router->post('/adminlog', function () { return (new \FS\Log\AdminLog('Logs', 'Audit'))->getAdminLogs(); });

    $router->post('/(proxy/mapping/\S+)', function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'Proxy'))->getProxyResult($query); });

    $router->route('/(oaf/statistics/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('OAF', 'Statistics'))->getTrackingResult($query); });

    $router->route('/cron/(\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'Crontab'))->getCronResult($query); });

    $router->group('/cms/', function () {
        $this->post('(queue/create)', function ($query) { return (new \FS\Services\CMSService('CMS', 'Customer'))->getResult($query); });
        $this->route('(customer/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\CMSService('CMS', 'Customer'))->getResult($query); });
        $this->get('config', function () { return (new \FS\Authentication\Config())->getConfig(); });
    });

    $router->post('/(lim/monitoring/\S+)', function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'LIM'))->getLIMResult($query); });

    $router->group('/(oaf/', function () {
        $this->post('background/\S+)', function ($query) { return (new FS\Services\InfrastructureService('OAF', 'Background Log'))->getBackgroundLogs($query); });
        $this->post('feature/log)', function ($query) { return (new FS\Services\InfrastructureService('OAF', 'Feature Usage'))->getTrackingResult($query); });
        $this->post('error/log)', function ($query) { return (new FS\Services\InfrastructureService('OAF', 'Errors'))->getTrackingResult($query); });
    });

    $router->group('/requestlog/', function () {
        $this->post('list', function () { return (new \FS\Log\RequestLog())->getLogs(); });
        $this->post('endpoint', function () { return (new \FS\Log\RequestLog())->getEndpoint(); });
        $this->post('tabpage', function () { return (new \FS\Log\RequestLog())->getTabPage(); });
        $this->post('getsummary', function () { return (new \FS\Log\RequestLog())->getSummary(); });
    });

    $router->route('/(download/\S+)', ['GET', 'POST'], function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'Download'))->getDownloadResult($query); });

    $router->post('/(rrcstatistics/\S+)', function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'RRC Service'))->getRRCResult($query); });

    $router->post('/(rrcservice/\S+)', function ($query) { return (new \FS\Services\InfrastructureService('Infrastructure', 'RRC Service'))->getRRCResult($query); });

    $router->execute();
} else {
    http_response_code(401);
    exit();
}
