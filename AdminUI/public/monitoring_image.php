<?php

if (!isset($_GET['i']) || !isset($_GET['t'])) {
    die('Error: Incorrect parameters');
}

require __DIR__ . '/../config/common.php';

try {
    $url   = INFRASTRUCTURE_SERVICE_LOCATION . 'monitoring/image';
    $token = INFRASTRUCTURE_SERVICE_TOKEN;
    $retryCount = 0;

    $postData = 'token=' . $token . '&id=' . base64_decode($_GET['i']) . '&type=' . base64_decode($_GET['t']);

    $opts = [
        CURLOPT_URL            => $url,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $postData
    ];

    $curl = curl_init();
    curl_setopt_array($curl, $opts);

    $response = curl_exec($curl);

    while ($response === false) {
        if ($retryCount < 20) {
            usleep(250);
            $response = curl_exec($curl);
            $retryCount++;
        } else {
            die('Error: Failed to get image via API from infrastructure');
        }
    }

    curl_close($curl);

    $responseArr = json_decode($response, true);

    if ($responseArr['response']['status'] != 'success') {
        die('Error: Failed to get image via API from infrastructure');
    }

    header('content-type: image/png');
    exit(base64_decode($responseArr['response']['data']));
} catch (Exception $e) {
    die('Error: ' . $e->getMessage() . PHP_EOL);
}

