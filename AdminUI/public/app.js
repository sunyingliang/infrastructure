(function () {
    Date.prototype.toISODateString = function () {
        return this.getFullYear() + '-' + ('0' + (this.getMonth() + 1)).slice(-2) + '-' + ('0' + this.getDate()).slice(-2);
    };
    var dateTest = document.createElement('input');
    try {
        dateTest.type = 'date';
    } catch (err) {
    }
    if (dateTest.type != 'date') {
        var css = document.createElement('link');
        css.setAttribute('rel', 'stylesheet');
        css.setAttribute('href', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css');
        document.body.appendChild(css);
        var j = document.createElement('script');
        j.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
        document.body.appendChild(j);
        var jui = document.createElement('script');
        jui.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
        document.body.appendChild(jui);
    }
    setInterval(function () {
        $.ajax({
            'url': '/api/session/refresh',
            'type': 'get',
            'complete': function (data, status, xhr) {
                console.log('Session refreshed...');
            }
        });
    }, 300000);
})();
angular.module('app', ['ngRoute', 'jlareau.pnotify', 'ui.toggle']).directive('input', function () {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function (scope, elm, attrs, ngModelCtrl) {
            var type = attrs.type;
            if (type == 'time') {
                ngModelCtrl.$parsers.push(function (value) {
                    if (value && value instanceof Date) {
                        return ('0' + value.getHours()).slice(-2) + ':' + ('0' + value.getMinutes()).slice(-2);
                    }
                    return value;
                });
                ngModelCtrl.$formatters.push(function (value) {
                    if (value && typeof value == 'string') {
                        var time = value.split(':');
                        return new Date(1970, 0, 1, time[0], time[1]);
                    }
                    return value;
                });
            } else if (type == 'date') {
                ngModelCtrl.$formatters.push(function (value) {
                    if (value && typeof value == 'string') {
                        return new Date(value);
                    }
                    return value;
                });
                var dateTest = document.createElement('input');
                try {
                    dateTest.type = 'date';
                } catch (err) {
                }
                if (dateTest.type != 'date') {
                    elm[0].style.width = '90px';
                    $(elm).datepicker({
                        dateFormat: "yy-mm-dd",
                        onSelect: function (date) {
                            ngModelCtrl.$setViewValue(date);
                        }
                    });
                }
            }
        }
    }
}).component('sortButton', {
    bindings: {
        property: '@',
        order: '='
    },
    template: '<input class="sort-btn btn btn-xs btn-default" type="button" ng-value="$ctrl.orderValue()" ng-click="$ctrl.orderBy()" ng-class="$ctrl.orderClass()"/>',
    controller: function () {
        var vm = this;
        vm.orderBy = function () {
            if (vm.order.property == vm.property) {
                if (vm.order.reverse) {
                    vm.order.property = '';
                    vm.order.sortOrder = '';
                }
                else {
                    vm.order.sortOrder = 'ASC';
                }
                vm.order.reverse = !vm.order.reverse;
            } else {
                vm.order.property = vm.property;
                vm.order.sortOrder = 'DESC';
            }
        };
        vm.orderValue = function () {
            if (vm.order.property == vm.property) {
                return vm.order.reverse ? '▲' : '▼';
            }
            return '▬';
        };
        vm.orderClass = function () {
            return vm.order.property == vm.property ? 'black' : 'lite';
        }
    }
}).factory('AuthInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    // redirect to login page when getting unauthorised error
    return {
        request: function (config) {
            var root = $rootScope.$$childHead.root;
            root.loading++;
            return config;
        },
        response: function (response) {
            var root = $rootScope.$$childHead.root;
            if (root.loading > 0) {
                root.loading--;
            }
            return response;
        },
        responseError: function responseError(rejection) {
            var root = $rootScope.$$childHead.root;
            if (root.loading > 0) {
                root.loading--;
            }
            if (rejection.status == 401) {
                location.href = 'famlogin.php?ReturnURL=' + encodeURIComponent(location.href);
            } else if (rejection.status == 403) {
                location.href = 'noaccess';
            } else if (rejection.status == 503) {
                location.href = '/';
            } else {
                return $q.reject(rejection);
            }
        }
    };
}]).factory('errorNotify', ['notificationService', function (notificationService) {
    return function (errorMessage) {
        notificationService.notify({
            styling: "bootstrap3",
            title: "Error",
            type: 'error',
            width: "600px",
            text: errorMessage
        })
    }
}]).factory('successNotify', ['notificationService', function (notificationService) {
    return function (successMessage) {
        if (typeof(successMessage) == 'undefined') {
            successMessage = '';
        }
        notificationService.notify({
            styling: "bootstrap3",
            title: "Success",
            type: 'success',
            text: successMessage
        })
    }
}]).factory('Auth', ['$http', function ($http) {
    var auth;
    return {
        setAuth: function (permission) {
            auth = permission;
        },
        isLogin: function () {
            return auth ? auth : false;
        }
    };
}]).filter('trim', function () {
    return function (value) {
        if (!angular.isString(value)) {
            return value;
        }
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
}).config(['$httpProvider', '$locationProvider', '$routeProvider', '$qProvider', function ($httpProvider, $locationProvider, $routeProvider, $qProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
    $qProvider.errorOnUnhandledRejections(false);
    $routeProvider.when('/', {
        templateUrl: 'template/home.html'
    }).when('/paymentconnector/', {
        templateUrl: 'template/paymentlog/paymentlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Payment Connectors',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/paymentConnectors/list',
                    init: function (vm, $http) {
                        $http.post('api/log/paymentConnectors/source').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.paymentSource = response.data.response.data;
                                if (!vm.filter.log) {
                                    vm.filter.log = vm.paymentSource[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    },
                    truncate: function (log) {
                        if (log.Message) {
                            if (log.Message.length > 40) {
                                log.truncateMessage = log.Message.substring(0, 40) + '...';
                                log.toggleMessage = 'Show Full';
                            } else {
                                log.truncateMessage = log.Message;
                            }
                        }
                        if (log.Exception) {
                            if (log.Exception.length > 40) {
                                log.truncateException = log.Exception.substring(0, 40) + '...';
                                log.toggleException = 'Show Full';
                            } else {
                                log.truncateException = log.Exception;
                            }
                        }
                    }
                }
            }
        }
    }).when('/limlog/', {
        templateUrl: 'template/limlog/limlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/LIM',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/lim/list'
                }
            }
        }
    }).when('/rrclog/', {
        templateUrl: 'template/rrclog/rrclog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/RRC',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/rrc/list',
                    init: function (vm, $http) {
                        $http.post('api/log/rrc/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.sites = response.data.response.data;
                                if (!vm.filter.site) {
                                    vm.filter.site = vm.sites[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/agilisyslog/', {
        templateUrl: 'template/agilisyslog/agilisyslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Agilisys Portal',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/agilisys/list',
                    init: function (vm, $http) {
                        $http.post('api/log/agilisys/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.sites = response.data.response.data;
                                if (!vm.filter.log) {
                                    vm.filter.log = vm.sites[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/log/agilisys/environment').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.environments = response.data.response.data;
                                if (!vm.filter.environment) {
                                    vm.filter.environment = vm.environments[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/ftplog/', {
        templateUrl: 'template/ftplog/ftplog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Solutions/FTP Log',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/ftp/list',
                    init: function (vm, $http) {
                        $http.post('api/ftp/log/customer').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.customer = response.data.response.data.map(function(value){
                                    return value.name;
                                });
                                if (!vm.filter.customer) {
                                    vm.filter.customer = vm.customer[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/filltasklog/', {
        templateUrl: 'template/filltasklog/filltasklog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/FillTask',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/fillTask/list'
                }
            }
        }
    }).when('/integrationlog/', {
        templateUrl: 'template/integrationlog/integrationlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Integration',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/integration/list',
                    init: function (vm, $http) {
                        $http.post('api/log/integration/source').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.loggers = response.data.response.data.logger;
                                vm.customers = response.data.response.data.customer;
                                if (!vm.filter.logger) {
                                    vm.filter.log = vm.loggers[0];
                                }
                                if (!vm.filter.customer) {
                                    vm.filter.customer = vm.customers[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    },
                    truncate: function (log) {
                        if (log.message) {
                            if (log.message.length > 40) {
                                log.truncateMessage = log.message.substring(0, 40) + '...';
                                log.toggleMessage = 'Show Full';
                            } else {
                                log.truncateMessage = log.message;
                            }
                        }
                        if (log.exception) {
                            if (log.exception.length > 35) {
                                log.truncateException = log.exception.substring(0, 35) + '...';
                                log.toggleException = 'Show Full';
                            } else {
                                log.truncateException = log.exception;
                            }
                        }
                    }
                }
            }
        }
    }).when('/componentslog/', {
        templateUrl: 'template/componentslog/componentslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Components',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/components/list',
                    init: function (vm, $http) {
                        $http.post('api/log/components/status').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.status = response.data.response.data;
                                if (!vm.filter.status) {
                                    vm.filter.status = vm.status[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/collectionslog/', {
        templateUrl: 'template/collectionslog/collectionslog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Collections',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/collections/list'
                }
            }
        }
    }).when('/downloadlog/', {
        templateUrl: 'template/downloadlog/downloadlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Download',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/download/list'
                }
            }
        }
    }).when('/backgroundlog/', {
        templateUrl: 'template/backgroundlog/backgroundlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Background Log',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/background/list',
                    init: function (vm, $http) {
                        $http.post('api/oaf/background/site').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.site = response.data.response.data;
                                if (!vm.filter.site) {
                                    vm.filter.site = vm.site[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/oaf/background/server').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.server = response.data.response.data;
                                if (!vm.filter.server) {
                                    vm.filter.server = vm.server[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                        $http.post('api/oaf/background/operation').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.operation = response.data.response.data;
                                if (!vm.filter.operation) {
                                    vm.filter.operation = vm.operation[0];
                                }
                            } else {
                                vm.offline = true;
                                vm.offlineError = response.data.message;
                            }
                        });
                    }
                }
            }
        }
    }).when('/limmonitor/', {
        templateUrl: 'template/limmonitor/limmonitor.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/LIM',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/lim/monitoring/list',
                    init: function (vm, $http, errorNotify) {
                        $http.post('api/lim/monitoring/resultcodes').then(function (response) {
                            if (response.data.status !== 'error') {
                                vm.resultCodes = response.data.response.data;
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                    }
                }
            }
        }
    }).when('/platformerrors/', {
        templateUrl: 'template/platformerrors/platformerrors.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Errors',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/error/log'
                }
            }
        }
    }).when('/featureusage/', {
        templateUrl: 'template/featureusage/featureusage.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'OAF/Feature Usage',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/oaf/feature/log'
                }
            }
        }
    }).when('/customer/', {
        templateUrl: 'template/customer/customer.html',
        controller: 'CustomerCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Customer',
        reloadOnSearch: false
    }).when('/globaladmin/', {
        templateUrl: 'template/globaladmin/globaladmin.html',
        controller: 'GlobalAdminCtrl',
        controllerAs: 'vm',
        title: 'OAF/Global Admin'
    }).when('/reverseproxy/', {
        templateUrl: 'template/reverseproxy/reverseproxy.html',
        controller: 'ReverseProxyCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Proxy',
        reloadOnSearch: false
    }).when('/cms/', {
        templateUrl: 'template/cms/cms.html',
        controller: 'CMSCtrl',
        controllerAs: 'vm',
        title: 'CMS/Customer',
        reloadOnSearch: false
    }).when('/apibroker/', {
        templateUrl: 'template/apibroker/apibroker.html',
        controller: 'APIBrokerCtrl',
        controllerAs: 'vm',
        title: 'API Broker/Remap',
        reloadOnSearch: false
    }).when('/apicustomer/', {
        templateUrl: 'template/apicustomer/apicustomer.html',
        controller: 'APICustomerCtrl',
        controllerAs: 'vm',
        title: 'API Broker/Customer',
        reloadOnSearch: false
    }).when('/holidays/', {
        templateUrl: 'template/holidays/holidays.html',
        controller: 'HolidaysCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Holidays',
        reloadOnSearch: false
    }).when('/collections/', {
        templateUrl: 'template/collections/collections.html',
        controller: 'CollectionsCtrl',
        controllerAs: 'vm',
        title: 'Solutions/Collections',
        reloadOnSearch: false
    }).when('/statistics/', {
        templateUrl: 'template/statistics/statistics.html',
        controller: 'StatisticsCtrl',
        controllerAs: 'vm',
        title: 'OAF/Statistics',
        reloadOnSearch: false
    }).when('/cronjob/', {
        templateUrl: 'template/cronjob/cronjob.html',
        controller: 'CronJobCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Crontab',
        reloadOnSearch: false
    }).when('/cronlog/', {
        templateUrl: 'template/cronlog/cronlog.html',
        controller: 'CronLogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Crontab',
        reloadOnSearch: false
    }).when('/download/', {
        templateUrl: 'template/download/download.html',
        controller: 'DownloadCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/Download',
        reloadOnSearch: false
    }).when('/auditlog/', {
        templateUrl: 'template/auditlog/auditlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Audit',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/adminlog'
                }
            }
        }
    }).when('/requestlog/', {
        templateUrl: 'template/requestlog/requestlog.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Request',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/requestlog/list',
                    init: function (vm, $http, errorNotify) {
                        $http.post('api/requestlog/endpoint').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.endpoints = response.data.response.data;
                                if (!vm.filter.endpoint) {
                                    vm.filter.endpoint = vm.endpoints[0];
                                }
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                        $http.post('api/requestlog/tabpage').then(function (response) {
                            if (!response.data.message) {
                                vm.offline = false;
                                vm.tab_pages = response.data.response.data;
                                if (!vm.filter.tab_page) {
                                    vm.filter.tab_page = vm.tab_pages[0];
                                }
                            } else {
                                errorNotify(response.data.message);
                            }
                        });
                    }
                }
            }
        }
    }).when('/emailsentlog/', {
        templateUrl: 'template/email/emailsent.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Email Sent',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/email/list?status=sent'
                }
            }
        }
    }).when('/emailfailedlog/', {
        templateUrl: 'template/email/emailfailed.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Logs/Email Failed',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/log/email/list?status=failed'
                }
            }
        }
    }).when('/rrcservice/', {
        templateUrl: 'template/rrcservice/rrcservice.html',
        controller: 'RRCServiceCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/RRC Service',
        reloadOnSearch: false
    }).when('/rrcstatistics/', {
        templateUrl: 'template/rrcstatistics/rrcstatistics.html',
        controller: 'LogCtrl',
        controllerAs: 'vm',
        title: 'Infrastructure/RRC Statistics',
        reloadOnSearch: false,
        resolve: {
            params: function () {
                return {
                    url: 'api/rrcstatistics/list'
                }
            }
        }
    }).when('/noaccess/', {
        templateUrl: 'noaccess.html'
    }).when('/monitoring/', {
        redirectTo: '/api/config'
    }).otherwise({
        redirectTo: '/'
    });
}]).controller('AppCtrl', ['$http', '$scope', '$window', 'errorNotify', '$location', 'Auth', function ($http, $scope, $window, errorNotify, $location, auth) {
    var vm = this, path = $window.location.pathname.substr(-1) == '/' ? $window.location.pathname.slice(0, -1) : $window.location.pathname;
    if (path == '/noaccess') {
        return;
    }
    vm.loading = 0;

    vm.tabActive = function (tab) {
        for (var i=0; i < vm.auth.permissions[tab].length; i++) {
            if (vm.auth.permissions[tab][i]['path'] + '/' === location.pathname) {
                return true;
            }
        }
    };

    vm.hasWritePermission = function (tab, page) {
        if (vm.auth) {
            for (var key in vm.auth.permissions[tab]) {
                if (vm.auth.permissions[tab][key]['page'] == page && vm.auth.permissions[tab][key]['permission'] == 'write') {
                    return true;
                }
            }
        }
        return false;
    };

    vm.isActive = function (viewLocation) {
        return viewLocation === location.pathname;
    };

    $http.get('api/login').then(function (response) {
        if (response.data.message) {
            errorNotify(response.data.message);
        } else {
            auth.setAuth(response.data);
            vm.auth = response.data;
        }
    }, function (data) {
        console.log(data)
    });

    $scope.$on('$routeChangeSuccess', function (event, data) {
        //Change page title, based on Route information
        document.title = 'Admin' + (data.title ? ' - ' + data.title : '');
    });
}]).run(['$rootScope', '$location', '$window', '$http', 'Auth', function ($rootScope, $location, $window, $http, auth) {
    $rootScope.$on('$routeChangeStart', function (event) {
        if (!auth.isLogin()) {
            var st = Math.floor(Date.now() / 1000);
            while (Math.floor(Date.now() / 1000) - st < 2) {;}
        }
    });
}]);
