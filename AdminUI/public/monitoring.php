<?php

session_start(['read_and_close' => true]);

require __DIR__ . '/../config/common.php';

if (isset($_SESSION['user']) || (isset($_GET['fam'], $_GET['token']) && defined('TOKEN_OVERRIDE_MONITORING') && $_GET['token'] === TOKEN_OVERRIDE_MONITORING)) {
    $pause        = isset($_GET['pause']) ? $_GET['pause'] : false;
    $hiddenInputs = '<input type="hidden" id="hdn_pause" name="pause" value="' . $pause . '">';
    if (empty($pause)) {
        // Random refresh interval. Value generated is in range 45-75 sec.
        $refresh_interval = mt_rand(45, 75);
        if (!empty($refresh_interval)) {
            $hiddenInputs .= '<input type="hidden" id="hdn_interval" name="interval" value="' . $refresh_interval . '">';
        }
    }

    $mute         = isset($_GET['mute']) ? $_GET['mute'] : false;
    $hiddenInputs .= '<input type="hidden" id="hdn_mute" name="mute" value="' . $mute . '">';
    $muteBird     = isset($_GET['mutebird']) ? $_GET['mutebird'] : false;
    $hiddenInputs .= '<input type="hidden" id="hdn_mutebird" name="mutebird" value="' . $muteBird . '">';
    $inverseStyle = isset($_GET['inverse']) ? 'on' : 'off';
    $muteStyle    = isset($_GET['mute']) ? 'on' : 'off';
    $pauseStyle   = isset($_GET['pause']) ? 'on' : 'off';
    $fam          = isset($_GET['fam']) ? $_GET['fam'] : false;
    $hiddenInputs .= '<input type="hidden" id="hdn_fam" name="fam" value="' . $fam . '">';
    $famToken     = isset($_GET['token']) ? $_GET['token'] : false;
    $hiddenInputs .= '<input type="hidden" id="hdn_famtoken" name="famtoken" value="' . $famToken . '">';

    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width">
            <title>Monitoring</title>
            <link type="text/css" rel="stylesheet" href="/style/monitoring/reset.css">
            <link type="text/css" rel="stylesheet" href="/style/monitoring/monitoring.css">
            <?php
            if (!empty($_GET['inverse'])) {
                echo '<link type="text/css" rel="stylesheet" href="/style/monitoring/inverse.css">';
            }
            ?>
        </head>
        <body>
            <div id="features" style="display: none">
                <div><button id="pause" onclick="features('pause')" class="<?php echo $pauseStyle ?>" value="<?php echo $pauseStyle ?>">Pause</button></div>
                <div><button id="mute" onclick="features('mute')" class="<?php echo $muteStyle ?>" value="<?php echo $muteStyle ?>">Mute</button></div>
                <div><button id="inverse" onclick="features('inverse')" class="<?php echo $inverseStyle ?>" value="<?php echo $inverseStyle ?>">Inverse</button></div>
            </div>
            <div id="container">
                <div class="functions">
                    <button id="showButton" onclick="showOptions()" style="display: inline-block"></button>
                    <button id="hideButton" onclick="showOptions()" style="display: none"></button>
                </div>
                <div id="tag">
                </div>
                <div id="content" class="content">
                    <table class="wrapper" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td colspan="6" id="header">
                                    Loading...
                                </td>
                            </tr>
                            <tr id="main">
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="hidden">
                    <?php echo $hiddenInputs; ?>
                </div>
            </div>
            <div class="scripts-container">
                <script type="text/javascript" src="/js/jquery/jquery-3.1.0.min.js"></script>
                <script type="text/javascript" src="/js/monitoring/default.js"></script>
            </div>
        </body>
    </html>
<?php
} else {
    header('Location: /famlogin.php?ReturnURL=' . $_SERVER['REQUEST_URI']);
}
