<?php

session_start(['read_and_close' => true]);

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/IO.php';

use FS\Common\IO;

// Used to dynamically adjust scripts to always pull the latest (No cache refresh needed on re-deploy)
$buildDate = rand(0, 9999);

if (file_exists(__DIR__ . '/build')) {
    try {
        $buildDate = str_replace(' ', '', str_replace('-', '', substr(file_get_contents(__DIR__ . '/build'), 15, 13)));
    } catch (Exception $e) {
        // Do nothing, this is expected
    }
}

// Check if selenium token in query string. If it is, login to AdminUI as selenium user
if (!empty($_GET['token']) && ($_GET['token'] == TOKEN_OVERRIDE_READ || $_GET['token'] == TOKEN_OVERRIDE_ADMIN)) {
    session_start();
    $memberof = [];
    $groups = [];

    $connection = IO::getPDOConnection(ADMINUI_CONNECTION);

    if ($_GET['token'] == TOKEN_OVERRIDE_READ) {
        $groups = $connection->query("SELECT `read` AS `user_group` FROM `permission` WHERE `read` IS NOT NULL GROUP BY `user_group`;")->fetchAll();
        $_SESSION['user']['displayname'] = 'SELENIUM READ';
    } else if ($_GET['token'] == TOKEN_OVERRIDE_ADMIN) {
        $groups = $connection->query("SELECT DISTINCT CONCAT(`read`,',', `admin`) AS `user_group` FROM `permission` WHERE `read` IS NOT NULL AND `admin` IS NOT NULL;")->fetchAll();
        $_SESSION['user']['displayname'] = 'SELENIUM ADMIN';
    }

    foreach ($groups as $group) {
        $groupArray = explode(',', $group['user_group']);
        foreach ($groupArray as $userGroup) {
            array_push($memberof, $userGroup);
        }
    }

    $_SESSION['user']['memberof'] = array_unique($memberof);
    session_write_close();
}

if (isset($_SESSION['user'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en" ng-app="app">
        <head>
            <meta charset="UTF-8">
            <base href="/">
            <title>Admin</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css"/>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css"/>
            <link rel="stylesheet" href="style/angular-bootstrap-toggle.min.css?build=<?php echo $buildDate; ?>">
            <link rel="stylesheet" href="style/mainform.css?build=<?php echo $buildDate; ?>"/>
        </head>
        <body>
            <div ng-controller="AppCtrl as root" style="min-width: 1280px">
                <div class="header-div">
                    <div class="header-left">
                        <span class="glyphicon glyphicon-refresh right-spinner" ng-class="{transparent: !root.loading}"></span>
                        <a href="/">
                            <img class="logo" src="style/firmstep-logo.png"/>
                            <span style="color: <?php echo ENVIRONMENT_COLOUR ?>" ng-init="root.environment =  '<?php echo ENVIRONMENT; ?>'">&nbsp;Admin
                                <?php if (!empty(ENVIRONMENT)) echo ' - '; echo ENVIRONMENT; ?>
                            </span>
                            <span ng-init="root.domain = '<?php echo DOMAIN; ?>'"></span>
                        </a>
                    </div>
                    <div class="header-right"><span ng-bind="root.auth.user"></span></div>
                </div>
                <div class="nav-bar">
                    <ul id="nav-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="/monitoring.php" target="_blank">Monitoring</a></li>
                        <li ng-cloak ng-repeat="(key, value) in root.auth.permissions">
                            <a href="{{value[0]['path']}}" ng-class="{active: root.tabActive('{{key}}')}">{{key}}</a>
                            <ul>
                                <li ng-repeat="page in root.auth.permissions[key] | orderBy:page" ng-class="{active: root.isActive('{{page.path}}/')}"><a href="{{page.path}}">{{page.page}}</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="maincontent" ng-view>
                </div>
                <div id="loading" ng-show="root.loading">
                    <div id="loading-icon">
                        <img src="../style/page-loader.gif" width="100px" height="100px">
                    </div>
                </div>
            </div>
            <footer>
                <div class="footer-right">
                    <?php echo '&copy; ', date("Y"), ".\r\n"; ?>
                </div>
            </footer>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular-route.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.confirm.min.js"></script>
            <script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>
            <script src="js/angular-pnotify.min.js?build=<?php echo $buildDate; ?>"></script>
            <script src="js/angular-bootstrap-toggle.min.js?build=<?php echo $buildDate; ?>"></script>
            <?php if (defined('DEBUG_MODE') && DEBUG_MODE === true) { ?>
                <script src="app.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/logController.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/collections/collections.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/cronlog/cronlog.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/cronjob/cronjob.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/globaladmin/globaladmin.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/reverseproxy/reverseproxy.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/statistics/statistics.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/cms/cms.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/apibroker/apibroker.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/apicustomer/apicustomer.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/holidays/holidays.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/customer/customer.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/download/download.js?build=<?php echo $buildDate; ?>"></script>
                <script src="template/rrcservice/rrcservice.js?build=<?php echo $buildDate; ?>"></script>
            <?php } else { ?>
                <script src="main.min.js?build=<?php echo $buildDate; ?>"></script>
            <?php } ?>
        </body>
    </html>
    <?php
} else {
    header('Location: /famlogin.php?ReturnURL=' . $_SERVER['REQUEST_URI']);
}
