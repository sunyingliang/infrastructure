<?php

if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/IO.php';

use FS\Common\IO;

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
$counter = 0;

while ($counter < 10) {
    try {
        $connection = IO::getPDOConnection(ADMINUI_CONNECTION);
        $counter = 10;
    } catch (Exception $e) {
        sleep(30);
        $counter++;
    }
}

if (isset($connection)) {
    // Admin Log table
    $sql = "CREATE TABLE IF NOT EXISTS `adminlogs` (
                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `action` varchar(250) NOT NULL,
                `username` varchar(250) NOT NULL,
                `data` text,
                PRIMARY KEY (`id`)
            )";

    $connection->exec($sql);
}

#region AdminUI Permission
$sql = "CREATE TABLE IF NOT EXISTS `permission`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `tab` VARCHAR(64) NOT NULL,
    `page` VARCHAR(255) NOT NULL,
    `path` VARCHAR(255) NOT NULL,
    `read` VARCHAR(1024),
    `admin` VARCHAR(1024),
    `enabled` TINYINT(1) DEFAULT 1,
    PRIMARY KEY(`id`)
) DEFAULT CHARACTER SET UTF8";

if ($connection->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

$result = $connection->query('SELECT COUNT(1) AS `total` FROM `permission`')->fetch();

if ($result['total'] == 0) {
    $tabs = [
        'cms'     => ['customer'],
        'infrastructure' => [
            'api broker',
            'crontab',
            'download',
            'proxy',
            'lim',    
        ],
        'solutions'   => [
            'collections',
            'customer',
            'ftp log',
            'holidays'
        ],
        'oaf'         => [
            'global admin',
            'statistics',
            'background log'
        ],
        'logs'        => [
            'audit',
            'agilisys portal',
            'collections',
            'components',
            'crontab',
            'filltask',
            'lim',
            'payment connectors',
            'rrc',
            'email'
        ]
    ];

    $values = '';

    if (!empty($tabs)) {
        foreach ($tabs as $tab => $pages) {
            foreach ($pages as $page) {
                $values .= "('${tab}', '${page}', 'developers'), ";
            }
        }

        $values = substr($values, 0, -2);
        $sql    = "INSERT INTO `permission`(`tab`, `page`, `admin`) VALUES ${values}";

        $connection->exec($sql);
    }
}

// Check if `path` column exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'permission' AND column_name = 'path'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `permission` ADD `path` VARCHAR(255) NOT NULL AFTER `page`;");
}

// Check if index `tab` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'permission' AND index_name = 'tab'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `permission` ADD INDEX `tab` (`tab`);");
}

// Check if index `page` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'permission' AND index_name = 'page'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `permission` ADD INDEX `page` (`page`);");
}

// Check if index `path` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'permission' AND index_name = 'path'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `permission` ADD INDEX `path` (`path`);");
}

// Check if index `enabled` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'permission' AND index_name = 'enabled'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `permission` ADD INDEX `enabled` (`enabled`);");
}

#endregion

// Create `tab` table if not exist
$sql = "CREATE TABLE IF NOT EXISTS `tab`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `sequence` SMALLINT NOT NULL,
    `enabled` TINYINT(1) DEFAULT 1,
    PRIMARY KEY(`id`)
) DEFAULT CHARACTER SET UTF8";

if ($connection->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `name` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'tab' AND index_name = 'name'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `tab` ADD INDEX `name` (`name`);");
}

// Check if index `enabled` exists, if not create it
$result = $connection->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'tab' AND index_name = 'enabled'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $connection->exec("ALTER TABLE `tab` ADD INDEX `enabled` (`enabled`);");
}


// Create `request_audit_log` table if not exist
$sql = "CREATE TABLE IF NOT EXISTS `request_audit_log`(
    `id` INT NOT NULL AUTO_INCREMENT,
    `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `tab_page` VARCHAR(32) NOT NULL,
    `endpoint` VARCHAR(32) NOT NULL,
    `parameters` VARCHAR(255) DEFAULT NULL,
    `execution_time` FLOAT,
    PRIMARY KEY(`id`),
    key `datetime` (`datetime`),
    key `tab_page` (`tab_page`),
    key `endpoint` (`endpoint`),
    key `parameters` (`parameters`)
) DEFAULT CHARACTER SET UTF8";

if ($connection->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

$connection = null;

?>
