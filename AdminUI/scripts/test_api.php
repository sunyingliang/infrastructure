<?php

$curl = curl_init();

$opts = [
    CURLOPT_URL            => 'http://customer.firmstep.com/report.asp?Page=1&Report=Sent&Key=XXX',
    //CURLOPT_CUSTOMREQUEST  => 'GET',
    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    //CURLOPT_SSL_VERIFYHOST => 0,
    //CURLOPT_SSL_VERIFYPEER => FALSE
];

curl_setopt_array($curl, $opts);
$response = curl_exec($curl);

//print_r($response);

$curlInfo = curl_getinfo($curl);
print_r($curlInfo);

$xmlReader = new XMLReader();
//$xmlReader->open('http://customer.firmstep.com/report.asp?Page=1&Report=Sent&Key=XXX');
$xmlReader->xml($response);

/*
while ($xmlReader->read()) {
    print_r($xmlReader->readOuterXml());
    print_r($xmlReader->readInnerXml());
    print_r($xmlReader->readString());
}
*/

while ($xmlReader->read() && $xmlReader->nodeType == \XMLReader::ELEMENT) {
    echo $xmlReader->name . PHP_EOL;
}


$result = ['TotalRowCount' => 0, 'Rows' => []];

if ($xmlReader->read() && $xmlReader->name == 'results') {
    $result['TotalRowCount'] = $xmlReader->getAttribute('count') ?? 0;
}

while ($xmlReader->read() && $xmlReader->nodeType == \XMLReader::ELEMENT) {
    print_r($xmlReader->name);

    if ($xmlReader->name == 'result') {
        if (!empty($row)) {
            $result['Rows'][] = $row;
        }

        $row = [];
    } else {
        $row[$xmlReader->name] = $xmlReader->readString();
    }
}

print_r($result);

class ExceptionTest
{
    public function __construct()
    {
        try {
            throw new \Exception('Throwable from ' . __METHOD__ . PHP_EOL);
            echo 'After throwable ...' . PHP_EOL;
        } catch (\Exception $e) {
            echo 'Caught in catch block ...' . PHP_EOL;
        }

        echo 'After try block ...' . PHP_EOL;
    }

}

$eTest = new ExceptionTest();

