<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/IO.php';

use FS\Common\IO;

try {
    // Truncate table
    $connection = IO::getPDOConnection(ADMINUI_CONNECTION);
    $connection->exec("TRUNCATE TABLE `request_audit_log`");
} catch (Exception $ex) {
    die($e->getMessage());
}