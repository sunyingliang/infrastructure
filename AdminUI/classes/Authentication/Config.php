<?php

namespace FS\Authentication;

class Config
{
    public function __construct()
    {
        if (!is_readable(__DIR__ . '/../../config/common.php')) {
            die('Configure file {' . __DIR__ . '/../../config/common.php} is not readable');
        }

        require_once __DIR__ . '/../../config/common.php';
    }

    public function getConfig()
    {
        return [
            'userName'  => $this->getUser() ? $this->getUser()['name'] : '',
            'userEmail' => $this->getUser() ? $this->getUser()['email'] : '',
        ];
    }

    #region Utils
    private function getUser()
    {
        if (isset($_SESSION['user'])) {
            return [
                'name'  => $_SESSION['user']['displayname'],
                'email' => $_SESSION['user']['mail'],
            ];
        }

        return null;
    }
    #endregion
}
