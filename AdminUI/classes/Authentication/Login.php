<?php

namespace FS\Authentication;

use FS\Services\ServiceBase;
use FS\Common\IO;

class Login extends ServiceBase
{
    private $user;

    public function __construct()
    {
        $this->user = $this->getUser();
    }

    public function login()
    {
        if (empty($this->user)) {
            http_response_code(401);
            return '';
        }
        http_response_code(200);
        return $this->user;
    }

    public function getUser()
    {
        if (isset($_SESSION['user'])) {
            // Construct permission for every page
            if (!defined('INFRASTRUCTURE_SERVICE_LOCATION') || !defined('INFRASTRUCTURE_SERVICE_TOKEN')) {
                throw new \Exception('Configuration file is not set up correctly');
            }

            try {
                $connection  = IO::getPDOConnection(ADMINUI_CONNECTION);
                $permissions = $connection->query(
                    'SELECT `permission`.`tab`, `permission`.`page`, `permission`.`path`, `permission`.`read`, `permission`.`admin`, `tab`.`sequence` 
                                FROM `permission`
                                INNER JOIN `tab` ON `tab`.`name` = `permission`.`tab` AND `tab`.`enabled` = 1
                                WHERE `permission`.`enabled` = 1
                                ORDER BY `tab`.`sequence`, `permission`.`page`;'
                )->fetchAll();

                $connection  = null;
            } catch (\Exception $ex) {
                throw new \Exception('Error fetching data from permission table: ' . $ex->getMessage());
            }

            //Set 'Everyone' to groups array
            $groups = ['Everyone'];

            if (isset($_SESSION['user']['memberof']) && !empty($_SESSION['user']['memberof'])) {
                $memberOf = $_SESSION['user']['memberof'];
                if (!is_array($memberOf)) {
                    $memberOf = [$memberOf];
                }

                // If 'Everyone' not in memberOf group, merges $memberOf into $groups'
                if (!in_array('Everyone', $memberOf)) {
                    $groups = array_merge($memberOf, $groups);
                } else {
                    $groups = $memberOf;
                }
            }

            $userPermissions = [];

            foreach ($permissions as $permission) {
                $permission = (array)$permission;

                // Get read and admin groups for each tab and page
                // If login user is 'SELENIUM READ', set admin column empty, only give the user read permission;
                // If login user is not 'SELENIUM READ', give the user both read and write permission based on permission table;
                $pageReadGroup  = array_map('trim', explode(',', $permission['read']));
                $pageAdminGroup = $_SESSION['user']['displayname'] == 'SELENIUM READ' ? [] : array_map('trim', explode(',', $permission['admin']));

                // Read access to 'Everyone' $read/$writes
                // Check if 'everyone' is in $pageReadGroup or $pageAdminGroup, if it is, give the user permission to access AdminUI
                $override = !empty(array_intersect($pageReadGroup, ['Everyone'])) || !empty(array_intersect($pageAdminGroup, ['Everyone']));

                // If some of the user's groups in $pageReadGroup or $pageAdminGroup or everyone,
                // store user permission info (getting from `permission` table) of this tab and page to $userPermissions
                if (!empty(array_intersect($groups, $pageReadGroup)) || !empty(array_intersect($groups, $pageAdminGroup)) || $override) {
                    if (!isset($userPermissions[$permission['tab']])) {
                        $userPermissions[$permission['tab']] = [];
                    }

                    $menuStructure = ['page' => $permission['page'], 'path' => $permission['path']];

                    // If some of the user's groups are in Admin Groups, give the user write permission of this tab and page
                    // If some of the user's groups are in Read Groups, give the user read permission of this tab and page
                    if (!empty(array_intersect($groups, $pageAdminGroup))) {
                        $menuStructure['permission'] = 'write';
                    } else if (!empty(array_intersect($groups, $pageReadGroup)) || $override) {
                        $menuStructure['permission'] = 'read';
                    }

                    array_push($userPermissions[$permission['tab']], $menuStructure);
                }
            }

            session_start();
            $_SESSION['user']['permissions'] = $userPermissions;
            session_write_close();

            if (!isset($this->user)) {
                $groups = $_SESSION['user']['memberof'];

                $this->user = [
                    'user'       => $_SESSION['user']['displayname'],
                    'permissions' => $_SESSION['user']['permissions']
                ];
            }

            return $this->user;
        }

        return '';
    }
}
