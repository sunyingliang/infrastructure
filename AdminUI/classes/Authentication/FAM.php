<?php

namespace FS\Authentication;

require __DIR__ . '/Crypto.php';

class FAM
{
    private $crypto;

    public function __construct($famKey, $famIV, $famCipher)
    {
        $this->crypto = new Crypto($famKey, $famIV, $famCipher);
    }

    public function generateRequest()
    {
        return htmlspecialchars($this->crypto->encrypt($this->generateSAML(), true));
    }

    public function decodeResponse($authToken)
    {
        return $this->loadSAML($this->crypto->decrypt($authToken, true));
    }

    private function generateSAML()
    {
        $saml = '<AuthnRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" ID="';
        $saml .= uniqid('', true);
        $saml .= '" Version="2.0" IssueInstant="';
        $saml .= date("c");
        $saml .= '" AttributeConsumingServiceIndex="0" xmlns="urn:oasis:names:tc:SAML:2.0:protocol"><Issuer xmlns="urn:oasis:names:tc:SAML:2.0:assertion">http://famdemo.itechcraft</Issuer><Extensions /><Subject xmlns="urn:oasis:names:tc:SAML:2.0:assertion" /><NameIDPolicy /><Conditions NotOnOrAfter="';
        $saml .= date("c", time() + 120);
        $saml .= '" xmlns="urn:oasis:names:tc:SAML:2.0:assertion" /><RequestedAuthnContext /><Scoping><IDPList /></Scoping></AuthnRequest>';

        return $saml;
    }

    private function loadSAML($document)
    {
        $saml = new \DOMDocument();
        $saml->loadXML($document);
        $samlXPath = new \DOMXPath($saml);
        $samlXPath->registerNamespace('saml', 'urn:oasis:names:tc:SAML:2.0:protocol');
        $samlXPath->registerNamespace('samlassertion', 'urn:oasis:names:tc:SAML:2.0:assertion');

        //first, check for a valid response
        $status = $samlXPath->query('/saml:Response/saml:Status/saml:StatusCode');

        if ($status->length == 0) {
            throw new \Exception('FAM response status not found.');
        }

        $statusValue = $status->item(0)->attributes->getNamedItem('Value');

        if ($statusValue == null || $statusValue->textContent != 'urn:oasis:names:tc:SAML:2.0:status:Success') {
            throw new \Exception('FAM response status failed.');
        }

        //next, check the timestamps. We allow +- 10 mins on this (this is to prevent replay attacks)
        $timeStamp = $samlXPath->query('/saml:Response/samlassertion:Assertion/samlassertion:AuthnStatement');

        if ($timeStamp->length == 0) {
            throw new \Exception('FAM response timestamp not found.');
        }

        $timeStampValue = $timeStamp->item(0)->attributes->getNamedItem('AuthnInstant');
        $timeStampTime  = new \DateTime($timeStampValue->textContent);
        $currentTime    = new \DateTime();

        if ($timeStampValue == null || abs($timeStampTime->getTimestamp() - $currentTime->getTimestamp()) > 600) {
            throw new \Exception('FAM response timestamp is too old.  Check clocks on both servers.');
        }

        //ok, now loop through each supplied attribute and build up an array object to return
        $attributes = $samlXPath->query('/saml:Response/samlassertion:Assertion/samlassertion:AttributeStatement/samlassertion:Attribute');
        $result     = [];

        foreach ($attributes as $attribute) {
            $friendlyName = $attribute->attributes->getNamedItem('FriendlyName')->textContent;

            if ($attribute->hasChildNodes()) {
                foreach ($attribute->childNodes as $childNode) {
                    if ($childNode->nodeName == 'AttributeValue') {
                        if (isset($result[$friendlyName])) {
                            //we've already set this attribute
                            //check if it's an array or single value
                            if (is_array($result[$friendlyName])) {
                                //we've already got an array for this attribute, so add to the array
                                array_push($result[$friendlyName], $childNode->textContent);
                            } else {
                                //replace the existing single value with an array of the old value and the new one
                                $result[$friendlyName] = [$result[$friendlyName], $childNode->textContent];
                            }
                        } else {
                            //single attribute value
                            $result[$friendlyName] = $childNode->textContent;
                        }
                    }
                }
            }
        }

        return $result;
    }
}
