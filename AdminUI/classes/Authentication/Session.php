<?php
namespace FS\Authentication;

class Session
{
    public static function refresh()
    {
        session_start();

        if (isset($_SESSION['user'])) {
            $_SESSION['user'] = $_SESSION['user'];
        }

        session_write_close();

        return 'refreshed';
    }
}
