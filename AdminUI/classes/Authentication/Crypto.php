<?php

namespace FS\Authentication;

class Crypto
{
    private $key;
    private $iv;
    private $cipher;

    public function __construct($key, $iv, $cipher = 'AES') 
    {
        $cipher = empty($cipher) ? 'AES' : $cipher;

        // Supported ciphers
        switch ($cipher) {
            CASE 'AES':
                $this->cipher = 'AES-256-CBC';
                break;
            CASE 'RC2':
                $this->cipher = 'RC2-CBC';
                break;
            CASE '3DES':
                $this->cipher = 'DES-EDE3-CBC';
                break;
            DEFAULT:
                throw new \Exception('Error: Unsupported cipher ' . $cipher);
                break;
        }

        $this->key = $key;
        $this->iv  = $iv;
    }

    public function encrypt($plainText, $encode = false)
    {
        // Append 4 bytes to the beginning of the string specifying the length
        $length = strlen($plainText);
        $raw    = sprintf("%c%c%c%c", ($length & 0x000000FF), ($length & 0x0000FF00) >> 8, ($length & 0x00FF0000) >> 16, ($length & 0xFF000000) >> 24);
        $raw    = $raw . $plainText;

        // Do the encryption
        $encrypted = openssl_encrypt(
            $raw,
            $this->cipher,
            base64_decode($this->key),
            OPENSSL_RAW_DATA,
            base64_decode($this->iv)
        );

        return $encode ? base64_encode($encrypted) : $encrypted;
    }

    public function decrypt($cipherText, $encoded = false)
    {
        // Apply the reverse operations from the encode() function in reverse order
        if ($encoded) {
            $cipherText = base64_decode($cipherText, true);
            if ($cipherText === false) {
                throw new \Exception('Error: Encryption failure');
            }
        }

        // Do the decryption
        $raw = openssl_decrypt(
            $cipherText,
            $this->cipher,
            base64_decode($this->key),
            OPENSSL_RAW_DATA,
            base64_decode($this->iv)
        );

        // Strip string length off the beginning
        $length1   = ord(substr($raw, 0, 1));
        $length2   = ord(substr($raw, 1, 1));
        $length3   = ord(substr($raw, 2, 1));
        $length4   = ord(substr($raw, 3, 1));
        $length    = $length1 + ($length2 << 8) + ($length3 << 16) + ($length4 << 24);
        $plainText = substr($raw, 4);

        // Sanity check on string length
        if (strlen($plainText) != $length) {
            throw new \Exception('Error: String length does not match received data');
        }

        return $plainText;
    }
}
?>
