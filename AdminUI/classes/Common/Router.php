<?php

namespace FS\Common;

use FS\Log\AdminLog;
use FS\Common\Exception\OfflineException;

class Router
{
    private $groupPattern;
    private $routes = [];

    public function get($pattern, $callback)
    {
        $this->route($pattern, 'GET', $callback);
    }

    public function post($pattern, $callback)
    {
        $this->route($pattern, 'POST', $callback);
    }

    public function put($pattern, $callback)
    {
        $this->route($pattern, 'PUT', $callback);
    }

    public function route($pattern, $method, $callback)
    {
        if (!empty($this->groupPattern)) {
            $pattern = $this->groupPattern . $pattern;
        }

        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

        if (is_array($method)) {
            foreach ($method as $m) {
                $this->routes[$pattern][$m] = $callback;
            }
        } else if (is_string($method)) {
            $this->routes[$pattern][$method] = $callback;
        }
    }

    public function group($groupPattern, $callback)
    {
        $this->groupPattern = $groupPattern;

        ($callback->bindTo($this, '\FS\Common\Router'))();

        $this->groupPattern = '';
    }

    public function execute()
    {
        $url    = $_SERVER['PATH_INFO'];
        $method = $_SERVER['REQUEST_METHOD'];
        foreach ($this->routes as $pattern => $methods) {
            if (preg_match($pattern, $url, $params)) {
                if (isset($methods[$method])) {
                    array_shift($params);

                    try {
                        $response = call_user_func_array($methods[$method], array_values($params));

                        if (isset($response)) {
                            if (is_array($response) || is_object($response)) {
                                // Convert array to json string
                                header('content-type: application/json');
                                $response = json_encode($response);
                            }
                            exit($response);
                        }
                        throw new \Exception('No data received when querying API server');
                    } catch (OfflineException $e) {
                        exit(json_encode(['message' => htmlentities($e->getMessage()), 'status'=>'offline']));
                    } catch (\Exception $e) {
                        // Internal Server Error
                        exit(json_encode(['message' => htmlentities($e->getMessage()), 'status'=>'error']));
                    }
                } else {
                    // Method Not Allowed
                    exit(json_encode(['message' => '405 Method Not Allowed received when connecting to API server']));
                }
            }
        }
        // Bad Request, no matching path found
        exit(json_encode(['message' => '400 Bad Request received when connecting to API server']));
    }
}
