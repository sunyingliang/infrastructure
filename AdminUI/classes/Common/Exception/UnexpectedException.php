<?php

namespace FS\Common\Exception;

class UnexpectedException extends FSException
{
    public function __construct($message, $code = 3000, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
