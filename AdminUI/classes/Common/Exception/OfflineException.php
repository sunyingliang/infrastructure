<?php

namespace FS\Common\Exception;

class OfflineException extends FSException
{
    public function __construct($message, $code = 1900, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
