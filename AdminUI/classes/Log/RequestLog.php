<?php

namespace FS\Log;

use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\Services\ServiceBase;

class RequestLog extends ServiceBase
{
    private $pdo;

    function __construct($tab = '', $page = '')
    {
        $this->tab  = 'Logs';
        $this->page = 'Request';
        $this->pdo  = IO::getPDOConnection(ADMINUI_CONNECTION);
    }

    public function getLogs()
    {
        $this->checkUserPermission('read', $this->tab, $this->page);

        $data         = $this->getJsonBody();
        $fromDate     = $data->fromDate ?? null;
        $toDate       = $data->toDate ?? null;
        $endpoint     = $data->endpoint ?? null;
        $parameters   = $data->parameters ?? null;
        $tabPage      = $data->tab_page ?? null;
        $offset       = $data->offset ?? 0;
        $limit        = $data->limit ?? 30;
        $sort         = $data->sort ?? 'Id';
        $sortOrder    = $data->sortOrder ?? 'DESC';
        $searchString = '';
        $params       = [];

        if (!empty($fromDate)) {
            $searchString       = $this->appendSearch($searchString, 'datetime >= :fromDate');
            $params['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString     = $this->appendSearch($searchString, 'datetime <= :toDate');
            $params['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($tabPage) && $tabPage !== 'All') {
            $searchString       = $this->appendSearch($searchString, 'tab_page LIKE :tab_page');
            $params['tab_page'] = '%' . $tabPage . '%';
        }

        if (!empty($endpoint) && $endpoint !== 'All') {
            $searchString       = $this->appendSearch($searchString, 'endpoint LIKE :endpoint');
            $params['endpoint'] = '%' . $endpoint . '%';
        }

        if (!empty($parameters)) {
            $searchString         = $this->appendSearch($searchString, 'parameters LIKE :parameters');
            $params['parameters'] = '%' . $parameters . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM `request_audit_log` ' . $searchString);
        $stmt->execute($params);
        $rowCount = $stmt->fetchColumn(0);

        $sql              = "SELECT * FROM `request_audit_log` " . $searchString . " ORDER BY " . $sort . " " . $sortOrder . " LIMIT :offset, :limit";
        $params['offset'] = $offset;
        $params['limit']  = $limit;

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($params) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $result = $stmt->fetchAll();

        return [
            'response' => [
                'status' => 'success',
                'data'   => [
                    'totalRowCount' => $rowCount,
                    'rows'          => $result
                ]
            ]
        ];
    }

    public function getSummary()
    {
        $this->checkUserPermission('read', $this->tab, $this->page);

        $stmtRow = $this->pdo->query("SELECT COUNT(DISTINCT `endpoint`) FROM `request_audit_log`");

        if ($stmtRow === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $rowCount = $stmtRow->fetchColumn(0);

        $stmt = $this->pdo->query("SELECT `endpoint`, `execution_time`, MIN(execution_time) AS min, MAX(execution_time) AS max, AVG(execution_time) AS average FROM `request_audit_log` GROUP BY `endpoint`");

        if ($stmt === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $result = $stmt->fetchAll();

        return [
            'response' => [
                'status' => 'success',
                'data'   => [
                    'totalRowCount' => $rowCount,
                    'rows'          => $result
                ]
            ]
        ];
    }

    public function getEndpoint()
    {
        $this->checkUserPermission('read', $this->tab, $this->page);

        $response['response']['data'] = ['All'];

        $stmt = $this->pdo->query("SELECT `endpoint` FROM `request_audit_log` GROUP BY `endpoint`");

        if ($stmt === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        while ($row = $stmt->fetch()) {
            $response['response']['data'][] = $row['endpoint'];
        }

        $response['response']['status'] = 'success';

        return $response;
    }

    public function getTabPage()
    {
        $this->checkUserPermission('read', $this->tab, $this->page);

        $response['response']['data'] = ['All'];

        $stmt = $this->pdo->query("SELECT `tab_page` FROM `request_audit_log` GROUP BY `tab_page`");

        if ($stmt === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        while ($row = $stmt->fetch()) {
            $response['response']['data'][] = $row['tab_page'];
        }

        $response['response']['status'] = 'success';

        return $response;
    }
}