<?php

namespace FS\Log;

use FS\Services\ServiceBase;
use FS\Common\IO;

class AdminLog extends ServiceBase
{
    private $pdo;

    function __construct($tab = '', $page = '')
    {
        $this->tab  = $tab;
        $this->page = $page;
        $this->pdo = IO::getPDOConnection(ADMINUI_CONNECTION);
    }

    public function logMessage2DB($username, $action, $data)
    {
        try {
            $stmt = $this->pdo->prepare('INSERT INTO adminlogs(action,username,data) VALUES(:action,:username,:data)');
            $stmt->execute([
                'action'   => $action,
                'username' => $username,
                'data'     => $data
            ]);
        } catch (\Exception $e) {
        }
    }

    public function getAdminLogs()
    {
        $this->checkUserPermission('read', $this->tab, $this->page);

        $data         = $this->getJsonBody();
        $fromDate     = $data->fromDate ?? null;
        $toDate       = $data->toDate ?? null;
        $action       = $data->action ?? null;
        $username     = $data->username ?? null;
        $offset       = $data->offset ?? 0;
        $limit        = $data->limit ?? 30;
        $sort         = $data->sort ?? 'Id';
        $sortOrder    = $data->sortOrder ?? 'DESC';
        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'datetime >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }
        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'datetime <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }
        if (!empty($action) || $action == 'All') {
            $searchString         = $this->appendSearch($searchString, 'action LIKE :action');
            $parameters['action'] = '%' . $action . '%';
        }
        if (!empty($username) || $username == 'All') {
            $searchString           = $this->appendSearch($searchString, 'username LIKE :username');
            $parameters['username'] = '%' . $username . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM adminlogs ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql                  = "SELECT `datetime`, `action`, `username`, `data` FROM adminlogs " . $searchString  . " ORDER BY " . $sort ." ". $sortOrder . " LIMIT :offset, :limit";
        $parameters['offset'] = $offset;
        $parameters['limit']  = $limit;

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($parameters);
        $result = $stmt->fetchAll();

        return [
            'response' => [
                'status' => 'success',
                'data'   => [
                    'totalRowCount' => $rowCount,
                    'rows'          => $result
                ]
            ]
        ];
    }
}
