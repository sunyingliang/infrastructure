<?php

namespace FS\Services;

class CMSService extends ServiceBase
{
    function __construct($tab = '', $page = '')
    {
        $this->token    = CMS_SERVICE_TOKEN;
        $this->location = CMS_SERVICE_LOCATION;
        $this->tab      = $tab;
        $this->page     = $page;
    }
    public function getResult($query)
    {
        $this->checkPermission($query, ['list'], ['add', 'create', 'update', 'delete']);

        $response = $this->getResponse($query);

        if (isset($response['response'], $response['response']->status) && $response['response']->status == 'error') {
                $this->logMessage('CMSError', $response['response']->message);
            } else {
            $this->logToAuditLog($query, $response['input'], $response['response']);
        }

        return $response['response'];
    }
}
