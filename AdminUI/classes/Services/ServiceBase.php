<?php

namespace FS\Services;

use FS\Authentication\Login;
use FS\Log\AdminLog;
use FS\Common\Exception\OfflineException;
use FS\Common\Exception\UnexpectedException;
use FS\Common\IO;

abstract class ServiceBase
{
    public $token    = '';
    public $location = '';
    public $tab      = '';
    public $page     = '';

    function __construct()
    {
        $this->checkAuthenticated();
    }

    protected function checkAuthenticated()
    {
        if (!isset($_SESSION['user'])) {
            http_response_code(401);
            exit();
        }

        return true;
    }

    protected function checkPermission($query, $requiresRead, $requiresAdmin)
    {
        $queries = explode('/', $query);

        if (is_numeric(end($queries))) {
            array_pop($queries);
        }

        if (!is_null($requiresAdmin) && in_array(end($queries), $requiresAdmin)) {
            $this->checkUserPermission('admin', $this->tab, $this->page);
        } else if (!is_null($requiresRead) && in_array(end($queries), $requiresRead)) {
            $this->checkUserPermission('read', $this->tab, $this->page);
        } else {
            http_response_code(404);
            exit('Not Found');
        }
    }

    protected function checkUserPermission($permission, $tab, $page)
    {
        if (isset($_SESSION['user']['permissions'][$tab])) {
            foreach ($_SESSION['user']['permissions'][$tab] as $item) {
                if (strtolower($item['page']) == strtolower($page)) {
                    if ($item['permission'] == 'write' || $item['permission'] == $permission) {
                        return;
                    }
                }
            }
        }

        http_response_code(403);
        exit('Access denied');
    }

    protected function getJsonBody()
    {
        $rawData = file_get_contents("php://input");
        $data    = json_decode($rawData);

        if (isset($this->token) && !empty($this->token)) {
            if (!isset($data)) {
                $data = new \stdClass();
            }
            $data->token = $this->token;
        }

        return $data;
    }

    protected function parseURL($path)
    {
        if (isset($_SERVER['QUERY_STRING'])) {
            return $this->location . $path . '?' . $_SERVER['QUERY_STRING'];
        }

        return $this->location . $path;
    }

    protected function logError($action, \Exception $error)
    {
        $this->logMessage($action, $error->getMessage());
    }

    protected function logErrorToAudit($action, $errorMessage)
    {
        $this->logMessage($action, $errorMessage);
        throw new \Exception($errorMessage);
    }

    protected function logToAuditLog($path, $body, $response)
    {
        $logData = '';
        $action  = '';

        try {
            $id = substr($path, strrpos($path, '/') + 1);

            switch ($path) {
                // Log Global Admin
                case 'sitemaster/globaladmin/create':
                    $action  = 'GlobalAdminCreate';
                    $logData = [
                        'sitename'                              => $body->sitename,
                        'fullname'                              => $body->fullname,
                        'email'                                 => $body->email,
                        'Created Successful and password link:' => $response
                    ];
                    break;
                // Log collection
                case 'sitemaster/collections/create':
                    $action  = 'CollectionsCreate';
                    $logData = [
                        'DatabaseName' => $body->name,
                        'Status'       => $response
                    ];
                    break;
                case 'sitemaster/collections/update':
                    $action  = 'CollectionsUpdate';
                    $logData = [
                        'DatabaseName'  => $body->siteName,
                        'additionalIPs' => isset($body->additionalIPs) ? $body->additionalIPs : '',
                        'authSiteHosts' => isset($body->authSiteHosts) ? $body->authSiteHosts : '',
                        'anonSiteHosts' => isset($body->anonSiteHosts) ? $body->anonSiteHosts : ''
                    ];
                    break;
                // Log API Broker
                case 'apibroker/update':
                    $action  = 'APIBrokerUpdate';
                    $logData = [
                        'host'      => $body->host,
                        'newServer' => $body->newServer,
                        'oldValues' => $body->result
                    ];
                    break;
                // Log CMS
                case 'customer/create':
                case 'customer/update':
                    $action  = (strpos($path, 'add') !== false) ? 'CMSCustomerCreate' : 'CMSCustomerUpdate';
                    $logData = [
                        'name'               => $body->name,
                        'devBranch'          => isset($body->devBranch) ? $body->devBranch : '',
                        'stagingBranch'      => isset($body->stagingBranch) ? $body->stagingBranch : '',
                        'liveBranch'         => isset($body->liveBranch) ? $body->liveBranch : '',
                        'liveDomain'         => isset($body->liveDomain) ? $body->liveDomain : '',
                        'liveAliases'        => isset($body->liveAliases) ? $body->liveAliases : '',
                        'liveTunnelEndpoint' => isset($body->liveTunnelEndpoint) ? $body->liveTunnelEndpoint : '',
                        'liveTunnelIp'       => isset($body->liveTunnelIp) ? $body->liveTunnelIp : '',
                        'repository'         => isset($body->repository) ? $body->repository : '',
                        'directory'          => isset($body->liveBranch) ? $body->liveBranch : ''
                    ];
                    break;
                case 'customer/delete/' . $id:
                    $action  = 'CMSCustomerDelete';
                    $logData = [
                        'Deleted Id' => $id,
                        'Message'    => 'Success'
                    ];
                    break;
                // Log CronJob
                case 'cronjob/create':
                case 'cronjob/update':
                    $action  = (strpos($path, 'add') !== false) ? 'CronJobCreate' : 'CronJobUpdate';
                    $logData = [
                        'Name'           => $body->name,
                        'Frequency'      => $body->frequency,
                        'Command'        => $body->command,
                        'Comment'        => isset($body->comment) ? $body->comment : '',
                        'Concurrent'     => $body->concurrent,
                        'Execution Time' => $body->execution_time,
                        'Enabled'        => $body->enabled,
                        'Message'        => 'Success'
                    ];
                    break;
                case 'cronjob/delete':
                    $action  = 'CronJobDelete';
                    $logData = [
                        'Deleted Id' => $body->id,
                        'Message'    => 'Success'
                    ];
                    break;
                // Log Proxy
                case 'proxy/mapping/create':
                case 'proxy/mapping/update':
                    $action  = (strpos($path, 'create') !== false) ? 'ProxyMappingCreate' : 'ProxyMappingUpdate';
                    $logData = [
                        'Comment'       => $body->comment,
                        'Path'          => $body->path,
                        'Url'           => $body->url,
                        'Test Url'      => $body->test_url,
                        'Http Dwngrade' => $body->comment,
                        'Message'       => 'Success'
                    ];
                    break;
                case 'proxy/mapping/delete':
                    $action  = 'ProxyDelete';
                    $logData = [
                        'Deleted Path' => $body->path,
                        'Message'      => 'Success'
                    ];
                    break;
                //Log holidays
                case 'holiday/create':
                    $action  = 'HolidayCreate';
                    $logData = [
                        'Calendar' => $body->calendar,
                        'Holiday'  => $body->holiday,
                        'Notes'    => $body->notes,
                        'Response' => $response
                    ];
                    break;
                case 'holiday/edit':
                    $action  = 'HolidayUpdate';
                    $logData = [
                        'Holiday ID' => $body->id,
                        'Holiday'    => $body->holiday,
                        'Notes'      => $body->notes,
                        'Response'   => $response
                    ];
                    break;
                case 'holiday/delete':
                    $action  = 'HolidayDelete';
                    $logData = [
                        'Holiday ID' => $body->id,
                        'Response'   => $response
                    ];
                    break;
                case 'download/component/create':
                    $action         = 'DownloadCreate';
                    $logData = [
                        'name'              => $body->name,
                        'Release Notes URL' => $body->release_notes_url,
                        'Notes'             => isset($body->notes) ? $body->notes : '',
                        'Versions'          => isset($body->versions) ? $body->versions : '',
                    ];
                    $body->Response = $response;
                    break;
                case 'download/component/update':
                    $action         = 'DownloadUpdate';
                    $logData = [
                        'ID'                => $body->id,
                        'name'              => $body->name,
                        'Release Notes URL' => $body->release_notes_url,
                        'Notes'             => isset($body->notes) ? $body->notes : '',
                        'Versions'          => isset($body->versions) ? $body->versions : '',
                    ];
                    $body->Response = $response;
                    break;
                case 'solutions/customer/update':
                    $action  = 'SolutionCustomerUpdate';
                    $logData = [
                        'Customer ID'     => $body->id,
                        'Notes'           => isset($body->notes) ? $body->notes : '',
                        'Enabled' => isset($body->enabled) ? $body->enabled : '',
                        'Response'        => $response
                    ];
                    break;
                case 'rrcservice/add':
                    $action = 'RRCServiceCreate';
                    $logData = [
                        'Name' => $body->name,
                        'URL' => $body->url,
                        'Response' => $response
                    ];
                    break;
                case 'rrcservice/edit':
                    $action = 'RRCServiceUpdate';
                    $logData = [
                        'Name' => $body->name,
                        'URL' => $body->url,
                        'Response' => $response
                    ];
                    break;
                case 'rrcservice/delete':
                    $action = 'RRCServiceDelete';
                    $logData = [
                        'ID' => $body->id,
                        'Name' => $body->name,
                        'URL' => $body->url,
                        'Response' => $response
                    ];
                    break;
                default:
                    return;
            }

            $this->logMessage($action, json_encode($logData));
        } catch (\Exception $e) {
            $logData['Status'] = $e->getMessage();
            $this->logMessage($action, json_encode($logData));
            throw $e;
        }
    }

    protected function logMessage($action, $message)
    {
        $username = $_SESSION['user']['displayname'];
        $adminLog = new AdminLog();
        $adminLog->logMessage2DB($username, $action, $message);
    }

    protected function convertJSONToForm($json)
    {
        if (empty($json)) {
            return $json;
        }

        $form = [];

        foreach ($json as $name => $value) {
            if (is_array($value) || is_object($value)) {
                continue;
            }
            $form[] = $name . '=' . urlencode($value);
        }

        return implode('&', $form);
    }

    // Log requests
    protected function logRequest ($query, $executionTime)
    {
        $parameters = (array)$this->getJsonBody();
        unset($parameters['token']);

        $pdo  = IO::getPDOConnection(ADMINUI_CONNECTION);
        $sql  = "INSERT INTO `request_audit_log` (`tab_page`, `endpoint`, `parameters`, `execution_time`) VALUES(:tab_page, :endpoint, :parameters, :execution_time)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'tab_page'       => $this->tab . "/" . $this->page,
            'endpoint'       => $query,
            'parameters'     => json_encode($parameters),
            'execution_time' => $executionTime
        ]);
    }

    public function getResponse($query, $asJSON = false)
    {
        $startTime     = microtime(true);
        $url           = $this->parseURL($query);
        $input         = $this->getJsonBody();
        $body          = $asJSON ? $input : $this->convertJSONToForm($input);
        $response      = $this->parseCURLRequest($_SERVER['REQUEST_METHOD'], $url, $body);
        $endTime       = microtime(true);
        $executionTime = $endTime - $startTime;

        $this->logRequest($query, $executionTime);

        return $responseArr = [
            'input' => $input,
            'response' => $response
        ];
    }

    protected function parseCURLRequest($method, $url, $body = null)
    {
        // Initialise CURL connection
        $ch = curl_init($url);

        // Set CURL options
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if (!empty($body)) {
            if (!is_string($body)) {
                $body = json_encode($body);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            }

            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Special extended timeout for some endpoints
        if (strpos($url, 'solutions/customer/create') !== false) {
            ini_set('max_execution_time', CURL_TIMEOUT_SOLUTIONS);
            curl_setopt($ch, CURLOPT_TIMEOUT, CURL_TIMEOUT_SOLUTIONS);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURL_TIMEOUT_SOLUTIONS);
        } else {
            curl_setopt($ch, CURLOPT_TIMEOUT, CURL_TIMEOUT);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURL_TIMEOUT);
        }

        // Execute and fetch CURL response
        $curl_response = curl_exec($ch);
        $response      = json_decode($curl_response);

        $status       = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error_number = curl_errno($ch);
        $endpoint     = strpos($url, '?') ? substr($url, 0, strpos($url, '?')) : $url;

        // Close CURL request
        curl_close($ch);

        // check if http referer includes query string 'offline=true',
        // if true set curl error to [CURLE_OPERATION_TIMEOUTED], enable offline mode
        if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'offline=true') !== false) {
            $error_number = CURLE_OPERATION_TIMEOUTED;
            $curl_response = 'Sudo Offline Mode';
        }

        $debug = '';

        if (defined('DEBUG_MODE') && DEBUG_MODE === true) {
            $debug = ' DEBUG: URL: "' . $url . '", Response: "' . $curl_response . '", Status: "' . $status . '", CURLError: "' . $error_number . '", GET: "' . var_export($_GET, true) . '", POST: "' . var_export($_POST, true) . "'";
        } else if (isset($response->response) && $response->response->status == 'error') {
            if (!empty($response->response->data)) {
                throw new \Exception($response->response->data);
            }
        }

        // Offline & decode related exceptions
        try {
            if (in_array($error_number, [CURLE_OPERATION_TIMEOUTED]) || in_array($error_number, [CURLE_COULDNT_CONNECT])) {
                throw new OfflineException('Timeout when connecting to ' . $endpoint . PHP_EOL . $debug);
            } else if ($status == '400') {
                throw new UnexpectedException('Unexpected ' . $status . ' received from ' . $endpoint . ', please check request' . PHP_EOL . $debug);
            } else if ($status == '401') {
                throw new UnexpectedException('Unexpected ' . $status . ' received from ' . $endpoint . ', invalid token' . PHP_EOL . $debug);
            } else if ($status == '403') {
                throw new UnexpectedException('Unexpected ' . $status . ' received from ' . $endpoint . ', please check token has appropriate permissions' . PHP_EOL . $debug);
            } else if ($status == '404') {
                throw new UnexpectedException('Unexpected ' . $status . ' received from ' . $endpoint . ', please check it is correct' . PHP_EOL . $debug);
            } else if ($status == '500') {
                throw new UnexpectedException('Unexpected ' . $status . ' received from ' . $endpoint . ', please check server logs' . PHP_EOL . $debug);
            } else if (json_last_error() !== JSON_ERROR_NONE) {
                throw new UnexpectedException('Unrecognised response from ' . $endpoint . ', could not decode' . PHP_EOL . $debug);
            } else if ($status == 0) {
                throw new OfflineException('Could not open connection to ' . $endpoint . PHP_EOL . $debug);
            } else if (empty($curl_response)) {
                throw new OfflineException('No valid response from ' . $endpoint . PHP_EOL . $debug);
            }

        } catch (UnexpectedException $e) {
            $response          = new \stdClass();
            $response->message = $e->getMessage();
            $response->status  = 'error';
        }

        return $response;
    }

    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') === false ? ' WHERE ' : ' AND ') . $query;
    }
}
