<?php

namespace FS\Services;

class SolutionsAdminService extends ServiceBase
{
    function __construct($tab = '', $page = '')
    {
        $this->token    = SOLUTIONS_ADMIN_SERVICE_TOKEN;
        $this->location = SOLUTIONS_ADMIN_SERVICE_LOCATION;
        $this->tab      = $tab;
        $this->page     = $page;
    }

    public function getResult($query)
    {
        $this->checkPermission($query, ['list', 'calendar', 'masters', 'list-year', 'list-calendar', 'customer', 'list-feature'], ['create', 'add', 'edit', 'update', 'delete', 'update-enabled', 'test', 'sync']);

        // Set curl timeout
        if (strpos($query, '/create') !== false || strpos($query, '/sync') !== false) {
            set_time_limit(CURL_TIMEOUT_SOLUTIONS);
        }

        $response = $this->getResponse($query, true);

        $this->logToAuditLog($query, $response['input'], $response['response']);

        return $response['response'];
    }
}
