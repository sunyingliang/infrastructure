<?php

namespace FS\Services;

class InfrastructureService extends ServiceBase
{
    function __construct($tab = 'Logs', $page = '')
    {
        $this->token    = INFRASTRUCTURE_SERVICE_TOKEN;
        $this->location = INFRASTRUCTURE_SERVICE_LOCATION;
        $this->tab      = $tab;
        $this->page     = $page;
    }

    // Get background log
    public function getBackgroundLogs($query)
    {
        $this->checkPermission($query, ['list', 'site', 'server', 'operation'], []);

        $response = $this->getResponse($query);

        return $response['response'];
    }

    public function getLogs($query)
    {
        $queries = explode('/', $query);
        end($queries);
        $this->page = preg_replace('/(?<!\ )[A-Z]/', ' $0', prev($queries));

        switch ($this->page) {
            case 'cron':
                $this->page = 'Crontab';
                break;
            case 'agilisys':
                $this->page = 'Agilisys Portal';
                break;
            case 'ftp':
                $this->page = 'FTP';
                break;
            case 'email':
                if (strpos($_SERVER['HTTP_REFERER'], 'emailsentlog') !== false) {
                    $this->page = 'Email Sent';
                } else if (strpos($_SERVER['HTTP_REFERER'], 'emailfailedlog') !== false) {
                    $this->page = 'Email Failed';
                }
                break;
        }

        $this->checkUserPermission('read', $this->tab, $this->page);

        $response = $this->getResponse($query);

        if (isset($response['response'], $response['response']->status) && $response['response']->status == 'error') {
            if (isset($response['response']->message)) {
                $this->logErrorToAudit('LogsError', $response['response']->message);
            }
        }

        return $response['response'];
    }

    // Get global admin list, create new global admin
    public function mapGlobalAdminPath($query)
    {
        $this->checkPermission($query, ['list'], ['create', 'update']);

        $fullname = '';
        $email    = '';
        $response = $this->getResponse($query);

        if (isset($_SESSION['user'])) {
            $fullname = $_SESSION['user']['displayname'];
            $email    = $_SESSION['user']['mail'];
        }

        $this->logToAuditLog($query, $response['input'], $response['response']->response->data);

        $response['response']->fullname = $fullname;
        $response['response']->email    = $email;

        return $response['response'];
    }

    // Create and update collection sites
    public function getCollectionsResult($query)
    {
        $this->checkPermission($query, ['list'], ['create', 'update', 'delete']);

        $response = $this->getResponse($query);

        $this->logToAuditLog($query, $response['input'], $response['response']);

        return $response['response'];
    }

    // Crontab service
    public function getCronResult($query)
    {
        $this->checkPermission($query, ['list', 'period'], ['add', 'create', 'update', 'delete']);

        $response = $this->getResponse($query);

        if (isset($response['response']->response, $response['response']->response->status) && $response['response']->response->status == 'error') {
            if (isset($response['response']->message)) {
                $this->logErrorToAudit('CronjobError', $response['response']->message);
            }
        } else {
            $this->logToAuditLog($query, $response['input'], $response['response']);
        }

        return $response['response'];
    }

    // Download service
    public function getDownloadResult($query)
    {
        $this->checkPermission($query, ['list', 'version'], ['create', 'update', 'delete']);

        $response = $this->getResponse($query, true);

        if (isset($response['response']->status) && $response['response']->status == 'error') {
            if (isset($response['response']->message)) {
                $this->logErrorToAudit('DownloadComponentError', $response['response']->message);
            }
        } else {
            $this->logToAuditLog($query, $response['input'], $response['response']);
        }

        return $response['response'];
    }

    // LIM Service
    public function getLIMResult($query)
    {
        $this->checkPermission($query, ['list', 'resultcodes'], []);

        $response = $this->getResponse($query);

        $this->logToAuditLog($query, $response['input'], $response['response']);

        return $response['response'];
    }

    // Proxy Service
    public function getProxyResult($query)
    {
        $this->checkPermission($query, ['list', 'search'], ['create', 'update', 'delete']);

        $response = $this->getResponse($query);

        if (isset($response['response'], $response['response']->status) && $response['response']->status == 'error') {
            if (isset($response['response']->message)) {
                $this->logErrorToAudit('ProxyError', $response['response']->message);
            }
        } else {
            $this->logToAuditLog($query, $response['input'], $response['response']);
        }

        return $response['response'];
    }

    // Tracking Service
    public function getTrackingResult($query)
    {
        $this->checkPermission($query, ['list', 'report', 'log'], []);

        $response = $this->getResponse($query);

        $this->logToAuditLog($query, $response['input'], $response['response']);

        return $response['response'];
    }

    // API Broker Service
    public function mapAPIBrokerPath($query)
    {
        $this->checkPermission($query, ['list', 'host', 'customer', 'listbyhost', 'environment'],
            ['process', 'update']);

        $response = $this->getResponse($query);

        $this->logToAuditLog($query, $response['input'], $response['response']);

        return $response['response'];
    }

    // RRC Service
    public function getRRCResult($query)
    {
        $this->checkPermission($query, ['list', 'check'], ['add', 'edit', 'delete', 'check']);

        $response = $this->getResponse($query);

        if (isset($response['response'], $response['response']->status) && $response['response']->status == 'error') {
            if (isset($response['response']->message)) {
                $this->logErrorToAudit('RRCError', $response['response']->message);
            }
        } else {
            $this->logToAuditLog($query, $response['input'], $response['response']);
        }

        return $response['response'];
    }

    // Monitoring Service
    public function getMonitoringResult($query)
    {
        $url   = $this->parseURL($query);
        $input = (array)$this->getJsonBody();
        $input = array_merge($input, $_POST);

        return $this->parseCURLRequest($_SERVER['REQUEST_METHOD'], $url, $input);
    }
}
