const gulp = require('gulp');
const concat = require('gulp-concat');
const closureCompiler = require('google-closure-compiler-js').gulp();

var files = [
    './public/app.js',
    './public/template/logController.js',
    './public/template/collections/collections.js',
    './public/template/cronlog/cronlog.js',
    './public/template/cronjob/cronjob.js',
    './public/template/globaladmin/globaladmin.js',
    './public/template/reverseproxy/reverseproxy.js',
    './public/template/statistics/statistics.js',
    './public/template/cms/cms.js',
    './public/template/apibroker/apibroker.js',
    './public/template/apicustomer/apicustomer.js',
    './public/template/holidays/holidays.js',
    './public/template/customer/customer.js',
    './public/template/download/download.js',
    './public/template/rrcservice/rrcservice.js'
];

gulp.task('default', ['watch']);

gulp.task('watch', function () {
    gulp.watch(files, ['concat-compile']);
});

gulp.task('concat-compile', function () {
    return gulp.src(files)
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./public/'))
        .pipe(closureCompiler({
            compilation_level: 'SIMPLE',
            js_output_file: 'main.min.js'
        }))
        .pipe(gulp.dest('./public/'))
});