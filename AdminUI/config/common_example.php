<?php

// FAM Settings
define('FAM_KEY', '');
define('FAM_IV', '');
define('FAM_CIPHER', 'AES');
define('FAM_URL', 'http://');
define('TOKEN_OVERRIDE_MONITORING', 'example');
define('TOKEN_OVERRIDE_READ', 'example');
define('TOKEN_OVERRIDE_ADMIN', 'example');

// DB Settings
define('ADMINUI_CONNECTION', [
    'dns'       => 'example',
    'username'  => 'example',
    'password'  => 'example'
]);

// Service Settings
define('CMS_SERVICE_LOCATION', 'https://your-domain/api/');
define('CMS_SERVICE_TOKEN', 'token');
define('INFRASTRUCTURE_SERVICE_LOCATION', 'https://your-domain/api/');
define('INFRASTRUCTURE_SERVICE_TOKEN', 'token');
define('SOLUTIONS_ADMIN_SERVICE_LOCATION', 'https://your-domain/api/');
define('SOLUTIONS_ADMIN_SERVICE_TOKEN', 'token');

// Define environment constants
define('ENVIRONMENT', 'test');
define('ENVIRONMENT_COLOUR', '#5cb85c');

// Define debug [true|false]
define('DEBUG_MODE', false);

// Migration of monitoring
define('DEFAULT_REFRESH_INTERVAL', 60);

// Define CURL TIMEOUT time
define('CURL_TIMEOUT', 180);
define('CURL_TIMEOUT_SOLUTIONS', 3600);

//Define domain
define('DOMAIN', 'example.com');
