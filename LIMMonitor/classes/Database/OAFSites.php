<?php

namespace FS\Database;

use FS\Common\IO;

class OAFSites
{
    public function getLIMs()
    {
        $limResults = [];

        if ($validation = IO::checkDBConstant('DATABASE_CONNECTION', 'oaf_master')) {
            $pdoMaster = IO::getPDOConnection(DATABASE_CONNECTION['oaf_master']);

            $stmtMaster  = $pdoMaster->query('SELECT host, site_connection FROM site');
            $connections = $stmtMaster->fetchAll();

            if ($connections) {
                foreach ($connections as $connection) {
                    try {
                        $pdoSite  = $this->pdoConnection($connection['site_connection']);
                        $stmtSite = $pdoSite->query("SELECT xml FROM objects WHERE name = 'LIM'");
                        $xml      = $stmtSite->fetchColumn(0);

                        if ($xml !== false) {

                            if (strpos($xml, 'MultiLIM') === false) {
                                $xml = '<MultiLIM>' . $xml . '</MultiLIM>';
                            }

                            $xmlReader = new \XMLReader();
                            $xmlReader->xml($xml);

                            while ($xmlReader->read()) {
                                if ($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->name == 'LIM') {
                                    $server = $xmlReader->getAttribute('server');
                                    $port   = $xmlReader->getAttribute('port');
                                    if (!empty($port) && $port != 80) {
                                        $server .= ':' . $port;
                                    }
                                    $path = $xmlReader->getAttribute('path');
                                    if ($server != 'nohost' && $path != '/bad_path') {
                                        $limResults[] = [
                                            'LIMName'      => $xmlReader->getAttribute('name'),
                                            'URL'          => $server . $path,
                                            'Key'          => $xmlReader->getAttribute('key'),
                                            'IV'           => $xmlReader->getAttribute('iv'),
                                            'Encryption'   => $xmlReader->getAttribute('enctype'),
                                            'CustomerName' => $connection['host'],
                                            'Form'         => 'OAF'
                                        ];
                                    }

                                }
                            }

                            $xmlReader->close();
                        }
                    } catch (\Exception $e) {
                        // Do nothing
                    }
                }
            }
        }

        return $limResults;
    }

    // Set PDO connection
    private function pdoConnection($conString)
    {
        $parts    = explode(';', $conString);
        $dns      = 'sqlsrv:' . $parts[0] . ';' . $parts[1] . ';ConnectionPooling= 0';
        $username = explode('=', $parts[2])[1];
        $password = explode('=', $parts[3])[1];
        $pdo      = new \PDO($dns, $username, $password);

        return $pdo;
    }
}
