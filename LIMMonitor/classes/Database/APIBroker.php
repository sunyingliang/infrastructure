<?php

namespace FS\Database;

use FS\Common\IO;

class APIBroker
{
    public function getLIMs()
    {
        $limResults = [];

        if ($validation = IO::checkDBConstant('DATABASE_CONNECTION', 'api_broker')) {
            foreach (DATABASE_CONNECTION['api_broker'] as $conn) {
                try {
                    $pdoMaster = new \PDO($conn['dns'], $conn['username'], $conn['password']);
                    $pdoMaster->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
                    $stmtMaster  = $pdoMaster->query('SELECT `Name`,`DB_Host`,`DB_Username`,`DB_Password`,`DB_Name` FROM `customers`');
                    $connections = $stmtMaster->fetchAll();
                    foreach ($connections as $connection) {
                        try {
                            $pdoSite  = $this->pdoConnection($connection);
                            $stmtSite = $pdoSite->query("SELECT `Name`,`URL`,`Key`,`IV`,`Encryption` FROM `lims` WHERE `URL` NOT LIKE '%shared-lim.ec2.firmstep.com%'");
                            if ($stmtSite) {
                                while ($row = $stmtSite->fetch()) {
                                    if ($row['URL'] != '#LIM_URL#' && !empty($row['URL'])) {
                                        $limResults[] = [
                                            'LIMName'      => $row['Name'],
                                            'URL'          => $row['URL'],
                                            'Key'          => $row['Key'],
                                            'IV'           => $row['IV'],
                                            'Encryption'   => $row['Encryption'],
                                            'CustomerName' => $connection['Name'],
                                            'Form'         => 'Forms'
                                        ];
                                    }
                                }
                            }
                        } catch (\Exception $e) {
                            // Do nothing
                        }
                    }
                } catch (\Exception $e) {
                    // Do nothing
                }
            }
        }

        return $limResults;
    }

    // Set PDO connection
    private function pdoConnection($conResult)
    {
        $dns = 'mysql:host=' . $conResult['DB_Host'] . ';dbname=' . $conResult['DB_Name'];
        $pdo = new \PDO($dns, $conResult['DB_Username'], $conResult['DB_Password']);
        $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

        return $pdo;
    }
}
