<?php

namespace FS\LIM;

use FS\Exception\WarningException;

class FirmstepLIM
{
    var $url;
    var $key;
    var $iv;
    var $cipher;

    function __construct($url, $key, $iv, $cipher = 'AES')
    {
        $cipher     = empty($cipher) ? 'AES' : $cipher;
        $cipher_map = [
            'AES'  => 'AES-256-CBC',
            'RC2'  => 'RC2-CBC',
            '3DES' => 'DES-EDE3-CBC'
        ];

        $this->url    = $url;
        $this->key    = $key;
        $this->iv     = $iv;
        $this->cipher = $cipher_map[strtoupper($cipher)];
    }

    function run($request)
    {
        $response = '';

        $this->catchWarnings();

        try {
            $cipherText = $this->encode($request);

            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $cipherText);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/xml; charset=UTF-8']);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);

            $full_response = curl_exec($ch);
            $status        = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $error         = curl_error($ch);
            curl_close($ch);

            $response = $this->decode($full_response);
        } catch (WarningException $we) {
            $this->releaseWarnings();
            throw new \Exception($we->getMessage());
        }

        $this->releaseWarnings();

        return $response;
    }

    private function encode($plain_text)
    {
        //append 4 bytes to the beginning of the string specifying the length
        $length     = strlen($plain_text);
        $raw_string = sprintf("%c%c%c%c", ($length & 0x000000FF), ($length & 0x0000FF00) >> 8, ($length & 0x00FF0000) >> 16, ($length & 0xFF000000) >> 24);
        $raw_string = $raw_string . $plain_text;

        //do the encryption
        $encrypted = openssl_encrypt($raw_string, $this->cipher, base64_decode($this->key), OPENSSL_RAW_DATA, base64_decode($this->iv));

        return $encrypted;

    }

    private function decode($cipher_text)
    {
        //OK, apply the reverse operations from the encode() function in reverse order:

        //do the decryption
        $raw_string = openssl_decrypt($cipher_text, $this->cipher, base64_decode($this->key), OPENSSL_RAW_DATA, base64_decode($this->iv));

        //strip string length off the beginning
        $length_1   = ord(substr($raw_string, 0, 1));
        $length_2   = ord(substr($raw_string, 1, 1));
        $length_3   = ord(substr($raw_string, 2, 1));
        $length_4   = ord(substr($raw_string, 3, 1));
        $length     = $length_1 + ($length_2 << 8) + ($length_3 << 16) + ($length_4 << 24);
        $plain_text = substr($raw_string, 4);

        //sanity check on string length
        if (strlen($plain_text) != $length) {
            throw new \Exception('String length does not match received data.');
        }

        return $plain_text;
    }

    private function catchWarnings()
    {
        set_error_handler([$this, 'warningHandler'], E_ALL);
    }

    private function warningHandler(int $errNo, string $errStr, string $errFile, string $errLine)
    {
        if (!(error_reporting() & $errNo)) {
            return false;
        }

        if ($errNo === E_WARNING) {
            throw new WarningException($errStr);
        }
    }

    private function releaseWarnings()
    {
        restore_error_handler();
    }
}
