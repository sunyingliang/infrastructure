<?php

namespace FS\LIM;

use FS\Common\IO;

class LIMTest
{
    private static $pdo = null;

    public static function testLIM(array &$limArr)
    {
        for ($i = 0; $i < count($limArr); $i++) {
            $limArr[$i]['Version']   = null;
            $limArr[$i]['BuildDate'] = null;
            $limArr[$i]['Framework'] = null;
            $limArr[$i]['Software']  = null;
            $limArr[$i]['Runtime']   = null;
            $limArr[$i]['Result']    = 0;
            $limArr[$i]['Response']  = null;

            if (strpos($limArr[$i]['URL'], 'http') !== 0) {
                $limArr[$i]['URL'] = 'http://' . $limArr[$i]['URL'];
            }

            $lim  = new FirmstepLIM($limArr[$i]['URL'], $limArr[$i]['Key'], $limArr[$i]['IV'], $limArr[$i]['Encryption']);
            $body = '<Requests><Request provider="test" id="someuniqueid"><Test/></Request></Requests>';

            try {
                $response = $lim->run($body);
                self::parseResult($response, $limArr[$i]);

                if (strpos($limArr[$i]['URL'], '://lim.ec2.firmstep.com') !== false) {
                    $limProxy = new FirmstepLIM(str_replace('://lim.ec2.firmstep.com', '://lim-2.ec2.firmstep.com', $limArr[$i]['URL']), $limArr[$i]['Key'], $limArr[$i]['IV'], $limArr[$i]['Encryption']);
                    // Add to state of original result using binary conversion to achieve corrected secondary state
                    $limArr[$i]['Result'] = $limArr[$i]['Result'] * 2 + 2;

                    $response = $limProxy->run($body);
                    self::parseResult($response, $limArr[$i]);
                }
            } catch (\Exception $e) {
                $limArr[$i]['Response'] = $e->getMessage();
            }
        }
    }

    public static function parseResult(&$response, array &$limResult)
    {
        if (strpos($response, '<Success>') !== false) {
            $xmlReader = new \XMLReader();
            $xmlReader->xml($response);
            $version   = null;
            $buildDate = null;
            $framework = null;
            $software  = null;
            $runtime   = null;

            while ($xmlReader->read()) {
                if ($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->name == 'LIMBuild') {
                    $version   = $xmlReader->getAttribute('version');
                    $buildDate = (new \DateTime($xmlReader->getAttribute('builddate')))->format(\DateTime::RFC3339);
                    $framework = $xmlReader->getAttribute('framework');
                } else if ($xmlReader->nodeType == \XMLReader::ELEMENT && $xmlReader->name == 'Server') {
                    $software = $xmlReader->getAttribute('software');
                    $runtime  = $xmlReader->getAttribute('runtime');
                }
            }

            $xmlReader->close();
            $limResult['Version']   = $version;
            $limResult['BuildDate'] = $buildDate;
            $limResult['Framework'] = $framework;
            $limResult['Software']  = $software;
            $limResult['Runtime']   = $runtime;
            $limResult['Result']    += 1;
        } else if (strpos($response, 'No such [test] provider') !== false) {
            $limResult['Result'] += 1;
        }
    }

    public static function persistLIM(array &$limArr)
    {
        if (is_null(self::$pdo)) {
            if (!$validation = IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure')) {
                throw new \Exception('Error: PDO connection is not configured properly');
            }

            self::$pdo = IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);;
        }

        if (self::$pdo->exec('TRUNCATE TABLE `lim_monitoring`') === false) {
            throw new \PDOException('Error: Failed to truncate table `lim_monitoring`');
        }

        // Begin transaction to make PDO object pool INTERTS to make single database call
        self::$pdo->beginTransaction();

        $markArr  = [];
        $valueArr = [];

        for ($i = 0; $i < count($limArr); $i++) {
            $markArr[]  = '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $valueArr[] = $limArr[$i]['LIMName'];
            $valueArr[] = $limArr[$i]['URL'];
            $valueArr[] = $limArr[$i]['CustomerName'];
            $valueArr[] = $limArr[$i]['Result'];
            $valueArr[] = $limArr[$i]['Version'];
            $valueArr[] = $limArr[$i]['BuildDate'];
            $valueArr[] = $limArr[$i]['Framework'];
            $valueArr[] = $limArr[$i]['Software'];
            $valueArr[] = $limArr[$i]['Runtime'];
            $valueArr[] = $limArr[$i]['Form'];
            $valueArr[] = $limArr[$i]['Response'];
            $limArr[$i] = null;
        }

        $sql  = 'INSERT INTO `lim_monitoring` (`LIMName`, `URL`, `CustomerName`, `Result`, `Version`, `BuildDate`, `Framework`, `Software`, `Runtime`,`Form`, `Response`) VALUES ' . implode(',', $markArr);
        $stmt = self::$pdo->prepare($sql);

        $stmt->execute($valueArr);

        self::$pdo->commit();
    }
}
