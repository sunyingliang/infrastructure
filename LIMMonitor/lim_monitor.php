<?php

require 'Autoload.php';
require 'config/common.php';

use FS\Database\APIBroker;
use FS\Database\OAFSites;
use FS\LIM\LIMTest;

$apiResults = (new APIBroker())->getLIMs();
$oafResults = (new OAFSites())->getLIMs();
$limList    = array_merge($apiResults, $oafResults);

try {
    LIMTest::testLIM($limList);
    LIMTest::persistLIM($limList);
} catch (\Exception $e) {
    echo 'Error: ' . $e->getMessage() . PHP_EOL;
}
