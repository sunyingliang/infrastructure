<?php

// Database configuration
define('INFRASTRUCTURE_CONNECTION', [
    'dns'      => 'mysql:host=example;dbname=example',
    'username' => 'example',
    'password' => 'example'
]);

define('API_BROKER_CONNECTIONS', [
    [
        'dns'      => 'mysql:host=example;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    [
        'dns'      => 'mysql:host=example;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ]
]);

define('OAF_MASTER_CONNECTION', [
    'dns'      => 'mysql:host=example;dbname=example',
    'username' => 'example',
    'password' => 'example'
]);
