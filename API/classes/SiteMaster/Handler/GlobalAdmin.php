<?php

namespace FS\SiteMaster\Handler;

use FS\Common\Exception\PDOQueryException;
use FS\Common\IO;

class GlobalAdmin extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['oaf_master']);
    }

    public function adminSiteList()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = [];
        $stmt                      = $this->pdo->query("SELECT host FROM site ORDER BY host");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['host'];
        }

        return $this->responseArr;
    }

    public function createGlobalAdminAccount()
    {
        $this->auth->hasPermission(['write']);

        $siteName   = IO::default($this->data, 'sitename');
        $fullName   = IO::default($this->data, 'fullname');
        $email      = IO::default($this->data, 'email');
        $authString = '';
        $siteString = '';


        // Gets connection strings for auth and site databases
        $stmt = $this->pdo->prepare("SELECT TOP 1 site_connection, auth_connection, background_event_host FROM site WHERE host = :siteName");
        $stmt->execute(['siteName' => $siteName]);

        $row = $stmt->fetch();

        if ($row) {
            $siteString = $row['site_connection'];
            $authString = $row['auth_connection'];
        } else {
            throw new PDOQueryException('Could not find a site "' . $siteName . '"');
        }

        // To get the Id of the user created in auth table which will be used to set other permissions
        $nucleus_id = $this->registerUser($authString, $fullName, $email);

        // Create weblogin account
        $passwordResetCode = $this->makeWebLogin($authString, $nucleus_id, $email);

        // Set global admin permissions
        $this->setGlobalAdminPermission($siteString, $nucleus_id);

        $fullSiteURL = $siteName;

        if (!empty($row['background_event_host'])) {
            $fullSiteURL = $row['background_event_host'];
        } else {
            // Strip [.-] etc bits from the siteName
            if (strpos($fullSiteURL, '[') !== false) {
                $fullSiteURL = substr($fullSiteURL, 0, strpos($fullSiteURL, '['));
            }

            if (strpos($fullSiteURL, '%') !== false) {
                $fullSiteURL = substr($fullSiteURL, 0, strpos($fullSiteURL, '%'));
            }

            // Remove trailing spaces
            while (substr($fullSiteURL, -1) == '.') {
                $fullSiteURL = substr($fullSiteURL, 0, -1);
            }

            // If we don't have a fully qualified hostname, assume the *.firmstep.com entry will work
            if (strpos($fullSiteURL, '.') === false) {
                $fullSiteURL = $fullSiteURL . HOST_SUFFIX['oaf_global_admin'];
            }
        }

        $resetLink = 'http://' . $fullSiteURL . '/LogIn/SetPassword.aspx?ID=' . $passwordResetCode;

        $this->responseArr['data'] = $resetLink;

        return $this->responseArr;
    }

    // Sets the user full name and email in auth table and return the id created which is used to set other permissions
    private function registerUser($authString, $displayName, $email)
    {
        $this->auth->hasPermission(['write']);

        $pdo  = $this->pdoConnection($authString);
        $stmt = $pdo->prepare("SELECT id FROM user_nucleus WHERE email = :email");

        $stmt->execute(['email' => $email]);

        $id = $stmt->fetchColumn(0);

        if (empty($id)) {
            $stmt = $pdo->prepare('INSERT INTO user_nucleus(created, display_name, email,alias) OUTPUT INSERTED.id VALUES(getdate(),:displayName,:email,\'\')');
            $stmt->execute(['displayName' => $displayName, 'email' => $email]);
            $id = $stmt->fetchColumn(0);
        }

        return $id;
    }

    // Sets the password salt and hash for the Id provided
    private function makeWebLogin($authString, $nucleus_id, $email)
    {
        $this->auth->hasPermission(['write']);

        $passwordResetCode = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(0, 65535));
        $pdo               = $this->pdoConnection($authString);
        $stmt              = $pdo->prepare('SELECT id FROM basic_auth_login WHERE user_name =:email');

        $stmt->execute(['email' => $email]);

        $id = $stmt->fetchColumn(0);

        if (empty($id)) {
            $stmt = $pdo->prepare('INSERT INTO basic_auth_login(newpassword_code,[enabled],pw_hash,salt,[user_name],password_reset_date, date_updated) OUTPUT INSERTED.id VALUES(:newCode,1,\'\', \'\' ,:email, GetDate(), GetDate())');
            $stmt->execute([
                'newCode' => $passwordResetCode,
                'email'   => $email
            ]);
        } else {
            $stmt = $pdo->prepare('UPDATE basic_auth_login SET newpassword_code=:newCode, password_reset_date=GetDate() WHERE user_name = :email');
            $stmt->execute([
                'newCode' => $passwordResetCode,
                'email'   => $email
            ]);
        }

        $stmt = $pdo->prepare("SELECT attachment_id FROM external_id_attachment WHERE nucleus_id = :nucleus_id");
        $stmt->execute(['nucleus_id' => $nucleus_id]);
        $attachment_id = $stmt->fetchColumn(0);

        if (empty($id)) {
            $stmt = $pdo->prepare('INSERT INTO external_id_attachment(external_id,nucleus_id,source_name) VALUES(:email,:nucleus_id,\'BASIC\') ');
            $stmt->execute(['email' => $email, 'nucleus_id' => $nucleus_id]);
        } else {
            $stmt = $pdo->prepare('UPDATE external_id_attachment SET external_id=:email WHERE attachment_id=:attachment_id');
            $stmt->execute([
                'email'         => $email,
                'attachment_id' => $attachment_id
            ]);
        }

        return $passwordResetCode;
    }

    // To set Global Admin permission
    private function setGlobalAdminPermission($siteString, $nucleus_id)
    {
        $pdo = $this->pdoConnection($siteString);

        // Retreives the Global admin Id from Identity table
        $stmt = $pdo->prepare('SELECT id FROM identity_type WHERE name = :name');
        $stmt->execute(['name' => 'Global Admin']);
        $id = $stmt->fetchColumn(0);

        if (empty($id)) {
            throw new \Exception('Global Admin identity type not found');
        }

        $stmt = $pdo->prepare('SELECT * FROM user_identity_type WHERE user_id = :nucleus_id AND identity_type_id = :global_admin_id');
        $stmt->execute(['nucleus_id' => $nucleus_id, 'global_admin_id' => $id]);
        $userID = $stmt->fetchColumn(0);

        // Sets the permission by creating entry for given user Id
        if (empty($userID)) {
            $stmt = $pdo->prepare('INSERT INTO user_identity_type(identity_type_id, user_id, approved) VALUES(:global_admin_id,:nucleus_id,1)');
        } else {
            $stmt = $pdo->prepare('UPDATE user_identity_type SET approved = 1 WHERE identity_type_id = :global_admin_id AND user_id = :nucleus_id');
        }

        $stmt->execute([
            'global_admin_id' => $id,
            'nucleus_id'      => $nucleus_id
        ]);
    }

    // Set PDO connection
    private function pdoConnection($connection)
    {
        $parts    = explode(';', $connection);
        $dns      = 'sqlsrv:' . $parts[0] . ';' . $parts[1] . ';ConnectionPooling= 0';
        $username = explode('=', $parts[2])[1];
        $password = explode('=', $parts[3])[1];

        return IO::getPDOConnection([
            'dns'      => $dns,
            'username' => $username,
            'password' => $password
        ]);
    }
}
