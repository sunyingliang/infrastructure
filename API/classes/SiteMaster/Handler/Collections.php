<?php

namespace FS\SiteMaster\Handler;

use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\DBDuplicationException;

class Collections extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['collections']);
    }
    
    public function listSites()
    {
        $this->auth->hasPermission(['read']);
        
        $name     = IO::default($this->data, 'name');
        $host     = IO::default($this->data, 'host');
        $dbserver = IO::default($this->data, 'dbserver');

        $searchString = '';
        $parameters   = [];
        
        if (!empty($name)) {
            $searchString           = $this->appendSearch($searchString, 'name LIKE :name');
            $parameters['name'] = '%' . $name . '%';
        }

        if (!empty($host)) {
            $searchString           = $this->appendSearch($searchString, 'host LIKE :host');
            $parameters['host'] = '%' . $host . '%';
        }

        if (!empty($dbserver)) {
            $searchString           = $this->appendSearch($searchString, 'dbserver LIKE :dbserver');
            $parameters['dbserver'] = '%' . $dbserver . '%';
        }

        $sql = 'SELECT `name`, `host`, `dbserver`, `dbname`, `dbuser`, `dbpassword`, `additionalips`, `authsitehosts`, `anonsitehosts`, `authusername`, `authpassword` FROM site '
            . $searchString
            . ' ORDER BY name DESC';

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();
        
        $this->responseArr['data'] = $stmt->fetchAll();
        
        return $this->responseArr;
    }

    public function createDatabase()
    {
        $this->auth->hasPermission(['write']);

        $name = IO::default($this->data, 'name');

        if (strpos($name, ' ') !== false) {
            throw new InvalidParameterException('Customer name cannot have a space');
        }

        if (strlen($name) > 15) {
            throw new InvalidParameterException('Customer name is too long');
        }

        if (strcasecmp($name, 'admin') == 0) {
            throw new InvalidParameterException('Customer name cannot be admin');
        }

        if (strcasecmp($name, 'master') == 0) {
            throw new InvalidParameterException('Customer name cannot be master');
        }

        if (preg_match('/\W/', $name)) {
            throw new InvalidParameterException('Customer name contains invalid characters');
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM site WHERE name = :name LIMIT 1');

        $stmt->execute(['name' => $name]);

        $response = $stmt->fetchColumn(0);

        if ($response > 0) {
            throw new DBDuplicationException('Customer name already exists');
        }

        $this->pdo->beginTransaction();

        // Create a <customer>_collections mysql database
        $databaseName = 'collections_' . $name;

        $this->pdo->exec("CREATE DATABASE $databaseName");
        // Create user
        $user     = 'c' . $name;
        $password = IO::generateRandom(8);
        $this->pdo->exec("CREATE USER '$user'@'%' IDENTIFIED BY '$password'");

        // Give the user full control over the database
        $this->pdo->exec("GRANT ALL PRIVILEGES ON $databaseName.* TO '$user'@'%'");
        $this->pdo->exec('FLUSH PRIVILEGES');

        // Generate a random http auth user password
        $authUser     = 'admin';
        $authPassword = IO::generateRandom(8);

        // Get the collection host name
        $host     = $name . HOST_SUFFIX['collection'];
        $dbServer = $this->getDBAddress(DATABASE_CONNECTION['collections']);

        // Add record to the collections_master table
        $stmt = $this->pdo->prepare("INSERT INTO collections_master.site(`name`, `host`, `dbserver`, `dbname`, `dbuser`, `dbpassword`, `authusername`, `authpassword`) VALUES(:name, :host, :dbserver, :dbname, :dbuser, :dbpassword, :authusername, :authpassword)");

        $stmt->execute([
            'name'         => $name,
            'host'         => $host,
            'dbserver'     => $dbServer,
            'dbname'       => $databaseName,
            'dbuser'       => $user,
            'dbpassword'   => $password,
            'authusername' => $authUser,
            'authpassword' => $authPassword
        ]);

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        $this->pdo->commit();

        return $this->responseArr;
    }

    public function updateSite()
    {
        $this->auth->hasPermission(['write']);

        $siteName      = IO::default($this->data, 'siteName');
        $additionalIPs = IO::default($this->data, 'additionalIPs');
        $authSiteHosts = IO::default($this->data, 'authSiteHosts');
        $anonSiteHosts = IO::default($this->data, 'anonSiteHosts');

        if (empty($siteName)) {
            throw new InvalidParameterException('No siteName specified');
        }
        
        if (!empty($additionalIPs)) {
            $valid = preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $additionalIPs);
            
            if (!$valid) {
                throw new InvalidParameterException('Invalid IP Address');
            }

        }

        $stmt = $this->pdo->prepare("UPDATE site SET `additionalips` = :additionalIPs, `authsitehosts` = :authSiteHosts, `anonsitehosts` = :anonSiteHosts WHERE `name` = :site");

        $stmt->execute([
            'site'          => $siteName,
            'additionalIPs' => $additionalIPs,
            'authSiteHosts' => $authSiteHosts,
            'anonSiteHosts' => $anonSiteHosts
        ]);
        
        return $this->responseArr;
    }

    public function deleteSite()
    {
        $this->auth->hasPermission(['write']);

        $siteName = IO::default($this->data, 'siteName');

        $this->pdo->beginTransaction();

        $sql  = "SELECT * FROM site WHERE name = :name LIMIT 1";
        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute(['name' => $siteName]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        };

        $site = $stmt->fetch();

        // Drop <customer>_collections mysql database
        $databaseName = 'collections_' . $siteName;
        $sqlDropDB = "DROP DATABASE $databaseName";

        if ($this->pdo->exec($sqlDropDB) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDropDB);
        };

        // Delete DB User
        $user     = $site['dbuser'];

        $sqlDropUser = "DROP USER $user";

        if ($this->pdo->exec($sqlDropUser) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDropUser);
        };

        // Delete collection site record from `site` table
        $sqlDelete = "DELETE FROM `site` WHERE `name` = :name";
        $stmt = $this->pdo->prepare($sqlDelete);

        if ($stmt->execute(['name' => $siteName])  === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDelete);
        };

        $this->pdo->commit();

        return $this->responseArr;
    }
}
