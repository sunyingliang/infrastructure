<?php

namespace FS\SiteMaster\Handler;

use FS\Common\IO;

class APIBrokerCustomer extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure']);
    }

    public function getCustomerList()
    {
        $this->auth->hasPermission(['read']);

        $customerName = IO::default($this->data, 'customer_name');
        $dbHost       = IO::default($this->data, 'db_host');
        $environment  = IO::default($this->data, 'environment');

        $searchString = '';
        $parameters   = [];

        if (!empty($customerName)) {
            $searchString               = $this->appendSearch($searchString, 'customer_name LIKE :customerName');
            $parameters['customerName'] = '%' . $customerName . '%';
        }

        if (!empty($dbHost)) {
            $searchString         = $this->appendSearch($searchString, 'db_host = :dbHost');
            $parameters['dbHost'] = $dbHost;
        }

        if (!empty($environment) && $environment !== 'ALL') {
            $searchString              = $this->appendSearch($searchString, 'environment =:environment');
            $parameters['environment'] = $environment;
        }

        $sql = "SELECT * FROM `customer_cache` " . $searchString . " ORDER BY `customer_name` ASC";

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $this->responseArr['data'] = $stmt->fetchAll();
        return $this->responseArr;
    }

    public function getByHost()
    {
        $this->auth->hasPermission(['read']);

        $customerName = IO::default($this->data, 'customer_name');
        $dbHost       = IO::default($this->data, 'db_host');
        $environment  = IO::default($this->data, 'environment');

        $searchString = '';
        $parameters   = [];

        if (!empty($customerName)) {
            $searchString               = $this->appendSearch($searchString, 'customer_name LIKE :customerName');
            $parameters['customerName'] = '%' . $customerName . '%';
        }

        if (!empty($dbHost)) {
            $searchString         = $this->appendSearch($searchString, 'db_host = :dbHost');
            $parameters['dbHost'] = $dbHost;
        }

        if (!empty($environment) && $environment !== 'ALL') {
            $searchString              = $this->appendSearch($searchString, 'environment =:environment');
            $parameters['environment'] = $environment;
        }

        $sql = "SELECT * FROM `customer_cache` " . $searchString . " ORDER BY `db_host` ASC";

        $this->responseArr['data'] = [];

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $jsonArr = ['hosts' => []];
        while ($row = $stmt->fetch()) {

            $hostIndex = $this->elementExistsInArray($row['db_host'], $jsonArr['hosts'], '');

            if ($hostIndex === false) {
                $host = [
                    'name'      => htmlentities($row['db_host']),
                    'customers' => [
                        [
                            'name'        => htmlentities($row['customer_name']),
                            'environment' => $row['environment'],
                            'users'       => $row['users'],
                            'cases'       => $row['cases'],
                        ]
                    ],
                    'total'     => 1
                ];

                $jsonArr['hosts'][] = $host;
            } else {
                $customerIndex = $this->elementExistsInArray($row['customer_name'], $jsonArr['hosts'][$hostIndex]['customers'], $row['environment']);

                if ($customerIndex === false) {
                    $customer = [
                        'name'        => htmlentities($row['customer_name']),
                        'environment' => $row['environment'],
                        'users'       => $row['users'],
                        'cases'       => $row['cases'],
                    ];

                    $jsonArr['hosts'][$hostIndex]['customers'][] = $customer;
                    $jsonArr['hosts'][$hostIndex]['total']++;
                }
            }
        }

        $this->responseArr['data'] = $jsonArr;
        return $this->responseArr;
    }

    public function getEnvironment()
    {
        $this->responseArr['data'] = ['ALL'];

        $stmt = $this->pdo->query("SELECT `environment` FROM `customer_cache` GROUP BY `environment` ORDER BY `environment` DESC");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['environment'];
        }
        return $this->responseArr;
    }

    private function elementExistsInArray($eleName, array &$array, $environment)
    {
        $count = count($array);

        for ($i = 0; $i < $count; $i++) {
            $currentEnvironment = isset($array[$i]['environment']) ? $array[$i]['environment'] : '';

            if ($array[$i]['name'] == $eleName && $currentEnvironment == $environment) {
                return $i;
            }
        }

        return false;
    }
}