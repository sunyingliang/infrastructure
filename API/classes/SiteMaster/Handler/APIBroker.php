<?php

namespace FS\SiteMaster\Handler;

use FS\Common\IO;

class APIBroker extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['oaf_master']);
    }
    
    public function getDatabaseList()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = [];
        $stmt   = $this->pdo->query("SELECT ServerName FROM database_host ORDER BY ServerName");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['ServerName'];
        }

        return $this->responseArr;
    }

    public function getDatabaseHost()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = [];

        if ($validation = IO::checkDBConstant('DATABASE_CONNECTION', 'api_broker')) {
            for ($i = 0; $i < count(DATABASE_CONNECTION['api_broker']); $i++) {
                $host                        = explode(';', DATABASE_CONNECTION['api_broker'][$i]['dns']);
                $this->responseArr['data'][] = preg_replace('/mysql:host=/', '', $host[0]);
            }
        }

        return $this->responseArr;
    }

    public function process()
    {
        $this->auth->hasPermission(['read']);

        $database = IO::default($this->data, 'database');
        $host     = IO::default($this->data, 'host');
        $values   = [];

        if ($validation = IO::checkDBConstant('DATABASE_CONNECTION', 'api_broker')) {
            foreach (DATABASE_CONNECTION['api_broker'] as $connection) {
                if (strpos($connection['dns'], $host) !== false) {
                    $pdo  =  $this->pdo = IO::getPDOConnection($connection);
                    $sql  = "SELECT `ID`, `Value` 
                             FROM `tokens` 
                             WHERE (
                                `Value` LIKE :dbSite OR 
                                `Value` LIKE :dbAuth OR 
                                `Value` LIKE :dbData) AND 
                                (   
                                    `Token` = 'AS_DATA_DB_CONN_STRING' 
                                    OR `Token` = 'AS_AUTH_DB_CONN_STRING' 
                                    OR `Token` = 'AS_SITE_DB_CONN_STRING'
                                )";
                    $params = [
                        'dbSite' => '%Database=' . $database . '_site%',
                        'dbAuth' => '%Database=' . $database . '_auth%',
                        'dbData' => '%Database=' . $database . '_data%'
                    ];

                    $stmt = $pdo->prepare($sql);

                    $stmt->execute($params);

                    while ($row = $stmt->fetch()) {
                        $values[] = $row;
                    }
                }
            }

            $this->responseArr['data'] = $values;
        }

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        $newServer = IO::default($this->data, 'newServer');
        $result    = IO::default($this->data, 'result');
        $host      = IO::default($this->data, 'host');

        $resultArray = explode(",", $result);

        for ($i = 0; $i < count($resultArray); $i++) {
            $value      = substr($resultArray[$i], strpos($resultArray[$i], ';')+1);
            $valueArray = explode(";", $resultArray[$i]);
            $id         = preg_replace('/ID=/', '', $valueArray[0], 1);
            $oldServer  = preg_replace('/Server=/', '', $valueArray[1], 1);
            $newValue   = preg_replace('/' . $oldServer . '/', $newServer, $value, 1);

            if ($validation = IO::checkDBConstant('DATABASE_CONNECTION', 'api_broker') && $oldServer !== $newServer) {
                foreach (DATABASE_CONNECTION['api_broker'] as $connection) {
                    if (strpos($connection['dns'], $host) !== false) {
                        $pdo    = $this->pdo = IO::getPDOConnection($connection);
                        $stmt   = $pdo->prepare("UPDATE `tokens` SET `Value`= :newValue WHERE `ID` = :id");

                        $stmt->execute([
                            'newValue'  => $newValue,
                            'id'        => $id
                        ]);
                    }
                }
            }
        }

        return $this->responseArr;
    }
}
