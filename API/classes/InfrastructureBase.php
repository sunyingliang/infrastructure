<?php

namespace FS;

use FS\Common\Auth;
use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;

class InfrastructureBase
{
    protected $responseArr;
    protected $timezone;
    protected $auth;
    protected $pdo;
    protected $data;

    public function __construct($connection = null, $securable = null)
    {
        $this->responseArr = IO::initResponseArray();
        $this->auth        = new Auth();
        $this->timezone    = date_default_timezone_get();

        // VALIDATION
        if (empty($this->timezone)) {
            $this->timezone = 'NZ';
        }

        // Used only if an entire handler has the same permissions, otherwise it's done within the handler
        if (!is_null($securable)) {
            if (!is_array($securable)) {
                throw new InvalidParameterException('Error: Securable should be array');
            }

            $this->auth->hasPermission($securable);
        }

        // Used only if an entire handler uses the same connection, otherwise it's done within the handler
        if (!is_null($connection)) {
            if (!isset($connection) || !is_array($connection)) {
                throw new InvalidParameterException('Error: PDO connection is not configured properly');
            }

            $this->pdo = IO::getPDOConnection($connection);
        }

        // Take POST as the default request method, can be overridden in specified method
        $this->data = IO::getPostParameters();
    }

    #region Methods
    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') === false ? ' WHERE ' : ' AND ') . $query;
    }

    protected function bindParams(\PDOStatement $stmt, $parameters)
    {
        foreach ($parameters as $parameter => $value) {
            $stmt->bindValue($parameter, $value);
        }

        return $stmt;
    }

    protected function getDateTime($date, $time, $defaultTime)
    {
        return empty($time) ? $date . ' ' . $defaultTime : $date . ' ' . $time;
    }

    protected function post($parameter, $default = null)
    {
        return isset($_POST[$parameter]) ? trim($_POST[$parameter]) : $default;
    }

    protected function getDBAddress($connection)
    {
        $parts    = explode(';', $connection['dns']);
        $keyValue = explode('=', $parts[0]);

        if (count($keyValue) == 2 || strpos($keyValue[0], 'Server') !== false) {
            return $keyValue[1];
        }

        return '';
    }
    #endregion
}
