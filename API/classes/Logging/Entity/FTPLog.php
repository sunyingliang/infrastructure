<?php

namespace FS\Logging\Entity;

use FS\Common\Exception\InvalidParameterException;

class FTPLog
{
    private $id        = 0;
    private $ftpUser   = '';
    private $fileName  = '';
    private $startTime = '0000-00-00 00:00:00';
    private $endTime   = '0000-00-00 00:00:00';
    private $response  = '';

    #region Construct
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['id'])) {
            if (!is_numeric($data['id'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->id = $data['id'];
            }
        }

        if (isset($data['ftpUser'])) {
            $this->ftpUser = $data['ftpUser'];
        }

        if (isset($data['fileName'])) {
            $this->fileName = $data['fileName'];
        }

        if (isset($data['startTime'])) {
            $this->startTime = $data['startTime'];
        }

        if (isset($data['endTime'])) {
            $this->endTime = $data['endTime'];
        }

        if (isset($data['response'])) {
            $this->response = $data['response'];
        }
    }
    #endregion

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getFTPUser()
    {
        return $this->ftpUser;
    }

    public function setFTPUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    public function getEndTime()
    {
        return $this->endTime;
    }

    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }
    #endregion

    #region Utils
    private function validateDateTime($datetime)
    {
        // Datetime validation, e.g. 2017-03-27 12:23:00
        $preg = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';

        return preg_match($preg, $datetime) === 1;
    }
    #endregion
}