<?php

namespace FS\Logging\Entity;

use FS\Common\Exception\InvalidParameterException;

class FailedLog
{
    #region Fields
    protected $id;
    protected $messageId;
    protected $fromAddress;
    protected $toAddress;
    protected $subject;
    protected $timeReceived;
    protected $reason;
    #endregion

    #region Construct
    public function __construct($data)
    {
        $this->id           = 0;
        $this->messageId    = '';
        $this->fromAddress  = '';
        $this->toAddress    = '';
        $this->subject      = '';
        $this->timeReceived = '0000-00-00 00:00:00';
        $this->reason       = '';

        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['id'])) {
            if (!is_numeric($data['id'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->id = $data['id'];
            }
        }

        if (isset($data['message_id'])) {
            $this->messageId = $data['message_id'];
        }

        if (isset($data['from_address'])) {
            $this->fromAddress = $data['from_address'];
        }

        if (isset($data['to_address'])) {
            $this->toAddress = $data['to_address'];
        }

        if (isset($data['subject'])) {
            $this->subject = $data['subject'];
        }

        if (isset($data['time_received'])) {
            if (!$this->validateDateTime($data['time_received'])) {
                throw new InvalidParameterException('Passed in parameter {time_received} is not in correct format, i.e. YYYY-MM-DD hh:mm:ss');
            } else {
                $this->timeReceived = $data['time_received'];
            }
        }

        if (isset($data['reason'])) {
            $this->reason = $data['reason'];
        }
    }
    #endregion

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getMessageId()
    {
        return $this->messageId;
    }

    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;
    }

    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;
    }

    public function getToAddress()
    {
        return $this->toAddress;
    }

    public function setToAddress($toAddress)
    {
        $this->toAddress = $toAddress;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getTimeReceived()
    {
        return $this->timeReceived;
    }

    public function setTimeReceived($timeReceived)
    {
        $this->timeReceived = $timeReceived;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }
    #endregion

    #region Utils
    private function validateDateTime($datetime)
    {
        // Datetime validation, e.g. 2017-03-27 12:23:00
        $preg = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';

        return preg_match($preg, $datetime) === 1;
    }

    private function validateEmail($email)
    {
        // Email validation, e.g. sample@gmail.com
        $preg = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

        return preg_match($preg, $email) === 1;
    }

    #endregion
}
