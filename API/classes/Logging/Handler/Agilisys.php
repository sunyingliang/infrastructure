<?php

namespace FS\Logging\Handler;

use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;

class Agilisys extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getEnvironmentList()
    {
        $this->responseArr['data'] = ['All'];

        $stmt = $this->pdo->query('SELECT `Environment` FROM Agilisys GROUP BY `Environment` ORDER BY `Environment`');

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['Environment'];
        }

        return $this->responseArr;
    }

    public function getSiteList()
    {
        $this->responseArr['data'] = ['All'];

        $stmt = $this->pdo->query('SELECT `Logger` FROM Agilisys GROUP BY `Logger` ORDER BY `Logger`');

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['Logger'];
        }

        return $this->responseArr;
    }

    public function getList()
    {
        $fromDate    = IO::default($this->data, 'fromDate');
        $toDate      = IO::default($this->data, 'toDate');
        $environment = IO::default($this->data, 'environment');
        $log         = IO::default($this->data, 'log');
        $message     = IO::default($this->data, 'message');
        $offset      = IO::default($this->data, 'offset', 0);
        $limit       = IO::default($this->data, 'limit', 30);
        $sort        = IO::default($this->data, 'sort', 'Id');
        $sortOrder   = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['Date', 'Environment', 'Logger', 'Message'])) {
            $sort = 'Id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'Date >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'Date <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($environment) && $environment != 'All') {
            $searchString              = $this->appendSearch($searchString, 'Environment = :environment');
            $parameters['environment'] = $environment;
        }

        if (!empty($log) && $log != 'All') {
            $searchString      = $this->appendSearch($searchString, 'Logger = :log');
            $parameters['log'] = $log;
        }

        if (!empty($message)) {
            $searchString          = $this->appendSearch($searchString, 'Message LIKE :message');
            $parameters['message'] = '%' . $message . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM Agilisys ' . $searchString);

        $stmt->execute($parameters);

        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT `Date`,`Environment`,`Logger`,`Message` FROM Agilisys '
            . $searchString
            . ' ORDER BY ' . $sort . ' ' . $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);

        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {

        if (!($validation = IO::required($requestData, ['date', 'environment', 'level', 'logger', 'message']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $exception = IO::default($requestData, 'exception');

        $sql = "INSERT INTO `Agilisys`(`date`,`environment`, `level`, `logger`, `message`, `Exception`) 
                      VALUES(:date, :environment, :level, :logger, :message, :exception)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'date'        => $requestData['date'],
            'environment' => $requestData['environment'],
            'level'       => $requestData['level'],
            'logger'      => $requestData['logger'],
            'message'     => $requestData['message'],
            'exception'   => $exception,
        ]);

        return $this->pdo->lastInsertId();
    }
}
