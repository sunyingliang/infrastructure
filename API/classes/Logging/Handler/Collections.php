<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class Collections extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $logger    = IO::default($this->data, 'logger');
        $hostname  = IO::default($this->data, 'hostname');
        $sitename  = IO::default($this->data, 'sitename');
        $message   = IO::default($this->data, 'message');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['id', 'timestamp', 'hostname', 'sitename', 'logger', 'message'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'timestamp >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'timestamp <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($hostname) && $hostname != 'All') {
            $searchString           = $this->appendSearch($searchString, 'hostname LIKE :hostname');
            $parameters['hostname'] = '%' . $hostname . '%';
        }

        if (!empty($sitename) && $sitename != 'All') {
            $searchString           = $this->appendSearch($searchString, 'sitename LIKE :sitename');
            $parameters['sitename'] = '%' . $sitename . '%';
        }

        if (!empty($logger) && $logger != 'All') {
            $logger               = str_replace('\\', '\\\\', $logger);
            $searchString         = $this->appendSearch($searchString, 'logger LIKE :logger');
            $parameters['logger'] = '%' . $logger . '%';
        }

        if (!empty($message)) {
            $searchString          = $this->appendSearch($searchString, 'message LIKE :message');
            $parameters['message'] = '%' . $message . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM collections ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT timestamp,logger,level,message,exception,thread,file,line,hostname,sitename FROM collections '
            . $searchString
            . ' ORDER BY ' . $sort . ' ' . $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {
        if (!($validation = IO::required($requestData, ['date', 'level', 'logger', 'message']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $exception = IO::default($requestData, 'exception');
        $thread    = IO::default($requestData, 'thread');
        $file      = IO::default($requestData, 'file');
        $line      = IO::default($requestData, 'line');
        $hostname  = IO::default($requestData, 'hostName');
        $sitename  = IO::default($requestData, 'siteName');

        $sql = "INSERT INTO `collections`(`timestamp`, `logger`, `level`, `message`, `exception`, `thread`, `file`, `line`, `hostname`, `sitename`) 
                      VALUES(:timestamp, :logger, :level, :message, :exception, :thread, :file, :line, :hostname, :sitename)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'timestamp' => $requestData['date'],
            'logger'    => $requestData['logger'],
            'level'     => $requestData['level'],
            'message'   => $requestData['message'],
            'exception' => $exception,
            'thread'    => $thread,
            'file'      => $file,
            'line'      => $line,
            'hostname'  => $hostname,
            'sitename'  => $sitename,
        ]);

        return $this->pdo->lastInsertId();
    }
}
