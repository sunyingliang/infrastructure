<?php

namespace FS\Logging\Handler;

use FS\Common\IO;
use FS\Logging\Entity\FTPLog;

class FTP extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log']);
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['ftpUser', 'fileName']))['valid']) {
            throw new \Exception($validation['message']);
        }

        $ftp = new FTPLog($this->data);

        // Insert ftp log for customer
        $sql = "INSERT INTO `ftp`(`ftp_user`, `file_name`) VALUES(:ftp_user, :file_name)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'ftp_user'  => $ftp->getFTPUser(),
            'file_name' => $ftp->getFileName()
        ]);

        $this->responseArr['data'] = [
            'id' => $this->pdo->lastInsertId()
        ];

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id', 'response']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $ftp = new FTPLog($this->data);

        $sql  = "UPDATE `ftp` SET `end_time` = CURRENT_TIMESTAMP, `response` = :response WHERE `id` = :id";
        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'id'       => $ftp->getId(),
            'response' => $ftp->getResponse()
        ]);

        return $this->responseArr;
    }

    public function getFTPLogs()
    {
        // Getters
        $customer  = IO::default($this->data, 'customer');
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $response  = IO::default($this->data, 'response');
        $fileName  = IO::default($this->data, 'file_name');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'start_time');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['start_time', 'end_time', 'response', 'file_name', 'ftp_user'])) {
            $sort = 'start_time';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        // Initialisers
        $searchString = '';
        $parameters   = [];

        if (!empty($customer) && $customer != 'All') {
            $searchString              = $this->appendSearch($searchString, 'ftp_user = :ftpUser');
            $parameters['ftpUser'] = $customer;
        }

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'start_time >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'start_time <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($response)) {
            $searchString           = $this->appendSearch($searchString, 'response LIKE :response');
            $parameters['response'] = '%' . $response . '%';
        }

        if (!empty($fileName)) {
            $searchString            = $this->appendSearch($searchString, 'file_name LIKE :file_name');
            $parameters['file_name'] = '%' . $fileName . '%';
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM `ftp` " . $searchString);

        $stmt->execute($parameters);

        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT ftp_user, start_time, end_time, response, file_name "
            . "FROM ftp "
            . $searchString . " "
            . ' ORDER BY ' . $sort .' '. $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);

        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }
}
