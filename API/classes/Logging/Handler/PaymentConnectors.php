<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class PaymentConnectors extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getSourceList()
    {
        $this->responseArr['data'] = ['All'];
        $stmt                      = $this->pdo->query('SELECT `logger` FROM `PaymentConnectors` GROUP BY `logger` ORDER BY `logger`');

        while ($row = $stmt->fetch()) {
            $logger = $row['logger'];

            if (!in_array($logger, $this->responseArr['data'])) {
                $this->responseArr['data'][] = $logger;
            }
        }

        return $this->responseArr;
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $timeFrom  = IO::default($this->data, 'timeFrom');
        $timeTo    = IO::default($this->data, 'timeTo');
        $reference = IO::default($this->data, 'reference');
        $level     = IO::default($this->data, 'level');
        $log       = IO::default($this->data, 'log');
        $message   = IO::default($this->data, 'message');
        $exception = IO::default($this->data, 'exception');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'Id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['Id', 'Date', 'Reference', 'Logger', 'Message', 'Exception'])) {
            $sort = 'Id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'Date >= :fromDate');
            $parameters['fromDate'] = $this->getDateTime($fromDate, $timeFrom, '00:00:00');
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'Date <= :toDate');
            $parameters['toDate'] = $this->getDateTime($toDate, $timeTo, '23:59:59');
        }

        if (!empty($level)) {
            $searchString        = $this->appendSearch($searchString, 'Level = :level');
            $parameters['level'] = $level;
        }

        if (!empty($log) && $log != 'All') {
            $searchString       = $this->appendSearch($searchString, 'Logger = :log');
            $parameters['log'] = $log;
        }

        if (!empty($reference)) {
            $searchString            = $this->appendSearch($searchString, 'Reference = :reference');
            $parameters['reference'] = $reference;
        }

        if (!empty($message)) {
            $urlDecodedMessage = urldecode($message);

            if ($message != $urlDecodedMessage) {
                $message = $message . ' OR ' . $urlDecodedMessage;
            }

            $searchString          = $this->appendSearch($searchString, 'Message LIKE :message');
            $parameters['message'] = '%' . $message . '%';
        }

        if (!empty($exception)) {
            $searchString            = $this->appendSearch($searchString, 'Exception LIKE :exception');
            $parameters['exception'] = '%' . $exception . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM PaymentConnectors ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT Reference,Date,Level,Logger,Exception,Message FROM PaymentConnectors '
            . $searchString
            . ' ORDER BY ' . $sort . ' ' . $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $result = $stmt->fetchAll();

        for ($i = 0; $i < count($result); $i++) {
            $result[$i]['Message'] = iconv(mb_detect_encoding($result[$i]['Message'], mb_detect_order(), true), 'UTF-8',
                $result[$i]['Message']);
        }

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $result
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {
        if (!($validation = IO::required($requestData, ['date', 'thread', 'level', 'logger', 'message']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $exception = IO::default($requestData, 'exception');
        $reference = IO::default($requestData, 'reference');

        $sql = "INSERT INTO `PaymentConnectors`(`Date`, `Thread`, `Level`, `Logger`, `Message`, `Exception`, `Reference`) 
                  VALUES(:date, :thread, :level, :logger, :message, :exception, :reference)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'date'      => $requestData['date'],
            'thread'    => $requestData['thread'],
            'level'     => $requestData['level'],
            'logger'    => $requestData['logger'],
            'message'   => $requestData['message'],
            'exception' => $exception,
            'reference' => $reference
        ]);

        return $this->pdo->lastInsertId();
    }
}