<?php

namespace FS\Logging\Handler;

use FS\Common\IO;

class Background extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $serverId  = IO::default($this->data, 'server');
        $site      = IO::default($this->data, 'site');
        $operation = IO::default($this->data, 'operation');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'timestamp');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['timestamp', 'site',  'server_id', 'operation'])) {
            $sort = 'timestamp';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'timestamp >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'timestamp <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($site) && $site != 'All') {
            $searchString       = $this->appendSearch($searchString, 'site = :site');
            $parameters['site'] = $site;
        }

        if (!empty($serverId) && $serverId != 'All') {
            $searchString            = $this->appendSearch($searchString, 'server_id = :server_id');
            $parameters['server_id'] = $serverId;
        }

        if (!empty($operation) && $operation != 'All') {
            $searchString            = $this->appendSearch($searchString, 'operation = :operation');
            $parameters['operation'] = $operation;
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM oaf_background" . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT `timestamp`, `server_id`, `site`, `operation` FROM oaf_background "
            . $searchString
            . " ORDER BY " . $sort . " " . $sortOrder . " LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function getSiteList()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = ['All'];

        $stmt = $this->pdo->query("SELECT `site` FROM `oaf_background` GROUP BY `site` ORDER BY `site`");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['site'];
        }

        return $this->responseArr;
    }

    public function getServerList()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = ['All'];

        $stmt = $this->pdo->query("SELECT `server_id` FROM `oaf_background` GROUP BY `server_id` ORDER BY `server_id`");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['server_id'];
        }

        return $this->responseArr;
    }

    public function getOperationList()
    {
        $this->auth->hasPermission(['read']);

        $this->responseArr['data'] = ['All'];

        $stmt = $this->pdo->query("SELECT `operation` FROM `oaf_background` GROUP BY `operation` ORDER BY `operation`");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['operation'];
        }

        return $this->responseArr;
    }
}
