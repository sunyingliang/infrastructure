<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\DBDuplicationException;
use FS\Logging\Entity\FailedLog;
use FS\Logging\Entity\SentLog;
use FS\Common\IO;

class Email extends \FS\InfrastructureBase
{
    private $pageTotal;

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log']);

        $this->pageTotal = 30;
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['status']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        switch (strtolower($this->data['status'])) {
            case 'sent':
                $table = 'smtp_sent';
                break;
            case 'failed':
                $table = 'smtp_failed';
                break;
            default:
                throw new InvalidParameterException('Unrecognised parameter {status} with value {' . $this->data['status'] . '}');
        }

        $sql         = "SELECT `id`, `time_received`, `from_address`, `to_address`, `subject`" . ($table == 'smtp_failed' ? ', `reason`' : '') . " FROM `{$table}`";
        $whereClause = '';
        $paramArr    = [];

        if (isset($this->data['start'])) {
            $whereClause       = $this->appendSearch($whereClause, '`time_received` >= :start');
            $paramArr['start'] = $this->data['start'];
        }

        if (isset($this->data['end'])) {
            $whereClause     = $this->appendSearch($whereClause, '`time_received` <= :end');
            $paramArr['end'] = $this->data['end'] . 'T23:59:59';
        }

        if (isset($this->data['from'])) {
            $whereClause      = $this->appendSearch($whereClause, '`from_address` LIKE :from');
            $paramArr['from'] = '%' . $this->data['from'] . '%';
        }

        // Need to changed to use fulltext index
        if (isset($this->data['to'])) {
            $whereClause    = $this->appendSearch($whereClause, '`to_address` LIKE :to');
            $paramArr['to'] = '%' . $this->data['to'] . '%';
        }

        if (isset($this->data['subject'])) {
            $whereClause         = $this->appendSearch($whereClause, '`subject` LIKE :subject');
            $paramArr['subject'] = '%' . $this->data['subject'] . '%';
        }

        $this->data['offset'] = $this->data['offset'] ?? 0;
        $this->data['limit']  = $this->data['limit'] ?? 30;
        
        $sort      = IO::default($this->data, 'sort', 'id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['time_received', 'to_address',  'subject', 'from_address', 'reason'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        // Get total row count
        $sqlTotal = "SELECT COUNT(1) 
                     FROM `{$table}`" . $whereClause;

        $stmt     = $this->pdo->prepare($sqlTotal);

        $stmt = $this->bindParams($stmt, $paramArr);

        $stmt->execute();

        $rowCount   = $stmt->fetchColumn(0);
        $limitCount = $this->pageTotal * $this->data['limit'];

        if (empty($whereClause) && $rowCount > $limitCount) {
            $rowCount = $limitCount;
        }

        $sql .= $whereClause  . ' ORDER BY ' . $sort .' '. $sortOrder . ' LIMIT :offset, :limit';

        if (($stmt = $this->pdo->prepare($sql)) === false) {
            throw new PDOCreationException('Failed to create PDOStatement by given SQL');
        }

        $stmt = $this->bindParams($stmt, $paramArr);

        $stmt->bindParam('offset', $this->data['offset'], \PDO::PARAM_INT);
        $stmt->bindParam('limit', $this->data['limit'], \PDO::PARAM_INT);

        $stmt->execute();

        $result = $stmt->fetchAll();

        for ($i = 0; $i < count($result); $i++) {
            $result[$i]['time_received'] = (new \DateTime($result[$i]['time_received'], new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->timezone))->format('Y-m-d H:i:s');
        }

        $this->responseArr['data'] = [
            'status'        => $this->data['status'],
            'totalRowCount' => $rowCount,
            'rows'          => $result
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['status']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        if ($this->data['status'] == 'sent') {
            if (!($validation = IO::required($this->data, ['time_received', 'source_ip', 'from_address', 'to_address', 'cc_address', 'subject']))['valid']) {
                throw new InvalidParameterException($validation['message']);
            }

            $email = new SentLog($this->data);

            $sql = "INSERT INTO `smtp_sent`(`time_received`, `source_ip`, `from_address`, `to_address`, `cc_address`, `subject`) VALUES(:time_received, :source_ip, :from_address, :to_address, :cc_address, :subject)";

            $values = [
                'time_received' => $email->getTimeReceived(),
                'source_ip'     => $email->getSourceIp(),
                'from_address'  => $email->getFromAddress(),
                'to_address'    => $email->getToAddress(),
                'cc_address'    => $email->getCcAddress(),
                'subject'       => $email->getSubject()
            ];
        } else if ($this->data['status'] == 'failed') {
            if (!($validation = IO::required($this->data, ['message_id', 'from_address', 'to_address', 'subject', 'time_received', 'reason']))['valid']) {
                throw new InvalidParameterException($validation['message']);
            }

            $email = new FailedLog($this->data);

            // Check existence of message_id
            $sql = "SELECT COUNT(1) AS `Total` 
                    FROM `smtp_failed` 
                    WHERE `message_id` = :message_id";

            if (($stmt = $this->pdo->prepare($sql)) === false) {
                throw new PDOCreationException('Failed to create PDOStatement by given SQL');
            }

            if (($stmt->execute(['message_id' => $email->getMessageId()])) === false) {
                throw new PDOExecutionException('Failed to execute PDOStatement by given SQL');
            }

            if (!($row = $stmt->fetch())) {
                throw new PDOExecutionException('Failed to fetch PDOStatement data by given SQL');
            }

            if ($row['Total'] > 0) {
                throw new DBDuplicationException('Record with the same message_id exists');
            }

            // Insert a new record
            $sql = "INSERT INTO `smtp_failed`(`message_id`, `from_address`, `to_address`, `subject`, `time_received`, `reason`) VALUES(:message_id, :from_address, :to_address, :subject, :time_received, :reason)";

            $values = [
                'message_id'    => $email->getMessageId(),
                'from_address'  => $email->getFromAddress(),
                'to_address'    => $email->getToAddress(),
                'subject'       => $email->getSubject(),
                'time_received' => $email->getTimeReceived(),
                'reason'        => $email->getReason()
            ];
        }

        if (($stmt = $this->pdo->prepare($sql)) === false) {
            throw new PDOCreationException('Failed to create PDOStatement by given SQL');
        }

        if (($stmt->execute($values)) === false) {
            throw new PDOCreationException('Failed to create PDOStatement by given SQL');
        }

        $this->responseArr['data'] = [
            'status'     => 'success',
            'id' => $this->pdo->lastInsertId()
        ];

        return $this->responseArr;
    }

    public function delete()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['status', 'id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        if ($this->data['status'] == 'sent') {
            $email = new SentLog($this->data);

            $sql = "DELETE FROM `smtp_sent` WHERE `id` = :id";
        } else if ($this->data['status'] == 'failed') {
            $email = new FailedLog($this->data);

            $sql = "DELETE FROM `smtp_failed` WHERE `id` = :id";
        }

        if (($stmt = $this->pdo->prepare($sql)) === false) {
            throw new PDOCreationException('Failed to create PDOStatement by given SQL');
        }

        if (($stmt->execute(['id' => $email->getId()])) === false) {
            throw new PDOCreationException('Failed to create PDOStatement by given SQL');
        }

        return $this->responseArr;
    }
}
