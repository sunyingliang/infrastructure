<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class Download extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $fromDate      = IO::default($this->data, 'fromDate');
        $toDate        = IO::default($this->data, 'toDate');
        $component     = IO::default($this->data, 'component');
        $version       = IO::default($this->data, 'version');
        $remoteAddress = IO::default($this->data, 'remoteAddress');
        $offset        = IO::default($this->data, 'offset', 0);
        $limit         = IO::default($this->data, 'limit', 30);
        $sort          = IO::default($this->data, 'sort', 'id');
        $sortOrder     = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['fromDate', 'toDate', 'component',  'version', 'remote_address'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'logtime >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'logtime <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($component)) {
            $searchString       = $this->appendSearch($searchString, 'component LIKE :component');
            $parameters['component'] = '%' . $component . '%';
        }

        if (!empty($version)) {
            $searchString            = $this->appendSearch($searchString, 'version LIKE :version');
            $parameters['version'] = '%' . $version . '%';
        }

        if (!empty($remoteAddress)) {
            $searchString            = $this->appendSearch($searchString, 'remote_address LIKE :remoteAddress');
            $parameters['remoteAddress'] = '%' . $remoteAddress . '%';
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM download" . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT `logtime`, `component`, `version`, `remote_address` FROM download "
            . $searchString
            . " ORDER BY " . $sort . " " . $sortOrder . " LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->insertData($this->data);
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {
        if (!($validation = IO::required($requestData, ['logTime', 'component', 'version', 'remoteAddress']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $sql = "INSERT INTO `download`(`logtime`, `component`, `version`, `remote_address`) 
                  VALUES(:logtime, :component, :version, :remoteAddress)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'logtime'       => $requestData['logTime'],
            'component'     => $requestData['component'],
            'version'       => $requestData['version'],
            'remoteAddress' => $requestData['remoteAddress']
        ]);

        $this->responseArr['data'] = [
            'id' => $this->pdo->lastInsertId()
        ];
    }
}
