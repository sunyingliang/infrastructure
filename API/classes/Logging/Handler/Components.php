<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class Components extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $client    = IO::default($this->data, 'client');
        $server    = IO::default($this->data, 'server');
        $status    = IO::default($this->data, 'status');
        $query     = IO::default($this->data, 'query');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'LogTime');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['LogTime', 'ClientHost', 'machine', 'servicestatus', 'processingtime', 'operation'])) {
            $sort = 'LogTime';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = "WHERE target <> '/iisstart.htm' AND target <> '/build.txt'";
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'LogTime >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'LogTime <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($client)) {
            $searchString             = $this->appendSearch($searchString, 'ClientHost LIKE :clientHost');
            $parameters['clientHost'] = '%' . $client . '%';
        }

        if (!empty($server)) {
            $searchString          = $this->appendSearch($searchString, 'machine LIKE :machine');
            $parameters['machine'] = '%' . $server . '%';
        }

        if (!empty($status) && $status != 'ALL') {
            $searchString                = $this->appendSearch($searchString, 'servicestatus LIKE :servicestatus');
            $parameters['servicestatus'] = '%' . $status . '%';
        }

        if (!empty($query)) {
            $searchString         = $this->appendSearch($searchString, '(target LIKE :query0 OR parameters LIKE :query1)');
            $parameters['query0'] = '%' . $query . '%';
            $parameters['query1'] = '%' . $query . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM Components ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT ClientHost, LogTime, processingtime, servicestatus, operation, target, parameters, machine FROM Components '
            . $searchString
            . ' ORDER BY ' . $sort .' '. $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);

        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function getStatusList()
    {
        $this->responseArr['data'] = ['ALL'];
        $stmt                      = $this->pdo->query("SELECT servicestatus FROM Components WHERE target <> '/iisstart.htm' AND target <> '/build.txt' GROUP BY servicestatus");

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['servicestatus'];
        }

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData) {
        if (!($validation = IO::required($requestData, ['logTime', 'clientHost', 'service', 'machine', 'serverIp', 'target']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $userName       = IO::default($requestData, 'userName');
        $processingTime = IO::default($requestData, 'processingTime');
        $bytesRecvd     = IO::default($requestData, 'bytesRecvd');
        $bytesSent      = IO::default($requestData, 'bytesSent');
        $serviceStatus  = IO::default($requestData, 'serviceStatus');
        $win32Status    = IO::default($requestData, 'win32Status');
        $operation      = IO::default($requestData, 'operation');
        $parameters     = IO::default($requestData, 'parameters');

        $sql = "INSERT INTO `Components`(`ClientHost`, `username`, `LogTime`, `service`, `machine`, `serverip`, `processingtime`, `bytesrecvd`, `bytessent`, `servicestatus`, `win32status`, `operation`, `target`, `parameters`) 
                  VALUES(:clientHost, :userName, :logTime, :service, :machine, :serverIp, :processingTime, :bytesRecvd, :bytesSent, :serviceStatus, :win32Status, :operation, :target, :parameters)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'clientHost'     => $requestData['clientHost'],
            'target'         => $requestData['target'],
            'logTime'        => $requestData['logTime'],
            'service'        => $requestData['service'],
            'machine'        => $requestData['machine'],
            'serverIp'       => $requestData['serverIp'],
            'userName'       => $userName,
            'processingTime' => $processingTime,
            'bytesRecvd'     => $bytesRecvd,
            'bytesSent'      => $bytesSent,
            'serviceStatus'  => $serviceStatus,
            'win32Status'    => $win32Status,
            'operation'      => $operation,
            'parameters'     => $parameters
        ]);

        return $this->pdo->lastInsertId();
    }
}
