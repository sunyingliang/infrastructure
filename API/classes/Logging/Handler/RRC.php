<?php

namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class RRC extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getSiteList()
    {
        $this->responseArr['data'] = ['All'];
        $stmt                      = $this->pdo->query('SELECT `Site` FROM `RRC` WHERE `Site` IS NOT NULL GROUP BY `Site` ORDER BY `Site`');

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row['Site'];
        }

        return $this->responseArr;
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $message   = IO::default($this->data, 'message');
        $site      = IO::default($this->data, 'site');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'Id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['Id', 'Date', 'Site', 'Message'])) {
            $sort = 'Id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'Date >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'Date <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($site) && $site != 'All') {
            $searchString       = $this->appendSearch($searchString, 'Site = :site');
            $parameters['site'] = $site;
        }

        if (!empty($message)) {
            $searchString          = $this->appendSearch($searchString, 'Message LIKE :message');
            $parameters['message'] = '%' . $message . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM RRC ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);
        $sql = 'SELECT `Date`, `Message`, `Site`, `Version` FROM RRC '
            . $searchString
            . ' ORDER BY ' . $sort . ' ' . $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' .  json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {
        if (!($validation = IO::required($requestData, ['date', 'thread', 'level', 'logger', 'message']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $exception = IO::default($requestData, 'exception');
        $site      = IO::default($requestData, 'site');
        $version   = IO::default($requestData, 'version');

        // Insert ftp log for customer
        $sql = "INSERT INTO `RRC`(`Date`, `Thread`, `Level`, `Logger`, `Message`, `Exception`, `Site`, `Version`)
                  VALUES(:date, :thread, :level, :logger, :message, :exception, :site, :version)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'date'      => $requestData['date'],
            'thread'    => $requestData['thread'],
            'level'     => $requestData['level'],
            'logger'    => $requestData['logger'],
            'message'   => $requestData['message'],
            'exception' => $exception,
            'site'      => $site,
            'version'   => $version
        ]);

        return $this->pdo->lastInsertId();
    }
}
