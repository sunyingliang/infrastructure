<?php


namespace FS\Logging\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class Integration extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['read']);
    }

    public function getSourceList()
    {
        $this->responseArr['data']['logger']   = $this->getLoggerList();
        $this->responseArr['data']['customer'] = $this->getCustomerList();

        return $this->responseArr;
    }

    private function getLoggerList()
    {
        $loggers = ['All'];

        $stmt = $this->pdo->query('SELECT `logger` FROM `integration` GROUP BY `logger` ORDER BY `logger`');

        while ($row = $stmt->fetch()) {
            $logger = $row['logger'];

            if (!in_array($logger, $loggers)) {
                $loggers[] = $logger;
            }
        }

        return $loggers;
    }

    private function getCustomerList()
    {
        $customers = ['All'];

        $stmt = $this->pdo->query('SELECT `customer` FROM `integration` GROUP BY `customer` ORDER BY `customer`');

        while ($row = $stmt->fetch()) {
            $customer = $row['customer'];

            if (!in_array($customer, $customers)) {
                $customers[] = $customer;
            }
        }

        return $customers;
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $level     = IO::default($this->data, 'level');
        $log       = IO::default($this->data, 'log');
        $message   = IO::default($this->data, 'message');
        $customer  = IO::default($this->data, 'customer');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'Id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['Id', 'Date', 'Message'])) {
            $sort = 'Id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'date >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'date <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($level) && $level != 'All') {
            $searchString        = $this->appendSearch($searchString, 'level = :level');
            $parameters['level'] = $level;
        }

        if (!empty($log) && $log != 'All') {
            $searchString         = $this->appendSearch($searchString, 'logger = :logger');
            $parameters['logger'] = $log;
        }

        if (!empty($customer) && $customer != 'All') {
            $searchString           = $this->appendSearch($searchString, 'customer = :customer');
            $parameters['customer'] = $customer;
        }

        if (!empty($message)) {
            $searchString          = $this->appendSearch($searchString, 'message LIKE :message');
            $parameters['message'] = '%' . $message . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM integration ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT `date`, `level`, `logger`, `message`, `exception`, `reference`, `customer` FROM `integration` '
            . $searchString
            . ' ORDER BY ' . $sort . ' ' . $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (isset($this->data['requests']) && !empty($this->data['requests'])) {
            $errorMessage              = '';
            $this->responseArr['data'] = [
                'id' => []
            ];

            foreach ($this->data['requests'] as $request) {
                try {
                    $this->responseArr['data']['id'][] = $this->insertData($request);
                } catch (\Exception $e) {
                    $errorMessage .= 'Error Message: ' . $e->getMessage() . ', Item: ' . json_encode($request) . PHP_EOL;
                }
            }
        } else {
            $this->responseArr['data'] = [
                'id' => $this->insertData($this->data)
            ];
        }

        if (!empty($errorMessage)) {
            throw new \Exception($errorMessage);
        }

        return $this->responseArr;
    }

    public function insertData($requestData)
    {
        if (!($validation = IO::required($requestData, ['date', 'thread', 'level', 'logger', 'message']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $exception = IO::default($requestData, 'exception');
        $reference = IO::default($requestData, 'reference');
        $customer  = IO::default($requestData, 'customer');

        $sql
            = "INSERT INTO `integration`(`date`, `thread`, `level`, `logger`, `message`, `exception`, `reference`, `customer`) 
                  VALUES(:date, :thread, :level, :logger, :message, :exception, :reference, :customer)";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'date'      => $requestData['date'],
            'thread'    => $requestData['thread'],
            'level'     => $requestData['level'],
            'logger'    => $requestData['logger'],
            'message'   => $requestData['message'],
            'exception' => $exception,
            'reference' => $reference,
            'customer'  => $customer
        ]);

        return $this->pdo->lastInsertId();
    }
}