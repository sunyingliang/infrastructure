<?php

namespace FS\Download\Handler;

use FS\Common\IO;
use SebastianBergmann\CodeCoverage\Report\PHP;

class Component extends \FS\InfrastructureBase
{
    private $postBody;

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure']);
    }

    #region for AdminUI
    public function getComponentList()
    {
        $this->auth->hasPermission(['read']);

        $name            = IO::default($this->data, 'name');
        $releaseNotesURL = IO::default($this->data, 'release_notes_url');
        $notes           = IO::default($this->data, 'notes');
        $version         = IO::default($this->data, 'version');
        $releaseDate     = IO::default($this->data, 'release_date');

        $searchString = '';
        $parameters   = [];

        if ($name !== null && $name !== '') {
            $searchString = $this->appendSearch($searchString, 'name LIKE :name');
            $parameters['name'] = '%' . $name . '%';
        }

        if ($releaseNotesURL !== null && $releaseNotesURL !== '') {
            $searchString = $this->appendSearch($searchString, 'release_notes_url LIKE :release_notes_url');
            $parameters['release_notes_url'] = '%' . $releaseNotesURL . '%';
        }

        if ($notes !== null && $notes !== '') {
            $searchString = $this->appendSearch($searchString, 'notes LIKE :notes');
            $parameters['notes'] = '%' . $notes . '%';
        }

        $sql = "SELECT c.id, c.name, c.release_notes_url, c.notes,
                        (
                            SELECT v.version
                            FROM download_version AS v
                            WHERE v.component_id = c.id
                            ORDER BY v.release_date DESC LIMIT 1
                        ) AS version,
                        (
                            SELECT v.release_date
                            FROM download_version AS v
                            WHERE v.component_id = c.id
                            ORDER BY v.release_date DESC LIMIT 1
                        ) AS release_date,
                        (
                            SELECT v.url
                            FROM download_version AS v
                            WHERE v.component_id = c.id
                            ORDER BY v.release_date DESC LIMIT 1
                        ) AS url
                    FROM download_component AS c "
                    . $searchString
                    . " ORDER BY c.name";

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $results = $stmt->fetchAll();

        // This 'where' logic applies to the returned version, release_date and url's which would be slow and complicated to do in SQL
        if (($version !== null && $version !== '') || ($releaseDate !== null && $releaseDate != '')) {
            $resultsToCheck = $results;
            $results        = [];

            foreach ($resultsToCheck as $result) {
                if ($version !== '' && strpos($result['version'], $version) !== false) {
                    if ($releaseDate == null) {
                        $results[] = $result;
                    } else if ($releaseDate !== '' && strpos($result['release_date'], $releaseDate) !== false) {
                        $results[] = $result;
                    }
                } else if ($releaseDate !== '' && strpos($result['release_date'], $releaseDate) !== false) {
                    if ($version == null) {
                        $results[] = $result;
                    } else if ($version !== '' && strpos($result['version'], $version) !== false) {
                        $results[] = $result;
                    }
                }
            }
        }

        $this->responseArr['data'] = $results;

        return $this->responseArr;
    }

    public function getVersionList($componentId)
    {
        $this->auth->hasPermission(['read']);

        $sql = "SELECT `id`, `version`, `release_date`, `url`, `public` 
                FROM `download_version` 
                WHERE `component_id` = :componentId 
                ORDER BY `release_date` DESC";

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'componentId' => $componentId
        ]);

        $this->responseArr['data'] = $stmt->fetchAll();

        return $this->responseArr;
    }

    public function deleteComponent()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $id = $this->data['id'];

        $this->pdo->exec("SET FOREIGN_KEY_CHECKS=0;");

        if (is_null($this->data['version'])) {
            $sql = "DELETE FROM `download_component` WHERE `id` = :id";
        } else {
            $sql = "DELETE FROM `download_component`, `download_version`
                USING `download_component` INNER JOIN `download_version`
                WHERE `download_component`.`id` = `download_version`.`component_id` AND `download_component`.`id` = :id;";

        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('Failed to execute sql statement');
        }

        $this->pdo->exec("SET FOREIGN_KEY_CHECKS=1;");

        return $this->responseArr;
    }

    public function createComponent()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['name', 'release_notes_url']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $inputParameters = [
            'name'              => $this->data['name'],
            'release_notes_url' => $this->data['release_notes_url']
        ];

        $inputParameters['notes'] = IO::default($this->data, 'notes', 'release_notes_url');

        $stmt = $this->pdo->prepare('INSERT INTO download_component (name, release_notes_url, notes) VALUES (:name, :release_notes_url, :notes)');

        $stmt->execute($inputParameters);

        $id       = $this->pdo->lastInsertId();
        $versions = IO::default($this->data, 'versions');

        if ($id && !empty($versions) && is_array($versions)) {
            $stmt = $this->pdo->prepare('INSERT INTO download_version (component_id, version, release_date, url, public) VALUES (:component_id, :version, :release_date, :url, :public)');

            foreach ($versions as $version) {
                $version['component_id'] = $id;
                $stmt->execute($version);
            }
        }

        return $this->responseArr;
    }

    public function updateComponent()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['name', 'release_notes_url', 'notes', 'id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $inputParameters = [
            'name'              => $this->data['name'],
            'release_notes_url' => $this->data['release_notes_url'],
            'notes'             => $this->data['notes'],
            'id'                => $this->data['id']
        ];

        $stmt = $this->pdo->prepare('UPDATE download_component SET name=:name, release_notes_url=:release_notes_url, notes=:notes WHERE id=:id');
        $stmt->execute($inputParameters);

        $versions = IO::default($this->data, 'versions');

        if (!empty($versions) && is_array($versions)) {
            foreach ($versions as $version) {
                if (is_array($version)) {
                    if (isset($version['id'])) {
                        if (isset($version['delete']) && $version['delete']) {
                            $stmt = $this->pdo->prepare('DELETE FROM download_version WHERE id=:id');
                            $stmt->execute(['id' => $version['id']]);
                        } else {
                            $stmt = $this->pdo->prepare('UPDATE download_version SET version=:version, release_date=:release_date, url=:url, public=:public WHERE id=:id');
                            $stmt->execute($version);
                        }
                    } else {
                        $version['component_id'] = $inputParameters['id'];
                        $stmt                    = $this->pdo->prepare('INSERT INTO download_version (component_id, version, release_date, url, public) VALUES (:component_id, :version, :release_date, :url, :public)');
                        $stmt->execute($version);
                    }
                }
            }
        }

        return $this->responseArr;
    }
    #endregion

    #region for custom download
    public function getCustomerComponentVersions()
    {
        $this->auth->hasPermission(['read']);

        $sql = 'SELECT name,release_notes_url,version,release_date 
                    FROM download_component JOIN download_version ON download_component.id=download_version.component_id 
                    WHERE public=1 
                    ORDER BY name,release_date DESC';

        $stmt    = $this->pdo->query($sql);
        $results = $stmt->fetchAll();

        $this->responseArr['data'] = [];

        foreach ($results as $row) {
            $versionDetail = [
                'version'     => $row['version'],
                'releaseDate' => $row['release_date']
            ];

            if (!isset($this->responseArr['data'][$row['name']])) {
                $this->responseArr['data'][$row['name']] = [
                    'versions'        => [],
                    'tag'             => $versionDetail,
                    'releaseNotesUrl' => $row['release_notes_url']
                ];
            }

            $this->responseArr['data'][$row['name']]['versions'][] = $versionDetail;
        }

        return $this->responseArr;
    }

    public function getCustomerLatestVersion($component)
    {
        $this->auth->hasPermission(['read']);

        $stmt = $this->pdo->prepare('SELECT version FROM download_component JOIN download_version ON download_component.id=download_version.component_id WHERE name=:name ORDER BY release_date DESC LIMIT 1');

        $stmt->execute([
            'name' => $component
        ]);

        $this->responseArr['data'] = $stmt->fetchColumn();

        return $this->responseArr;
    }

    public function getCustomerDownload($component)
    {
        $this->auth->hasPermission(['read']);

        $version = $this->data['version'] ?? null;

        if (empty($version)) {
            $version = $this->getCustomerLatestVersion($component);
        }

        $remoteAddress = $this->data['remoteAddress'] ?? '';

        $logPDO = IO::getPDOConnection(DATABASE_CONNECTION['log']);

        $stmt = $logPDO->prepare('INSERT INTO download (component, version, remote_address) VALUES (:component, :version, :remoteAddress)');
        $stmt->execute([
            'component'     => $component,
            'version'       => $version,
            'remoteAddress' => $remoteAddress
        ]);

        $sql = 'SELECT url 
                    FROM download_component 
                          JOIN download_version 
                          ON download_component.id=download_version.component_id 
                    WHERE name=:name AND version=:version
                    LIMIT 1';

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            'name'    => $component,
            'version' => $version
        ]);

        $this->responseArr['data'] = $stmt->fetchColumn();

        return $this->responseArr;
    }
    #endregion
}
