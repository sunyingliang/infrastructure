<?php

namespace FS\CronTab\Entity;

use FS\Common\Exception\InvalidParameterException;

class Job
{
    #region Fields
    protected $id;
    protected $name;
    protected $frequency;
    protected $command;
    protected $comment;
    protected $concurrent;
    protected $started;
    protected $startTime;
    protected $executionTime;
    protected $enabled;
    protected $delay;
    #endregion

    #region Construct
    public function __construct($data)
    {
        $this->id            = 0;
        $this->name          = '';
        $this->frequency     = '';
        $this->command       = '';
        $this->comment       = '';
        $this->concurrent    = '';
        $this->started       = 0;
        $this->executionTime = 59;
        $this->enabled       = 0;
        $this->delay         = 0;

        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['id'])) {
            if (!is_numeric($data['id'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->id = $data['id'];
            }
        }

        if (isset($data['name'])) {
            if (strlen($data['name']) > 64) {
                throw new InvalidParameterException('Passed in parameter {name} exceeds maximum length (64)');
            } else {
                $this->name = $data['name'];
            }
        }

        if (isset($data['frequency'])) {
            if (strlen($data['frequency']) > 32) {
                throw new InvalidParameterException('Passed in parameter {frequency} exceeds maximum length (32)');
            } else {
                $this->frequency = $data['frequency'];
            }
        }

        if (isset($data['command'])) {
            if (strlen($data['command']) > 512) {
                throw new InvalidParameterException('Passed in parameter {command} exceeds maximum length (512)');
            } else {
                $this->command = $data['command'];
            }
        }

        if (isset($data['comment'])) {
            if (strlen($data['comment']) > 255) {
                throw new InvalidParameterException('Passed in parameter {comment} exceeds maximum length (255)');
            } else {
                $this->comment = $data['comment'];
            }
        }

        if (isset($data['concurrent'])) {
            if ($data['concurrent'] != 0 && $data['concurrent'] != 1) {
                throw new InvalidParameterException('Passed in parameter {concurrent} must be either 0 or 1');
            } else {
                $this->concurrent = $data['concurrent'];
            }
        }

        if (isset($data['started'])) {
            if ($data['started'] != 0 && $data['started'] != 1) {
                throw new InvalidParameterException('Passed in parameter {started} must be either 0 or 1');
            } else {
                $this->started = $data['started'];
            }
        }

        if (isset($data['execution_time'])) {
            if (!is_numeric($data['execution_time'])) {
                throw new InvalidParameterException('Passed in parameter {execution_time} must be integer');
            } else {
                $this->executionTime = $data['execution_time'];
            }
        }

        if (isset($data['enabled'])) {
            if ($data['enabled'] != 0 && $data['enabled'] != 1) {
                throw new InvalidParameterException('Passed in parameter {enabled} must be either 0 or 1');
            } else {
                $this->enabled = $data['enabled'];
            }
        }

        if (isset($data['delay'])) {
            if ($data['delay'] != 0 && $data['delay'] != 1) {
                throw new InvalidParameterException('Passed in parameter {delay} must be either 0 or 1');
            } else {
                $this->delay = $data['delay'];
            }
        }
    }
    #endregion

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if (!is_numeric($id)) {
            throw new InvalidParameterException('Passed in parameter {id} must be integer');
        } else {
            $this->id = $id;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if (strlen($name) > 64) {
            throw new InvalidParameterException('Passed in parameter {name} exceeds maximum length (64)');
        } else {
            $this->name = $name;
        }
    }

    public function getFrequency()
    {
        return $this->frequency;
    }

    public function setFrequency($frequency)
    {
        if (strlen($frequency) > 32) {
            throw new InvalidParameterException('Passed in parameter {frequency} exceeds maximum length (32)');
        } else {
            $this->frequency = $frequency;
        }
    }

    public function getCommand()
    {
        return $this->command;
    }

    public function setCommand($command)
    {
        if (strlen($command) > 512) {
            throw new InvalidParameterException('Passed in parameter {command} exceeds maximum length (512)');
        } else {
            $this->command = $command;
        }
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment($comment)
    {
        if (strlen($comment) > 255) {
            throw new InvalidParameterException('Passed in parameter {comment} exceeds maximum length (255)');
        } else {
            $this->comment = $comment;
        }
    }

    public function getConcurrent()
    {
        return $this->concurrent;
    }

    public function setConcurrent($concurrent)
    {
        if ($concurrent != 0 && $concurrent != 1) {
            throw new InvalidParameterException('Passed in parameter {concurrent} must be either 0 or 1');
        } else {
            $this->concurrent = $concurrent;
        }
    }

    public function getStarted()
    {
        return $this->started;
    }

    public function setStarted($started)
    {
        if ($started != 0 && $started != 1) {
            throw new InvalidParameterException('Passed in parameter {started} must be either 0 or 1');
        } else {
            $this->started = $started;
        }
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    public function setExecutionTime($executionTime)
    {
        if (!is_numeric($executionTime)) {
            throw new InvalidParameterException('Passed in parameter {execution_time} must be integer');
        } else {
            $this->executionTime = $executionTime;
        }
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        if ($enabled != 0 && $enabled != 1) {
            throw new InvalidParameterException('Passed in parameter {enabled} must be either 0 or 1');
        } else {
            $this->enabled = $enabled;
        }
    }

    public function getDelay()
    {
        return $this->delay;
    }

    public function setDelay($delay)
    {
        if ($delay != 0 && $delay != 1) {
            throw new InvalidParameterException('Passed in parameter {delay} must be either 0 or 1');
        } else {
            $this->$delay = $delay;
        }
    }
    #endregion

    #region Utils
    public function validateFrequency($frequency)
    {
        $frequencyArr = explode(' ', trim($frequency));

        if (count($frequencyArr) !== 5) {
            return false;
        }

        // Frequency validation, e.g. * * * * *
        $preg = '/^\*$|^\d{1,2}(,\d{1,2}){0,60}$|^\*\/\d{1,2}$|^\d{1,2}-\d{1,2}$/';

        foreach ($frequencyArr as $item) {
            if (preg_match($preg, $item) !== 1) {
                return false;
            }
        }

        return true;
    }
    #endregion
}
