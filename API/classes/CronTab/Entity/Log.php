<?php

namespace FS\CronTab\Entity;

use FS\Common\Exception\InvalidParameterException;

class Log
{
    #region Fields
    protected $id;
    protected $jobId;
    protected $startTime;
    protected $response;
    protected $offset;
    protected $limit;
    #endregion

    #region Construct
    public function __construct($data)
    {
        $this->id = 0;
        $this->jobId = 0;

        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['id'])) {
            if (!is_numeric($data['id'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->id = $data['id'];
            }
        }

        if (isset($data['job_id'])) {
            if (!is_numeric($data['job_id'])) {
                throw new InvalidParameterException('Passed in parameter {job_id} must be integer');
            } else {
                $this->jobId = $data['job_id'];
            }
        }

        if (isset($data['date'])) {
            if (!$this->validateDate($data['date'])) {
                throw new InvalidParameterException('Passed in parameter {date} is not a legal date. The format should be: YYYY-MM-DD');
            } else {
                $this->startTime = $data['date'];
            }
        }

        if (isset($data['offset'])) {
            if (!is_numeric($data['offset'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->offset = $data['offset'];
            }
        }

        if (isset($data['limit'])) {
            if (!is_numeric($data['limit'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be integer');
            } else {
                $this->limit = $data['limit'];
            }
        }

        if (isset($data['response'])) {
            $this->response = $data['response'];
        }
    }
    #endregion

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if (!is_numeric($id)) {
            throw new InvalidParameterException('Passed in parameter {id} must be integer');
        } else {
            $this->id = $id;
        }
    }

    public function getJobId()
    {
        return $this->jobId;
    }

    public function setJobId($jobId)
    {
        if (!is_numeric($jobId)) {
            throw new InvalidParameterException('Passed in parameter {job_id} must be integer');
        } else {
            $this->jobId = $jobId;
        }
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setStartTime($startTime)
    {
        if (!$this->validateDate($startTime)) {
            throw new InvalidParameterException('Passed in parameter {date} is not a legal date. The format should be: YYYY-MM-DD');
        } else {
            $this->startTime = $startTime;
        }
    }

    public function getOffset() {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        if (!is_numeric($offset)) {
            throw new InvalidParameterException('Passed in parameter {offset} must be integer');
        } else {
            $this->offset = $offset;
        }
    }

    public function getLimit() {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        if (!is_numeric($limit)) {
            throw new InvalidParameterException('Passed in parameter {limit} must be integer');
        } else {
            $this->limit = $limit;
        }
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }
    #endregion

    #region Utils
    private function validateDate($date)
    {
        // Check data format, e.g. YYYY-mm-dd
        $dateReg = '/(\d{4})-(\d{2})-(\d{2})/';

        return preg_match($dateReg, $date) === 1;
    }
    #endregion
}
