<?php

namespace FS\CronTab\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\CronTab\Entity\Log;

class CronLog extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log']);
    }

    #region Methods
    public function getList()
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['job_id', 'date', 'offset', 'limit']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $log = new Log($this->data);

        $this->responseArr['data'] = ['totalRowCount' => 0, 'data' => []];

        $sql = "SELECT COUNT(1) AS `total`
                    FROM `cron` 
                    WHERE `start_time` BETWEEN :start_date AND :end_date AND `job_id` = :job_id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $startDate = (new \DateTime($log->getStartTime() . ' 00:00:00', new \DateTimeZone($this->timezone)))->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');
        $endDate   = (new \DateTime($log->getStartTime() . ' 23:59:59', new \DateTimeZone($this->timezone)))->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');

        if ($stmt->execute([
                'start_date' => $startDate,
                'end_date'   => $endDate,
                'job_id'     => $log->getJobId(),
            ]) === false
        ) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $this->responseArr['data']['totalRowCount'] = $row['total'];
        }

        $sql = "SELECT `id`, `start_time`, `response` 
                    FROM `cron`
                    WHERE `start_time` BETWEEN :start_date AND :end_date AND `job_id` = :job_id
                    ORDER BY `start_time` DESC
                    LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $stmt->bindValue('start_date', $startDate, \PDO::PARAM_STR);
        $stmt->bindValue('end_date', $endDate, \PDO::PARAM_STR);
        $stmt->bindValue('job_id', intval($log->getJobId()), \PDO::PARAM_INT);
        $stmt->bindValue('offset', intval($log->getOffset()), \PDO::PARAM_INT);
        $stmt->bindValue('limit', intval($log->getLimit()), \PDO::PARAM_INT);

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        while ($row = $stmt->fetch()) {
            $this->responseArr['data']['data'][] = [
                'id'        => $row['id'],
                'startTime' => (new \DateTime($row['start_time'], new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->timezone))->format('Y-m-d H:i:s'),
                'response'  => utf8_encode($row['response'])
            ];
        }

        return $this->responseArr;
    }

    public function getLogDate()
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['job_id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $log = new Log($this->data);

        $sql = "SELECT `start_time` 
                    FROM `cron` 
                    WHERE `job_id` = :job_id 
                    ORDER BY `start_time` DESC";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute(['job_id' => $log->getJobId()]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [];

        $startTimeArr = [];

        while ($row = $stmt->fetch()) {
            $startTimeArr[] = (new \DateTime($row['start_time'], new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->timezone))->format('Y-m-d');
        }

        $startTimeArr = array_unique($startTimeArr);

        foreach ($startTimeArr as $startTime) {
            $this->responseArr['data'][] = [
                'start_time' => $startTime
            ];
        }

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['job_id', 'response']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $log = new Log($this->data);

        $sql = "INSERT INTO `cron`(`job_id`, `response`) VALUES(:job_id, :response)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute(['job_id' => $log->getJobId(), 'response' => $log->getResponse()]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }
    #endregion
}
