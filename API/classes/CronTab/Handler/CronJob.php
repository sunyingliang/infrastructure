<?php

namespace FS\CronTab\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\CronTab\Entity\Job;

class CronJob extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure']);
    }

    #region Methods
    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $job    = new Job($this->data);
        $params = '';
        $values = [];

        if (isset($this->data['name'])) {
            $params .= empty($params) ? ("`name` LIKE :name") : (" AND `name` LIKE :name");
            $values['name'] = '%' . $job->getName() . '%';
        }

        if (isset($this->data['frequency'])) {
            $params .= empty($params) ? ("`frequency` LIKE :frequency") : (" AND `frequency` LIKE :frequency");
            // Not using $job->getFrequency because it has validation that is incompatable with LIKE clause
            $values['frequency'] = '%' . $job->getFrequency() . '%';
        }

        if (isset($this->data['command'])) {
            $params .= empty($params) ? ("`command` LIKE :command") : (" AND `command` LIKE :command");
            $values['command'] = '%' . $job->getCommand() . '%';
        }

        if (isset($this->data['concurrent']) && $this->data['concurrent'] != '') {
            $params .= empty($params) ? ("`concurrent` = :concurrent") : (" AND `concurrent` = :concurrent");
            $values['concurrent'] = $job->getConcurrent();
        }

        if (isset($this->data['started'])) {
            $params .= empty($params) ? ("`started` = :started") : (" AND `started` = :started");
            $values['started'] = $job->getStarted();
        }

        if (isset($this->data['execution_time'])) {
            $params .= empty($params) ? ("`execution_time` = :execution_time") : (" AND `execution_time` = :execution_time");
            $values['execution_time'] = $job->getExecutionTime();
        }

        if (isset($this->data['enabled']) && ($this->data['enabled'] === '0' || $this->data['enabled'] === '1')) {
            $params .= empty($params) ? ("`enabled` = :enabled") : (" AND `enabled` = :enabled");
            $values['enabled'] = $job->getEnabled();
        }

        if (empty($params)) {
            $sql = "SELECT * FROM `cron_job` ORDER BY `name`";
        } else {
            $sql = "SELECT * FROM `cron_job` WHERE " . $params . " ORDER BY `name`";
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [];

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = [
                'id'             => $row['id'],
                'name'           => $row['name'],
                'frequency'      => $row['frequency'],
                'command'        => $row['command'],
                'comment'        => $row['comment'],
                'concurrent'     => $row['concurrent'],
                'started'        => $row['started'],
                'start_time'     => $row['start_time'],
                'execution_time' => $row['execution_time'],
                'enabled'        => $row['enabled'],
                'delay'          => $row['delay']
            ];
        }

        return $this->responseArr;
    }

    public function getById()
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $job = new Job($this->data);

        $sql = "SELECT *
                    FROM `cron_job`
                    WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute(['id' => $job->getId()]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [];

        if ($row = $stmt->fetch()) {
            $this->responseArr['data'] = [
                'id'             => $row['id'],
                'name'           => $row['name'],
                'frequency'      => $row['frequency'],
                'command'        => $row['command'],
                'comment'        => $row['comment'],
                'concurrent'     => $row['concurrent'],
                'started'        => $row['started'],
                'start_time'     => $row['start_time'],
                'execution_time' => $row['execution_time'],
                'enabled'        => $row['enabled'],
                'delay'          => $row['delay']
            ];
        }

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['name', 'frequency', 'command', 'concurrent', 'execution_time', 'enabled', 'delay']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $job = new Job($this->data);

        $validateFrequency = $job->validateFrequency($this->data['frequency']);
        if ($validateFrequency !== true) {
            throw new InvalidParameterException('Passed in parameter {frequency} is illegal');
        }

        $columns = "`name`, `frequency`, `command`, `comment`, `concurrent`, `execution_time`, `enabled`, `delay`";
        $params  = ":name, :frequency, :command, :comment, :concurrent, :execution_time, :enabled, :delay";
        $values  = [
            'name'           => $job->getName(),
            'frequency'      => $job->getFrequency(),
            'command'        => $job->getCommand(),
            'comment'        => $job->getComment(),
            'concurrent'     => $job->getConcurrent(),
            'execution_time' => $job->getExecutionTime(),
            'enabled'        => $job->getEnabled(),
            'delay'          => $job->getDelay()
        ];

        $sql = "SELECT COUNT(1) AS `total` FROM `cron_job` WHERE `name` = :name";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute(['name' => trim($values['name'])]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            if ($row['total'] > 0) {
                throw new \Exception('Cron job name exists');
            }
        }

        $sql = "INSERT INTO `cron_job` (" . $columns . ") VALUES (" . $params . ")";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [
            'id' => $this->pdo->lastInsertId()
        ];

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $job = new Job($this->data);

        $validateFrequency = $job->validateFrequency($this->data['frequency']);
        if ($validateFrequency !== true) {
            throw new InvalidParameterException('Passed in parameter {frequency} is illegal');
        }

        $columns = '';
        $values  = [];
        $fields  = ['name', 'frequency', 'command', 'comment', 'concurrent', 'started', 'execution_time', 'enabled', 'delay'];

        foreach ($fields as $field) {
            if (isset($this->data[$field])) {
                $columns .= empty($columns) ? ('`' . $field . '` = :' . $field) : (', `' . $field . '` = :' . $field);
                $values[$field] = $this->data[$field];
            }
        }

        if (empty($columns)) {
            throw new InvalidParameterException('No column is specified to be changed');
        }

        $values['id'] = $job->getId();

        $sql = "UPDATE `cron_job` SET " . $columns . " WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function delete()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $job = new Job($this->data);

        $sql = "DELETE FROM `cron_job` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute(['id' => $job->getId()]) === false) {
            throw new PDOExecutionException('Failed to execute sql statement');
        }

        return $this->responseArr;
    }

    public function updateStatus()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id', 'started']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $job = new Job($this->data);

        if (intval($job->getStarted()) === 1) {
            $sql = "UPDATE `cron_job` SET `started` = 1, `start_time` = NOW() WHERE `id` = :id";
        } else {
            $sql = "UPDATE `cron_job` SET `started` = 0 WHERE `id` = :id";
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute(['id' => $job->getId()]) === false) {
            throw new PDOExecutionException('Failed to execute sql statement');
        }

        return $this->responseArr;
    }
    #endregion
}
