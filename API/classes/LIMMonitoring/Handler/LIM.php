<?php

namespace FS\LIMMonitoring\Handler;

use FS\Common\IO;

class LIM extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure'], ['read']);
    }

    public function getResultCodes()
    {
        $this->responseArr['data'] = [
            'Fail',
            'Success',
            'Fail/Fail',
            'Fail/Success',
            'Success/Fail',
            'Success/Success'
        ];

        return $this->responseArr;
    }

    public function getReport()
    {
        $form     = IO::default($this->data, 'form');
        $result   = IO::default($this->data, 'result');
        $customer = IO::default($this->data, 'customer');
        $lim      = IO::default($this->data, 'lim');
        $url      = IO::default($this->data, 'url');
        $version  = IO::default($this->data, 'version');

        $searchString = '';
        $parameters   = [];

        if (!empty($form) && $form != 'All') {
            $searchString       = $this->appendSearch($searchString, 'Form = :form');
            $parameters['form'] = $form;
        }

        // Using is_null as $result can be int 0 which empty() does not catch
        if (!is_null($result) && $result != '') {
            $searchString         = $this->appendSearch($searchString, 'Result = :result');
            $parameters['result'] = $result;
        }

        if (!empty($customer)) {
            $searchString           = $this->appendSearch($searchString, 'CustomerName LIKE :customer');
            $parameters['customer'] = '%' . $customer . '%';
        }

        if (!empty($lim)) {
            $searchString      = $this->appendSearch($searchString, 'LIMName LIKE :lim');
            $parameters['lim'] = '%' . $lim . '%';
        }

        if (!empty($url)) {
            $searchString      = $this->appendSearch($searchString, 'URL LIKE :url');
            $parameters['url'] = '%' . $url . '%';
        }

        if (!empty($version)) {
            $searchString          = $this->appendSearch($searchString, 'Version LIKE :version');
            $parameters['version'] = '%' . $version . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM lim_monitoring ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT `LIMName`, `URL`, `CustomerName`, `Result`, `Version`, `BuildDate`, `Framework`, `Software`, `Runtime`, `Form`, `Response` FROM lim_monitoring "
            . $searchString
            . " ORDER BY `CustomerName`";

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }
}
