<?php

namespace FS\Proxy\Entity;

class Location
{
    public $head;
    public $proxyPass;
    public $comment;
    public $forceProxy;
    public $keepAlive;
    public $downgrade;
    public $pooled;

    function __construct($head, $proxyPass, $comment, $forceProxy, $keepAlive, $downgrade, $pooled)
    {
        $this->head       = $head;
        $this->proxyPass  = $proxyPass;
        $this->comment    = $comment;
        $this->forceProxy = $forceProxy;
        $this->keepAlive  = $keepAlive;
        $this->downgrade  = $downgrade;
        $this->pooled     = $pooled;
    }
}
