<?php

namespace FS\Proxy\Entity;

use FS\Common\Exception\InvalidParameterException;

class Mapping
{
    #region Fields
    protected $id;
    protected $path;
    protected $url;
    protected $httpDowngrade;
    protected $keepAlive;
    protected $pooled;
    protected $comment;
    protected $testUrl;
    #endregion

    public function __construct($data)
    {
        if (!is_null($data) && !is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        $this->id            = 0;
        $this->path          = '';
        $this->url           = '';
        $this->httpDowngrade = 0;
        $this->keepAlive     = 0;
        $this->pooled        = 0;
        $this->comment       = '';
        $this->testUrl       = '';

        if (isset($data['id'])) {
            if (!is_numeric($data['id'])) {
                throw new InvalidParameterException('Passed in parameter {id} must be numeric');
            }

            $this->id = $data['id'];
        }

        if (isset($data['path'])) {
            if (strlen($data['path']) > 200) {
                throw new InvalidParameterException('Passed in parameter {path} exceeds maximum length (200)');
            }

            $this->path = $data['path'];
        }

        if (isset($data['url'])) {
            if (strlen($data['url']) > 500) {
                throw new InvalidParameterException('Passed in parameter {url} exceeds maximum length (500)');
            }

            $this->url = $data['url'];
        }

        if (isset($data['http_downgrade'])) {
            if (!is_numeric($data['http_downgrade']) || ($data['http_downgrade'] != 0 && $data['http_downgrade'] != 1)) {
                throw new InvalidParameterException('Passed in parameter {http_downgrade} must be either 0 or 1');
            }

            $this->httpDowngrade = intval($data['http_downgrade']);
        }

        if (isset($data['keep_alive'])) {
            if (!is_numeric($data['keep_alive']) || ($data['keep_alive'] != 0 && $data['keep_alive'] != 1)) {
                throw new InvalidParameterException('Passed in parameter {keep_alive} must be either 0 or 1');
            }

            $this->keepAlive = intval($data['keep_alive']);
        }

        /*
        if (isset($data['pooled'])) {
            if (!is_numeric($data['pooled']) || ($data['pooled'] != 0 && $data['pooled'] != 1)) {
                throw new InvalidParameterException('Passed in parameter {pooled} must be either 0 or 1');
            }

            $this->pooled = intval($data['pooled']);
        }
        */

        $this->pooled = $this->keepAlive;

        if (isset($data['comment'])) {
            if (strlen($data['comment']) > 200) {
                throw new InvalidParameterException('Passed in parameter {comment} exceeds maximum length (200)');
            }

            $this->comment = $data['comment'];
        }

        if (isset($data['test_url'])) {
            if (strlen($data['test_url']) > 200) {
                throw new InvalidParameterException('Passed in parameter {test_url} exceeds maximum length (200)');
            }

            $this->testUrl = $data['test_url'];
        }
    }

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getHttpDowngrade()
    {
        return $this->httpDowngrade;
    }

    public function setHttpDowngrade($httpDowngrade)
    {
        $this->httpDowngrade = $httpDowngrade;
    }

    public function getKeepAlive()
    {
        return $this->keepAlive;
    }

    public function setKeepAlive($keepAlive)
    {
        $this->keepAlive = $keepAlive;
    }

    public function getPooled()
    {
        return $this->pooled;
    }

    public function setPooled($pooled)
    {
        $this->pooled = $pooled;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getTestUrl()
    {
        return $this->testUrl;
    }

    public function setTestUrl($testUrl)
    {
        $this->testUrl = $testUrl;
    }
    #endregion
}
