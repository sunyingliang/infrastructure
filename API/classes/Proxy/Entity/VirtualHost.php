<?php

namespace FS\Proxy\Entity;

class VirtualHost
{
    protected $cT;
    protected $cCRLF;
    protected $cCRLFT;
    protected $head;
    protected $locations;

    public function __construct()
    {
        $this->cT     = "\t";
        $this->cCRLF  = "\r\n";
        $this->cCRLFT = $this->cCRLF . $this->cT;
        $this->head   = $this->cCRLFT . 'DocumentRoot /var/www/Proxy' . $this->cCRLFT .
            'SSLProxyEngine on' . $this->cCRLFT .
            'SSLProxyCheckPeerName off' . $this->cCRLFT .
            'SSLProxyVerify none' . $this->cCRLFT .
            'SSLProxyCheckPeerCN off' . $this->cCRLFT .
            'SSLProxyCheckPeerExpire off' . $this->cCRLFT;
    }

    #region Methods
    public function addLocation($location)
    {
        $this->locations[] = $location;
    }

    public function generateOutput()
    {
        return htmlentities($this->generateXML());
    }
    #endregion

    #region Utils
    public function generateXML()
    {
        $result = $this->head . $this->cCRLF;

        foreach ($this->locations as $item) {
            if (isset($item->comment) && !empty($item->comment)) {
                $result .= $this->cCRLFT . '#' . $item->comment;
            }

            $result .= $this->cCRLFT . '<Location /' . stripslashes($item->head) . '>';
            $result .= $this->cCRLFT . $this->cT . 'ProxyPass ' . $item->proxyPass . ' retry=0 disablereuse=On keepalive=';

            if ($item->keepAlive == 1) {
                $result .= 'On';
			}
			else {
				$result .= 'Off';
            }

            if ($item->downgrade == 1) {
				$result .= $this->cCRLFT . $this->cT . 'SetEnv force-proxy-request-1.0 1';
                $result .= $this->cCRLFT . $this->cT . 'SetEnv downgrade-1.0 1';
            }

            $result .= $this->cCRLFT . $this->cT . 'SetEnv proxy-initial-not-pooled 1';
            $result .= $this->cCRLFT . '</Location>' . $this->cCRLF;
        }

        return $result;
    }
    #endregion
}
