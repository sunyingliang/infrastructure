<?php

namespace FS\Proxy\Handler;

use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOQueryException;
use FS\Common\IO;
use FS\Common\Parser;
use FS\Proxy\Entity;

class Mapping extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure']);
    }

    #region APIs
    public function getConfig()
    {
        $this->auth->hasPermission(['read']);

        $virtualHost = new Entity\VirtualHost();

        $sql = "SELECT COUNT(1) AS `total` FROM `proxy_mapping`";

        $stmt = $this->pdo->query($sql);

        if ($stmt === false) {
            throw new PDOQueryException('PDO query failed');
        }

        $row = $stmt->fetch();

        if ($row === false || $row['total'] == 0) {
            throw new \Exception('There is no mapping entry in database');
        }

        $sql = "SELECT * FROM `proxy_mapping` ORDER BY `id`";

        $stmt = $this->pdo->query($sql);

        if ($stmt === false) {
            throw new PDOQueryException('PDO query failed');
        }

        while ($row = $stmt->fetch()) {
            $virtualHost->addLocation(new Entity\Location(
                Parser::parsePath($row['path']),
                Parser::parseURL($row['url']),
                Parser::parseComment($row['comment']),
                intval($row['http_downgrade']),
                intval($row['keep_alive']),
                intval($row['http_downgrade']),
                intval($row['pooled'])
            ));
        }

        $this->responseArr['data'] = $virtualHost->generateOutput();

        return $this->responseArr;
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $params     = '';
        $values     = [];
        $limits     = '';
        $optionals  = ['path', 'url', 'comment', 'pooled'];
        $sort       = IO::default($this->data, 'sort', 'id');
        $sortOrder  = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['path', 'url',  'comment', 'keep_alive', 'http_downgrade'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        foreach ($optionals as $optional) {
            if (isset($_POST[$optional]) && !empty($_POST[$optional])) {
                $params .= empty($params) ? ('`' . $optional . '` LIKE :' . $optional) : (' AND `' . $optional . '` LIKE :' . $optional);
                $values[$optional] = '%' . $_POST[$optional] . '%';
            }
        }

        if (isset($_POST['http_downgrade']) && !is_null($_POST['http_downgrade']) && $_POST['http_downgrade'] != '') {
            $params .= empty($params) ? ('`http_downgrade` = :http_downgrade') : (' AND `http_downgrade` = :http_downgrade');
            $values['http_downgrade'] = $_POST['http_downgrade'];
        }

        if (isset($_POST['keep_alive']) && !is_null($_POST['keep_alive'])  && $_POST['keep_alive'] != '') {
            $params .= empty($params) ? ('`keep_alive` = :keep_alive') : (' AND `keep_alive` = :keep_alive');
            $values['keep_alive'] = $_POST['keep_alive'];
        }

        if (isset($_POST['offset']) && isset($_POST['limit'])) {
            $limits .= ' LIMIT :offset, :limit';
        }

        $sql = "SELECT COUNT(1) AS `total` FROM `proxy_mapping`";

        if (!empty($params)) {
            $sql .= " WHERE " . $params;
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        foreach ($values as $column => $value) {
            $stmt->bindValue($column, $value);
        }

        if ($stmt->execute() === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [];

        if ($row = $stmt->fetch()) {
            $this->responseArr['data']['totalRowCount'] = $row['total'];
        }

        $sql = "SELECT * FROM `proxy_mapping`";

        if (!empty($params)) {
            $sql .= " WHERE " . $params;
        }

        $sql .= " ORDER BY " . $sort . " " . $sortOrder;

        if (!empty($limits)) {
            $sql .= $limits;
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        foreach ($values as $column => $value) {
            $stmt->bindValue($column, $value);
        }

        if (!empty($limits)) {
            $stmt->bindValue('offset', intval($this->data['offset']), \PDO::PARAM_INT);
            $stmt->bindValue('limit', intval($this->data['limit']), \PDO::PARAM_INT);
        }

        if ($stmt->execute() === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        $this->responseArr['data']['rows'] = [];

        while ($row = $stmt->fetch()) {
            $this->responseArr['data']['rows'][] = [
                'id'             => $row['id'],
                'path'           => $row['path'],
                'http_downgrade' => $row['http_downgrade'],
                'keep_alive'     => $row['keep_alive'],
                'pooled'         => $row['pooled'],
                'url'            => $row['url'],
                'comment'        => $row['comment'],
                'test_url'       => $row['test_url']
            ];
        }

        return $this->responseArr;
    }

    public function search()
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['path']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $mapping = new Entity\Mapping($this->data);

        $searchString = '';
        $parameters   = [];

        if (!empty($mapping->getPath())) {
            $searchString       = $this->appendSearch($searchString, '`path` LIKE :path');
            $parameters['path'] = '%' . $mapping->getPath() . '%';
        }
        if (!empty($mapping->getUrl())) {
            $searchString       = $this->appendSearch($searchString, '`url` LIKE :url');
            $parameters['url'] = '%' . $mapping->getUrl() . '%';
        }
        if (!empty($mapping->getComment())) {
            $searchString       = $this->appendSearch($searchString, '`comment` LIKE :comment');
            $parameters['comment'] = '%' . $mapping->getComment() . '%';
        }

        $sql = "SELECT COUNT(1) AS `total` FROM `proxy_mapping`" . $searchString;
        
        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($parameters) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }
        
        $this->responseArr['data'] = [];

        if ($row = $stmt->fetch()) {
            $this->responseArr['data']['totalRowCount'] = $row['total'];
        }

        $sql = "SELECT * FROM `proxy_mapping` " . $searchString . " ORDER BY `path`";
        
        if (isset($this->data['offset']) && isset($this->data['limit'])) {
            $sql .= ' LIMIT :offset, :limit';
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        $stmt = $this->bindParams($stmt, $parameters);
        
        if (isset($this->data['offset']) && isset($this->data['limit'])) {
            $stmt->bindValue('offset', intval($this->data['offset']), \PDO::PARAM_INT);
            $stmt->bindValue('limit', intval($this->data['limit']), \PDO::PARAM_INT);
        }

        if ($stmt->execute() === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        $this->responseArr['data']['rows'] = [];

        while ($row = $stmt->fetch()) {
            $this->responseArr['data']['rows'][] = [
                'id'             => $row['id'],
                'path'           => $row['path'],
                'http_downgrade' => $row['http_downgrade'],
                'keep_alive'     => $row['keep_alive'],
                'pooled'         => $row['pooled'],
                'url'            => $row['url'],
                'comment'        => $row['comment'],
                'test_url'       => $row['test_url']
            ];
        }

        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['path', 'url', 'http_downgrade']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $mandatory = ['path', 'url', 'http_downgrade'];
        $count     = 0;

        $mapping = new Entity\Mapping($this->data);

        $sql = "SELECT COUNT(1) AS `total` FROM `proxy_mapping` WHERE `path` = :path";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['path' => $mapping->getPath()]) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $count = $row['total'];
        }

        if (intval($count) > 0) {
            throw new DBDuplicationException('Mapping already exists for path: ' . $mapping->getPath());
        }

        $columns = '';
        $params  = '';
        $values  = [];

        foreach ($mandatory as $column) {
            $columns .= empty($columns) ? ('`' . $column . '`') : (', `' . $column . '`');
            $params .= empty($params) ? (':' . $column) : (', :' . $column);
            $values[$column] = $this->data[$column];
        }

        if (isset($this->data['keep_alive'])) {
            $columns .= ', `keep_alive`';
            $params .= ', :keep_alive';
            $values['keep_alive'] = $mapping->getKeepAlive();
        }

        if (isset($this->data['pooled'])) {
            $columns .= ', `pooled`';
            $params .= ', :pooled';
            $values['pooled'] = $mapping->getPooled();
        }

        if (isset($this->data['test_url'])) {
            $columns .= ', `test_url`';
            $params .= ', :test_url';
            $values['test_url'] = $mapping->getTestUrl();
        }

        if (isset($this->data['comment'])) {
            $columns .= ', `comment`';
            $params .= ', :comment';
            $values['comment'] = $mapping->getComment();
        }

        $sql = "INSERT INTO `proxy_mapping`(" . $columns . ") VALUES(" . $params . ")";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['path', 'url', 'http_downgrade']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $mandatory = ['path', 'url', 'http_downgrade'];

        $mapping = new Entity\Mapping($this->data);

        $id = 0;

        $sql = "SELECT `id` FROM `proxy_mapping` WHERE `path` = :path";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['path' => $mapping->getPath()]) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $id = $row['id'];
        }

        if (empty($id)) {
            throw new DBDuplicationException('No mapping found for path: ' . $mapping->getPath() . ' cannot update');
        }

        $columns = '';
        $values  = ['id' => $id];

        foreach ($mandatory as $column) {
            $columns .= empty($columns) ? ('`' . $column . '` = :' . $column) : (', `' . $column . '` = :' . $column);
            $values[$column] = $this->data[$column];
        }

        if (isset($this->data['keep_alive'])) {
            $columns .= ', `keep_alive` = :keep_alive';
            $values['keep_alive'] = $mapping->getKeepAlive();
        }

        if (isset($this->data['pooled'])) {
            $columns .= ', `pooled` = :pooled';
            $values['pooled'] = $mapping->getPooled();
        }

        if (isset($this->data['test_url'])) {
            $columns .= ', `test_url` = :test_url';
            $values['test_url'] = $mapping->getTestUrl();
        }

        if (isset($this->data['comment'])) {
            $columns .= ', `comment` = :comment';
            $values['comment'] = $mapping->getComment();
        }

        $sql = "UPDATE `proxy_mapping` SET " . $columns . " WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function delete()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['path']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $mapping = new Entity\Mapping($this->data);

        $id = 0;

        $sql = "SELECT `id` FROM `proxy_mapping` WHERE `path` = :path";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['path' => $mapping->getPath()]) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $id = $row['id'];
        }

        if (empty($id)) {
            throw new DBDuplicationException('No mapping found for path: ' . $mapping->getPath() . ' cannot delete');
        };

        $sql = "DELETE FROM `proxy_mapping` WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }
    #endregion
}
