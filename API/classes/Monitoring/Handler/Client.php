<?php

namespace FS\Monitoring\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;

class Client extends \FS\InfrastructureBase
{
    public function __construct()
    {
        set_time_limit(50);

        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    public function listUrl()
    {
        $this->auth->hasPermission(['read']);

        $sql = "SELECT `id`, `url`, `successcriteria` 
                FROM `monitoring` 
                WHERE `state` = 'on'";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        $this->responseArr['data'] = [];

        while ($row = $stmt->fetch()) {
            $this->responseArr['data'][] = $row;
        }

        if (count($this->responseArr['data']) === 0) {
            $this->responseArr['message'] = 'No data is retrieved from API.';
        }

        return $this->responseArr;
    }

    public function reportStatistics()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id', 'host', 'status', 'delay'], true))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $sqlSelect = "SELECT COUNT(1) AS `total` 
                      FROM `monitor_host` 
                      WHERE `monitoring_id` = " . intval($this->data['id']) . " 
                          AND `host` = '" . $this->data['host'] . "'";

        if (($stmtSelect = $this->pdo->query($sqlSelect)) === false) {
            throw new PDOCreationException('PDO query failed');
        }

        $rowSelect = $stmtSelect->fetch();

        if ($rowSelect['total'] > 0) {
            // Update respective item
            $sqlUpdate = "UPDATE `monitor_host` 
                          SET `updated` = NOW(), `status` = :status, `delay` = :delay, `failed_counter` = " . ($this->data['status'] == 1 ? '0' : '`failed_counter` + 1') . " 
                          WHERE `monitoring_id` = :id 
                              AND `host` = :host";

            $stmtUpdate = $this->pdo->prepare($sqlUpdate);

            if ($stmtUpdate === false) {
                throw new PDOCreationException('PDOStatement prepare failed.');
            }

            if ($stmtUpdate->execute([
                    ':status' => $this->data['status'],
                    ':delay'  => $this->data['delay'],
                    ':id'     => $this->data['id'],
                    ':host'   => $this->data['host']
                ]) === false
            ) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlUpdate);
            }
        } else {
            $sqlInsert = "INSERT INTO `monitor_host`(`monitoring_id`, `host`, `status`, `delay`, `failed_counter`) 
                          VALUES (:id, :host, :status, :delay, :failed_counter)";

            $stmtInsert = $this->pdo->prepare($sqlInsert);

            if ($stmtInsert === false) {
                throw new PDOCreationException('PDOStatement prepare failed.');
            }

            if ($stmtInsert->execute([
                    ':id'             => $this->data['id'],
                    ':host'           => $this->data['host'],
                    ':status'         => $this->data['status'],
                    ':delay'          => $this->data['delay'],
                    ':failed_counter' => $this->data['status'] == 1 ? 0 : 1
                ]) === false
            ) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlInsert);
            }
        }

        return $this->responseArr;
    }
}
