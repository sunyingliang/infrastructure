<?php

namespace FS\Monitoring\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\FileNotReadableException;
use FS\Common\Exception\FileReadException;
use FS\Common\IO;

class Image extends \FS\InfrastructureBase
{
    private $cachePath     = __DIR__ . '/../../../cache/';
    private $fileExtension = '.png';

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring'], ['read']);
    }

    public function getImage()
    {
        if (!($validation = IO::required($this->data, ['id', 'type'], true))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $path = $this->cachePath . $this->data['id'] . $this->data['type'] . $this->fileExtension;

        if (!file_exists($path)) {
            throw new FileNotReadableException('The requested file does not exist');
        }

        if (($file = file_get_contents($path)) === false) {
            throw new FileReadException('Failed to get the content of the requested file');
        }

        $this->responseArr['data'] = base64_encode($file);

        return $this->responseArr;
    }
}
