<?php

namespace FS\Monitoring\Handler;

use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\InvalidParameterException;

class SwitchState extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring'], ['write']);
    }

    public function update()
    {
        if (isset($this->data['pname'])) {
            // Required parameters: pname [$string], enabled [true,false]
            // Optional parameters: type [monitoring, monitor_server, server_names]
            if (!($validation = IO::required($this->data, ['pname', 'enabled'], true))['valid']) {
                throw new InvalidParameterException($validation['message']);
            }

            if (!in_array($this->data['enabled'], ['true', 'false'])) {
                throw new InvalidParameterException('Passed in parameter {enabled} is not set properly. Only true and false are valid values.');
            }

            $enabled = (strtolower($this->data['enabled']) === 'true') ? 1 : 0;
            $state   = (strtolower($this->data['enabled']) === 'true') ? 'on' : 'off';
            $type    = isset($this->data['type']) && !empty($this->data['type']) ? $this->data['type'] : '';

            if (empty($type) || $type === 'monitor_server') {
                $sql = "UPDATE `monitor_server` 
                        SET `enabled` = :enabled 
                        WHERE `prettyname` = :prettyname";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute(['enabled' => $enabled, 'prettyname' => $this->data['pname']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
            }

            if (empty($type) || $type === 'monitoring') {
                $sql = "UPDATE `monitoring` 
                        SET `state` = :state 
                        WHERE `prettyname` = :prettyname";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute(['state' => $state, 'prettyname' => $this->data['pname']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
            }

            if (empty($type) || $type === 'server_names') {
                $basename =  trim(str_replace(range(0,9),'',$this->data['pname']));
                $sql = "UPDATE `server_names` 
                        SET `enabled` = :enabled 
                        WHERE `basename` = :basename";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute(['enabled' => $enabled, 'basename' => $basename]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
            }
        } else if (isset($this->data['name'])) {
            // Deprecated code (included for ease of migration)
            if (!($validation = IO::required($this->data, ['name', 'enabled'], true))['valid']) {
                throw new InvalidParameterException($validation['message']);
            }

            if (!in_array($this->data['enabled'], ['true', 'false'])) {
                throw new InvalidParameterException('Passed in parameter {enabled} is not set properly. Only true and false are valid values.');
            }

            $enabled = (strtolower($this->data['enabled']) === 'true') ? 1 : 0;

            $sql = "UPDATE `monitor_server` 
                    SET `enabled` = :enabled 
                    WHERE `name` = :name";

            $stmt = $this->pdo->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute(['enabled' => $enabled, 'name' => $this->data['name']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
            }
        } else {
            // Deprecated code (included for ease of migration)
            if (!($validation = IO::required($this->data, ['guid', 'state'], true))['valid']) {
                throw new InvalidParameterException($validation['message']);
            }

            if (!in_array($this->data['state'], ['on', 'off'])) {
                throw new InvalidParameterException('Passed in parameter {state} is not set properly. Only on and off are valid values.');
            }

            $sql = "UPDATE `monitoring` 
                    SET `state` = :state 
                    WHERE `guid` = :guid 
                        AND `state` IS NOT NULL";

            $stmt = $this->pdo->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute(['state' => $this->data['state'], 'guid' => $this->data['guid']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
            }
        }

        return $this->responseArr;
    }
}
