<?php

namespace FS\Monitoring\Handler;

use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;

class ToggleState extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring'], ['write']);
    }

    public function update()
    {
        $group = isset($this->data['group']) ? strtolower($this->data['group']) : 'app';

        if ($group !== 'sql') {
            $group = 'app';
        }

        $sql = "UPDATE `monitor_server` 
                SET `group` = :group 
                WHERE `prettyname` IN ('SQLDump', 'CouchDB', 'CouchDB2')";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute(['group' => $group]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        return $this->responseArr;
    }
}
