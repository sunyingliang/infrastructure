<?php

namespace FS\Monitoring\Handler;

use FS\Common\Exception\CURLException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\Monitoring\Entity\PingdomCache;

class Page extends \FS\InfrastructureBase
{
    #region Fields
    protected $errorCount          = 0;
    protected $hasSeriousError     = 0;
    protected $hasVerySeriousError = 0;
    protected $htmlDelimiter       = 'FS_DELIMITER_HTML';
    protected $delayThreshold      = 15;
    #endregion

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring'], ['read']);
    }

    public function getHtml() {
        $responseHeader = $this->getHeaderHtml();
        $responseMain   = $this->getMainHtml();
        $responseTag    = $this->getAdditionalHtml();
        $responseHTML   = $responseHeader . $this->htmlDelimiter . $responseMain . $this->htmlDelimiter . $responseTag;

        $this->responseArr['data'] = $responseHTML;

        return $this->responseArr;
    }

    #region API Methods
    // For getHeaderHtml
    public function getHeaderHtml()
    {
        try {
            // Construct header part1
            $body = '<div><table class="delays-table"><tbody>';
            $body .= '<tr><th></th>';

            $sql = "SELECT `id`, `url`, `prettyname`, `colour` 
                    FROM `monitoring` 
                    WHERE `monitoring`.`type` IS NULL 
                        AND `state` = 'on' 
                    ORDER BY `sequence` ASC";

            $stmt = $this->pdo->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute() === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
            }

            $monitoredMachines = $stmt->fetchAll();

            foreach ($monitoredMachines as $monitoredMachine) {
                $name   = empty($monitoredMachine['prettyname']) ? $monitoredMachine['url'] : $monitoredMachine['prettyname'];
                $colour = empty($monitoredMachine['colour']) ? 'FFFFFF' : $monitoredMachine['colour'];

                $body .= '<th style="width:10em;background-color:#' . $colour . ';border:1px solid gray; color: #efefef;font-size:100%">';
                $body .= $name;
                $body .= '</th>';
            }

            $body .= '</tr>';

            $sqlPollingMachines = "SELECT `host` FROM `monitor_host` GROUP BY `host` ORDER BY `host` ASC";

            $stmt = $this->pdo->prepare($sqlPollingMachines);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute() === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlPollingMachines);
            }

            $pollingMachines = $stmt->fetchAll();

            $errorHosts = [];

            foreach ($pollingMachines as $pollingMachine) {
                $body .= '<tr><td>' . $pollingMachine['host'] . '</td>';

                foreach ($monitoredMachines as $monitoredMachine) {
                    $id       = $monitoredMachine['id'];
                    $sqlDelay = "SELECT `delay`, `status`, `updated`, NOW() AS `now`
                                 FROM `monitor_host`
                                 WHERE `monitoring_id` = :monitoring_id 
                                     AND `host` = :host";

                    $stmt = $this->pdo->prepare($sqlDelay);

                    if ($stmt === false) {
                        throw new PDOCreationException('PDOStatement creation failed.');
                    }

                    if ($stmt->execute([
                            'monitoring_id' => $id,
                            'host'          => $pollingMachine['host']
                        ]) === false
                    ) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDelay);
                    }

                    if ($rowDelay = $stmt->fetch()) {
                        $delayStatus = '';

                        if ($this->isStale($rowDelay['now'], $rowDelay['updated'])) {
                            $delayStatus = 'isstale';   
                        }

                        if ($rowDelay['delay'] >= 5) {
                            $delayStatus           = 'ispoor';
                            $this->hasSeriousError = 1;
                        }

                        if ($rowDelay['status'] != 1 || $rowDelay['delay'] >= $this->delayThreshold) {
                            $delayStatus     = 'isverypoor';
                            $errorHosts[$id] = isset($errorHosts[$id]) ? $errorHosts[$id] + 1 : 1;
                        }

                        $body .= '<td class="delay result ' . $delayStatus . '">';
                        $body .= number_format($rowDelay['delay'], 2) . 's' . ($rowDelay['status'] == 0 ? '[!]' : '');
                        $body .= '</td>';
                    } else {
                        $body .= '<td class="delay result nodata"></td>';
                    }
                }
                $body .= '</tr>';
            }

            $body .= '<tr class="row"><td>Pingdom</td>';

            foreach ($errorHosts as $object => $value) {
                if ($value > 1) {
                    $this->hasVerySeriousError = 1;
                    break;
                }
            }

            $sqlPingdom = "SELECT * 
                           FROM `pingdom_cache` 
                           WHERE SUBSTRING(`name`,1,8) = ? 
                               OR CONCAT('http://', `hostname`) = SUBSTRING(?, 1, LENGTH(`hostname`) + 7) 
                               OR CONCAT('https://', `hostname`) = SUBSTRING(?, 1, LENGTH(`hostname`) + 8)";

            $stmt = $this->pdo->prepare($sqlPingdom);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            foreach ($monitoredMachines as $monitoredMachine) {
                if ($stmt->execute([
                        $monitoredMachine['prettyname'],
                        $monitoredMachine['url'],
                        $monitoredMachine['url']
                    ]) === false
                ) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlPingdom);
                }

                $pingdomStatus = '';
                $pingdomTime   = '&#160;';

                if ($rowPingdom = $stmt->fetch()) {
                    if ($rowPingdom['lastresponsetime'] > 3000) {
                        $pingdomStatus = 'ispoor';
                    }

                    if ($rowPingdom['status'] != 'up') {
                        $pingdomStatus = 'isverypoor';
                    }

                    if (!empty($pingdomStatus)) {
                        $this->errorCount++;
                    }

                    $pingdomTime = number_format($rowPingdom['lastresponsetime'] / 1000, 2) . 's';
                } else {
                    $pingdomStatus = 'isempty';
                }

                $body .= '<td class="delay result ' . $pingdomStatus . '">' . $pingdomTime . '</td>';
            }

            $body .= '</tr>';
            $body .= '</tbody></table></div>';

            $body .= $this->renderSQSTable();
            // Construct header part2
            $body .= '<div><table class="header-part2" cellpadding="0" cellspacing="0"><tbody><tr class="part2-tr">';

            $sqlPingdomAll = "SELECT `pingdom_cache`.`status`, `pingdom_cache`.`name`, `pingdom_cache`.`lastresponsetime` 
                              FROM `pingdom_cache` 
                                  LEFT JOIN `monitoring` ON SUBSTRING(`pingdom_cache`.`name`,1,8) = `monitoring`.`prettyname` 
                                      OR SUBSTRING(`pingdom_cache`.`name`,1,8) = CONCAT(`monitoring`.`prettyname`, '.') 
                                      OR CONCAT('http://', `pingdom_cache`.`hostname`) = SUBSTRING(`monitoring`.`url`, 1, LENGTH(`pingdom_cache`.`hostname`)+7) 
                                      OR CONCAT('https://', `pingdom_cache`.`hostname`) = SUBSTRING(`monitoring`.`url`, 1, LENGTH(`hostname`)+8) 
                              WHERE (`monitoring`.`state` = 'on' OR `monitoring`.`state` IS NULL)
                                  AND `pingdom_cache`.`status` != 'paused'
                              ORDER BY `pingdom_cache`.`name` ASC";

            $stmt = $this->pdo->prepare($sqlPingdomAll);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute() === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlPingdomAll);
            }

            $pingdomCount = 0;

            while ($rowPingdomAll = $stmt->fetch()) {
                $pingdomCount++;

                if ($rowPingdomAll['status'] == 'down') {
                    $this->hasVerySeriousError = 1;
                }

                if ($rowPingdomAll['status'] != 'up') {
                    $this->errorCount++;
                }

                $body .= '<td><span class="status ' . $rowPingdomAll['status'] . '"></span></td>';
                $body .= '<td style="font-size:1em; white-space:nowrap;">' . substr($rowPingdomAll['name'], 0, 10) . '</td>';
                $body .= '<td style="font-size:1em; white-space:nowrap;">' . $rowPingdomAll['lastresponsetime'] . 'ms</td>';

                if ($pingdomCount % 12 == 0) {
                    $body .= '</tr><tr>';
                }
            }

            $body .= '</tr></tbody></table></div>';
        } catch (\Exception $e) {
            $body = 'Failed to generate header html for default page.' . $e->getMessage();
        }
        
        return $body;
    }

    // For getMainHtml
    public function getMainHtml()
    {
        $body = '';

        try {
            $sqlGroups = "SELECT `group`, SUM(`email`) AS `email`, SUM(`session`) AS `session` 
                          FROM `monitor_server_cache`
                          GROUP BY `group` 
                          ORDER BY `group` ASC";

            $stmt = $this->pdo->prepare($sqlGroups);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute() === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlGroups);
            }

            // Group width = 100% / Column count
            $rowGroups = $stmt->fetchAll();
            $columnWidth = 100 / count($rowGroups);

            foreach($rowGroups as $rowGroup) {
                $sqlDownServers = "SELECT `monitor_server_cache`.`name`, `monitor_server_cache`.`prettyname`, `monitor_server_cache`.`group` 
                                   FROM `monitor_server_cache` 
                                   WHERE `monitor_server_cache`.`group` = :group 
                                       AND `monitor_server_cache`.`last_time` IS NULL
                                       AND `monitor_server_cache`.`name` NOT LIKE '%old' 
                                       AND `monitor_server_cache`.`prettyname` NOT LIKE 'STAG-12%'
                                       AND `monitor_server_cache`.`auto_scaled` = 0
                                   ORDER BY `monitor_server_cache`.`prettyname` ASC";

                $stmtDownServers = $this->pdo->prepare($sqlDownServers);

                if ($stmtDownServers === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmtDownServers->execute(['group' => $rowGroup['group']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDownServers);
                }

                $downMessage = 'Missing server: ';
                $downServers = '';

                while ($rowDownServer = $stmtDownServers->fetch()) {
                    if (!empty($downServers)) {
                        $downServers .= ',';
                    }

                    $downServers .= $rowDownServer['prettyname'];
                }

                $body .= '<td class="server-group" style="width: ' . $columnWidth . '%"><div>';

                if (!empty($downServers)) {
                    $body .= '<div style="color:red;font-size:1.5em;text-align:center;width:100%;">';
                    $body .= $downMessage . $downServers;
                    $body .= '</div>';
                }

                $body .= '<table class="servers">';
                $body .= '<colgroup>';
                $body .= '<col />';
                $body .= '<col style="border: 1px solid gray;" />';
                $body .= '<col style="border: 1px solid gray;" />';
                $body .= '<col style="border: 1px solid gray;" />';
                $body .= '<col style="border: 1px solid gray;" />';
                $body .= '<col style="border: 1px solid gray;" />';
                $body .= '</colgroup>';
                $body .= '<tr><td></td><td class="title" colspan="5">' . strtoupper($rowGroup['group']) . '</td></tr>';
                $body .= '<tr>';
                $body .= '<th></th>';
                $body .= '<th>CPU</th>';
                $body .= '<th>MEM</th>';

                if (is_numeric($rowGroup['email'])) {
                    $body .= '<th title="EMail">E</th>';
                }

                if (is_numeric($rowGroup['session'])) {
                    $body .= '<th title="Session">S</th>';
                }

                $body .= '<th>DISK</th>';
                $body .= '</tr>';

                $sqlServers = "SELECT `monitor_server_cache`.*, NOW() AS `now`
                               FROM `monitor_server_cache` 
                               WHERE `monitor_server_cache`.`group` = :group
                                   AND`monitor_server_cache`.`last_time` IS NOT NULL
                               ORDER BY `monitor_server_cache`.`prettyname` ASC";

                $stmtSQLServers = $this->pdo->prepare($sqlServers);

                if ($stmtSQLServers === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmtSQLServers->execute(['group' => $rowGroup['group']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlServers);
                }

                while ($rowServer = $stmtSQLServers->fetch()) {
                    $memChart      = '';
                    $cpuChart      = '';
                    $cpuLast       = -1;
                    $atLast        = 0;
                    $memLast       = 0;
                    $cpuCurrent    = 0;
                    $timeDiff      = 0;
                    $memWarn       = '';
                    $sessionLast   = -1;
                    $emailLast     = -1;
                    $memTotla      = 34000;
                    $goodDrives    = '';
                    $okDrives      = '';
                    $diskTitleGood = '';
                    $diskTitleOk   = '';

                    if ($rowGroup['group'] == 'sql') {
                        $warnThreshold  = 980000;
                        $errorThreshold = 1000000;
                    } else {
                        $warnThreshold  = 820000;
                        $errorThreshold = 940000;
                    }

                    if ($rowServer['mem_last'] > $rowServer['mem_total'] * $warnThreshold) {
                        $memWarn = 'warn';
                    }

                    if ($rowServer['mem_last'] > $rowServer['mem_total'] * $errorThreshold) {
                        $memWarn = 'badbadbad';
                    }

                    if (substr_count($rowServer['name'], '-') > 1) {
                        $rowTitle = strtoupper(substr($rowServer['name'], strpos($rowServer['name'], '-') + 1, strlen($rowServer['name'])));
                    } else {
                        $rowTitle = $rowServer['name'];
                    }

                    $body .= '<tr>';
                    $body .= '<td class="name" title="' . $rowTitle . '">' . $rowServer['prettyname'];

                    if ($this->isStale($rowServer['now'], $rowServer['last_time'])) {
                        $body .= '<br/><span style="color:red; font-size: 0.9em;">Updated: ' . (new \DateTime($rowServer['last_time'], new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->timezone))->format('H:i:s') . '</span>';
                    }

                    $body .= '</td>';

                    $body .= '<td class="cpuchart">';
                    $body .= '<span style="' . ($rowServer['cpu_current'] > 50 ? 'color:red' : '') . '" class="cpu">' . round($rowServer['cpu_current'], 1) . '%</span>';
                    $body .= '<img src="/monitoring_image.php?i=' . base64_encode($rowServer['id']) . '&t=' . base64_encode('c') . '&r=' . time() . (isset($this->data['fam']) ? '&fam=' . $this->data['fam'] : '') . (isset($this->data['famToken']) ? '&token=' . $this->data['famToken'] : '') . '" />';
                    $body .= '</td>';

                    $body .= '<td class="memchart">';
                    $body .= '<span class="mem ' . $memWarn . ' ">' . $this->humanReadable($rowServer['mem_last']) . '</span>';
                    $body .= '<img src="/monitoring_image.php?i=' . base64_encode($rowServer['id']) . '&t=' . base64_encode('m') . '&r=' . time() . (isset($this->data['fam']) ? '&fam=' . $this->data['fam'] : '') . (isset($this->data['famToken']) ? '&token=' . $this->data['famToken'] : '') . '" />';
                    $body .= '</td>';
                    
                    if (is_numeric($rowGroup['email'])) {
                        $body .= '<td class="emailcount">' . ($rowServer['email'] >= 0 ? $rowServer['email'] : '') . '</td>';
                    }

                    if (is_numeric($rowGroup['session'])) {
                        $body .= '<td class="sessioncount">' . ($rowServer['session'] >= 0 ? $rowServer['session'] : '') . '</td>';
                    }

                    $body .= '<td class="disk">';

                    // Disk cell
                    $sqlDisk = "SELECT * 
                                FROM `monitor_server_disk_warn_cache` 
                                WHERE `monitor_server_cache_id` = :monitor_server_cache_id";                                   
                    
                    $stmtDisk = $this->pdo->prepare($sqlDisk);

                    if ($stmtDisk === false) {
                        throw new PDOCreationException('PDOStatement creation failed.');
                    }

                    if ($stmtDisk->execute(['monitor_server_cache_id' => $rowServer['id']]) === false) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDisk);
                    }

                    while ($rowDisk = $stmtDisk->fetch()) {
                        $diskWarn  = '';
                        $diskUsage = round($rowDisk['usage'], 1);

                        if ($diskUsage >= 80) {
                            $diskWarn = 'earlywarn';
                        }

                        if ($diskUsage >= 90) {
                            $diskWarn              = 'warn';
                            $this->hasSeriousError = 1;
                            $this->errorCount++;
                        }

                        if ($diskUsage >= 95) {
                            $diskWarn                  = 'badbadbad';
                            $this->hasVerySeriousError = 1;
                            $this->errorCount++;
                        }

                        if (!empty($diskWarn)) {
                            $body .= '<div class="disk disk' . $diskWarn . '">';
                            $body .= '<span class="pctfree ' . $diskWarn . '" title="' . $rowDisk['title'] . '">' . $rowDisk['name'] . ' ' .$diskUsage . '% (' . $rowDisk['label'] . ')</span>';
                            $body .= '</div>';
                        }
                    }

                    if (!empty($rowServer['good_drives'])) {
                        $body .= '<div class="disk allgood">';
                        $body .= '<span class="pctfree" title="' . $rowServer['disk_title_good'] . '">&lt; 50%: ' . $rowServer['good_drives'] . '</span>';
                        $body .= '</div>';
                    }

                    if (!empty($rowServer['ok_drives'])) {
                        $body .= '<div class="disk allgood">';
                        $body .= '<span class="pctfree" title="' . $rowServer['disk_title_ok'] . '">50-80%: ' . $rowServer['ok_drives'] . '</span>';
                        $body .= '</div>';
                    }

                    $body .= '</td>';
                    $body .= '</tr>';
                }

                $body .= '</table></div></td>';
            }

        } catch (\Exception $e) {
            $body = 'Failed to generate main html for default page.' . $e->getMessage();
        }

        return $body;
    }

    // For getAdditionalHtml, need to pass parameters {mute}, {mutebird} by GET method
    public function getAdditionalHtml()
    {
        $body = '';

        try {
            $body .= '<div class="lastupdated">';
            $now = new \DateTime();
            $body .= '<span class="time">' . $now->format('H:i') . '</span>';
            $body .= '<span class="date">' . $now->format('Y-m-d') . '</span>';
            $body .= '</div>';
            $body .= '<div class="errorstatus">ERROR COUNT: ';

            $style = '';

            if ($this->errorCount >= 5) {
                $style                 = 'style="color:#FFC000"';
                $this->hasSeriousError = 1;
            }

            if ($this->errorCount >= 8) {
                $style                     = 'style="color:red"';
                $this->hasVerySeriousError = 1;
            }

            $body .= '<span ' . $style . '>' . $this->errorCount . '</span>';

            $mute     = (isset($this->data['mute']) && !empty($this->data['mute'])) ? $this->data['mute'] : false;
            $muteBird = (isset($this->data['mutebird']) && !empty($this->data['mutebird'])) ? $this->data['mutebird'] : false;

            if ($this->hasVerySeriousError == 1) {
                $body .= ' / <span style="color:red">VerySeriousError</span>';
                if ($mute == false) {
                    $body .= '<audio src="https://fs-filestore-eu.s3.amazonaws.com/customer.firmstep.com/ALARM.WAV" autoplay="autoplay"/>';
                }
            } else if ($this->hasSeriousError == 1) {
                $body .= ' / <span style="color:#FFC000">SeriousError</span>';
                if ($mute == false && $muteBird == false) {
                    $body .= '<audio src="https://fs-filestore-eu.s3.amazonaws.com/standard/sound/siren.wav" autoplay="autoplay"/>';
                }
            }

            $body .= '</div>';
        } catch (\Exception $e) {
            $body = 'Failed to generate main html for default page.' . $e->getMessage();
        }

        return $body;
    }
    #endregion

    #region Internal methods
    private function humanReadable($value)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $index = 0;

        while ($value > 1100 && $index < 4) {
            $value /= 1024;
            $index++;
        }

        if ($value > 10) {
            $value = round($value, 0);
        } else {
            $value = round($value, 1);
        }

        return $value . $units[$index];
    }
    #endregion

    protected function renderSQSTable()
    {
        $sql = "SELECT `queue`, `alias`, `messagesAvailable`, `messagesInFlight` 
                FROM `queue_monitoring`
                ORDER BY `queue` ASC";

        $results = $this->fetchAll($sql);
        $content = '<div>';

        if (!empty($results)) {
            $content .= '<table class="sqsqueue"><tr>';
            $content .= '<td style="width:64px;border:0;padding:0;text-align:left;">SQS</td>';

            foreach ($results as $result) {

                $class = 'ok';

                if ($result['messagesAvailable'] >= SIMPLE_QUEUE_SERVICE['error'] || $result['messagesInFlight'] >= SIMPLE_QUEUE_SERVICE['error']) {
                    $class = 'badbadbad';
                } else if ($result['messagesAvailable'] >= SIMPLE_QUEUE_SERVICE['warning'] || $result['messagesInFlight'] >= SIMPLE_QUEUE_SERVICE['warning']) {
                    $class = 'warn';
                }

                $content .= '<td class="' . $class . '"><ul>';
                $content .= '<li title="Queue" style="padding-right:4px;">' . $result['alias'] ?? $result['queue'] . '&nbsp;</li>';
                $content .= '<li title="Available">' . $result['messagesAvailable'] . '</li>';
                $content .= '<li>/</li>';
                $content .= '<li title="InFlight">' . $result['messagesInFlight'] . '</li>';
                $content .= '</ul></td>';
			}

            $content .= '</tr></table>';
		}

		$content .= '</div>';

        return $content;
    }

    // Stale after 2 x 60s (2 minutes)
    private function isStale($now, $updated)
    {    
        return (((new \DateTime($now, new \DateTimeZone('UTC')))->getTimestamp() - (new \DateTime($updated, new \DateTimeZone('UTC')))->getTimestamp()) > 2 * 60);
    }

    private function fetchAll($sql)
    {
        $stmtSQS = $this->pdo->prepare($sql);

        if ($stmtSQS === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmtSQS->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }
        return $stmtSQS->fetchAll();
    }
}
