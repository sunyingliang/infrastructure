<?php

namespace FS\Monitoring\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\InvalidOperationEception;
use FS\Monitoring\Entity\HostMonitoring;
use FS\Monitoring\Entity\DiskMonitoring;
use FS\Monitoring\Entity\BackgroundLogging;
use FS\Common\IO;
use FS\Common\CURL;

class Log extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring'], ['write']);
    }

    public function create()
    {   
        if (!isset($this->data['op']) || empty($this->data['op'])) {
            throw new InvalidParameterException('No operation specified!');
        }

        if (isset($this->data['serverID'])) {
            $this->data['serverId'] = $this->data['serverID'];
        }

        switch (trim($this->data['op'])) {
            // For logServer
            case 'logServer':
                $hostMonitoring = new HostMonitoring($this->data);
                $columns        = "`server_id`, `cpu_used_micros`, `cpu_count`, `mem_virt`, `mem_work`, `mem_priv`, `mem_paged`";
                $params         = ":server_id, :cpu_used_micros, :cpu_count, :mem_virt, :mem_work, :mem_priv, :mem_paged";
                $values         = [
                    'server_id'       => $hostMonitoring->getServerId(),
                    'cpu_used_micros' => $hostMonitoring->getCpuUsed(),
                    'cpu_count'       => $hostMonitoring->getCpuCount(),
                    'mem_virt'        => $hostMonitoring->getMemVirt(),
                    'mem_work'        => $hostMonitoring->getMemWork(),
                    'mem_priv'        => $hostMonitoring->getMemPriv(),
                    'mem_paged'       => $hostMonitoring->getMemPaged()
                ];

                if ($hostMonitoring->getEmailCount() !== null) {
                    $columns .= ", `email`";
                    $params .= ", :email";
                    $values['email'] = $hostMonitoring->getEmailCount();
                }

                if ($hostMonitoring->getSessionCount() !== null) {
                    $columns .= ", `session`";
                    $params .= ", :session";
                    $values['session'] = $hostMonitoring->getSessionCount();
                }

                if ($hostMonitoring->getMemTotal() !== null) {
                    $columns .= ", `mem_total`";
                    $params .= ", :mem_total";
                    $values['mem_total'] = $hostMonitoring->getMemTotal();
                }

                $sql = "INSERT INTO host_monitoring (" . $columns . ") 
                        VALUES (" . $params . ")";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute($values) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }

                $this->responseArr['message'] = 'logged!';
                break;
            // For logDrive
            case 'logDrive':
                $diskMonitoring = new DiskMonitoring($this->data);
                $columns        = "`server_id`, `name`, `label`, `type`, `free`, `size`";
                $params         = ":server_id, :name, :label, :type, :free, :size";
                $values         = [
                    'server_id' => $diskMonitoring->getServerId(),
                    'name'      => $diskMonitoring->getName(),
                    'label'     => $diskMonitoring->getLabel(),
                    'type'      => $diskMonitoring->getType(),
                    'free'      => $diskMonitoring->getFree(),
                    'size'      => $diskMonitoring->getSize()
                ];

                $sql= "SELECT COUNT(1) AS `total` 
                       FROM `disk_monitoring` 
                       WHERE `server_id` = :server_id 
                          AND `name` = :name 
                          AND `label` = :label 
                          AND `type` = :type";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if ($stmt->execute(['server_id' => $values['server_id'],
                                    'name' => $values['name'],
                                    'label' => $values['label'],
                                    'type'  => $values['type']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                if ($row = $stmt->fetch()) {
                    if ($row['total'] > 0) {
                        $sql = "UPDATE `disk_monitoring` 
                                SET `free` = :free, `size` = :size, `as_at` = CURRENT_TIMESTAMP
                                WHERE `server_id` = :server_id 
                                  AND `name` = :name 
                                  AND `label` = :label 
                                  AND `type` = :type
                                ORDER BY `as_at` DESC
                                LIMIT 1";
                    } else {
                        $sql = "INSERT INTO `disk_monitoring` (" . $columns . ") VALUES (" . $params . ")";
                    }
                }

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute($values) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
                break;
            // For logRDS
            case 'logRDS':
                // Insert into `disk_monitoring`
                if (!isset($this->data['serverID'])) {
                    throw new InvalidParameterException('Parameter {serverID} is not passed in correctly');
                }

                if (!isset($this->data['freeDisk'])) {
                    throw new InvalidParameterException('Parameter {freeDisk} is not passed in correctly');
                }

                if (!isset($this->data['totalStorage'])) {
                    throw new InvalidParameterException('Parameter {totalStorage} is not passed in correctly');
                }

                if (!isset($this->data['freeRAM'])) {
                    throw new InvalidParameterException('Parameter {freeRAM} is not passed in correctly');
                }

                if (!isset($this->data['totalRAM'])) {
                    throw new InvalidParameterException('Parameter {totalRAM} is not passed in correctly');
                }

                if (!isset($this->data['cpuUsage'])) {
                    throw new InvalidParameterException('Parameter {cpuUsage} is not passed in correctly');
                }

                $this->data['name']     = 'Main';
                $this->data['label']    = 'Main';
                $this->data['type']     = 'Fixed';
                $this->data['serverID'] = $this->data['serverID'];
                $this->data['size']     = $this->data['totalStorage'];
                $this->data['free']     = $this->data['freeDisk'];

                $diskMonitoring = new DiskMonitoring($this->data);

                // Select from `host_monitoring`
                $sql = "SELECT * 
                        FROM `host_monitoring` 
                        WHERE `server_id` = :server_id 
                        ORDER BY `as_at` DESC 
                        LIMIT 1";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute(['server_id' => $diskMonitoring->getServerId()]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }

                if ($row = $stmt->fetch()) {
                    $lastHostCpu = $row['cpu_used_micros'];
                } else {
                    $lastHostCpu = 0;
                }

                $totalRam = intval($this->data['totalRAM']);
                $usedRam  = $totalRam - intval($this->data['freeRAM']);
                $totalRam *= 1.2;
                $cpuUsage = intval($this->data['cpuUsage']);

                $lastHostCpu += 100000 * $cpuUsage;

                $hostMonitoring = new HostMonitoring([
                    'serverId' => $this->data['serverID'],
                    'cpuUsed'  => $lastHostCpu,
                    'memVirt'  => $usedRam,
                    'memWork'  => $usedRam,
                    'memPriv'  => $usedRam,
                    'memPaged' => $usedRam,
                    'memTotal' => $totalRam
                ]);

                $columns = "`server_id`, `cpu_used_micros`, `cpu_count`, `mem_virt`, `mem_work`, `mem_priv`, `mem_paged`, `mem_total`";
                $params  = ":server_id, :cpu_used_micros, :cpu_count, :mem_virt, :mem_work, :mem_priv, :mem_paged, :mem_total";
                $values  = [
                    'server_id'       => $hostMonitoring->getServerId(),
                    'cpu_used_micros' => $hostMonitoring->getCpuUsed(),
                    'cpu_count'       => $hostMonitoring->getCpuCount(),
                    'mem_virt'        => $hostMonitoring->getMemVirt(),
                    'mem_work'        => $hostMonitoring->getMemWork(),
                    'mem_priv'        => $hostMonitoring->getMemPriv(),
                    'mem_paged'       => $hostMonitoring->getMemPaged(),
                    'mem_total'       => $hostMonitoring->getMemTotal()
                ];

                $sql = "INSERT INTO `host_monitoring` (" . $columns . ") 
                        VALUES (" . $params . ")";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute($values) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }

                // Log into disk_monitoring table
                $columns = "`server_id`, `name`, `label`, `type`, `free`, `size`";
                $params  = ":server_id, :name, :label, :type, :free, :size";
                $values  = [
                    'server_id' => $diskMonitoring->getServerId(),
                    'name'      => $diskMonitoring->getName(),
                    'label'     => $diskMonitoring->getLabel(),
                    'type'      => $diskMonitoring->getType(),
                    'free'      => $diskMonitoring->getFree(),
                    'size'      => $diskMonitoring->getSize()
                ];

                $sql= "SELECT COUNT(1) AS `total` 
                       FROM `disk_monitoring` 
                       WHERE `server_id` = :server_id
                          AND `name` = :name 
                          AND `label` = :label 
                          AND `type` = :type";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if ($stmt->execute(['server_id' => $values['server_id'],
                                    'name' => $values['name'],
                                    'label' => $values['label'],
                                    'type'  => $values['type']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                if ($row = $stmt->fetch()) {
                    if ($row['total'] > 0) {
                        $sql = "UPDATE `disk_monitoring` 
                                SET `free` = :free, `size` = :size, `as_at` = CURRENT_TIMESTAMP 
                                WHERE `server_id` = :server_id 
                                    AND `name` = :name 
                                    AND `label` = :label 
                                    AND `type` = :type
                                ORDER BY `as_at` DESC
                                LIMIT 1";
                    } else {
                        $sql = "INSERT INTO `disk_monitoring` (" . $columns . ") VALUES (" . $params . ")";
                    }
                }

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if ($stmt->execute($values) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
                break;
            // For logSQS
            case 'logSQS':
                if (!isset($this->data['serverID'])) {
                    throw new InvalidParameterException('Parameter {serverID} is not passed in correctly');
                }

                if (!isset($this->data['messagesAvailable'])) {
                    throw new InvalidParameterException('Parameter {messagesAvailable} is not passed in correctly');
                }

                if (!isset($this->data['messagesInFlight'])) {
                    throw new InvalidParameterException('Parameter {messagesInFlight} is not passed in correctly');
                }

                if (SIMPLE_QUEUE_SERVICE['mode'] == 'insert') {
                    $sql = "INSERT INTO queue_monitoring (`queue`, `messagesAvailable`, `messagesInFlight`) 
                            VALUES (:queue, :messagesAvailable, :messagesInFlight) ON DUPLICATE KEY UPDATE `messagesAvailable` = :messagesAvailable, `messagesInFlight` = :messagesInFlight";
                } else {
                    $sql = "UPDATE queue_monitoring 
                            SET `messagesAvailable` = :messagesAvailable, `messagesInFlight` = :messagesInFlight
                            WHERE `queue` = :queue";
                }

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if ($stmt->execute([
                        'queue'             => $this->data['serverID'],
                        'messagesAvailable' => $this->data['messagesAvailable'],
                        'messagesInFlight'  => $this->data['messagesInFlight'],
                    ]) === false
                ) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }
                break;
            default:
                $message = 'Unknown operation {' . $this->data['op'] . '} for monitoring logging, passed in parameters are as follows:' . PHP_EOL;

                foreach ($this->data as $key => $value) {
                    $message .= $key . '=' . $value . '&';
                }

                $message = substr($message, 0, -1);

                $data = '{"channel": "' . SLACK['channel'] . '", "username": "' . SLACK['username'] . '", "text": "' . '<!channel> ' . $message . '"}';

                $curlOpts = [
                    CURLOPT_URL            => SLACK['url'],
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER         => false,
                    CURLOPT_POST           => true,
                    CURLOPT_POSTFIELDS     => $data
                ];

                $curl = CURL::init($curlOpts);

                CURL::getResult($curl);
                CURL::close($curl);

                throw new InvalidOperationEception('Unknown operation');
                break;
        }

        return $this->responseArr;
    }
}
