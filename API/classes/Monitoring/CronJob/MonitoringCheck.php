<?php

namespace FS\Monitoring\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOCreationException;

class MonitoringCheck extends CronTab
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    public function doCheck()
    {
        // Fetch records from 'server_names'
        $sql = "SELECT * FROM `server_names`";

        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        while ($row = $stmt->fetch()) {
            echo 'Checking for ' . $row['basename'] . ' servers' . PHP_EOL;

            $numberCount    = 0;
            $number         = '01';
            $ignore_servers = [];

            if (isset($row['ignore']) && !empty($row['ignore'])) {
                $ignore_ids = explode(',', $row['ignore']);

                foreach ($ignore_ids as $ignore_id) {
                    array_push($ignore_servers, str_replace('i-%', '', $row['searchstring']) . $ignore_id);
                }
            }

            if ($row['enabled'] == 1) {
                #region Handle insert action
                $sqlForInsert = "SELECT `host_monitoring`.`server_id` 
                                 FROM `host_monitoring` 
                                    LEFT JOIN `monitor_server` ON `host_monitoring`.`server_id` = `monitor_server`.`name` 
                                 WHERE `host_monitoring`.`server_id` LIKE :searchString 
                                    AND `host_monitoring`.`as_at` > DATE_SUB(NOW(), INTERVAL 5 MINUTE) 
                                    AND `monitor_server`.`name` IS NULL
                                 GROUP BY `host_monitoring`.`server_id`";

                $stmtForInsert = $this->db->prepare($sqlForInsert);

                if ($stmtForInsert === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if (!$stmtForInsert->execute(['searchString' => $row['searchstring']])) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForInsert);
                }

                foreach ($stmtForInsert->fetchAll() as $rowForInsert) {
                    if (in_array($rowForInsert['server_id'], $ignore_servers)) {
                        continue;
                    }

                    $exists = 1;

                    while ($exists > 0) {
                        $numberCount++;
                        $number = $numberCount < 10 ? ('0' . $numberCount) : $numberCount;

                        $sqlExists = "SELECT COUNT(1) AS `count` 
                                      FROM `monitor_server` 
                                      WHERE `prettyname` = :prettyName
                                          AND `enabled` = 1";

                        $stmtExists = $this->db->prepare($sqlExists);

                        if ($stmtExists === false) {
                            throw new PDOCreationException('PDOStatement prepare failed.');
                        }

                        if (!$stmtExists->execute(['prettyName' => ($row['basename'] . $number)])) {
                            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlExists);
                        }

                        if ($rowExists = $stmtExists->fetch()) {
                            $exists = $rowExists['count'];
                        }
                    }

                    $sqlInsert = "INSERT INTO `monitor_server`(`name`, `prettyname`, `charts`, `group`, `auto_scaled`, `enabled`) 
                                  VALUES (:name, :prettyName, :charts, :group, :auto_scaled, :enabled)";

                    $stmtInsert = $this->db->prepare($sqlInsert);

                    if ($stmtInsert === false) {
                        throw new PDOCreationException('PDOStatement prepare failed.');
                    }

                    if (!$stmtInsert->execute([
                        'name'        => $rowForInsert['server_id'],
                        'prettyName'  => ($row['basename'] . $number),
                        'charts'      => $row['charts'],
                        'group'       => $row['group'],
                        'auto_scaled' => 1,
                        'enabled'     => 1
                    ])
                    ) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlInsert);
                    }
                }
                #endregion

                #region Handle delete old monitor_server records
                $sqlForDelete = "SELECT `monitor_server`.`name` 
                                 FROM `monitor_server` 
                                     LEFT JOIN `host_monitoring` ON `host_monitoring`.`server_id` = `monitor_server`.`name`
                                         AND `host_monitoring`.`as_at` > date_sub(now(), INTERVAL 5 MINUTE)
                                 WHERE `monitor_server`.`name` LIKE :searchString
                                     AND `monitor_server`.`auto_scaled` = 1
                                     AND `host_monitoring`.`server_id` IS NULL
                                 GROUP BY `monitor_server`.`name`";

                // Delete ignore server columns from monitor_server table
                if (count($ignore_servers) > 0) {
                    $sqlForDelete .= " UNION
                            SELECT `monitor_server`.`name`
                            FROM `monitor_server`
                            WHERE `monitor_server`.`name` IN ('" . implode("','", $ignore_servers) . "')
                                AND `monitor_server`.`auto_scaled` = 1
                            GROUP BY `monitor_server`.`name`";
                }

                $stmtForDelete = $this->db->prepare($sqlForDelete);

                if ($stmtForDelete === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if (!$stmtForDelete->execute(['searchString' => $row['searchstring']])) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForDelete);
                }
                
                // Explicitly checking auto_scaled = 1 just in case something erronious tries to remove a non-auto_scaled server
                $sqlDelete = "DELETE FROM `monitor_server` 
                              WHERE `name` = :name
                                AND `auto_scaled` = 1";

                $stmtDelete = $this->db->prepare($sqlDelete);

                if ($stmtDelete === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                foreach ($stmtForDelete->fetchAll() as $rowForDelete) {
                    if (!$stmtDelete->execute(['name' => $rowForDelete['name']])) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDelete);
                    }
                }
                #endregion

                #region Handle update action
                $sqlForUpdate  = "SELECT `searchstring`, `basename` 
                                  FROM `server_names`";

                $stmtForUpdate = $this->db->prepare($sqlForUpdate);

                if ($stmtForUpdate === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if (!$stmtForUpdate->execute()) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForUpdate);
                }

                foreach ($stmtForUpdate->fetchAll() as $item) {
                    $counterForMonitorServerUpdate = 1;
                    $sqlForMonitorServerUpdate     = "SELECT `name`, `prettyname`
                                                      FROM `monitor_server`
                                                      WHERE `name` LIKE :searchString
                                                          AND `auto_scaled` = 1";

                    $stmtForMonitorServerUpdate    = $this->db->prepare($sqlForMonitorServerUpdate);

                    if ($stmtForMonitorServerUpdate === false) {
                        throw new PDOCreationException('PDOStatement prepare failed.');
                    }

                    if (!$stmtForMonitorServerUpdate->execute(['searchString' => $item['searchstring']])) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForMonitorServerUpdate);
                    }

                    foreach ($stmtForMonitorServerUpdate->fetchAll() as $result) {
                        $prettyName   = $item['basename'] . ($counterForMonitorServerUpdate < 10 ? ('0' . $counterForMonitorServerUpdate) : $counterForMonitorServerUpdate);

                        if ($prettyName !== $result['prettyname']) {
                            $sqlForUpdate = "UPDATE `monitor_server`
                                             SET `prettyname` = :prettyName
                                             WHERE `name` = :name";

                            $stmtUpdate = $this->db->prepare($sqlForUpdate);
                            
                            $stmtUpdate->execute(['prettyName' => $prettyName, 'name' => $result['name']]);
                        }

                        $counterForMonitorServerUpdate += 1;
                    }
                }
                #endregion
            } else {
                // Delete any records from monitor_server where server_name is not enabled
                $sqlForDelete = "SELECT `monitor_server`.`name` 
                                 FROM `monitor_server` 
                                 WHERE `monitor_server`.`name` LIKE :searchString 
                                 GROUP BY `monitor_server`.`name`
                                    AND `monitor_server`.`auto_scaled` = 1";

                $stmtForDelete = $this->db->prepare($sqlForDelete);

                if ($stmtForDelete === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                if (!$stmtForDelete->execute(['searchString' => $row['searchstring']])) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForDelete);
                }

                // Explicitly checking auto_scaled = 1 just in case something erronious tries to remove a non-auto_scaled server
                $sqlDelete = "DELETE FROM `monitor_server` 
                              WHERE `name` = :name
                                AND `auto_scaled` = 1";

                $stmtDelete = $this->db->prepare($sqlDelete);

                if ($stmtDelete === false) {
                    throw new PDOCreationException('PDOStatement prepare failed.');
                }

                foreach ($stmtForDelete->fetchAll() as $rowForDelete) {
                    if (!$stmtDelete->execute(['name' => $rowForDelete['name']])) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDelete);
                    }
                }
            }
        }
    }
}
