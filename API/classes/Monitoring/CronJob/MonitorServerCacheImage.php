<?php
// Check server api
if (php_sapi_name() != 'cli') {
    die('Error: Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

require __DIR__ . '/../../Common/autoload.php';

use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\FileWriteException;
use FS\Common\Exception\DirCreateException;
use FS\Common\CURL;

class MonitorServerCacheImage
{
    private $id     = 0;
    private $cpuUrl = '';
    private $memUrl = '';
    private $curl   = null;
    private $path   = __DIR__ . '/../../../cache/';

    public function __construct($id, $cpuChart, $memTotal, $memChart)
    {
        $this->id     = $id;
        $this->cpuUrl = 'http://chart.apis.google.com/chart?chco=33EE33&cht=ls&chds=0,100&chd=t:' . $cpuChart . '&chs=120x36&chf=bg,s,65432100';
        $this->memUrl = 'http://chart.apis.google.com/chart?chco=33EE33&cht=ls&chds=0,' . $memTotal . '&chd=t:' . $memChart . '&chs=120x36&chf=bg,s,65432100';
        $this->curl   = CURL::init();

        if (!file_exists($this->path)) {
            if (!mkdir($this->path)) {
                throw new DirCreateException('Failed to create cache directory {' . $this->path . '}');
            }
        }
    }

    public function getImage()
    {
        // Generate cpu chart
        $opts = [
            CURLOPT_URL            => $this->cpuUrl,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
        ];

        CURL::setOptArray($this->curl, $opts);

        $response = CURL::getResult($this->curl);

        $fh = fopen($this->path . $this->id . 'c.png', 'w');
        fwrite($fh, $response);
        fclose($fh);

        // Generate mem chart
        $opts = [
            CURLOPT_URL            => $this->memUrl,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
        ];

        CURL::setOptArray($this->curl, $opts);

        $response = CURL::getResult($this->curl);

        $fh = fopen($this->path . $this->id . 'm.png', 'w');
        fwrite($fh, $response);
        fclose($fh);
    }

    public function __destruct()
    {
        CURL::close($this->curl);
    }
}

// Generate image based on passed parameters
try {
    if (count($argv) != 5 || !is_numeric($argv[1]) || !is_string($argv[2]) || !is_numeric($argv[3]) || !is_string($argv[4])) {
        throw new InvalidParameterException('Passed in parameters are not correct');
    }
    
    $id       = $argv[1];
    $cpuChart = $argv[2];
    $memTotal = $argv[3];
    $memChart = $argv[4];

    $cacheImage = new MonitorServerCacheImage($id, $cpuChart, $memTotal, $memChart);

    $cacheImage->getImage();
} catch (Exception $e) {
    IO::message('Error: Failed to generate image, reason: ' . $e->getmessage());
}
