<?php

namespace FS\Monitoring\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;

class MonitoringTruncate extends CronTab
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    public function truncateTables()
    {
        // Truncate table `disk_monitoring`
        $sql = "DELETE FROM `disk_monitoring` 
                WHERE `as_at` < DATE_SUB(NOW(), INTERVAL 10 MINUTE)";

        $flag = $this->db->exec($sql);

        if ($flag === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        // Truncate table `history`
        $sql = "DELETE FROM `history` 
                WHERE `updated` < DATE_SUB(NOW(), INTERVAL 1 HOUR)";

        $flag = $this->db->exec($sql);

        if ($flag === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        // Truncate table `host_monitoring`
        $sql = "DELETE FROM `host_monitoring` 
                WHERE `as_at` < DATE_SUB(NOW(), INTERVAL 1 HOUR)";

        $flag = $this->db->exec($sql);

        if ($flag === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        // Truncate table `monitor_host`
        $sql = "DELETE FROM `monitor_host` 
                WHERE `updated` < DATE_SUB(NOW(), INTERVAL 1 DAY)";

        $flag = $this->db->exec($sql);

        if ($flag === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }
    }
}
