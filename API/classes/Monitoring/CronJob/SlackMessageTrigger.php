<?php

namespace FS\Monitoring\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\CURL;
use FS\Common\IO;

class SlackMessageTrigger extends CronTab
{
    protected $delayThreshold    = 15;
    protected $warningThreshold  = 2;
    protected $criticalThreshold = 2;
    protected $slackNotification = [];
    protected $asgSlackMessage   = '';
    protected $errorFile      = __DIR__ . '/../../../log/connection_error.txt';

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    public function triggerMonitoringServers()
    {
        // Header servers polling
        $sql = "SELECT `id`, `url`, COALESCE(`prettyname`, `url`) AS `prettyname`, COALESCE(`colour`, 'FFFFFF') AS `colour` 
                FROM `monitoring` 
                WHERE `type` IS NULL 
                    AND `state` = 'on' 
                    AND `updated` < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                ORDER BY `sequence`";

        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        $monitoredMachines = $stmt->fetchAll();

        $sqlPollingMachines = "SELECT `host` FROM `monitor_host` GROUP BY `host`";

        $stmt = $this->db->prepare($sqlPollingMachines);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlPollingMachines);
        }

        $pollingMachines = $stmt->fetchAll();

        foreach ($pollingMachines as $pollingMachine) {
            foreach ($monitoredMachines as $monitoringMachine) {
                $sqlDelay = "SELECT * 
                             FROM `monitor_host` 
                                 INNER JOIN `monitoring` ON `monitoring`.`id` = `monitor_host`.`monitoring_id`
                                 LEFT JOIN `monitor_server` ON `monitor_server`.`prettyname` = `monitoring`.`prettyname`
                                     AND `monitor_server`.`enabled` = 0
                             WHERE `monitor_host`.`monitoring_id` = :monitoring_id 
                                 AND `monitor_host`.`host` = :host
                                 AND `monitor_server`.`id` IS NULL";

                $stmt = $this->db->prepare($sqlDelay);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute([
                        'monitoring_id' => $monitoringMachine['id'],
                        'host'          => $pollingMachine['host']
                    ]) === false
                ) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDelay);
                }

                if ($rowDelay = $stmt->fetch()) {
                    if (!isset($this->slackNotification[$monitoringMachine['prettyname']])) {
                        $this->slackNotification[$monitoringMachine['prettyname']]                = [];
                        $this->slackNotification[$monitoringMachine['prettyname']]['downCounter'] = 0;
                        $this->slackNotification[$monitoringMachine['prettyname']]['slowCounter'] = 0;
                    }

                    if (!isset($this->slackNotification[$monitoringMachine['prettyname']]['host'])) {
                        $this->slackNotification[$monitoringMachine['prettyname']]['host'] = [];
                    }

                    if ($rowDelay['status'] != 1 && $rowDelay['failed_counter'] > 1) {
                        $this->slackNotification[$monitoringMachine['prettyname']]['downCounter']++;
                        $this->slackNotification[$monitoringMachine['prettyname']]['host'][$pollingMachine['host']] = 'DOWN';
                    } else {
                        $delay                                                                                      = number_format($rowDelay['delay'], 2);
                        $this->slackNotification[$monitoringMachine['prettyname']]['host'][$pollingMachine['host']] = $delay . 's';
                        if (intval($delay) >= $this->delayThreshold) {
                            $this->slackNotification[$monitoringMachine['prettyname']]['slowCounter']++;
                        }
                    }
                }
            }
        }

        // Pingdom polling
        $sqlPingdom = "SELECT * 
                       FROM `pingdom_cache` 
                       WHERE SUBSTRING(`name`,1,8) = ? 
                           OR CONCAT('http://', `hostname`) = SUBSTRING(?, 1, LENGTH(`hostname`) + 7) 
                           OR CONCAT('https://', `hostname`) = SUBSTRING(?, 1, LENGTH(`hostname`) + 8)";

        $stmt = $this->db->prepare($sqlPingdom);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        foreach ($monitoredMachines as $monitoringMachine) {
            if ($stmt->execute([
                    $monitoringMachine['prettyname'],
                    $monitoringMachine['url'],
                    $monitoringMachine['url']
                ]) === false
            ) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlPingdom);
            }

            if ($rowPingdom = $stmt->fetch()) {
                if (!isset($this->slackNotification[$monitoringMachine['prettyname']])) {
                    $this->slackNotification[$monitoringMachine['prettyname']]                = [];
                    $this->slackNotification[$monitoringMachine['prettyname']]['downCounter'] = 0;
                    $this->slackNotification[$monitoringMachine['prettyname']]['slowCounter'] = 0;
                }

                if (!isset($this->slackNotification[$monitoringMachine['prettyname']]['host'])) {
                    $this->slackNotification[$monitoringMachine['prettyname']]['host'] = [];
                }

                if ($rowPingdom['status'] != 'up') {
                    $this->slackNotification[$monitoringMachine['prettyname']]['downCounter']++;
                    $this->slackNotification[$monitoringMachine['prettyname']]['host']['Pingdom'] = 'DOWN';
                } else {
                    $delay                                                                        = number_format($rowPingdom['lastresponsetime'] / 1000, 2);
                    $this->slackNotification[$monitoringMachine['prettyname']]['host']['Pingdom'] = $delay . 's';
                    if (intval($delay) >= $this->delayThreshold) {
                        $this->slackNotification[$monitoringMachine['prettyname']]['slowCounter']++;
                    }
                }
            }
        }

        // Servers polling
        $sqlGroups = "SELECT `monitor_server`.`group` AS `group`, SUM(`host_monitoring`.`email`) AS `email`, SUM(`host_monitoring`.`session`) AS `session` 
                      FROM `monitor_server` 
                        INNER JOIN `host_monitoring` ON `monitor_server`.`name` = `host_monitoring`.`server_id`
                          AND `host_monitoring`.`as_at` > DATE_SUB(NOW(), INTERVAL 10 MINUTE)  
                      WHERE `monitor_server`.`enabled` = 1  
                          AND `monitor_server`.`updated` < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                      GROUP BY `monitor_server`.`group` 
                      ORDER BY `monitor_server`.`group`";

        $stmt = $this->db->prepare($sqlGroups);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlGroups);
        }

        while ($rowGroups = $stmt->fetch()) {
            $sqlDownServers = "SELECT `monitor_server`.`name`, `monitor_server`.`prettyname`, `monitor_server`.`group` 
                               FROM `monitor_server` 
                               WHERE `monitor_server`.`group` = :group 
                                   AND `monitor_server`.`name` NOT IN (
                                       SELECT `host_monitoring`.`server_id` 
                                       FROM `host_monitoring` 
                                       WHERE `host_monitoring`.`as_at` > DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                                   ) 
                                   AND `monitor_server`.`name` NOT LIKE '%old' 
                                   AND `monitor_server`.`prettyname` NOT LIKE 'STAG-12%' 
                                   AND `monitor_server`.`auto_scaled` = 0 
                                   AND `monitor_server`.`enabled` = 1
                                   AND `monitor_server`.`updated` < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                               ORDER BY `monitor_server`.`name`";

            $stmtDownServers = $this->db->prepare($sqlDownServers);

            if ($stmtDownServers === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmtDownServers->execute(['group' => $rowGroups['group']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDownServers);
            }

            while ($rowDownServer = $stmtDownServers->fetch()) {
                $this->slackNotification[$rowDownServer['prettyname']] = 1;
            }
        }

        // Check autoscaled servers missing
        $sql = "SELECT `basename`, `threshold`
                FROM `server_names`
                WHERE `threshold` > 0
                    AND `enabled` = 1";

        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        while ($row = $stmt->fetch()) {
            $sqlForServerCount  = "SELECT COUNT(1) AS `total` 
                                   FROM `monitor_server_cache` 
                                   WHERE `name` LIKE :name
                                       AND `last_time` IS NOT NULL";

            $stmtForServerCount = $this->db->prepare($sqlForServerCount);

            if ($stmtForServerCount === false) {
                throw new PDOCreationException('PDOStatement prepare failed.');
            }

            if (!$stmtForServerCount->execute(['name' => '%' . $row['basename'] . '%'])) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForServerCount);
            }

            $currentServerCount = $stmtForServerCount->fetchColumn(0);

            if (empty($currentServerCount)) {
                $currentServerCount = 0;
            }

            if ($currentServerCount < $row['threshold']) {
                // Check if asg exist in monitor_server
                $sqlCheckASGExist = "SELECT COUNT(1) AS `total` FROM `monitor_server` WHERE `name` LIKE :name";
                $stmtCheckASGExist = $this->db->prepare($sqlCheckASGExist);
                if(!$stmtCheckASGExist->execute(['name' => '%' . $row['basename'] . '%'])) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlCheckASGExist);
                }

                $existCount = $stmtCheckASGExist->fetchColumn(0);

                if ($existCount > 0) {
                    // If ASG group has updated more than 10 mins, then report ASG group server missing
                    $sqlForASGServersCount  = "SELECT COUNT(1) AS `total` 
                                   FROM `monitor_server` 
                                   WHERE `name` LIKE :name
                                       AND `updated` > DATE_SUB(NOW(), INTERVAL 10 MINUTE)";

                    $stmtForASGServersCount = $this->db->prepare($sqlForASGServersCount);

                    if ($stmtForASGServersCount === false) {
                        throw new PDOCreationException('PDOStatement prepare failed.');
                    }

                    if (!$stmtForASGServersCount->execute(['name' => '%' . $row['basename'] . '%'])) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlForASGServersCount);
                    }

                    $serverCount = $stmtForASGServersCount->fetchColumn(0);

                    if ($serverCount == 0) {
                        $this->slackNotification['autoscaledServer'] = 2;
                        $this->asgSlackMessage .= 'ASG ' . $row['basename'] . ' ' . $this->environment . ': ' . $currentServerCount . ' running, ' . $row['threshold'] . ' expected' . PHP_EOL;
                    }
                }
            }
        }

        // Trigger slack notifications
        if (!file_exists($this->errorFile)) {
            mkdir(dirname($this->errorFile), 0777, true);
            IO::writeFile($this->errorFile, 'w', '');
        }

        $errorFileContent      = file_get_contents($this->errorFile);
        $errorFileContentArray = explode(PHP_EOL, $errorFileContent);
        $distinctSlackMessages = '';
        $solvedSlackMessages   = '';
        $messages              = '';
        $messagesHeader        = [];

        foreach ($this->slackNotification as $machine => $notification) {
            if ($notification === 1) {
                $serverMissingMessage[] = $machine;
            } else if ($notification === 2) {
                $messages = $this->asgSlackMessage;
            } else {
                $message = '';
                if ($notification['downCounter'] == count($notification['host'])) {
                    $message = 'Server down ' . $this->environment . ': ' . $machine . ' [';
                } else if ($notification['downCounter'] >= $this->criticalThreshold) {
                    $message = 'Server critical ' . $this->environment . ': ' . $machine . ' [';
                } else if ($notification['slowCounter'] == count($notification['host'])) {
                    $message = 'Server warning ' . $this->environment . ': ' . $machine . ' [';
                } else if ($notification['slowCounter'] >= $this->warningThreshold) {
                    $message = 'Server warning ' . $this->environment . ': ' . $machine . ' [';
                }

                if (!empty($message)) {
                    foreach ($notification['host'] as $hostName => $hostValue) {
                        $message .= $hostName . ': ' . $hostValue . ', ';
                    }
                    $message = substr($message, 0, -2);
                    $message .= ']';

                    $messagesHeader[] = $message;
                }
            }
        }

        if (!empty($messagesHeader)) {
            $messages .= implode(PHP_EOL, $messagesHeader);
        }

        if (!empty($serverMissingMessage)) {
            // For each server in $serverMissingMessage array, check if the server exist in error file,
            // if not, it means this missing server has been solved, add to $solvedSlackMessages
            foreach ($errorFileContentArray as $key => $value) {
                if (!empty($value) && substr($value, 0, 14) == 'Server missing') {
                    $missingServers = explode(', ', substr($value, 17));
                    $solvedElements = array_diff($missingServers, $serverMissingMessage);

                    if (!empty($solvedElements)) {
                        $solvedSlackMessages .= 'The following notification(s) have been resolved: {Server missing : ' . implode(', ', $solvedElements) . '}' . PHP_EOL;
                    }
                }
            }

            $messages .= (empty($message) ? '' : PHP_EOL) . 'Server missing : ' . implode(', ', $serverMissingMessage);
        }

        // Monitoring grace period, check error file

        if (!empty($messages)) {
            $errorArray = explode(PHP_EOL, $messages);

            if (!empty($errorFileContent)) {
                // Check if certain error is in error file,
                // if not, it means the error has been solved, add it to $solvedSlackMessages
                foreach ($errorFileContentArray as $oldError) {
                    if (!empty($oldError) && !in_array($oldError, $errorArray)) {
                        if (strpos($oldError, 'Server missing') !== false && !empty($serverMissingMessage)) {
                            break;
                        } else {
                            $solvedSlackMessages .= 'The following notification(s) have been resolved: {' . $oldError . '}' . PHP_EOL;
                        }
                    }
                }

                // Check if an error in new error messages is in error file,
                // if not, means this error is a new error, add it to $distinctSlackMessages
                foreach ($errorArray as $currentError) {
                    if (!empty($currentError) && !in_array($currentError, $errorFileContentArray)) {
                        $distinctSlackMessages .= $currentError . PHP_EOL;
                    }
                }
            } else {
                $distinctSlackMessages = $messages;
            }

            IO::writeFile($this->errorFile, 'w', $messages);
        } else if (!empty($errorFileContent)) {
            $solvedSlackMessages .= 'The following notification(s) have been resolved: {' . $errorFileContent . '}' . PHP_EOL;
            IO::writeFile($this->errorFile, 'w', '');
        }

        if (!empty($distinctSlackMessages)) {
            IO::slack(SLACK['url'], SLACK['channel'], SLACK['username'], $distinctSlackMessages);
        }
        if (!empty($solvedSlackMessages)) {
            IO::slack(SLACK['url'], SLACK['channel'], SLACK['username'], $solvedSlackMessages);
        }
    }
}
