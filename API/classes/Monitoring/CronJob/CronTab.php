<?php

namespace FS\Monitoring\CronJob;

use FS\Common\IO;

class CronTab
{
    protected $db;
    protected $environment = '';

    public function __construct($config)
    {
        if (!is_array($config)) {
            throw new InvalidParameterException('Passed in parameter {config} must be an array');
        }

        if (defined('COMMON') && array_key_exists('environment', COMMON) && !empty(COMMON['environment'])) {
            $this->environment = '[' . COMMON['environment'] . ']';
        }   

        $this->db = IO::getPDOConnection($config);
    }
}
