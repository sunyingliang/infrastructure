<?php

namespace FS\Monitoring\CronJob;

use FS\Common\Exception\CURLException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\CURL;
use FS\Monitoring\Entity\PingdomCache;

class PingdomCacheUpdate extends CronTab
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    #region Methods
    public function doUpdate()
    {
        $curlOpts = [
            CURLOPT_URL            => PINGDOM['url'],
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic ' . PINGDOM['auth'],
                'App-Key: ' . PINGDOM['key']
            ]
        ];

        $curl = CURL::init($curlOpts);

        $responseCurl = CURL::getResult($curl);

        CURL::close($curl);

        $pingdomArr = json_decode($responseCurl, true);

        if (isset($pingdomArr['error'])) {
            throw new CURLException('CURL returned error info from api.');
        }

        $sql = "DELETE FROM `pingdom_cache`";

        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if (false === $stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        $sql = "INSERT INTO `pingdom_cache`(`id`, `created`, `name`, `hostname`, `resolution`, `type`, `lasterrortime`, `lasttesttime`, `lastresponsetime`, `status`) 
                VALUES (:id, :created, :name, :hostname, :resolution, :type, :lasterrortime, :lasttesttime, :lastresponsetime, :status)";

        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        foreach ($pingdomArr['checks'] as $pingdom) {
            $pingdomObj = new PingdomCache($pingdom);
            
            $params = [
                'id'               => $pingdomObj->getId(),
                'created'          => $pingdomObj->getCreated(),
                'name'             => $pingdomObj->getName(),
                'hostname'         => $pingdomObj->getHostName(),
                'resolution'       => $pingdomObj->getResolution(),
                'type'             => $pingdomObj->getType(),
                'lasterrortime'    => $pingdomObj->getLastErrorTime(),
                'lasttesttime'     => $pingdomObj->getLastTestTime(),
                'lastresponsetime' => $pingdomObj->getLastResponseTime(),
                'status'           => $pingdomObj->getStatus()
            ];

            if (false === $stmt->execute($params)) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
            }
        }
    }

    #endregion
}
