<?php

namespace FS\Monitoring\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\IO;

class MonitorServerCalculator extends CronTab
{
    protected $errorCount          = 0;
    protected $hasSeriousError     = 0;
    protected $hasVerySeriousError = 0;

    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['monitoring']);
    }

    public function calculate()
    {
        // fill up table `monitor_server_cache` based on table `monitor_server` instead of truncating,
        // this is temporary and will be controlled under check in future.
        $insertArray = [];
        $updateArray = [];
        $deleteArray = [];
        $sqlUpsert   = "SELECT c.id AS `cid`, s.*
                        FROM `monitor_server_cache` AS `c` RIGHT JOIN (
                        SELECT `monitor_server`.`id`, `monitor_server`.`name`, `monitor_server`.`prettyname`, `monitor_server`.`charts`,
                              `monitor_server`.`group`, `monitor_server`.`auto_scaled`, `monitor_server`.`enabled`, 
                            MAX(`host_monitoring`.`as_at`) AS `last_time`
                        FROM `monitor_server`
                        LEFT JOIN `host_monitoring` ON `host_monitoring`.`server_id` = `monitor_server`.`name`
                            AND `host_monitoring`.`as_at` > DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                        WHERE `monitor_server`.`enabled` = 1
                        GROUP BY `monitor_server`.`id`, `monitor_server`.`name`, `monitor_server`.`prettyname`, `monitor_server`.`charts`,
                             `monitor_server`.`group`, `monitor_server`.`auto_scaled`, `monitor_server`.`enabled`
                        ) AS `s` ON `c`.`id` = `s`.`id`";

        $resultUpsert = $this->db->query($sqlUpsert)->fetchAll();

        foreach ($resultUpsert as $result) {
            if (empty($result['cid'])) {
                $insertArray[] = $result;
            } else {
                $updateArray[] = $result;
            }
        }

        $sqlDelete = "SELECT c.id AS `cid`, s.*
                      FROM `monitor_server_cache` AS `c` LEFT JOIN (
                      SELECT `monitor_server`.`id`, `monitor_server`.`name`, `monitor_server`.`prettyname`, `monitor_server`.`charts`,
                            `monitor_server`.`group`, `monitor_server`.`auto_scaled`, `monitor_server`.`enabled`, 
                          MAX(`host_monitoring`.`as_at`) AS `last_time`
                      FROM `monitor_server`
                      LEFT JOIN `host_monitoring` ON `host_monitoring`.`server_id` = `monitor_server`.`name`
                          AND `host_monitoring`.`as_at` > DATE_SUB(NOW(), INTERVAL 10 MINUTE)
                      WHERE `monitor_server`.`enabled` = 1
                      GROUP BY `monitor_server`.`id`, `monitor_server`.`name`, `monitor_server`.`prettyname`, `monitor_server`.`charts`,
                           `monitor_server`.`group`, `monitor_server`.`auto_scaled`, `monitor_server`.`enabled`
                      ) AS `s` ON `c`.`id` = `s`.`id`";

        $resultDelete = $this->db->query($sqlDelete)->fetchAll();

        foreach ($resultDelete as $result) {
            if (empty($result['id'])) {
                $deleteArray[] = ['cid' => $result['cid']];
            }
        }

        if (!empty($insertArray)) {
            $sqlCacheInsert = "INSERT INTO `monitor_server_cache`(`id`, `name`, `prettyname`, `charts`, `group`, `auto_scaled`, `enabled`, `last_time`) 
                           VALUES(:id, :name, :prettyname, :charts, :group, :auto_scaled, :enabled, :last_time)";

            if (($stmtCacheInsert = $this->db->prepare($sqlCacheInsert)) === false) {
                throw new PDOCreationException('PDOStatement creation failed with sql {' . $sqlCacheInsert . '}.');
            }

            foreach ($insertArray as $insert) {
                $stmtCacheInsert->execute([
                    'id'          => $insert['id'],
                    'name'        => $insert['name'],
                    'prettyname'  => $insert['prettyname'],
                    'charts'      => $insert['charts'],
                    'group'       => $insert['group'],
                    'auto_scaled' => $insert['auto_scaled'],
                    'enabled'     => $insert['enabled'],
                    'last_time'   => $insert['last_time']
                ]);
            }
        }

        if (!empty($updateArray)) {
            $sqlCacheUpdate = "UPDATE `monitor_server_cache`
                               SET `name` = :name, `prettyname` = :prettyname, `charts` = :charts, `group` = :group, 
                                   `auto_scaled` = :auto_scaled, `enabled` = :enabled, `last_time` = :last_time
                               WHERE `id` = :id";

            if (($stmtCacheUpdate = $this->db->prepare($sqlCacheUpdate)) === false) {
                throw new PDOCreationException('PDOStatement creation failed with sql {' . $sqlCacheUpdate . '}.');
            }

            foreach ($updateArray as $update) {
                $stmtCacheUpdate->execute([
                    'id'          => $update['id'],
                    'name'        => $update['name'],
                    'prettyname'  => $update['prettyname'],
                    'charts'      => $update['charts'],
                    'group'       => $update['group'],
                    'auto_scaled' => $update['auto_scaled'],
                    'enabled'     => $update['enabled'],
                    'last_time'   => $update['last_time']
                ]);
            }
        }

        if (!empty($deleteArray)) {
            $sqlCacheDelete = "DELETE FROM `monitor_server_cache`
                               WHERE `id` = :id";

            if (($stmtCacheDelete = $this->db->prepare($sqlCacheDelete)) === false) {
                throw new PDOCreationException('PDOStatement creation failed with sql {' . $sqlCacheDelete . '}.');
            }

            foreach ($deleteArray as $delete) {
                $stmtCacheDelete->execute(['id' => $delete['cid']]);
            }
        }

        // Calculate cache and update/insert rows in table `monitor_server_cache`
        $sql = "SELECT `id`, `name`, `group` 
                FROM `monitor_server_cache` 
                ORDER BY `id` ASC";

        $results = $this->db->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

        // Prepare PDOStatement for operations on table `monitor_server_cache` & `monitor_server_disk_warn_cache`
        $sqlHistory = "SELECT * 
                       FROM `host_monitoring` 
                       WHERE `server_id` = :name
                           AND `as_at` > DATE_SUB(NOW(), INTERVAL 10 MINUTE) 
                       ORDER BY `as_at` ASC";

        if (($stmtHistory = $this->db->prepare($sqlHistory)) === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        $sqlDisk = "SELECT `name`, `label`, (((`size` - `free`) / `size`) * 100) AS `usage`, `as_at`, `size`, `free`
                    FROM `disk_monitoring`
                    WHERE `server_id` = :name";

        if (($stmtDisk = $this->db->prepare($sqlDisk)) === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        $sqlDeleteDiskCache = "DELETE FROM `monitor_server_disk_warn_cache` 
                               WHERE `monitor_server_cache_id` = :monitor_server_cache_id";

        if (($stmtDeleteDiskCache = $this->db->prepare($sqlDeleteDiskCache)) === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        $sqlInsertDiskCache = "INSERT INTO `monitor_server_disk_warn_cache`(`title`, `name`, `label`, `usage`, `monitor_server_cache_id`) 
                               VALUES (:title, :name, :label, :usage, :monitor_server_cache_id)";

        if (($stmtInsertDiskCache = $this->db->prepare($sqlInsertDiskCache)) === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        $sqlUpdateServerCache = "UPDATE `monitor_server_cache` 
                                 SET `email` = :email, 
                                     `session` = :session, 
                                     `cpu_current` = :cpu_current,  
                                     `mem_last` = :mem_last, 
                                     `mem_total` = :mem_total,
                                     `good_drives` = :good_drives, 
                                     `disk_title_good` = :disk_title_good, 
                                     `ok_drives` = :ok_drives, 
                                     `disk_title_ok` = :disk_title_ok
                                 WHERE `id` = :id";

        if (($stmtUpdateServerCache = $this->db->prepare($sqlUpdateServerCache)) === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        foreach ($results as $row) {
            $memChart      = '';
            $cpuChart      = '';
            $cpuLast       = -1;
            $cpuCurrent    = 0;
            $timeDiff      = 0;
            $sessionLast   = null;
            $emailLast     = null;
            $memLast       = 0;
            $memTotal      = 34000;
            $goodDrives    = '';
            $okDrives      = '';
            $diskTitleGood = '';
            $diskTitleOk   = '';

            // Calculate email/session/cpu/mem info
            if ($stmtHistory->execute(['name' => $row['name']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlHistory);
            }

            while ($rowHistory = $stmtHistory->fetch()) {
                if (!empty($memChart)) {
                    $memChart .= ',';
                }

                $memLast = max($rowHistory['mem_work'], $rowHistory['mem_priv'], $rowHistory['mem_paged']);
                $memChart .= round($memLast / 1000000, 0);

                if ($cpuLast == -1) {
                    $cpuLast = $rowHistory['cpu_used_micros'];
                    $atLast  = $rowHistory['as_at'];
                } else {
                    if (!empty($cpuChart)) {
                        $cpuChart .= ',';
                    }

                    $timeDiff = (new \DateTime($rowHistory['as_at'], new \DateTimeZone('UTC')))->getTimestamp() - (new \DateTime($atLast, new \DateTimeZone('UTC')))->getTimestamp();

                    if ($timeDiff > 0) {
                        $cpuCurrent = ($rowHistory['cpu_used_micros'] - $cpuLast) / $timeDiff;
                        $cpuCurrent /= $rowHistory['cpu_count'] * 10000;
                        $cpuCurrent = round($cpuCurrent, 2);
                        $cpuChart .= $cpuCurrent;
                    }

                    $cpuLast = $rowHistory['cpu_used_micros'];

                    if ($memLast > 0) {
                        $atLast = $rowHistory['as_at'];
                    }
                }

                if (is_numeric($rowHistory['email'])) {
                    $emailLast = $rowHistory['email'];
                }

                if (is_numeric($rowHistory['session'])) {
                    $sessionLast = $rowHistory['session'];
                }

                if (is_numeric($rowHistory['mem_total'])) {
                    $memTotal = round($rowHistory['mem_total'] / 1000000, 0);
                }
            }

            if (substr($memChart, -1) == ',') {
                $memChart = substr($memChart, 0, -1);
            }

            if (substr($cpuChart, -1) == ',') {
                $cpuChart = substr($cpuChart, 0, -1);
            }

            // Clean disk warn cache for the current server_cache_id in table `monitor_server_disk_warn_cache`
            if ($stmtDeleteDiskCache->execute(['monitor_server_cache_id' => $row['id']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDeleteDiskCache);
            }

            // Calculate disk info
            if ($stmtDisk->execute(['name' => $row['name']]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlDisk);
            }

            while ($rowDisk = $stmtDisk->fetch()) {
                $diskWarn  = '';
                $diskUsage = round($rowDisk['usage'], 1);
                $diskUsed  = $rowDisk['size'] - $rowDisk['free'];

                if ($rowDisk['size'] < 1000) {
                    $diskTitle = $diskUsed . 'b / ' . $rowDisk['size'] . 'b';
                } elseif ($rowDisk['size'] < 1000000 && $rowDisk['size'] >= 1000) {
                    $diskTitle = round($diskUsed / 1000, 1) . 'k / ' . round($rowDisk['size'] / 1000, 1) . 'k';
                } elseif ($rowDisk['size'] < 1000000000 && $rowDisk['size'] >= 1000000) {
                    $diskTitle = round($diskUsed / 1000000, 1) . 'm / ' . round($rowDisk['size'] / 1000000, 1) . 'm';
                } else {
                    $diskTitle = round($diskUsed / 1000000000, 1) . 'g / ' . round($rowDisk['size'] / 1000000000, 1) . 'g';
                }

                if ($diskUsage >= 80) {
                    $diskWarn = 'earlywarn';
                }

                if ($diskUsage >= 90) {
                    $diskWarn              = 'warn';
                    $this->hasSeriousError = 1;
                    $this->errorCount++;
                }

                if ($diskUsage >= 95) {
                    $diskWarn                  = 'badbadbad';
                    $this->hasVerySeriousError = 1;
                    $this->errorCount++;
                }

                if ($diskUsage < 50) {
                    if (!empty($goodDrives)) {
                        $goodDrives .= ',';
                    }
                    $goodDrives .= str_replace('/dev', '', $rowDisk['name']);

                    if (!empty($diskTitleGood)) {
                        $diskTitleGood .= '&#013;';
                    }
                    $diskTitleGood .= str_replace('/dev', '', $rowDisk['name']) . ': ' . $diskTitle;
                }

                if ($diskUsage >= 50 && $diskUsage < 80) {
                    if (!empty($okDrives)) {
                        $okDrives .= ',';
                    }
                    $okDrives .= str_replace('/dev', '', $rowDisk['name']);

                    if (!empty($diskTitleOk)) {
                        $diskTitleOk .= '&#013;';
                    }
                    $diskTitleOk .= str_replace('/dev', '', $rowDisk['name']) . ': ' . $diskTitle;
                }

                if (!empty($diskWarn)) {
                    $values = [
                        'title'                   => $diskTitle,
                        'name'                    => $rowDisk['name'],
                        'label'                   => $rowDisk['label'],
                        'usage'                   => $diskUsage,
                        'monitor_server_cache_id' => $row['id']
                    ];

                    if ($stmtInsertDiskCache->execute($values) === false) {
                        throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlInsertDiskCache);
                    }
                }
            }

            // Update table `monitor_server_cache`
            $values = [
                'email'           => $emailLast,
                'session'         => $sessionLast,
                'cpu_current'     => $cpuCurrent,
                'mem_last'        => $memLast,
                'mem_total'       => $memTotal,
                'good_drives'     => $goodDrives,
                'disk_title_good' => $diskTitleGood,
                'ok_drives'       => $okDrives,
                'disk_title_ok'   => $diskTitleOk,
                'id'              => $row['id']
            ];

            if ($stmtUpdateServerCache->execute($values) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sqlUpdateServerCache);
            }

            // Draw image for cpu and mem via google api
            $command = 'php -f ' . __DIR__ . DIRECTORY_SEPARATOR . 'MonitorServerCacheImage.php ' . $row['id'] . ' ' . escapeshellarg($cpuChart) . ' ' . escapeshellarg($memTotal) . ' ' . escapeshellarg($memChart);
            $this->backgroundExec($command);

            usleep (100);
        }

        // Delete orphaned records from monitor_server_disk_warn_cache
        $sql = "DELETE `monitor_server_disk_warn_cache` 
                FROM `monitor_server_disk_warn_cache` 
                    LEFT JOIN `monitor_server_cache` ON `monitor_server_cache`.`id` = `monitor_server_disk_warn_cache`.`monitor_server_cache_id`
                WHERE `monitor_server_cache`.`id` IS NULL";

        if ($this->db->exec($sql) === false) {
            throw new PDOExecutionException('Failed to execute sql statement for deleting orphaned monitor_server_disk_warn_cache records');
        }
    }

    private function backgroundExec($cmd)
    {
        if (strtoupper(substr(php_uname(), 0, 3)) == 'WIN') {
            pclose(popen('start /B ' . $cmd, 'r'));
        } else {
            exec($cmd . ' > /dev/null 2> /dev/null &');
        }
    }
}
