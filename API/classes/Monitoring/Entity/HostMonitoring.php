<?php

namespace FS\Monitoring\Entity;

use FS\Common\Exception\InvalidParameterException;

class HostMonitoring
{
    #region fields
    protected $serverId;
    protected $cpuUsed;
    protected $cpuCount;
    protected $memVirt;
    protected $memWork;
    protected $memPriv;
    protected $memPaged;
    protected $emailCount;
    protected $sessionCount;
    protected $memTotal;
    protected $asAt;
    #endregion

    #region Methods
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['serverId'])) {
            if (strlen($data['serverId']) > 100) {
                $this->serverId = substr($data['serverId'], 0, 100);
            } else {
                $this->serverId = $data['serverId'];
            }
        } else {
            $this->serverId = '';
        }

        if (isset($data['cpuUsed'])) {
            $this->cpuUsed = $data['cpuUsed'];
        } else {
            $this->cpuUsed = 0;
        }

        if (isset($data['cpuCount'])) {
            $this->cpuCount = $data['cpuCount'];
        } else {
            $this->cpuCount = 1;
        }

        if (isset($data['memVirt'])) {
            $this->memVirt = $data['memVirt'];
        } else {
            $this->memVirt = 0;
        }

        if (isset($data['memWork'])) {
            $this->memWork = $data['memWork'];
        } else {
            $this->memWork = 0;
        }

        if (isset($data['memPriv'])) {
            $this->memPriv = $data['memPriv'];
        } else {
            $this->memPriv = 0;
        }

        if (isset($data['memPaged'])) {
            $this->memPaged = $data['memPaged'];
        } else {
            $this->memPaged = 0;
        }

        if (isset($data['emailCount'])) {
            $this->emailCount = $data['emailCount'];
        } else {
            $this->emailCount = null;
        }

        if (isset($data['sessionCount'])) {
            $this->sessionCount = $data['sessionCount'];
        } else {
            $this->sessionCount = null;
        }

        if (isset($data['memTotal'])) {
            $this->memTotal = $data['memTotal'];
        } else {
            $this->memTotal = null;
        }

        if (isset($data['asAt'])) {
            $asAt       = (new \DateTime($data['asAt']))->setTimezone(new \DateTimeZone('UTC'));
            $this->asAt = $asAt->format('Y-m-d H:i:s');
        } else {
            $this->asAt = (new \DateTime())->setTimezone(new \DateTimezone('UTC'))->format('Y-m-d H:i:s');
        }
    }
    #endregion

    #region Getters & Setters
    public function getServerId()
    {
        return $this->serverId;
    }

    public function setServerId($serverId)
    {
        if (strlen($serverId) > 100) {
            $this->serverId = substr($serverId, 0, 100);
        } else {
            $this->serverId = $serverId;
        }
    }

    public function getCpuUsed()
    {
        return $this->cpuUsed;
    }

    public function setCpuUsed($cpuUsed)
    {
        $this->cpuUsed = $cpuUsed;
    }

    public function getCpuCount()
    {
        return $this->cpuCount;
    }

    public function setCpuCount($cpuCount)
    {
        $this->cpuCount = $cpuCount;
    }

    public function getMemVirt()
    {
        return $this->memVirt;
    }

    public function setMemVirt($memVirt)
    {
        $this->memVirt = $memVirt;
    }

    public function getMemWork()
    {
        return $this->memWork;
    }

    public function setMemWork($memWork)
    {
        $this->memWork = $memWork;
    }

    public function getMemPriv()
    {
        return $this->memPriv;
    }

    public function setMemPriv($memPriv)
    {
        $this->memPriv = $memPriv;
    }

    public function getMemPaged()
    {
        return $this->memPaged;
    }

    public function setMemPaged($memPaged)
    {
        $this->memPaged = $memPaged;
    }

    public function getEmailCount()
    {
        return $this->emailCount;
    }

    public function setEmailCount($emailCount)
    {
        $this->emailCount = $emailCount;
    }

    public function getSessionCount()
    {
        return $this->sessionCount;
    }

    public function setSessionCount($sessionCount)
    {
        $this->sessionCount = $sessionCount;
    }

    public function getMemTotal()
    {
        return $this->memTotal;
    }

    public function setMemTotal($memTotal)
    {
        $this->memTotal = $memTotal;
    }

    public function getAsAt()
    {
        return $this->asAt;
    }

    public function setAsAt($asAt)
    {
        $this->asAt = $asAt;
    }
    #endregion
}
