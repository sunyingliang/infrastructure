<?php

namespace FS\Monitoring\Entity;

use FS\Common\Exception\InvalidParameterException;

class DiskMonitoring
{
    #region Fields
    protected $serverId;
    protected $name;
    protected $label;
    protected $type;
    protected $free;
    protected $size;
    #endregion

    #region Methods
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['serverId'])) {
            if (strlen($data['serverId']) > 100) {
                $this->serverId = substr($data['serverId'], 0, 100);
            } else {
                $this->serverId = $data['serverId'];
            }
        } else {
            $this->serverId = '';
        }

        if (isset($data['name'])) {
            if (strlen($data['name']) > 100) {
                $this->name = substr($data['name'], 0, 100);
            } else {
                $this->name = $data['name'];
            }
        } else {
            $this->name = '';
        }

        if (isset($data['label'])) {
            if (strlen($data['label']) > 100 ) {
                $this->label = substr($data['label'], 0, 100);
            } else {
                $this->label = $data['label'];
            }
        } else {
            $this->label = '';
        }

        if (isset($data['type'])) {
            if (strlen($data['type']) > 100) {
                $this->type = substr($data['type'], 0, 100);
            } else {
                $this->type = $data['type'];
            }
        } else {
            $this->type = '';
        }

        if (isset($data['free'])) {
            if (strlen($data['free']) > 100) {
                $this->free = substr($data['free'], 0, 100);
            } else {
                $this->free = $data['free'];
            }
        } else {
            $this->free = 0;
        }

        if (isset($data['size'])) {
            $this->size = $data['size'];
        } else {
            $this->size = 0;
        }
    }
    #endregion

    #region Getters and Setters
    public function getServerId()
    {
        return $this->serverId;
    }

    public function setServerId($serverId)
    {
        if (strlen($serverId) > 100) {
            $this->serverId = substr($serverId, 0, 100);
        } else {
            $this->serverId = $serverId;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if (strlen($name) > 100) {
            $this->name = substr($name, 0, 100);
        } else {
            $this->name = $name;
        }
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        if (strlen($label) > 100) {
            $this->label = substr($label, 0, 100);
        } else {
            $this->label = $label;
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        if (strlen($type) > 100) {
            $this->type = substr($type, 0, 100);
        } else {
            $this->type = $type;
        }
    }

    public function getFree()
    {
        return $this->free;
    }

    public function setFree($free)
    {
        $this->free = $free;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }
    #endregion
}
