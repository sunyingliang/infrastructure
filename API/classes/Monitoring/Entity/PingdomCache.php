<?php

namespace FS\Monitoring\Entity;

use FS\Common\Exception\InvalidParameterException;

class PingdomCache
{
    #region Fields
    protected $id;
    protected $created;
    protected $name;
    protected $hostName;
    protected $resolution;
    protected $type;
    protected $lastErrorTime;
    protected $lastTestTime;
    protected $lastResponseTime;
    protected $status;
    protected $asAt;
    #endregion

    #region Methods
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['created'])) {
            $this->created = $data['created'];
        } else {
            $this->created = 0;
        }

        if (isset($data['name'])) {
            if (strlen($data['name']) > 50) {
                $this->name = substr($data['name'], 0, 50);
            } else {
                $this->name = $data['name'];
            }
        } else {
            $this->name = '';
        }

        if (isset($data['hostname'])) {
            if (strlen($data['hostname']) > 100) {
                $this->hostName = substr($data['hostname'], 0, 100);
            } else {
                $this->hostName = $data['hostname'];
            }
        } else {
            $this->hostName = '';
        }

        if (isset($data['resolution'])) {
            $this->resolution = $data['resolution'];
        } else {
            $this->resolution = 0;
        }

        if (isset($data['type'])) {
            if (strlen($data['type']) > 50) {
                $this->type = substr($data['type'], 0, 50);
            } else {
                $this->type = $data['type'];
            }
        } else {
            $this->type = '';
        }

        if (isset($data['lasterrortime'])) {
            $this->lastErrorTime = $data['lasterrortime'];
        }

        if (isset($data['lasttesttime'])) {
            $this->lastTestTime = $data['lasttesttime'];
        }

        if (isset($data['lastresponsetime'])) {
            $this->lastResponseTime = $data['lastresponsetime'];
        }

        if (isset($data['status'])) {
            if (strlen($data['status']) > 50) {
                $this->status = substr($data['status'], 0, 50);
            } else {
                $this->status = $data['status'];
            }
        } else {
            $this->status = '';
        }

        if (isset($data['asAt'])) {
            $this->asAt = $data['asAt'];
        }
    }
    #endregion

    #region Getters / Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getHostName()
    {
        return $this->hostName;
    }

    public function setHostName($hostName)
    {
        $this->hostName = $hostName;
    }

    public function getResolution()
    {
        return $this->resolution;
    }

    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getLastErrorTime()
    {
        return $this->lastErrorTime;
    }

    public function setLastErrorTime($lastErrorTime)
    {
        $this->lastErrorTime = $lastErrorTime;
    }

    public function getLastTestTime()
    {
        return $this->lastTestTime;
    }

    public function setLastTestTime($lastTestTime)
    {
        $this->lastTestTime = $lastTestTime;
    }

    public function getLastResponseTime()
    {
        return $this->lastResponseTime;
    }

    public function setLastResponseTime($lastResponseTime)
    {
        $this->lastResponseTime = $lastResponseTime;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getAsAt()
    {
        return $this->asAt;
    }

    public function setAsAt($asAt)
    {
        $this->asAt = $asAt;
    }
    #endregion
}
