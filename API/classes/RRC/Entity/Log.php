<?php

namespace FS\RRC\Entity;

use FS\Common\Exception\InvalidParameterException;

class Log
{
    protected $id;
    protected $dateCreated;
    protected $ipAddress;
    protected $controlType;
    protected $controlVersion;
    protected $siteUrl;
    protected $customerKey;
    protected $method;
    protected $pageType;
    protected $serverId;
    protected $path;

    public function __construct($data)
    {
        $mandatoryFields = ['ipAddress', 'controlType', 'controlVersion', 'siteUrl', 'method', 'pageType', 'serverid'];

        foreach ($mandatoryFields as $field) {
            if (!isset($data[$field])) {
                throw new InvalidParameterException('Parameter {' . $field . '} is missing.');
            }
        }

        // Optional path
        $this->path = '';

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['dateCreated'])) {
            // Regex for valid date format
            $regexp = '/^(\d{4})-(\d(2))-(\d{2})\s{1}(\d{2}):(\d{2}):(\d{2})$/';

            preg_match($regexp, $data['dateCreated'], $matches);

            if (empty($matches)) {
                throw new InvalidParameterException('Passed in parameter {dateCreated} is not legal date. e.g. YYYY-MM-DD hh:mm:ss');
            } else {
                $this->dateCreated = $data['dateCreated'];
            }
        }

        if (strlen($data['ipAddress']) > 255) {
            throw new InvalidParameterException('Passed in parameter {ipAddress} is longer than the limited maximum length(255).');
        } else {
            $this->ipAddress = $data['ipAddress'];
        }

        if (strlen($data['controlType']) > 255) {
            throw new InvalidParameterException('Passed in parameter {controlType} is longer than the limited maximum length(255).');
        } else {
            $this->controlType = $data['controlType'];
        }

        if (strlen($data['controlVersion']) > 255) {
            throw new InvalidParameterException('Passed in parameter {controlVersion} is longer than the limited maximum length(255).');
        } else {
            $this->controlVersion = $data['controlVersion'];
        }

        if (strlen($data['siteUrl']) > 255) {
            throw new InvalidParameterException('Passed in parameter {siteUrl} is longer than the limited maximum length(255).');
        } else {
            $this->siteUrl = $data['siteUrl'];
        }

        if (strlen($data['method']) > 255) {
            throw new InvalidParameterException('Passed in parameter {method} is longer than the limited maximum length(255).');
        } else {
            $this->method = $data['method'];
        }

        if (strlen($data['pageType']) > 255) {
            throw new InvalidParameterException('Passed in parameter {pageType} is longer than the limited maximum length(255).');
        } else {
            $this->pageType = $data['pageType'];
        }

        if (strlen($data['serverid']) > 255) {
            throw new InvalidParameterException('Passed in parameter {serverid} is longer than the limited maximum length(255).');
        } else {
            $this->serverId = $data['serverid'];
        }

        // Optional path parameter
        if (isset(($data['path']))) {
            if (strlen($data['path']) > 1024) {
                throw new InvalidParameterException('Passed in parameter {path} is longer than the limited maximum length(1024).');
            }

            $this->path = $data['path'];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    public function getControlType()
    {
        return $this->controlType;
    }

    public function getControlVersion()
    {
        return $this->controlVersion;
    }

    public function getSiteUrl()
    {
        return $this->siteUrl;
    }

    public function getCustomerKey()
    {
        return $this->customerKey;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getPageType()
    {
        return $this->pageType;
    }

    public function getServerId()
    {
        return $this->serverId;
    }

    public function getPath()
    {
        return $this->path;
    }
}
