<?php

namespace FS\RRC\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\RRC\Entity\Log as RRCLogEntity;

class Log extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['tracking'], ['write']);
    }

    public function log()
    {
        if (!($validation = IO::required($this->data, ['ipAddress', 'controlType', 'controlVersion', 'siteUrl', 'method', 'pageType', 'serverid']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $entity = new RRCLogEntity($this->data);

        $sql = "INSERT INTO `rrc_log`(`ip_address`, `control_type`, `control_version`, `site_url`, `method`, `page_type`, `server_id`, `path`) VALUES 
                    (:ip_address, :control_type, :control_version, :site_url, :method, :page_type, :server_id, :path)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $result = $stmt->execute([
            "ip_address"      => $entity->getIpAddress(),
            "control_type"    => $entity->getControlType(),
            "control_version" => $entity->getControlVersion(),
            "site_url"        => $entity->getSiteUrl(),
            "method"          => $entity->getMethod(),
            "page_type"       => $entity->getPageType(),
            "server_id"       => $entity->getServerId(),
            "path"            => $entity->getPath()
        ]);

        if (!$result) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->responseArr['message'] = 'RRC log succeed';

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }
}
