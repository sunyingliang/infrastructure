<?php

namespace FS\RRC\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\RRC\Entity\Log as RRCLogEntity;
use FS\InfrastructureBase;

class Statistics extends InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure'], ['write']);
    }

    public function getList()
    {
        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $totalHitsBySite = IO::default($this->data, 'totalHitsBySite');
        $nafHitsBySite = IO::default($this->data, 'nafHitsBySite');
        $totalFormSubmissionBySite = IO::default($this->data, 'totalFormSubmissionBySite');
        $clientsInUse = IO::default($this->data, 'clientsInUse');

        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'date');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        $searchString = "";
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, 'date >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, 'date <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($totalHitsBySite)) {
            $searchString           = $this->appendSearch($searchString, 'total_hits_by_site LIKE :totalHitsBySite');
            $parameters['totalHitsBySite'] = '%' . $totalHitsBySite . '%';
        }

        if (!empty($nafHitsBySite)) {
            $searchString         = $this->appendSearch($searchString, 'naf_hits_by_site LIKE :nafHitsBySite');
            $parameters['nafHitsBySite'] = '%' . $nafHitsBySite . '%';
        }

        if (!empty($totalFormSubmissionBySite)) {
            $searchString         = $this->appendSearch($searchString, 'total_form_submission_by_site LIKE :totalFormSubmissionBySite');
            $parameters['totalFormSubmissionBySite'] = '%' . $totalFormSubmissionBySite . '%';
        }

        if (!empty($clientsInUse)) {
            $searchString         = $this->appendSearch($searchString, 'clients_in_use LIKE :clientsInUse');
            $parameters['clientsInUse'] = '%' . $clientsInUse . '%';
        }

        $stmt = $this->pdo->prepare('SELECT COUNT(1) FROM `rrc_statistics` ' . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = 'SELECT * FROM `rrc_statistics` '
            . $searchString
            . ' ORDER BY ' . $sort .' '. $sortOrder . ' LIMIT :offset, :limit';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);

        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }
}
