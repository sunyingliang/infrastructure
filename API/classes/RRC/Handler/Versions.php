<?php

namespace FS\RRC\Handler;

use FS\Common\CURL;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;
use FS\InfrastructureBase;
use FS\Common\Exception\PDOCreationException;

class Versions extends InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['infrastructure']);
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $name    = IO::default($this->data, 'name');
        $url     = IO::default($this->data, 'url');
        $version = IO::default($this->data, 'version');

        $searchString = '';
        $parameters   = [];

        if (!empty($name)) {
            $searchString       = $this->appendSearch($searchString, 'name LIKE :name');
            $parameters['name'] = '%' . $name . '%';
        }

        if (!empty($url)) {
            $searchString      = $this->appendSearch($searchString, 'url LIKE :url');
            $parameters['url'] = '%' . $url . '%';
        }

        if (!empty($version)) {
            $searchString          = $this->appendSearch($searchString, 'version LIKE :version');
            $parameters['version'] = '%' . $version . '%';
        }

        $sql = "SELECT `id`, `name`, `url`, `version`, `enabled`, `last_checked` FROM `rrc_versions` " . $searchString . " ORDER BY `name` ASC";

        $stmt = $this->pdo->prepare($sql);
        $stmt = $this->bindParams($stmt, $parameters);
        $stmt->execute();

        $this->responseArr['data'] = $stmt->fetchAll();
        return $this->responseArr;
    }

    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['name', 'url']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $stmt = $this->pdo->prepare("INSERT INTO `rrc_versions` (`name`, `url`) VALUES (:name, :url)");
        $stmt->execute([
            'name'    => $this->data['name'],
            'url'     => $this->data['url']
        ]);

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        $columns = '';
        $values  = ['id' => $this->data['id']];

        if (!empty($this->data['name'])) {
            $columns        .= empty($columns) ? '`name` =:name' : ', `name` =:name';
            $values['name'] = $this->data['name'];
        }

        if (!empty($this->data['url'])) {
            $columns       .= empty($columns) ? '`url` =:url' : ', `url` =:url';
            $values['url'] = $this->data['url'];
        }

        if ($this->data['enabled'] == '1' || ($this->data['enabled'] == '0')) {
            $columns           .= empty($columns) ? '`enabled` =:enabled' : ', `enabled` =:enabled';
            $values['enabled'] = $this->data['enabled'];
        }

        $stmt = $this->pdo->prepare("UPDATE `rrc_versions` SET " . $columns . " WHERE `id` = :id");

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        return $this->responseArr;

    }

    public function delete()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $stmt = $this->pdo->prepare("DELETE FROM `rrc_versions` WHERE `id` = :id");

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function check($cronjob = false)
    {
        $errorString = '';

        if (($cronjob == true) || (isset($this->data['data']) && $this->data['data'] == 'all')) {
            $results = $this->pdo->query("SELECT `id`, `url` FROM `rrc_versions` WHERE `enabled`=1")->fetchAll();
            foreach ($results as $result) {
                $response = $this->updateVersion($result['id'], $result['url']);
                if ($response['status'] == 'error') {
                    $errorString .= $response['message'] . PHP_EOL;
                }
            }
        } else if (!empty($this->data['id'] && !empty($this->data['url']))) {
            $stmt = $this->pdo->prepare("SELECT `id`, `url` FROM `rrc_versions` WHERE `id` = :id AND `enabled` = 1");
            $stmt->execute(['id' => $this->data['id']]);
            if ($stmt->rowCount() > 0) {
                $response = $this->updateVersion($this->data['id'], $this->data['url']);
                if ($response['status'] == 'error') {
                    $errorString .= $response['message'] . PHP_EOL;
                }
            } else {
                throw new \Exception('This RRC Version check is disabled, please enable.');
            }
        } else {
            throw new InvalidParameterException('Please pass in parameter {id}, {url}');
        }

        if (!empty($errorString)) {
            throw new \Exception($errorString);
        }

        return $this->responseArr;
    }

    // Get RRC Version from endpoint and update rrc_versions table
    private function updateVersion($id, $url)
    {
        try {
            // Set version blank before checking version
            $stmt = $this->pdo->prepare("UPDATE `rrc_versions` SET `version` = :version, `last_checked` = CURRENT_TIMESTAMP WHERE `id` =:id");
            $stmt->execute(['version' => '', 'id' => $id]);

            $options = [
                CURLOPT_URL            => $url,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST           => true
            ];

            $curl     = CURL::init($options);
            $response = CURL::exec($curl);

            CURL::close($curl);

            if ($response === false) {
                throw new \Exception('Failed to get response from {' . $url . '}. Please check the url is valid. ');
            }

            $response = json_decode($response);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception('Could not parse data returned from ' . $url);
            }

            if (!is_null($response) && isset($response->version)) {
                $newVersion = $response->version;
            } else {
                throw new \Exception('Failed to get version from {' . $url . '}. Please check the url is valid. ');
            }

            // If get new version successfully, update rrc record with new version
            $stmt->execute(['version' => $newVersion, 'id' => $id]);
        } catch (\Exception $ex) {
            return ['status' => 'error', 'message' => $ex->getMessage()];
        }

        return ['status' => 'success'];
    }
}