<?php

namespace FS\OAF\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\OAF\Entity\BackgroundLogging as BackgroundLoggingEntity;

class BackgroundLogging extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['log'], ['write']);
    }

    public function create()
    {
        $data['serverId'] = IO::default($this->data, 'serverID');
        $data['op']       = IO::default($this->data, 'op');

        if (is_null($data['serverId']) || empty($data['serverId'])) {
            throw new InvalidParameterException('No serverId specified!');
        }

        if (is_null($data['op']) || empty($data['op'])) {
            throw new InvalidParameterException('No operation specified!');
        }

        switch ($data['op']) {
            case 'logBackgroundStart':
            case 'logBackgroundEnd':
                $backgroundLogging = new BackgroundLoggingEntity($this->data);
                $columns           = "`server_id`, `site`, `operation`";
                $params            = ":server_id, :site, :operation";
                $values            = [
                    'server_id' => $backgroundLogging->getServerId(),
                    'site'      => $backgroundLogging->getSite(),
                    'operation' => $data['op']
                ];

                $sql = "INSERT INTO `oaf_background` (" . $columns . ") VALUES (" . $params . ")";

                $stmt = $this->pdo->prepare($sql);

                if ($stmt === false) {
                    throw new PDOCreationException('PDOStatement creation failed.');
                }

                if ($stmt->execute($values) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
                }

                $this->responseArr['message'] = 'logged!';
                break;
            default:
                throw new InvalidParameterException('Operation is not valid!');
                break;
        }

        return $this->responseArr;
    }
}
