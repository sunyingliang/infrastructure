<?php

namespace FS\OAF\Handler;

use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\OAF\Entity\PlatformErrors as PlatformErrorsEntity;

class PlatformErrors extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['tracking'], ['write']);
    }

    public function create()
    {
        if (!($validation = IO::required($this->data, ['host', 'server', 'requestFolders', 'requestParams', 'error', 'location']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $entity = new PlatformErrorsEntity($this->data);

        $sql = "INSERT INTO platform_errors
                    (`error`, `location`, `host`, `server`, `request_folders`, `request_params`) VALUES 
                    (:error, :location, :host, :server, :request_folders, :request_params)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $result = $stmt->execute([
            "error"           => $entity->getError(),
            "location"        => $entity->getLocation(),
            "host"            => $entity->getHost(),
            "server"          => $entity->getServer(),
            "request_folders" => $entity->getRequestFolders(),
            "request_params"  => $entity->getRequestParams(),
        ]);

        if (!$result) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $fromDate       = IO::default($this->data, 'fromDate');
        $toDate         = IO::default($this->data, 'toDate');
        $error          = IO::default($this->data, 'error');
        $location       = IO::default($this->data, 'location');
        $host           = IO::default($this->data, 'host');
        $server         = IO::default($this->data, 'server');
        $requestFolders = IO::default($this->data, 'request_folders');
        $requestParams  = IO::default($this->data, 'request_params');
        $offset         = IO::default($this->data, 'offset', 0);
        $limit          = IO::default($this->data, 'limit', 30);
        $sort           = IO::default($this->data, 'sort', 'id');
        $sortOrder      = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['when', 'error',  'location', 'host', 'server', 'request_folders', 'request_params'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, '`when` >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, '`when` <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($error)) {
            $searchString       = $this->appendSearch($searchString, '`error` LIKE :error');
            $parameters['error'] = '%' . $error . '%';
        }

        if (!empty($location)) {
            $searchString            = $this->appendSearch($searchString, '`location` LIKE :location');
            $parameters['location'] = '%' . $location . '%';
        }
        if (!empty($host)) {
            $searchString       = $this->appendSearch($searchString, '`host` LIKE :host');
            $parameters['host'] = '%' . $host . '%';
        }

        if (!empty($server)) {
            $searchString            = $this->appendSearch($searchString, '`server` LIKE :server');
            $parameters['server'] = '%' . $server . '%';
        }
        if (!empty($requestFolders)) {
            $searchString       = $this->appendSearch($searchString, '`request_folders` LIKE :request_folders');
            $parameters['request_folders'] = '%' . $requestFolders . '%';
        }

        if (!empty($requestParams)) {
            $searchString            = $this->appendSearch($searchString, '`request_params` LIKE :request_params');
            $parameters['request_params'] = '%' . $requestParams . '%';
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM `platform_errors`" . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT * FROM `platform_errors` "
            . $searchString
            . " ORDER BY `" . $sort . "` " . $sortOrder . " LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }
}
