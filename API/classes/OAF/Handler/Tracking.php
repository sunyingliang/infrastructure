<?php

namespace FS\OAF\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\OAF\Entity\Tracking as TrackingEntiry;

class Tracking extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['tracking'], ['write']);
    }

    public function create()
    {
        if (!($validation = IO::required($this->data, ['host', 'feature']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $entity = new TrackingEntiry($this->data);

        $sql = "INSERT INTO tracking(`host`, `feature`) VALUES (:host, :feature)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $result = $stmt->execute([
            "host"    => $entity->getHost(),
            "feature" => $entity->getFeature(),
        ]);

        if (!$result) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $fromDate  = IO::default($this->data, 'fromDate');
        $toDate    = IO::default($this->data, 'toDate');
        $host      = IO::default($this->data, 'host');
        $feature   = IO::default($this->data, 'feature');
        $offset    = IO::default($this->data, 'offset', 0);
        $limit     = IO::default($this->data, 'limit', 30);
        $sort      = IO::default($this->data, 'sort', 'id');
        $sortOrder = IO::default($this->data, 'sortOrder', 'DESC');

        if (!in_array($sort, ['when', 'host',  'feature'])) {
            $sort = 'id';
        }

        if (!in_array($sortOrder, ['ASC', 'DESC'])) {
            $sortOrder = 'DESC';
        }

        $searchString = '';
        $parameters   = [];

        if (!empty($fromDate)) {
            $searchString           = $this->appendSearch($searchString, '`when` >= :fromDate');
            $parameters['fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $searchString         = $this->appendSearch($searchString, '`when` <= :toDate');
            $parameters['toDate'] = $toDate . 'T23:59:59';
        }

        if (!empty($host)) {
            $searchString       = $this->appendSearch($searchString, '`host` LIKE :host');
            $parameters['host'] = '%' . $host . '%';
        }

        if (!empty($feature)) {
            $searchString            = $this->appendSearch($searchString, '`feature` LIKE :feature');
            $parameters['feature'] = '%' . $feature . '%';
        }

        $stmt = $this->pdo->prepare("SELECT COUNT(1) FROM `tracking`" . $searchString);
        $stmt->execute($parameters);
        $rowCount = $stmt->fetchColumn(0);

        $sql = "SELECT * FROM `tracking` "
                . $searchString
                . " ORDER BY `" . $sort . "` " . $sortOrder . " LIMIT :offset, :limit";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, \PDO::PARAM_INT);
        $stmt = $this->bindParams($stmt, $parameters);

        $stmt->execute();

        $this->responseArr['data'] = [
            'totalRowCount' => $rowCount,
            'rows'          => $stmt->fetchAll()
        ];

        return $this->responseArr;
    }
}
