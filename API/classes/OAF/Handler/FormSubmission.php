<?php

namespace FS\OAF\Handler;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;
use FS\OAF\Entity\FormSubmission as FormSubmissionEntity;

class FormSubmission extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['tracking'], ['write']);
    }

    public function create()
    {
        if (!($validation = IO::required($this->data, ['host', 'formName', 'formGroupName', 'status', 'impersonated', 'userType']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $entity = new FormSubmissionEntity($this->data);

        // Insert into Tracking
        $sql = "INSERT INTO form_submission_tracking
                    (`host`, `form_name`, `form_group_name`, `status`, `impersonated`, `user_type`, `product`) VALUES
                    (:host, :form_name, :form_group_name, :status, :impersonated, :user_type, :product)";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $result = $stmt->execute([
            ":host"            => $entity->getHost(),
            ":form_name"       => $entity->getFormName(),
            ":form_group_name" => $entity->getFormGroupName(),
            ":status"          => $entity->getStatus(),
            ":impersonated"    => $entity->getImpersonated(),
            ":user_type"       => $entity->getUserType(),
            ":product"         => $entity->getProduct()
        ]);

        if (!$result) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        // Retrieve customer_id from stats
        $statsPDO = IO::getPDOConnection(DATABASE_CONNECTION['stats']);

        $sqlCustomerId = "SELECT `customer_id` FROM `customer_domains` WHERE TRIM(`domain`) = :domain";

        $stmtCustomerId = $statsPDO->prepare($sqlCustomerId);

        if ($stmtCustomerId === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmtCustomerId->execute([":domain" => trim($entity->getHost())]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        if (!($rowCustomerId = $stmtCustomerId->fetch())) {
            $customerId = '00000000-0000-0000-0000-000000000000';
        } else {
            $customerId = $rowCustomerId['customer_id'];
        }

        // Insert into stats
        $sqlStats = "INSERT INTO form_submissions
                        (`domain`, `form_name`, `customer_id`, `timestamp`, `product`, `oaf`) VALUES
                        (:domain, :form_name, :customer_id, NOW(), :product, 1)";

        $stmtStats = $statsPDO->prepare($sqlStats);

        if ($stmtStats === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        $resultStats = $stmtStats->execute([
            ":domain"      => $entity->getHost(),
            ":form_name"   => $entity->getFormName(),
            ":customer_id" => $customerId,
            ":product"     => $entity->getProduct()
        ]);

        if (!$resultStats) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->responseArr['data'] = ['id' => $this->pdo->lastInsertId()];

        return $this->responseArr;
    }
}
