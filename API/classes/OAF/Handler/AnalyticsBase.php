<?php

namespace FS\OAF\Handler;

require __DIR__ . '/../php-ga/src/autoload.php';

use UnitedPrototype\GoogleAnalytics;
use FS\Common\Auth;

class AnalyticsBase
{
    protected $tracker;
    protected $session;
    protected $visitor;

    public function __construct()
    {
        $this->auth = new Auth();

        $this->auth->hasPermission(['read']);

        date_default_timezone_set('Europe/London');

        $this->tracker = new GoogleAnalytics\Tracker(GOOGLE_ANALYTICS['ga_id'], GOOGLE_ANALYTICS['ga_domain']);
        $this->visitor = new GoogleAnalytics\Visitor();
        $this->session = new GoogleAnalytics\Session();
    }
}
