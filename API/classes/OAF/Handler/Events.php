<?php

namespace FS\OAF\Handler;

use FS\Common\IO;
use UnitedPrototype\GoogleAnalytics;

class Events extends AnalyticsBase
{
    public function event()
    {
        $event = new GoogleAnalytics\Event();

        $inputData = IO::getPostParameters();

        if (isset($inputData['category'])) {
            $event->setCategory($inputData['category']);
        } else {
            $event->setCategory('No category');
        }

        if (isset($inputData['action'])) {
            $event->setAction($inputData['action']);
        } else {
            $event->setAction('No action');
        }

        if (isset($inputData['label'])) {
            $event->setLabel($inputData['label']);
        }
        
        if (isset($inputData['value'])) {
            $event->setValue($inputData['value']);
        }

        $this->tracker->trackEvent($event, $this->session, $this->visitor);

        return [
            'visitor-id'          => $this->visitor->getUniqueId(),
            'session-id'          => $this->session->getSessionId(),
            'first-visit-time'    => $this->visitor->getFirstVisitTime()->format('Y-m-d H:i:s'),
            'previous-visit-time' => $this->visitor->getPreviousVisitTime()->format('Y-m-d H:i:s'),
            'current-visit-time'  => $this->visitor->getCurrentVisitTime()->format('Y-m-d H:i:s'),
            'visit-count'         => $this->visitor->getVisitCount()
        ];
    }
}
