<?php

namespace FS\OAF\Handler;

use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;
use UnitedPrototype\GoogleAnalytics;

class PageViews extends AnalyticsBase
{
    public function pageViews()
    {
        $inputData = IO::getPostParameters();

        if (!isset($inputData['path'])) {
            throw new InvalidParameterException('Path not specified.');
        }

        $page = new GoogleAnalytics\Page($inputData['path']);

        $this->tracker->trackPageview($page, $this->session, $this->visitor);

        return [ 
            'visitor-id'          => $this->visitor->getUniqueId(),
            'session-id'          => $this->session->getSessionId(),
            'first-visit-time'    => $this->visitor->getFirstVisitTime()->format('Y-m-d H:i:s'),
            'previous-visit-time' => $this->visitor->getPreviousVisitTime()->format('Y-m-d H:i:s'),
            'current-visit-time'  => $this->visitor->getCurrentVisitTime()->format('Y-m-d H:i:s'),
            'visit-count'         => $this->visitor->getVisitCount()
        ];
    }
}


