<?php

namespace FS\OAF\Handler;

use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;

class Statistics extends \FS\InfrastructureBase
{
    public function __construct()
    {
        parent::__construct(DATABASE_CONNECTION['tracking'], ['read']);
    }

    public function getList()
    {
        $sql = "SELECT `id`, `period`, `data_date`, `type`
                    FROM `statistics_report`
                    ORDER BY `type`, `period`, `data_date` DESC";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->responseArr['data'] = [];

        // Fetch json data from database
        while ($row = $stmt->fetch()) {
            if (!isset($this->responseArr['data'][$row['type']])) {
                $this->responseArr['data'][$row['type']] = [];
            }

            if (!isset($this->responseArr['data'][$row['type']][$row['period']])) {
                $this->responseArr['data'][$row['type']][$row['period']] = [];
            }

            $this->responseArr['data'][$row['type']][$row['period']][] = [
                'id'   => $row['id'],
                'date' => $row['data_date']
            ];
        }

        return $this->responseArr;
    }

    public function report()
    {
        if (!isset($this->data['id'])) {
            throw new InvalidParameterException('Parameter {id} is not passed in correctly');
        }

        // Select json string from database
        $sql = "SELECT `data`
                    FROM `statistics_report` 
                    WHERE `id` = :id";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute(['id' => $this->data['id']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        // Fetch json data from database
        if ($row = $stmt->fetch()) {
            $this->responseArr['data'] = json_decode($row['data'], true);
        } else {
            $this->responseArr['data']    = '';
        }

        return $this->responseArr;
    }
}
