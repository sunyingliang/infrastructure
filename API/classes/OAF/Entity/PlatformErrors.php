<?php

namespace FS\OAF\Entity;

use FS\Common\Exception\InvalidParameterException;

class PlatformErrors
{
    protected $id;
    protected $error;
    protected $location;
    protected $when;
    protected $host;
    protected $server;
    protected $requestFolders;
    protected $requestParams;

    public function __construct($data)
    {
        $mandatoryFields = ['host', 'server', 'requestFolders', 'requestParams', 'error', 'location'];

        foreach ($mandatoryFields as $field) {
            if (!isset($data[$field])) {
                throw new InvalidParameterException('Parameter {' . $field . '} is missing.');
            }
        }

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['when'])) {
            $regexp = '/^(\d{4})-(\d{2})-(\d{2})\s{1}(\d{2}):(\d{2}):(\d{2})$/';

            preg_match($regexp, $data['when'], $matches);

            if (empty($matches)) {
                throw new InvalidParameterException('Passed in parameter {when} is not legal date. e.g. YYYY-MM-DD hh:mm:ss');
            } else {
                $this->when = $data['when'];
            }
        }

        if (strlen($data['host']) > 255) {
            throw new InvalidParameterException('Passed in parameter {host} is longer than the limited maximum length(255).');
        } else {
            $this->host = $data['host'];
        }

        if (strlen($data['server']) > 55) {
            throw new InvalidParameterException('Passed in parameter {server} is longer than the limited maximum length(55).');
        } else {
            $this->server = $data['server'];
        }

        if (strlen($data['requestFolders']) > 255) {
            throw new InvalidParameterException('Passed in parameter {requestFolders} is longer than the limited maximum length(255).');
        } else {
            $this->requestFolders = $data['requestFolders'];
        }

        if (isset($data['requestParams'])) {
            $this->requestParams = $data['requestParams'];
        }

        $this->error    = $data['error'];
        $this->location = $data['location'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getWhen()
    {
        return $this->when;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getRequestFolders()
    {
        return $this->requestFolders;
    }

    public function getRequestParams()
    {
        return $this->requestParams;
    }
}
