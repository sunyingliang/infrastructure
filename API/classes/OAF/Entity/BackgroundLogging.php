<?php

namespace FS\OAF\Entity;

class BackgroundLogging
{
    #region Fields
    protected $serverId;
    protected $site;
    #endregion

    #region Methods
    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        $this->serverId = isset($data['serverID']) ? $data['serverID'] : '';
        $this->site     = isset($data['site']) ? $data['site'] : '';
    }
    #endregion

    #region Getters and Setters
    public function getServerId()
    {
        return $this->serverId;
    }

    public function setServerId($serverId)
    {
        if (strlen($serverId) > 100) {
            throw new InvalidParameterException('Parameter {serverId} cannot be greater than 100 characters');
        }
        
        $this->serverId = $serverId;
    }

    public function getSite()
    {
        return $this->site;
    }

    public function setSite($site)
    {
        if (strlen($site) > 200) {
            throw new InvalidParameterException('Parameter {site} cannot be greater than 200 characters');
        }

        $this->site = $site;
    }

    #endregion
}
