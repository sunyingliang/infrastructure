<?php

namespace FS\OAF\Entity;

use FS\Common\Exception\InvalidParameterException;

class FormSubmission
{
    protected $id;
    protected $host;
    protected $when;
    protected $formName;
    protected $formGroupName;
    protected $status;
    protected $impersonated;
    protected $userType;
    protected $product;

    public function __construct($data)
    {
        $mandatoryFields = ['host', 'formName', 'formGroupName', 'status', 'impersonated', 'userType'];

        foreach ($mandatoryFields as $field) {
            if (!isset($data[$field])) {
                throw new InvalidParameterException('Parameter {' . $field . '} is missing.');
            }
        }

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['when'])) {
            $regexp = '/^(\d{4})-(\d(2))-(\d{2})\s{1}(\d{2}):(\d{2}):(\d{2})$/';

            preg_match($regexp, $data['when'], $matches);

            if (empty($matches)) {
                throw new InvalidParameterException('Passed in parameter {when} is not legal date. e.g. YYYY-MM-DD hh:mm:ss');
            } else {
                $this->when = $data['when'];
            }
        }

        if (isset($data['impersonated'])) {
            if ($data['impersonated'] >= 2) {
                throw new InvalidParameterException('Passed in parameter {impersonated} is not legal value. Either 0 or 1 is allowed.');
            } else {
                $this->impersonated = $data['impersonated'];
            }
        }

        if (isset($data['userType'])) {
            if (strlen($data['userType']) > 10) {
                throw new InvalidParameterException('Passed in parameter {userType} is longer than the limited maximum length(10).');
            } else {
                $this->userType = $data['userType'];
            }
        }

        if (isset($data['product'])) {
            if (strlen($data['product']) > 20) {
                throw new InvalidParameterException('Passed in parameter {userType} is longer than the limited maximum length(20).');
            } else {
                $this->product = $data['product'];
            }
        }else{
            $this->product = 'native';
        }

        if (strlen($data['host']) > 255) {
            throw new InvalidParameterException('Passed in parameter {host} is longer than the limited maximum length(255).');
        } else {
            $this->host = $data['host'];
        }

        if (strlen($data['formName']) > 255) {
            throw new InvalidParameterException('Passed in parameter {formName} is longer than the limited maximum length(255).');
        } else {
            $this->formName = $data['formName'];
        }

        if (strlen($data['formGroupName']) > 255) {
            throw new InvalidParameterException('Passed in parameter {formGroupName} is longer than the limited maximum length(255).');
        } else {
            $this->formGroupName = $data['formGroupName'];
        }

        if (strlen($data['status']) > 50) {
            throw new InvalidParameterException('Passed in parameter {status} is longer than the limited maximum length(50).');
        } else {
            $this->status = $data['status'];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getWhen()
    {
        return $this->when;
    }

    public function getFormName()
    {
        return $this->formName;
    }

    public function getFormGroupName()
    {
        return $this->formGroupName;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getImpersonated()
    {
        return $this->impersonated;
    }

    public function getUserType()
    {
        return $this->userType;
    }

    public function getProduct()
    {
        return $this->product;
    }
}
