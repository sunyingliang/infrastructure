<?php

namespace FS\OAF\Entity;

use FS\Common\Exception\InvalidParameterException;

class Tracking
{
    protected $id;
    protected $host;
    protected $feature;
    protected $when;

    public function __construct($data)
    {
        $mandatoryFields = ['host', 'feature'];

        foreach ($mandatoryFields as $field) {
            if (!isset($data[$field])) {
                throw new InvalidParameterException('Parameter {' . $field . '} is missing.');
            }
        }

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['when'])) {
            $regexp = '/^(\d{4})-(\d(2))-(\d{2})\s{1}(\d{2}):(\d{2}):(\d{2})$/';

            preg_match($regexp, $data['when'], $matches);

            if (empty($matches)) {
                throw new InvalidParameterException('passed in parameter {when} is not legal date. e.g. YYYY-MM-DD hh:mm:ss');
            } else {
                $this->when = $data['when'];
            }
        }

        if (strlen($data['host']) > 255) {
            throw new InvalidParameterException('passed in parameter {host} is longer than the limited maximum length(255).');
        } else {
            $this->host = $data['host'];
        }

        if (strlen($data['feature']) > 255) {
            throw new InvalidParameterException('passed in parameter {feature} is longer than the limited maximum length(255).');
        } else {
            $this->feature = $data['feature'];
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getFeature()
    {
        return $this->feature;
    }

    public function getWhen()
    {
        return $this->when;
    }
}
