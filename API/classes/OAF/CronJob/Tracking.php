<?php

namespace FS\OAF\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;

class Tracking extends Report
{
    public function __construct($config)
    {
        if (!is_array($config)) {
            throw new InvalidParameterException('passed in parameter {config} must be an array');
        }

        parent::__construct($config);
    }

    public function getReportMessage()
    {
        #region Construct error message by hostname
        $message  = '';
        $lastHost = '---invalid---';
        $sql      = "SELECT host, COUNT(feature) AS total, feature 
                     FROM tracking 
                     WHERE TIMESTAMPDIFF(MINUTE, `when`, NOW()) < 1440 AND 
                         feature NOT LIKE '%pompt_user_navigation%' AND 
                         feature NOT LIKE '%extra_stylesheet_link%' AND 
                         feature NOT LIKE '%progress_location%' AND 
                         feature NOT LIKE '%val_error_class%' AND 
                         feature NOT LIKE '%number_questions%' 
                     GROUP BY host, feature 
                     ORDER BY host, feature";

        $stmt     = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        while ($row = $stmt->fetch()) {
            if ($row['host'] != $lastHost) {
                $message .= PHP_EOL . $row['host'] . ':' . PHP_EOL;
                $lastHost = $row['host'];
            }
            
            $message .= $this->padRight($row['total'], 8) . ' ' . $row['feature'] . PHP_EOL;
        }
        #endregion

        $this->mail->setMessage($message);
        return $message;
    }
}
