<?php

namespace FS\OAF\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;

class StatisticsWrapper extends Report
{
    #region Fields
    protected $scanner;
    protected $firstAllDate;
    protected $lastAllDate;
    protected $firstYearDate;
    protected $lastYearDate;
    protected $firstMonthDate;
    protected $lastMonthDate;
    protected $firstWeekDate;
    protected $lastWeekDate;
    protected $reportArr;
    #endregion

    #region Methods
    public function __construct($config)
    {
        set_time_limit(3600);

        if (!is_array($config)) {
            throw new InvalidParameterException('Passed in parameter {config} must be an array');
        }

        parent::__construct($config);

        $this->scanner   = new StatisticsScanner($config);
        $this->reportArr = [];

        $this->init();
    }

    public function report()
    {
        // Report all
        $date = $this->firstAllDate;
        if ($date->getTimestamp() <= $this->lastAllDate->getTimestamp()) {
            if (!isset($this->reportArr['json']['all'])) {
                $this->scanner->init(['period' => 'all', 'date' => $date->format('Y-m-d H:i:s'), 'status' => 'insert']);
                $this->scanner->scan();
            } else {
                $this->scanner->init(['period' => 'all', 'date' => $date->format('Y-m-d H:i:s'), 'status' => 'update']);
                $this->scanner->scan();
            }
        }

        // Report yearly
        $date = $this->firstYearDate;
        while ($date->getTimestamp() <= $this->lastYearDate->getTimestamp()) {
            if (!isset($this->reportArr['json']['year']) || !in_array($date->format('Y-m-d H:i:s'), $this->reportArr['json']['year'])) {
                $this->scanner->init(['period' => 'year', 'date' => $date->format('Y-m-d H:i:s')]);
                $this->scanner->scan();
            }

            $date->add(new \DateInterval('P1Y'));
        }

        // Report monthly
        $date = $this->firstMonthDate;
        while ($date->getTimestamp() <= $this->lastMonthDate->getTimestamp()) {
            if (!isset($this->reportArr['json']['month']) || !in_array($date->format('Y-m-d H:i:s'), $this->reportArr['json']['month'])) {
                $this->scanner->init(['period' => 'month', 'date' => $date->format('Y-m-d H:i:s')]);
                $this->scanner->scan();
            }

            $date->add(new \DateInterval('P1M'));
        }

        // Report weekly
        $date = $this->firstWeekDate;
        while ($date->getTimestamp() <= $this->lastWeekDate->getTimestamp()) {
            if (!isset($this->reportArr['json']['week']) || !in_array($date->format('Y-m-d H:i:s'), $this->reportArr['json']['week'])) {
                $this->scanner->init(['period' => 'week', 'date' => $date->format('Y-m-d H:i:s')]);
                $this->scanner->scan();
            }

            $date->add(new \DateInterval('P1W'));
        }
    }
    #endregion

    #region Utils
    private function init()
    {
        $sql = "SELECT `when`
                FROM `form_submission_tracking`
                ORDER BY `when` ASC
                LIMIT 1";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        // Fetch json data from database
        $row = $stmt->fetch();
        if ($row === false || count($row) == 0) {
            throw new \Exception('No data to be scanned in table form_submission_tracking');
        }

        $this->firstAllDate   = $this->calculateDate($row['when'], 'a');
        $this->firstYearDate  = $this->calculateDate($row['when'], 'y');
        $this->firstMonthDate = $this->calculateDate($row['when'], 'm');
        $this->firstWeekDate  = $this->calculateDate($row['when'], 'w');

        $currentDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d H:i:s');

        $this->lastAllDate   = $this->calculateDate($currentDate, 'a');
        $this->lastYearDate  = $this->calculateDate($currentDate, 'y')->sub(new \DateInterval('P1D'));
        $this->lastMonthDate = $this->calculateDate($currentDate, 'm')->sub(new \DateInterval('P1M'));
        $this->lastWeekDate  = $this->calculateDate($currentDate, 'w')->sub(new \DateInterval('P1W'));

        $sql = "SELECT `type`, `period`, `data_date`
                FROM `statistics_report`
                ORDER BY `type`, `period`, `data_date`";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        while ($row = $stmt->fetch()) {
            if (!isset($this->reportArr[$row['type']])) {
                $this->reportArr[$row['type']] = [];
            }

            if (!isset($this->reportArr[$row['type']][$row['period']])) {
                $this->reportArr[$row['type']][$row['period']] = [];
            }

            $this->reportArr[$row['type']][$row['period']][] = $row['data_date'];
        }
    }

    private function calculateDate($date, $period)
    {
        $date = new \DateTime($date, new \DateTimeZone('UTC'));
        $date = new \DateTime($date->format('Y-m-d') . ' 00:00:00');

        switch ($period) {
            case 'a':
                $date = new \DateTime($date->format('Y-m-d') . ' 00:00:00');
                break;
            case 'y':
                $date = new \DateTime($date->format('Y') . '-01-01 00:00:00');
                break;
            case 'm':
                $date = new \DateTime($date->format('Y-m') . '-01 00:00:00');
                break;
            case 'w':
                $dayOfWeek     = intval($date->format('w'));
                $diffStartDate = $dayOfWeek === 0 ? 6 : ($dayOfWeek - 1);
                $date->sub(new \DateInterval('P' . $diffStartDate . 'D'));
                break;
        }

        return $date;
    }
    #endregion
}
