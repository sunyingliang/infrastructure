<?php

namespace FS\OAF\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;

class RRC extends Report
{
    public function __construct($config)
    {
        // One hour time-out 
        set_time_limit(3600);

        if (!is_array($config)) {
            throw new InvalidParameterException('Passed in parameter {config} must be an array');
        }

        parent::__construct($config);
    }

    public function cleanUp()
    {
        $sql  = "SELECT * FROM `rrc_log` WHERE `date_created` < DATE_SUB(NOW(), INTERVAL 14 DAY) ORDER BY id LIMIT 1";
        $stmt = $this->pdo->prepare($sql);

        while (true) {
            if (!$stmt->execute()) {
                throw new PDOExecutionException('PDOStatement execution failed.');
            }

            $row = $stmt->fetch();

            if (!$row['id']) {
                break;
            }

            $sqlDelete = "DELETE FROM `rrc_log` WHERE `id` < " . ($row['id'] + 10000);

            $this->pdo->exec($sqlDelete);

            echo 'Deleting from ' . $row['id'] . PHP_EOL . PHP_EOL;
        }
    }

    public function statistics()
    {
        $insertData = [
            'date'=> date('Y-m-d H:i:s'),
            'total_hits' => 0,
            'naf_hits' => 0,
            'total_hits_by_site' => '',
            'naf_hits_by_site' => '',
            'total_form_submission_by_site' => '',
            'clients_in_use' => ''
        ];

        $message = 'RRC Usage stats for the week ending ' . $insertData['date'] . PHP_EOL . PHP_EOL;
        $sql     = "SELECT count(1) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY)";
        $stmt    = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $row = $stmt->fetch();
        $message .= 'Total hits for period = ' . $row['total'] . PHP_EOL;
        $insertData['total_hits'] = $row['total'];

        $sql  = "SELECT count(1) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY) AND  `method` LIKE 'New%'";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $row = $stmt->fetch();
        $message .= 'NAF hits for period = ' . $row['total'] . PHP_EOL;
        $insertData['naf_hits'] = $row['total'];

        $sql  = "SELECT `site_url`, count(`site_url`) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY `site_url` ORDER BY `total` DESC";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        if (count($row) > 0) {
            $message .= PHP_EOL . 'Total hits by site:' . PHP_EOL . PHP_EOL;
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . PHP_EOL;
            $insertData['total_hits_by_site'] .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . "\n";
        }

        $sql  = "SELECT `site_url`, count(`site_url`) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `method` LIKE 'New%' GROUP BY `site_url` ORDER BY `total` DESC";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        if (count($row) > 0) {
            $message .= PHP_EOL . 'NAF hits by site:' . PHP_EOL . PHP_EOL;
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . PHP_EOL;
            $insertData['naf_hits_by_site'] .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . "\n";
        }

        $sql  = "SELECT `site_url`, count(`site_url`) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `method` LIKE '%Form Submit%' GROUP BY `site_url` ORDER BY `total` DESC";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        if (count($row) > 0) {
            $message .= PHP_EOL . 'Total form submissions by site:' . PHP_EOL . PHP_EOL;
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . PHP_EOL;
            $insertData['total_form_submission_by_site'] .= $this->pad($row['site_url'], 54) . "\t" . $row['total'] . "\n";
        }

        $sql  = "SELECT `site_url`, `control_type`, `control_version`, count(`site_url`) AS `total` FROM `rrc_log` WHERE `date_created` > DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY `site_url`, `control_type`, `control_version` ORDER BY `site_url`, `control_type`, `control_version`";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        if (count($row) > 0) {
            $message .= PHP_EOL . 'Clients in use:' . PHP_EOL . PHP_EOL;
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->pad($row['site_url'], 54) . "\t" . $this->pad($row['total'], 10) . "\t" . $this->pad($row['control_type'], 20) . "\t" . $row['control_version'] . PHP_EOL;
            $insertData['clients_in_use'] .= $this->pad($row['site_url'], 54) . "\t" . $this->pad($row['total'], 10) . "\t" . $this->pad($row['control_type'], 20) . "\t" . $row['control_version'] . "\n";
        }

        $sql  = "DELETE FROM `rrc_log` WHERE `date_created` < DATE_SUB(NOW(), INTERVAL 14 DAY)";
        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        $this->mail->setMessage($message);

        // Cache the statistics data to database
        $pdoConnection = IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $sql  = "INSERT INTO `rrc_statistics` (`date`, `total_hits`, `naf_hits`, `total_hits_by_site`, `naf_hits_by_site`, `total_form_submission_by_site`, `clients_in_use`) 
                  VALUES (:date, :total_hits, :naf_hits, :total_hits_by_site, :naf_hits_by_site, :total_form_submission_by_site, :clients_in_use)";
        $stmt = $pdoConnection->prepare($sql);

        if (!$stmt->execute($insertData)) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }
    }

    private function pad($str, $length)
    {
        for ($i = 0; $i < $length; $i++) {
            $str .= ' ';
        }

        return substr($str, 0, $length);
    }
}
