<?php

namespace FS\OAF\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;

class Error extends Report
{
    public function __construct($config)
    {
        if (!is_array($config)) {
            throw new InvalidParameterException('passed in parameter {config} must be an array');
        }

        parent::__construct($config);
    }

    public function getReportMessage()
    {
        #region Construct error message by hostname
        $message = 'Errors by hostname:' . PHP_EOL . PHP_EOL;
        $sql     = "SELECT host, COUNT(host) AS total 
                    FROM platform_errors 
                    WHERE TIMESTAMPDIFF(MINUTE, `when`, NOW()) < 1440 
                    GROUP BY host HAVING total > 0 
                    ORDER BY total DESC";
        $stmt    = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->padRight($row['total'], 8) . ' ' . trim($row['host']) . PHP_EOL;
        }
        #endregion

        #region Construct error message by description
        $message .= PHP_EOL . PHP_EOL . 'Errors by description:' . PHP_EOL . PHP_EOL;

        $sql = "SELECT COUNT(error) AS total, error 
                FROM (
                    SELECT SUBSTRING(error, 1, 120) AS error 
                    FROM platform_errors 
                    WHERE TIMESTAMPDIFF(MINUTE,`when`,NOW()) < 1440
                    ) AS temp 
                GROUP BY error HAVING total > 0 
                ORDER BY total DESC";

        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->padRight($row['total'], 8) . ' ' . $row['error'] . PHP_EOL;
        }
        #endregion

        #region Construct error message by location
        $message .= PHP_EOL . PHP_EOL . 'Errors by location:' . PHP_EOL . PHP_EOL;

        $sql = "SELECT COUNT(location) AS total, location 
                FROM (
                    SELECT SUBSTRING(location, 1, instr(location, ')')) AS location 
                    FROM platform_errors 
                    WHERE TIMESTAMPDIFF(MINUTE,`when`,NOW()) < 1440) AS temp 
                GROUP BY location HAVING total > 0 
                ORDER BY total DESC";

        $stmt = $this->pdo->prepare($sql);

        if (!$stmt->execute()) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        while ($row = $stmt->fetch()) {
            $message .= $this->padRight($row['total'], 8) . ' ' . $row['location'] . PHP_EOL;
        }
        #endregion

        $this->mail->setMessage($message);
        return $message;
    }
}
