<?php

namespace FS\OAF\CronJob;

use FS\Common\Mail;

class Report extends \FS\InfrastructureBase
{
    protected $mail;

    public function __construct(array $config)
    {
        parent::__construct($config['db']);
        $this->mail = new Mail($config['mail']);
    }

    public function padRight($s, $l)
    {
        for ($i = 0; $i < $l; $i++) {
            $s = ' ' . $s;
        }
        return substr($s, 0 - $l);
    }

    public function sendMail()
    {
        $this->mail->send();
    }

    protected function getReportMessage()
    {
        return 'This is a sample parent method that should be overridden in child class';
    }
}
