<?php

namespace FS\OAF\CronJob;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOCreationException;

class StatisticsScanner extends Report
{
    #region Fields
    private $startDate;
    private $endDate;
    private $period;
    private $dataDate;
    private $status;
    #endregion

    #region Methods
    public function __construct(array $config)
    {
        set_time_limit(3600);

        if (!is_array($config)) {
            throw new InvalidParameterException('Passed in parameter {config} must be an array');
        }

        parent::__construct($config);
    }

    public function init($params)
    {
        if (!isset($params['period']) || !isset($params['date'])) {
            throw new InvalidParameterException('Passed in parameter {config} is not set properly');
        }

        $this->period    = $params['period'];
        $this->startDate = $params['date'];
        $this->status    = isset($params['status']) ?  $params['status'] : '';

        // Calculate the end date for statistics scanning
        switch ($this->period) {
            case 'all':
                $endDate = new \DateTime(null, new \DateTimeZone('UTC'));
                $this->endDate = $endDate->format('Y-m-d H:i:s');
                break;
            case 'year':
                $endDate = new \DateTime($this->startDate, new \DateTimeZone('UTC'));
                $endDate->add(new \DateInterval('P1Y'))->sub(new \DateInterval('PT1S'));
                $this->endDate = $endDate->format('Y-m-d H:i:s');
                break;
            case 'month':
                $endDate = new \DateTime($this->startDate, new \DateTimeZone('UTC'));
                $endDate->add(new \DateInterval('P1M'))->sub(new \DateInterval('PT1S'));
                $this->endDate = $endDate->format('Y-m-d H:i:s');
                break;
            case 'week':
                $endDate = new \DateTime($this->startDate, new \DateTimeZone('UTC'));
                $endDate->add(new \DateInterval('P1W'))->sub(new \DateInterval('PT1S'));
                $this->endDate = $endDate->format('Y-m-d H:i:s');
                break;
        }

        // Set field dataDate to field startDate
        $this->dataDate = $this->startDate;
    }

    public function scan()
    {
        $jsonArr = ['hosts' => []];

        // Construct sql statement
        $sql = "SELECT `host`, `form_name`, CASE WHEN `impersonated`=1 THEN 'CSA' ELSE COALESCE(`user_type`,'Unknown') END `user_type_customed`, COUNT(0) `submissions`
                FROM form_submission_tracking
                WHERE `when` BETWEEN :startDate AND :endDate
                GROUP BY `host`, `form_name`, user_type_customed";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement prepare failed.');
        }

        if (!$stmt->execute(['startDate' => $this->startDate, 'endDate' => $this->endDate])) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }

        #region Construct json array
        while ($row = $stmt->fetch()) {
            $this->sanitizeHost($row['host']);

            $hostIndex = $this->elementExistsInArray($row['host'], $jsonArr['hosts']);

            if ($hostIndex === false) {
                $host = [
                    'name'    => htmlentities($row['host']),
                    'CSA'     => 0,
                    'Ext'     => 0,
                    'Int'     => 0,
                    'Anon'    => 0,
                    'Unknown' => 0,
                    'Total'   => 0,
                    'forms'   => [
                        [
                            'name'    => htmlentities($row['form_name']),
                            'CSA'     => 0,
                            'Ext'     => 0,
                            'Int'     => 0,
                            'Anon'    => 0,
                            'Unknown' => 0,
                            'Total'   => 0
                        ]
                    ]
                ];

                if (isset($host[$row['user_type_customed']])) {
                    $host[$row['user_type_customed']] += $row['submissions'];
                    $host['Total'] += $row['submissions'];
                    $host['forms'][0][$row['user_type_customed']] += $row['submissions'];
                    $host['forms'][0]['Total'] += $row['submissions'];
                    $jsonArr['hosts'][] = $host;
                }
            } else {
                $jsonArr['hosts'][$hostIndex][$row['user_type_customed']] += $row['submissions'];
                $jsonArr['hosts'][$hostIndex]['Total'] += $row['submissions'];
                $formIndex = $this->elementExistsInArray($row['form_name'], $jsonArr['hosts'][$hostIndex]['forms']);
                if ($formIndex === false) {
                    $form = [
                        'name'    => htmlentities($row['form_name']),
                        'CSA'     => 0,
                        'Ext'     => 0,
                        'Int'     => 0,
                        'Anon'    => 0,
                        'Unknown' => 0,
                        'Total'   => 0
                    ];
                    $form[$row['user_type_customed']] += $row['submissions'];
                    $form['Total'] += $row['submissions'];
                    $jsonArr['hosts'][$hostIndex]['forms'][] = $form;
                } else {
                    $jsonArr['hosts'][$hostIndex]['forms'][$formIndex][$row['user_type_customed']] += $row['submissions'];
                    $jsonArr['hosts'][$hostIndex]['forms'][$formIndex]['Total'] += $row['submissions'];
                }
            }
        }
        #endregion

        if ($this->period == 'all' && $this->status == 'update') {
            $sql = "UPDATE `statistics_report`
                    SET `data` = :jsonString, `data_date` = :dataDate
                    WHERE `period` = :period";
        } else {
            //region Save json string into db
            $sql  = "INSERT INTO `statistics_report`(`data`, `data_date`, `period`) VALUES (:jsonString, :dataDate, :period)";
        }

        $stmt = $this->pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if (!$stmt->execute([
            'jsonString' => json_encode($jsonArr),
            'dataDate'   => $this->dataDate,
            'period'     => $this->period
        ])
        ) {
            throw new PDOExecutionException('PDOStatement execution failed.');
        }
        //endregion
    }
    #endregion

    #region Utils
    private function elementExistsInArray($eleName, array &$array)
    {
        $count = count($array);

        for ($i = 0; $i < $count; $i++) {
            if ($array[$i]['name'] == $eleName) {
                return $i;
            }
        }

        return false;
    }

    private function sanitizeHost(&$host)
    {
        $host = preg_replace('/-phone|-mobile/', '', $host);
        $host = rtrim($host, ':8082');
        if (substr($host, 0, 4) == 'www.') {
            $host = preg_replace('/www./', '', $host, 1);
        }
    }
    #endregion
}
