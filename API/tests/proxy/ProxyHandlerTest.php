<?php

class ProxyHandlerTest extends PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = '0632AA47-B96C-5825-92F2-E7294F4E4D1F';
    }

    public function testGetConfig()
    {
        $response = $this->runCurl('proxy/config/file?token=' . $this->token);
        $this->assertEquals(200, $response['status']);
    }

    public function testGet()
    {
        $response = $this->runCurl('proxy/mapping/get?token=' . $this->token);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    public function testList()
    {
        $data = '{"token":"0632AA47-B96C-5825-92F2-E7294F4E4D1F"}';
        $response = $this->runCurl('proxy/mapping/list', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    public function testCreate()
    {
        $data = 'path=pathParam'
            . '&url=urlParam'
            . '&http_downgrade=0';
        $response = $this->runCurl('proxy/mapping/create', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    public function testSearch()
    {
        $data = 'path=pathParam';
        $response = $this->runCurl('proxy/mapping/search', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    public function testUpdate()
    {
        $data = 'path=pathParam'
            . '&url=urlParam'
            . '&http_downgrade=1';
        $response = $this->runCurl('proxy/mapping/update', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    public function testDelete()
    {
        $data = 'path=pathParam';
        $response = $this->runCurl('proxy/mapping/delete', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post . '&token=' . $this->token);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
