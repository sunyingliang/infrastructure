<?php

class MakeCollectionsTest extends PHPUnit_Framework_TestCase
{

    public function testList()
    {
        $response = $this->runCurl('sitemaster/collections/list');
        $this->assertEquals(200, $response['status']);
    }

    public function testCreate()
    {
        $response = $this->runCurl('sitemaster/collections/create', [], '{"name":"collectionParam"}');
        $this->assertEquals(200, $response['status']);
    }

    public function testUpdate()
    {
        $data = '{"siteName":"siteParam", "additionalIPs":"110.110.110.1", "authSiteHosts":"authSiteHostsParam.com"}';
        $response = $this->runCurl('sitemaster/collections/update', [], $data);
        $this->assertEquals(200, $response['status']);
    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }

}