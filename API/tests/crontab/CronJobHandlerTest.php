<?php


class CronJobHandlerTest extends PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = '0632AA47-B96C-5825-92F2-E7294F4E4D1F';
    }

    public function testGetList()
    {
        $response = $this->runCurl('cronjob/list?token=' . $this->token);
        $this->assertEquals(200, $response['status']);
    }

    public function testFunction()
    {
        //testAdd
        $data =
            'name=phpunit_name'
            . '&frequency=0 0 * * *'
            . '&command=phpunit_command'
            . '&comment=phpunit_comment'
            . '&concurrent=1'
            . '&execution_time=60'
            . '&enabled=1';

        $response = $this->runCurl('cronjob/create', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
        $id = $decode->response->id;

        //testGetById
        $response = $this->runCurl('cronjob/get', [], 'id=' . $id);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('phpunit_command', $response['body']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);

        //testUpdate
        $data =
            'id='.$id
            . '&name=phpunit_name'
            . '&frequency=0 0 0 * *'
            . '&command=phpunit_command'
            . '&comment=phpunit_comment'
            . '&concurrent=0'
            . '&started=1'
            . '&execution_time=90'
            . '&enabled=0';
        $response = $this->runCurl('cronjob/update', [], $data);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);

        //testUpdateStatus
        $data = "id=" . $id . "&started=1";
        $response = $this->runCurl('cronjob/update-status', [], $data);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);

        //testDelete
        $data = "id=" . $id;
        $response = $this->runCurl('cronjob/delete', [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertEquals('success', $decode->response->status);
    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post. '&token=' . $this->token);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
