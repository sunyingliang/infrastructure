<?php


class CronLogHandlerTest extends PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = '0632AA47-B96C-5825-92F2-E7294F4E4D1F';
    }

    public function testAddRestrict()
    {
        //testAdd
        $data = 'job_id=1'. '&response=phpunit_response';
        $response = $this->runCurl('cronlog/create', [], $data);
        $this->assertEquals(200, $response['status']);

        //testList
        $data = 'job_id=1'
            . '&date=2017-01-31'
            . '&offset=0'
            . '&limit=2';
        $response = $this->runCurl('cronlog/list', [], $data);
        $this->assertEquals(200, $response['status']);
        $this->assertContains('2017-01-31', $response['body']);

    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post. '&token=' . $this->token);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
