<?php
require __DIR__ . '/../classes/Common/autoload.php';

use FS\Common\IO;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\CURL;

try {
    if (!($validation = IO::required($_POST, ['ipAddress', 'controlType', 'controlVersion', 'siteUrl', 'method', 'pageType', 'serverid']))['valid']) {
        throw new InvalidParameterException($validation['message']);
    }

    foreach ($params as $param) {
        $post[$param] = $_POST[$param];
    }

    $options = [
        CURLOPT_URL            => 'localhost/api/tracking/rrc/log',
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $post
    ];

    $curl     = CURL::init($options);
    $response = CURL::getResult($curl);
    
    CURL::close($curl);

    $responseArr = json_decode($response, true);

    echo $responseArr['response']['message'];
} catch (Exception $e) {
    echo 'Error: ' . $e->getMessage();
}
