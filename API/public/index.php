<?php

ini_set('max_execution_time', 180);

require __DIR__ . '/../classes/Common/autoload.php';
require __DIR__ . '/../config/common.php';

$router = new FS\Common\Router();

// CRONTAB API
$router->group('/api/cronjob/', function () {
    $this->post('list', function () { return (new \FS\CronTab\Handler\CronJob())->getList(); });
    $this->post('get', function () { return (new \FS\CronTab\Handler\CronJob())->getById(); });
    $this->post('create', function () { return (new \FS\CronTab\Handler\CronJob())->create(); });
    $this->post('update', function () { return (new \FS\CronTab\Handler\CronJob())->update(); });
    $this->post('update-status', function () { return (new \FS\CronTab\Handler\CronJob())->updateStatus(); });
    $this->post('delete', function () { return (new \FS\CronTab\Handler\CronJob())->delete(); });
});

// PROXY API
$router->post('/api/proxy/config', function () { return (new \FS\Proxy\Handler\Mapping())->getConfig(); });

$router->group('/api/proxy/mapping/', function () {
    $this->post('get', function () { return (new \FS\Proxy\Handler\Mapping())->getList(); });
    $this->post('list', function () { return (new \FS\Proxy\Handler\Mapping())->getList(); });
    $this->post('search', function () { return (new \FS\Proxy\Handler\Mapping())->search(); });
    $this->post('create', function () { return (new \FS\Proxy\Handler\Mapping())->create(); });
    $this->post('update', function () { return (new \FS\Proxy\Handler\Mapping())->update(); });
    $this->post('delete', function () { return (new \FS\Proxy\Handler\Mapping())->delete(); });
});

// LOGGING API
$router->group('/api/log/cron/', function () {
    $this->post('list', function () { return (new \FS\CronTab\Handler\CronLog())->getList(); });
    $this->post('period', function () { return (new \FS\CronTab\Handler\CronLog())->getLogDate(); });
    $this->post('create', function () { return (new \FS\CronTab\Handler\CronLog())->create(); });
});

$router->group('/api/log/agilisys/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Agilisys())->getList(); });
    $this->post('environment', function () { return (new \FS\Logging\Handler\Agilisys())->getEnvironmentList(); });
    $this->post('site', function () { return (new \FS\Logging\Handler\Agilisys())->getSiteList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Agilisys())->create(); });
});

$router->group('/api/log/collections/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Collections())->getList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Collections())->create(); });
});

$router->group('/api/log/components/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Components())->getList(); });
    $this->post('status', function () { return (new \FS\Logging\Handler\Components())->getStatusList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Components())->create(); });
});

$router->group('/api/log/fillTask/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\FillTask())->getList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\FillTask())->create(); });
});

$router->group('/api/log/integration/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Integration())->getList(); });
    $this->post('source', function () { return (new \FS\Logging\Handler\Integration())->getSourceList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Integration())->create(); });
});

$router->group('/api/log/lim/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\LIM())->getList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\LIM())->create(); });
});

$router->group('/api/log/paymentConnectors/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\PaymentConnectors())->getList(); });
    $this->post('source', function () { return (new \FS\Logging\Handler\PaymentConnectors())->getSourceList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\PaymentConnectors())->create(); });
});

$router->group('/api/log/rrc/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\RRC())->getList(); });
    $this->post('site', function () { return (new \FS\Logging\Handler\RRC())->getSiteList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\RRC())->create(); });
});

$router->group('/api/log/email/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Email())->getList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Email())->create(); });
    $this->post('delete', function () { return (new \FS\Logging\Handler\Email())->delete(); });
});

$router->group('/api/log/ftp/', function () {
    $this->post('create', function () { return (new \FS\Logging\Handler\FTP())->create(); });
    $this->post('update', function () { return (new \FS\Logging\Handler\FTP())->update(); });
    $this->post('list', function () { return (new \FS\Logging\Handler\FTP())->getFTPLogs(); });
});

$router->group('/api/log/download/', function () {
    $this->post('list', function () { return (new \FS\Logging\Handler\Download())->getList(); });
    $this->post('create', function () { return (new \FS\Logging\Handler\Download())->create(); });
});

// SITEMASTER API
$router->group('/api/sitemaster/globaladmin/', function () {
    $this->post('list', function () { return (new \FS\SiteMaster\Handler\GlobalAdmin())->adminSiteList(); });
    $this->post('create', function () { return (new \FS\SiteMaster\Handler\GlobalAdmin())->createGlobalAdminAccount(); });
});

$router->group('/api/sitemaster/collections/', function () {
    $this->post('list', function () { return (new \FS\SiteMaster\Handler\Collections())->listSites(); });
    $this->post('create', function () { return (new \FS\SiteMaster\Handler\Collections())->createDatabase(); });
    $this->post('update', function () { return (new \FS\SiteMaster\Handler\Collections())->updateSite(); });
    $this->post('delete', function () { return (new \FS\SiteMaster\Handler\Collections())->deleteSite(); });
});

$router->group('/api/lim/monitoring/', function () {
    $this->post('resultcodes', function () { return (new \FS\LIMMonitoring\Handler\LIM())->getResultCodes(); });
    $this->post('list', function () { return (new \FS\LIMMonitoring\Handler\LIM())->getReport(); });
});

// APIBROKER API
$router->group('/api/apibroker/customer/', function () {
    $this->post('list', function () { return (new \FS\SiteMaster\Handler\APIBrokerCustomer())->getCustomerList(); });
    $this->post('listbyhost', function () { return (new \FS\SiteMaster\Handler\APIBrokerCustomer())->getByHost(); });
    $this->post('environment', function () { return (new \FS\SiteMaster\Handler\APIBrokerCustomer())->getEnvironment(); });
});

$router->group('/api/apibroker/remap/', function () {
    $this->post('list', function () { return (new \FS\SiteMaster\Handler\APIBroker())->getDatabaseList(); });
    $this->post('host', function () { return (new \FS\SiteMaster\Handler\APIBroker())->getDatabaseHost(); });
    $this->post('process', function () { return (new \FS\SiteMaster\Handler\APIBroker())->process(); });
    $this->post('update', function () { return (new \FS\SiteMaster\Handler\APIBroker())->update(); });
});

// For Downloads
$router->post('/api/download/version/(\w+)', function ($id) { return (new \FS\Download\Handler\Component())->getVersionList($id); });

$router->group('/api/download/component/', function () {
    $this->post('list', function () { return (new \FS\Download\Handler\Component())->getComponentList(); });
    $this->post('create', function () { return (new \FS\Download\Handler\Component())->createComponent(); });
    $this->post('update', function () { return (new \FS\Download\Handler\Component())->updateComponent(); });
    $this->post('delete', function () { return (new \FS\Download\Handler\Component())->deleteComponent(); });
});

// For Customer Downloads
$router->group('/api/download/', function () {
    $this->post('customer-components', function () { return (new \FS\Download\Handler\Component())->getCustomerComponentVersions(); });
    $this->post('customer-version/(\w+)', function ($id) { return (new \FS\Download\Handler\Component())->getCustomerLatestVersion($id); });
    $this->post('customer-download/(\w+)', function ($component) { return (new \FS\Download\Handler\Component())->getCustomerDownload($component); });
});

// OAF API
$router->post('/api/rrc/log', function() { return (new \FS\RRC\Handler\Log())->log(); });

$router->group('/api/oaf/', function() {
    $this->post('feature', function () { return (new \FS\OAF\Handler\Tracking())->create(); });
    $this->post('feature/log', function () { return (new \FS\OAF\Handler\Tracking())->getList(); });
    $this->post('submission', function () { return (new \FS\OAF\Handler\FormSubmission())->create(); });
    $this->post('error', function () { return (new \FS\OAF\Handler\PlatformErrors())->create(); });
    $this->post('error/log', function() { return (new \FS\OAF\Handler\PlatformErrors())->getList(); });
    $this->post('statistics/list', function () { return (new \FS\OAF\Handler\Statistics())->getList(); });
    $this->post('statistics/report', function () { return (new \FS\OAF\Handler\Statistics())->report(); });
    $this->post('analytics/events', function () { return (new \FS\OAF\Handler\Events())->event(); });
    $this->post('analytics/page-views', function () { return (new \FS\OAF\Handler\PageViews())->pageViews(); });
    $this->post('background/log', function () { return (new \FS\OAF\Handler\BackgroundLogging())->create(); });
    $this->post('background/list', function () { return (new \FS\Logging\Handler\Background())->getList(); });
    $this->post('background/site', function () { return (new \FS\Logging\Handler\Background())->getSiteList(); });
    $this->post('background/server', function () { return (new \FS\Logging\Handler\Background())->getServerList(); });
    $this->post('background/operation', function () { return (new \FS\Logging\Handler\Background())->getOperationList(); });
});

// MONITORING API
$router->group('/api/monitoring/', function() {
    $this->post('default', function () { return (new \FS\Monitoring\Handler\Page())->getHtml(); });
    $this->post('switch', function () { return (new \FS\Monitoring\Handler\SwitchState())->update(); });
    $this->post('toggle', function () { return (new \FS\Monitoring\Handler\ToggleState())->update(); });
    $this->post('log', function () { return (new \FS\Monitoring\Handler\Log())->create(); });
    $this->post('client/list', function () { return (new \FS\Monitoring\Handler\Client())->listUrl(); });
    $this->post('client/report', function () { return (new \FS\Monitoring\Handler\Client())->reportStatistics(); });
    $this->post('image', function () { return (new \FS\Monitoring\Handler\Image())->getImage(); });
});

$router->group('/api/rrcservice/', function () {
    $this->post('list', function () { return (new \FS\RRC\Handler\Versions())->getList(); });
    $this->post('add', function () { return (new \FS\RRC\Handler\Versions())->create(); });
    $this->post('edit', function () { return (new \FS\RRC\Handler\Versions())->update(); });
    $this->post('delete', function () { return (new \FS\RRC\Handler\Versions())->delete(); });
    $this->post('check', function () { return (new \FS\RRC\Handler\Versions())->check(); });
});

$router->post('/api/rrcstatistics/list', function () { return (new \FS\RRC\Handler\Statistics())->getList(); });

$router->execute();
