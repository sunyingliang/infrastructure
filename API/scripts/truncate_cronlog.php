<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'log', ['dns', 'username', 'password'])) {
    die('Connection information for {LOG_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

try {
    // Truncate cron logs to 1 month (daily script)
    $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['log']);
    $pdo->exec("DELETE FROM `cron` WHERE `start_time` <= DATE_SUB(NOW(), INTERVAL 1 MONTH)");
} catch (\Exception $e) {
    die($e->getMessage());
}
