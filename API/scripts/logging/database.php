<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'log', ['dns', 'username', 'password'])) {
    die('Connection information for {LOG_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['log']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Failed to build connection to  "' . $config['db']['dns'] . '"');
}

// Agilisys
$sql = "CREATE TABLE IF NOT EXISTS `Agilisys`(		
	`id` INT NOT NULL AUTO_INCREMENT,		
	`date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,		
	`environment` VARCHAR(255) NOT NULL,		
	`level`  VARCHAR(50) NOT NULL,		
	`logger` VARCHAR(255) NOT NULL,		
 	`message` VARCHAR(10000) NOT NULL,		
 	`Exception` VARCHAR(2000) NULL,		
 	CONSTRAINT `PK_Agilisys` PRIMARY KEY (`id`),		
    INDEX `Agilisys_Date_Logger` (`date`)		
 ) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Components
$sql = "CREATE TABLE IF NOT EXISTS `Components`(
	`Id` BIGINT NOT NULL AUTO_INCREMENT,
	`ClientHost` VARCHAR(50) NULL,
	`username` VARCHAR(255) NULL,
	`LogTime` DATETIME NULL,
	`service` VARCHAR(100) NULL,
	`machine` VARCHAR(100) NULL,
	`serverip` VARCHAR(50) NULL,
	`processingtime` INT NULL,
	`bytesrecvd` INT NULL,
	`bytessent` INT NULL,
	`servicestatus` INT NULL,
	`win32status` INT NULL,
	`operation` VARCHAR(255) NULL,
	`target` VARCHAR(255) NULL,
	`parameters` VARCHAR(255) NULL,
	CONSTRAINT `PK_Components` PRIMARY KEY (`Id` ASC),
	INDEX `Components_Master` (`LogTime`, `target`, `machine`, `ClientHost`, `parameters`, `servicestatus`, `processingtime`, `operation`),
	INDEX `Components_Target` (`target`),
	INDEX `Components_LogTime` (`LogTime`, `target`),
	INDEX `Components_ClientHost` (`ClientHost`, `target`),
	INDEX `Components_Machine` (`machine`, `target`),
	INDEX `Components_ServiceStatus` (`servicestatus`, `target`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// FillTask
$sql = "CREATE TABLE IF NOT EXISTS `FillTask`(
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Thread` VARCHAR(255) NOT NULL,
	`Level` VARCHAR(50) NOT NULL,
	`Logger` VARCHAR(255) NOT NULL,
	`Message` VARCHAR(10000) NOT NULL,
	CONSTRAINT `PK_FillTask_Log` PRIMARY KEY (`Id`),
	INDEX `FillTask_Level` (`Level`),
	INDEX `FillTask_Date` (`Date`),
	INDEX `FillTask_Level_Date` (`Level`, `Date`),
	FULLTEXT INDEX `FTS_FillTask_Message` (`Message`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `FillTask_Date_Logger` exists, if it is, drop it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'FillTask' AND index_name = 'FillTask_Date_Logger'")->fetch(PDO::FETCH_ASSOC);

if ($result !== false) {
    $pdo->exec("ALTER TABLE `FillTask` DROP INDEX `FillTask_Date_Logger`;");
}

// Check if index `FillTask_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'FillTask' AND index_name = 'FillTask_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `FillTask` ADD INDEX `FillTask_Date` (`Date`);");
}

// Check if index `FillTask_Level_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'FillTask' AND index_name = 'FillTask_Level_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `FillTask` ADD INDEX `FillTask_Level_Date` (`Level`, `Date`);");
}

// LIM
$sql = "CREATE TABLE IF NOT EXISTS `LIM`(
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Thread` VARCHAR(255) NOT NULL,
	`Level` VARCHAR(50) NOT NULL,
	`Logger` VARCHAR(255) NOT NULL,
	`Message` VARCHAR(10000) NOT NULL,
	`Exception` VARCHAR(2000) NULL,
	CONSTRAINT `PK_FillTask_Log` PRIMARY KEY (`Id` ASC),
	INDEX `LIM_Date_Logger` (`Date`),
	FULLTEXT INDEX `FTS_LIM_Log_Message` (`Message`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Payment Connector
$sql = "CREATE TABLE IF NOT EXISTS `PaymentConnectors`(
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Thread` VARCHAR(255) NOT NULL,
	`Level` VARCHAR(50) NOT NULL,
	`Logger` VARCHAR(255) NOT NULL,
	`Message` VARCHAR(10000) NOT NULL,
	`Exception` VARCHAR(2000) NULL,
	`Reference` VARCHAR(255) NULL,
	CONSTRAINT `PK_PaymentConnectors` PRIMARY KEY (`Id` ASC),	
	INDEX `PC_Logger` (`Logger`),
	INDEX `PC_Level` (`Level`),
	INDEX `PC_Reference` (`Reference`),
	INDEX `PC_Date` (`Date`),
	INDEX `PC_Logger_Date` (`Logger`, `Date`),
	INDEX `PC_Level_Date` (`Level`, `Date`),
	INDEX `PC_Reference_Date` (`Reference`, `Date`),
	INDEX `PC_Logger_Level_Date` (`Logger`, `Level`, `Date`),
	INDEX `PC_Index` (`Logger`, `Level`, `Reference`, `Date`),
	FULLTEXT INDEX `FTS_PC_Message_Exception` (`Message`, `Exception`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `PC_Date_Logger` exist, if it is drop it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Date_Logger'")->fetch(PDO::FETCH_ASSOC);

if ($result !== false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` DROP INDEX `PC_Date_Logger`;");
}

// Check if index `PC_Date_Level_Logger` exists, if it is, drop it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Date_Level_Logger'")->fetch(PDO::FETCH_ASSOC);

if ($result !== false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` DROP INDEX `PC_Date_Level_Logger`;");
}

// Check if index `PC_Master` exists, if it is, drop it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Master'")->fetch(PDO::FETCH_ASSOC);

if ($result !== false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` DROP INDEX `PC_Master`;");
}
// Check if index `PC_Logger` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Logger'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Logger` (`Logger`);");
}

// Check if index `PC_Level` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Level'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Level` (`Level`);");
}

// Check if index `PC_Reference` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Reference'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Reference` (`Reference`);");
}

// Check if index `PC_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Date` (`Date`);");
}

// Check if index `PC_Logger_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Logger_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Logger_Date` (`Logger`,`Date`);");
}

// Check if index `PC_Level_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Level_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Level_Date` (`Level`,`Date`);");
}

// Check if index `PC_Reference_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Reference_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Reference_Date` (`Reference`, `Date`);");
}

// Check if index `PC_Logger_Level_Date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Logger_Level_Date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Logger_Level_Date` (`Logger`, `Level`, `Date`);");
}

// Check if index `PC_Index` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'PaymentConnectors' AND index_name = 'PC_Index'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `PaymentConnectors` ADD INDEX `PC_Index` (`Logger`, `Level`, `Reference`, `Date`);");
}

// RRCLog
$sql = "CREATE TABLE IF NOT EXISTS `RRC`(
	`Id` INT NOT NULL AUTO_INCREMENT,
	`Date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`Thread` VARCHAR(255) NOT NULL,
	`Level` VARCHAR(50) NOT NULL,
	`Logger` VARCHAR(255) NOT NULL,
	`Message` VARCHAR(10000) NOT NULL,
	`Exception` VARCHAR(2000) NULL,
	`Site` VARCHAR(255) NULL,
	CONSTRAINT `PK_RRC_Log` PRIMARY KEY (`Id` ASC),
	INDEX `RRC_Date_Logger` (`Date`),
	INDEX `RRC_Site` (`Site`),
	FULLTEXT INDEX `FTS_RRC_Message` (`Message`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Add column `Version` if not exist
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'RRC' AND column_name = 'Version'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `RRC` ADD `Version` VARCHAR(50) DEFAULT NULL");
}

// CollectionsLog
$sql = "CREATE TABLE IF NOT EXISTS `collections` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `timestamp` datetime DEFAULT NULL,
    `logger` varchar(255) DEFAULT NULL,
    `level` varchar(32) DEFAULT NULL,
    `message` varchar(4000) DEFAULT NULL,
    `exception` varchar(4000) DEFAULT NULL,
    `thread` int(11) DEFAULT NULL,
    `file` varchar(255) DEFAULT NULL,
    `line` varchar(10) DEFAULT NULL,
    `hostname` varchar(255) DEFAULT NULL,
    `sitename` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `timestamp` (`timestamp`),
    KEY `logger` (`logger`),
    KEY `hostname` (`hostname`),
    FULLTEXT KEY `message` (`message`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Download log
$sql = 'CREATE TABLE IF NOT EXISTS `download` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `logtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `component` varchar(50) NOT NULL,
    `version` varchar(10) NOT NULL,
    `remote_address` varchar(50) NOT NULL,
    PRIMARY KEY (`id`)
);';

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Background log
$sql = "CREATE TABLE IF NOT EXISTS `oaf_background`(
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `server_id` varchar(100) NOT NULL DEFAULT '',
        `site` varchar(200) NOT NULL DEFAULT '',
        `operation` varchar(50) NOT NULL DEFAULT '',
        PRIMARY KEY (`id`),
        INDEX `site` (`site`),
        INDEX `server_id` (`server_id`),
        INDEX `timestamp` (`timestamp`),
        INDEX `operation` (`operation`)
);";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// SMTP log
$sql = "CREATE TABLE IF NOT EXISTS `smtp_sent` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`time_received` DATETIME NOT NULL,
	`source_ip` VARCHAR(64) NOT NULL,
	`from_address` VARCHAR(255) NOT NULL,
	`to_address` VARCHAR(10240) NOT NULL,
	`cc_address` VARCHAR(10240) NOT NULL,
	`subject` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `idx_from` (`from_address`),
	FULLTEXT INDEX `idx_to` (`to_address`),
	FULLTEXT INDEX `idx_cc` (`cc_address`),
	INDEX `subject` (`subject`),
	INDEX `idx_to_address` (`to_address`), 
	INDEX `idx_cc_address` (`cc_address`),
	INDEX `idx_mix` (`from_address`, `to_address`, `subject`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `idx_to_address` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'smtp_sent' AND index_name = 'idx_to_address'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `smtp_sent` ADD INDEX `idx_to_address` (`to_address`);");
}

// Check if index `idx_cc_address` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'smtp_sent' AND index_name = 'idx_cc_address'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `smtp_sent` ADD INDEX `idx_cc_address` (`cc_address`);");
}

// Check if index `idx_mix` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'smtp_sent' AND index_name = 'idx_mix'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `smtp_sent` ADD INDEX `idx_mix` (`from_address`, `to_address`, `subject`);");
}

$sql = "CREATE TABLE IF NOT EXISTS `smtp_failed` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`message_id` VARCHAR(200) NOT NULL,
	`from_address` VARCHAR(255) NOT NULL,
	`to_address` VARCHAR(10240) NOT NULL,
	`subject` VARCHAR(255) NOT NULL,
	`time_received` DATETIME NOT NULL,
	`reason` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `idx_message_id` (`message_id`),
	INDEX `idx_from` (`from_address`),
	FULLTEXT INDEX `idx_to` (`to_address`),
	INDEX `subject` (`subject`),
	INDEX `idx_to_address` (`to_address`),
	INDEX `idx_mix` (`from_address`, `to_address`, `subject`)
) DEFAULT CHARACTER SET UTF8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `idx_to_address` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'smtp_failed' AND index_name = 'idx_to_address'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `smtp_failed` ADD INDEX `idx_to_address` (`to_address`);");
}

// Check if index `idx_mix` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'smtp_failed' AND index_name = 'idx_mix'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `smtp_failed` ADD INDEX `idx_mix` (`from_address`, `to_address`, `subject`);");
}

// Cron log
$sql = "CREATE TABLE IF NOT EXISTS `cron` (
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `job_id` BIGINT(20) NOT NULL DEFAULT '0',
            `start_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `response` LONGTEXT NOT NULL,
            PRIMARY KEY (`id`),
            INDEX `START_ID` (`start_time`, `job_id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// FTP log
$sql = "CREATE TABLE IF NOT EXISTS `ftp` (
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `ftp_user` VARCHAR(64) NOT NULL DEFAULT '',
            `file_name` VARCHAR(64) NOT NULL DEFAULT '',
            `start_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `end_time` DATETIME NULL DEFAULT NULL,
            `response` LONGTEXT NULL,
            PRIMARY KEY (`id`),
            INDEX `ftp_user` (`ftp_user`),
            INDEX `start_time` (`start_time`),
            INDEX `end_time` (`end_time`),
            INDEX `file_name` (`file_name`)
        )";

//remove `Path` field if exist - 11/12/2017
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE table_schema=DATABASE() AND table_name='RRC' AND column_name='Path'")->fetch(PDO::FETCH_ASSOC);
if ($result !== false) {
    $pdo->exec("ALTER TABLE `RRC` DROP COLUMN `Path`");
}

// Add index for ftp `file_name`
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'ftp' AND index_name = 'file_name';")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `ftp` ADD INDEX `file_name` (`file_name`);");
}

// Check if index `user_file` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'ftp' AND index_name = 'user_file'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `ftp` ADD INDEX `user_file` (`ftp_user`, `file_name`);");

}

// Integration log
$sql = "CREATE TABLE IF NOT EXISTS `integration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `thread` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  `logger` varchar(255) NOT NULL,
  `message` varchar(4000) NOT NULL,
  `exception` varchar(2000) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `date` (`date`),
  INDEX `level` (`level`),
  INDEX `logger` (`logger`),
  INDEX `reference` (`reference`),
  INDEX `customer` (`customer`),
  FULLTEXT INDEX `message` (`message`)
)";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
