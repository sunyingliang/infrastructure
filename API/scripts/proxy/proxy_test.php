<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/CURL.php';
require __DIR__ . '/../../classes/Common/IO.php';

use FS\Common\CURL;
use FS\Common\IO;

if (!defined('PROXY_TEST_ENDPOINT') || empty(PROXY_TEST_ENDPOINT)) {
    exit();
}

try {
    // Call Proxy test endpoint
    $options = [
        CURLOPT_URL            => PROXY_TEST_ENDPOINT,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER         => false,
        CURLOPT_POST           => true
    ];

    $curl = CURL::init($options);

    CURL::exec($curl);

    $info = CURL::getInfo($curl);

    CURL::close($curl);

    if ($info === false) {
        throw new Exception('Failed to get curl information');
    }

    if ($info['http_code'] == 200) {
        IO::message('Proxy was contacted from Infrastructure.');
    } else {
        throw new Exception('Error: ' . $info['http_code'] . ', Proxy cannot contact Infrastructure.');
    }
} catch (Exception $e) {
    IO::slack(SLACK['url'], SLACK['channel'], SLACK['username_proxy'], $e->getMessage());
    IO::message($e->getMessage());
}
