<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

#region Create tables for NET-745
// Create table 'proxy_mapping' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `proxy_mapping`(
            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `comment` VARCHAR(200) NOT NULL DEFAULT '',
            `path` VARCHAR(200) NOT NULL DEFAULT '',
            `url` VARCHAR(500) NOT NULL DEFAULT '',
            `http_downgrade` TINYINT(1) NOT NULL DEFAULT 0,
            `keep_alive` TINYINT(1) NOT NULL DEFAULT 0,
            `pooled` TINYINT(1) NOT NULL DEFAULT 0,
            `test_url` VARCHAR(200) NOT NULL DEFAULT '',
            PRIMARY KEY (`id`),
            UNIQUE INDEX `path` (`path`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion
