<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'log', ['dns', 'username', 'password'])) {
    die('Connection information for {LOG_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

try {
    $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['log']);

    if ($pdo === null) {
        die('Error: Cannot connect to database');
    }

    // Truncate FillTask logs to 6 months
    $pdo->exec("DELETE FROM `FillTask` WHERE `Date` < DATE_SUB(NOW(), INTERVAL 6 MONTH)");
}catch (Exception $e) {
    die($e->getMessage());
}
