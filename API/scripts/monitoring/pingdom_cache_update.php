<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Error: Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

use FS\Common\IO;

// Config settings validation
if (!$validation = IO::checkDBConstant('DATABASE_CONNECTION', 'monitoring')){
    IO::message('Configuration of {MONITORING_CONNECTION} is not setup properly', null, true);
}

// Task reports
try {
    $updatePingdomCache = new \FS\Monitoring\CronJob\PingdomCacheUpdate();

    $updatePingdomCache->doUpdate();

    IO::message('Update pingdom cache has been finished successfully...');
} catch (Exception $e) {
    IO::message('Error: ' . $e->getMessage());
}
