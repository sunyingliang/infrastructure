<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'monitoring', ['dns', 'username', 'password'])) {
    die('Connection information for {MONITORING_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['monitoring']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}


// Create table 'disk_monitoring' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `disk_monitoring`(
            `server_id` varchar(100) NOT NULL DEFAULT '',
            `name` varchar(100) NOT NULL DEFAULT '',
            `label` varchar(100) NOT NULL DEFAULT '',
            `type` varchar(100) NOT NULL DEFAULT '',
            `free` bigint(20) NOT NULL DEFAULT '0',
            `size` bigint(20) NOT NULL DEFAULT '0',
            `as_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'history' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `history`(
            `host` varchar(500) DEFAULT NULL,
            `updated` datetime DEFAULT NULL,
            `status` int(11) DEFAULT NULL,
            `delay` float DEFAULT NULL,
            `monitoring_id` int(11) DEFAULT NULL
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'host_monitoring' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `host_monitoring`(
            `server_id` varchar(100) NOT NULL DEFAULT '',
            `cpu_used_micros` bigint(20) NOT NULL DEFAULT '0',
            `cpu_count` int(11) NOT NULL DEFAULT '0',
            `mem_virt` bigint(20) NOT NULL DEFAULT '0',
            `mem_work` bigint(20) NOT NULL DEFAULT '0',
            `mem_priv` bigint(20) NOT NULL DEFAULT '0',
            `mem_paged` bigint(20) NOT NULL DEFAULT '0',
            `as_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `email` int(11) DEFAULT NULL,
            `session` int(11) DEFAULT NULL,
            `mem_total` bigint(20) DEFAULT NULL
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'monitor_host' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `monitor_host`(
            `monitoring_id` int(11) NOT NULL DEFAULT '0',
            `host` varchar(500) DEFAULT NULL,
            `updated` datetime DEFAULT CURRENT_TIMESTAMP,
            `status` int(11) DEFAULT NULL,
            `delay` float(23,14) DEFAULT NULL
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'monitoring' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `monitoring` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `url` varchar(1000) DEFAULT NULL,
          `successcriteria` varchar(500) DEFAULT NULL,
          `prettyname` varchar(50) DEFAULT NULL,
          `sequence` int(11) DEFAULT NULL,
          `colour` varchar(50) DEFAULT NULL,
          `type` varchar(50) DEFAULT NULL,
          `guid` varchar(50) DEFAULT NULL,
          `state` varchar(30) DEFAULT NULL,
          `updated` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if `updated` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'monitoring' AND column_name = 'updated'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `monitoring` ADD COLUMN `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `state`;");
}

// Create table 'queue_monitoring' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `queue_monitoring` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `queue` varchar(255) NOT NULL,
	  `messagesAvailable` int(11) NOT NULL,
	  `messagesInFlight` int(11) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `queue` (`queue`)
	)";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if `alias` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'queue_monitoring' AND column_name = 'alias'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `queue_monitoring` ADD `alias` varchar(255) DEFAULT NULL;");
}

// Create table 'pingdom_cache' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `pingdom_cache`(
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `created` int(11) NOT NULL DEFAULT '0',
            `name` varchar(50) NOT NULL DEFAULT '',
            `hostname` varchar(100) NOT NULL DEFAULT '',
            `resolution` int(11) NOT NULL DEFAULT '0',
            `type` varchar(50) NOT NULL DEFAULT '',
            `lasterrortime` int(11) DEFAULT NULL,
            `lasttesttime` int(11) DEFAULT NULL,
            `lastresponsetime` int(11) DEFAULT NULL,
            `status` varchar(50) NOT NULL DEFAULT '',
            `as_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'server_names' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `server_names`(
            `basename` varchar(50) NOT NULL DEFAULT '',
            `searchstring` varchar(50) NOT NULL DEFAULT '',
            `charts` tinyint(4) NOT NULL DEFAULT '0',
            `group` varchar(50) DEFAULT NULL,
            `ignore` varchar(255) DEFAULT NULL,
            `enabled` tinyint(1) DEFAULT 1,
            `threshold` int DEFAULT 0
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if `ignore` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'server_names' AND column_name = 'ignore'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `server_names` ADD `ignore` varchar(255) DEFAULT NULL;");
}

// Check if `enabled` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'server_names' AND column_name = 'enabled'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `server_names` ADD `enabled` tinyint(1) DEFAULT 1;");
}

// Check if `threshold` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'server_names' AND column_name = 'threshold'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `server_names` ADD `threshold` int DEFAULT 0;");
}

// Create table 'monitor_server' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `monitor_server`(
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `name` varchar(50) NOT NULL DEFAULT '',
            `prettyname` varchar(200) NOT NULL DEFAULT '',
            `charts` tinyint(4) NOT NULL DEFAULT '0',
            `group` varchar(50) DEFAULT NULL,
            `auto_scaled` tinyint(1) DEFAULT 0,
            `enabled` tinyint(1) DEFAULT 1,
            `updated` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if `enabled` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'monitor_server' AND column_name = 'enabled'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `monitor_server` ADD `enabled` tinyint(1) DEFAULT 1;");
}

// Check if `updated` column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'monitor_server' AND column_name = 'updated'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `monitor_server` ADD COLUMN `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `enabled`;");
}

// Add index `as_at` to host_monitoring
$exists = 0;
$sql    = "SHOW INDEX FROM `monitoring`.`host_monitoring`";

foreach ($pdo->query($sql) as $row) {
    if ($row['Column_name'] == 'as_at') {
        $exists = 1;
        break;
    }
}

if ($exists === 0) {
    $sql = "ALTER TABLE `host_monitoring` ADD INDEX `as_at` (`as_at`)";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}

// Add index `server_id_as_at` to host_monitoring
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'host_monitoring' AND index_name = 'server_id_as_at'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `host_monitoring` ADD INDEX `server_id_as_at` (`server_id`, `as_at`);");
}

// Add index `server_id` to disk_monitoring
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'disk_monitoring' AND index_name = 'server_id'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `disk_monitoring` ADD INDEX `server_id` (`server_id`);");
}

// Add index `as_at` to disk_monitoring
$exists = 0;
$sql    = "SHOW INDEX FROM `monitoring`.`disk_monitoring`";

foreach ($pdo->query($sql) as $row) {
    if ($row['Column_name'] == 'as_at') {
        $exists = 1;
        break;
    }
}

if ($exists === 0) {
    $sql = "ALTER TABLE `disk_monitoring` ADD INDEX `as_at` (`as_at`)";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}
#endregion

#region Ignore auto-scaled server when polling missing server, add failed_counter field to monitor_host
$result = $pdo->query("SELECT COUNT(*) AS `total` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = DATABASE() AND `TABLE_NAME` = 'monitor_server' AND `COLUMN_NAME` = 'auto_scaled'")->fetch();
if ($result['total'] == 0) {
    $sql = "ALTER TABLE `monitor_server` ADD COLUMN `auto_scaled` TINYINT(1) DEFAULT 0 AFTER `group`";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}

$result = $pdo->query("SELECT COUNT(*) AS `total` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = DATABASE() AND `TABLE_NAME` = 'monitor_host' AND `COLUMN_NAME` = 'failed_counter'")->fetch();
if ($result['total'] == 0) {
    $sql = "ALTER TABLE `monitor_host` ADD COLUMN `failed_counter` INT DEFAULT 0 AFTER `delay`";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}
#endregion

#region PHP-7
// Create table 'monitor_server_cache' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `monitor_server_cache`(
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NOT NULL DEFAULT '',
            `prettyname` VARCHAR(200) NOT NULL DEFAULT '',
            `charts` TINYINT(4) NOT NULL DEFAULT '0',
            `group` VARCHAR(50) NULL DEFAULT NULL,
            `auto_scaled` TINYINT(1) NULL DEFAULT '0',
            `enabled` TINYINT(1) NULL DEFAULT '1',
	        `last_time` DATETIME NULL DEFAULT '0000-00-00 00:00:00',
            `email` INT(11) NULL DEFAULT NULL, 
            `session` INT(11) NULL DEFAULT NULL,
            `cpu_current` BIGINT(20) NOT NULL DEFAULT 0,
            `mem_last` BIGINT(20) NOT NULL DEFAULT 0,
            `mem_total` BIGINT(20) NOT NULL DEFAULT 0,
            `good_drives` VARCHAR(64) NOT NULL DEFAULT '', 
            `disk_title_good` VARCHAR(64) NOT NULL DEFAULT '', 
            `ok_drives` VARCHAR(64) NOT NULL DEFAULT '', 
            `disk_title_ok` VARCHAR(64) NOT NULL DEFAULT '',
            PRIMARY KEY (`id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Increase size of column `disk_title_good` and column `disk_title_ok` of `monitor_server_cache` table
$result = $pdo->query("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.columns WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'monitor_server_cache' AND COLUMN_NAME = 'disk_title_good'")->fetch();
if ($result['CHARACTER_MAXIMUM_LENGTH'] != '256') {
    $sql = "ALTER TABLE `monitor_server_cache` MODIFY `disk_title_good` VARCHAR(256);";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}

$result = $pdo->query("SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.columns WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'monitor_server_cache' AND COLUMN_NAME = 'disk_title_ok'")->fetch();
if ($result['CHARACTER_MAXIMUM_LENGTH'] != '256') {
    $sql = "ALTER TABLE `monitor_server_cache` MODIFY `disk_title_ok` VARCHAR(256);";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}

// Create table 'monitor_server_disk_warn_cache' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `monitor_server_disk_warn_cache`(
            `id` INT(1) NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(200) NOT NULL DEFAULT '',
            `name` VARCHAR(50) NULL DEFAULT NULL,
            `label` VARCHAR(50) NULL DEFAULT NULL,
            `usage` FLOAT NULL DEFAULT 0,
            `monitor_server_cache_id` BIGINT(20) NOT NULL DEFAULT 0,
            PRIMARY KEY (`id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Pre-populate invalid state on monitoring table for performance reasons
$sql = "UPDATE `monitoring` SET `state` = 'on' WHERE `state` IS NULL OR `state` = ''";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

#endregion
