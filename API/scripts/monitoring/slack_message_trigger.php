<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Error: Must be run via cli');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

use FS\Common\IO;

// Config settings validation
if (!$validation = IO::checkDBConstant('DATABASE_CONNECTION', 'monitoring')){
    IO::message('Configuration of {MONITORING_CONNECTION} is not setup properly', null, true);
}

// Task reports
try {
    $slackMessageTrigger = new \FS\Monitoring\CronJob\SlackMessageTrigger();

    $slackMessageTrigger->triggerMonitoringServers();
} catch (Exception $e) {
    IO::message('Error: ' . $e->getMessage());
}
