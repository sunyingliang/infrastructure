<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Error: Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

use FS\Common\IO;
use FS\Common\Exception\PDOCreationException;

// Config settings validation
if (!$validation = IO::checkDBConstant('DATABASE_CONNECTION', 'monitoring')){
    IO::message('Configuration of {MONITORING_CONNECTION} is not setup properly', null, true);
}

// Task reports
try {
    if (!($pdo = IO::getPDOConnection(DATABASE_CONNECTION['monitoring']))) {
        IO::message('Failed to get PDO connection based on {MONITORING_CONNECTION}', null, true);
    };

    $failedIds = [];

    $sql = "SELECT `id` FROM `monitoring` WHERE `monitoring`.`guid` IS NULL";

    $results = $pdo->query('SELECT `id` FROM `monitoring` WHERE `monitoring`.`guid` IS NULL')->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($results)) {
        $sql = 'UPDATE `monitoring` SET `guid` = :guid WHERE `id` = :id';

        $stmt = $pdo->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        foreach ($results as $row) {
            if ($stmt->execute(['guid' => IO::guid(), 'id' => $row['id']]) === false) {
                $failedIds[] = $row['id'];
                
                IO::message('Failed to generate guid for row {' . $row['id'] . '}');
            }else{
                IO::message('Succeed to generate guid for row {' . $row['id'] . '}');
            }
        }
    }

    if (!empty($failedIds)) {
        IO::message('Monitoring failed to generate guid for rows with id {' . implode(',', $failedIds) . '}');
    }else{
        IO::message('Monitoring check has been finished successfully...');
    }
} catch (Exception $e) {
    IO::message('Error: ' . $e->getMessage());
}
