<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

#region Create tables for NET-746
// Create table 'cron_job' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `cron_job` (
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(64) NOT NULL DEFAULT '',
            `frequency` VARCHAR(32) NOT NULL DEFAULT '',
            `command` VARCHAR(512) NOT NULL DEFAULT '',
            `comment` VARCHAR(255) NOT NULL DEFAULT '',
            `concurrent` TINYINT(4) NOT NULL DEFAULT '0',
            `started` TINYINT(4) NOT NULL DEFAULT '0',
            `start_time` DATETIME NULL DEFAULT NULL,
            `execution_time` INT(11) NOT NULL DEFAULT '0',
            `enabled` TINYINT(4) NOT NULL DEFAULT '1',
            `delay` TINYINT(4) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            UNIQUE INDEX `name` (`name`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if column `delay` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = DATABASE() AND TABLE_NAME = 'cron_job' AND COLUMN_NAME = 'delay'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `cron_job` ADD `delay` TINYINT(4) NOT NULL DEFAULT '0'");
}
