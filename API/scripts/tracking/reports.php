<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not enabled, please check configuration in php.ini');
}

$commands = ['Tracking', 'Error'];

if (empty($argv) || count($argv) > 3 || !isset($argv[1]) || !in_array($argv[1],
        $commands) || (isset($argv[2]) && !in_array($argv[2], $commands))
) {
    die('Error: Format of command should be like: php reports.php Tracking|Error|Tracking Error');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

// Config settings
$config         = [];
$config['db']   = [
    'dns'      => DATABASE_CONNECTION['tracking']['dns'],
    'username' => DATABASE_CONNECTION['tracking']['username'],
    'password' => DATABASE_CONNECTION['tracking']['password']
];
$config['mail'] = [
    'server'  => MAIL_SERVICE['server'],
    'port'    => MAIL_SERVICE['port'],
    'from'    => MAIL_SERVICE['from'],
    'message' => MAIL_SERVICE['message'],
];

// Task reports
try {
    for ($i = 1; $i < count($argv); $i++) {
        $paramCommand = $argv[$i];

        if ($paramCommand == 'Tracking') {
            $config['mail']['to']      = MAIL_SERVICE['to_tracking'];
            $config['mail']['subject'] = MAIL_SERVICE['subject_tracking'];
        } else if ($paramCommand == 'Error') {
            $config['mail']['to']      = MAIL_SERVICE['to_error'];
            $config['mail']['subject'] = MAIL_SERVICE['subject_error'];
        }

        $class  = '\\FS\\OAF\\CronJob\\' . ucfirst($paramCommand);
        $object = new $class($config);

        $object->getReportMessage();
        $object->sendMail();
        echo('Report for ' . $argv[$i] . ' has been done..' . PHP_EOL);
    }
} catch (Exception $e) {
    echo('Error: ' . $e->getMessage());
}
