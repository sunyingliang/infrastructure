<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'tracking', ['dns', 'username', 'password'])) {
    die('Connection information for {TRACKING_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo     = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['tracking']);
        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Failed to build connection to  "' . $config['db']['dns'] . '"');
}

#region Create database and tables for NET-665
// Create table 'form_submission_tracking' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `form_submission_tracking` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `host` varchar(255) NOT NULL,
          `when` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `form_name` varchar(255) NOT NULL,
          `form_group_name` varchar(255) NOT NULL,
          `status` varchar(50) NOT NULL,
          `impersonated` tinyint(4) DEFAULT '0',
          `user_type` varchar(10) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'platform_errors' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `platform_errors` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `error` text NOT NULL,
          `location` text NOT NULL,
          `when` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `host` varchar(255) DEFAULT NULL,
          `server` varchar(55) DEFAULT NULL,
          `request_folders` varchar(255) DEFAULT NULL,
          `request_params` text,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create table 'tracking' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `tracking` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `host` varchar(255) NOT NULL,
          `when` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `feature` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

#endregion

#region Create tables for NET-253 and NET-411
// Create table 'statistics_report' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `statistics_report` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `data` mediumtext,
          `period` varchar(10) NOT NULL DEFAULT 'month',
          `data_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	      `type` VARCHAR(10) NOT NULL DEFAULT 'json',
          PRIMARY KEY (`id`),
          INDEX `data_date` (`data_date`),
	      INDEX `period` (`period`),
	      INDEX `created` (`created`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if index `period` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'statistics_report' AND index_name = 'period'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `statistics_report` ADD INDEX `period` (`period`);");
}

// Check if index `data_date` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'statistics_report' AND index_name = 'data_date'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `statistics_report` ADD INDEX `data_date` (`data_date`);");
}

// Check if index `created` exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'statistics_report' AND index_name = 'created'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `statistics_report` ADD INDEX `created` (`created`);");
}

// Add index on `form_submission_tracking`(`when`) to improve performance
$exists = 0;
$sql    = "SHOW INDEX FROM `tracking`.`form_submission_tracking`";

foreach ($pdo->query($sql) as $row) {
    if ($row['Column_name'] == 'when') {
        $exists = 1;
        break;
    }
}

if ($exists === 0) {
    $sql = "ALTER TABLE `form_submission_tracking` ADD INDEX `when` USING BTREE (`when` ASC)";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}
#endregion

#region Create tables for NET-682
// Create table 'rrc_log' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `rrc_log`(
            `id` BIGINT NOT NULL AUTO_INCREMENT,
            `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `ip_address` VARCHAR(255) NULL,
            `control_type` VARCHAR(255) NULL,
            `control_version` VARCHAR(255) NULL,
            `site_url` VARCHAR(255) NULL,
            `customer_key` VARCHAR(255) NULL,
            `method` VARCHAR(255) NULL,
            `page_type` VARCHAR(255) NULL,
            `server_id` VARCHAR(255) NULL,
            `path` VARCHAR(1024) NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion

#region Alter table `form_submission_tracking` for NET-851
$sqlS = "SELECT COUNT(1) AS `Count` FROM `information_schema`.`COLUMNS` WHERE `TABLE_NAME` = 'form_submission_tracking' AND `COLUMN_NAME` = 'product'";

$stmt = $pdo->query($sqlS);

if ($stmt === false) {
    die('Error: Failed to retrieve column {product}');
}

$row = $stmt->fetch();

if ($row['Count'] == 0) {
    $sql = "ALTER TABLE `form_submission_tracking` ADD COLUMN `product` VARCHAR(50) DEFAULT NULL";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}
#endregion

#region Alter table `rrc_log` for ISSUE #166
$sqlS = "SELECT COUNT(1) AS `Count` FROM `information_schema`.`COLUMNS` WHERE `TABLE_NAME` = 'rrc_log' AND `COLUMN_NAME` = 'path'";

$stmt = $pdo->query($sqlS);

if ($stmt === false) {
    die('Error: Failed to retrieve column {product}');
}

$row = $stmt->fetch();

if ($row['Count'] == 0) {
    $sql = "ALTER TABLE `rrc_log` ADD COLUMN `path` VARCHAR(1024) DEFAULT NULL";

    if ($pdo->exec($sql) === false) {
        die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
    }
}
#endregion
