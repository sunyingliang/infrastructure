<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

// Config settings
$config         = [];
$config['db']   = [
    'dns'      => DATABASE_CONNECTION['tracking']['dns'],
    'username' => DATABASE_CONNECTION['tracking']['username'],
    'password' => DATABASE_CONNECTION['tracking']['password']
];
$config['mail'] = [
    'server'  => MAIL_SERVICE['server'],
    'port'    => MAIL_SERVICE['port'],
    'from'    => MAIL_SERVICE['from_rrc'],
    'to'      => MAIL_SERVICE['to_rrc'],
    'subject' => MAIL_SERVICE['subject_rrc']
];

// Task reports
try {
    $rrc = new \FS\OAF\CronJob\RRC($config);
    $rrc->statistics();
    $rrc->sendMail();
} catch (Exception $e) {
    echo('Error: ' . $e->getMessage());
}
