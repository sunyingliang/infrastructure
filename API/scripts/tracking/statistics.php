<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Error: Must be run via cli');
}

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';

//config settings
$config         = [];
$config['db']   = [
    'dns'      => DATABASE_CONNECTION['tracking']['dns'],
    'username' => DATABASE_CONNECTION['tracking']['username'],
    'password' => DATABASE_CONNECTION['tracking']['password']
];
$config['mail'] = [
    'server' => MAIL_SERVICE['server'],
    'port'   => MAIL_SERVICE['port']
];

// Task reports
try {
    $statisticsWrapper = new \FS\OAF\CronJob\StatisticsWrapper($config);
    $statisticsWrapper->report();

    echo('Statistics scanning task has been completed...' . PHP_EOL);
} catch (Exception $e) {
    echo('Error: ' . $e);
}
