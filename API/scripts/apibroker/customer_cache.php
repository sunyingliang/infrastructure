<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

use FS\Common\IO;

// Check connection is working before proceeding (DNS might not be fully working on @reboot)

IO::message('Starting customer cache script');

if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    IO::message("Connection information for {DATABASE_CONNECTION['infrastructure']} is not setup correctly" . PHP_EOL . $validation['message'],
        null, true);
}

if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'api_broker')) {
    IO::message('{API_BROKER_CONNECTIONS} is not setup correctly', null, true);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        break;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    IO::message('Error: Cannot connect to database', null, true);
}

//Truncate table
try {
    $pdo->exec("TRUNCATE `customer_cache`");
} catch (\Exception $e) {
    IO::message('Error: ' . $e->getMessage(), null, true);
}

// Fetch customer details
foreach (DATABASE_CONNECTION['api_broker'] as $connection) {
    try {
        if (!($validation = \FS\Common\IO::required($connection, ['dns', 'username', 'password', 'name'], true))['valid']) {
            IO::message('Connection information for {API_BROKER_CONNECTIONS} is not setup correctly' . PHP_EOL . $validation['message'], null, true);
        }

        $environment = $connection['name'];
        IO::message('Connecting to ' . $connection['dns']);

        $apiBrokerConnection = IO::getPDOConnection($connection);

        $sql = "SELECT `Name`, `DB_Host`, `DB_Username`, `DB_Password`, `DB_Name` FROM `customers`";

        IO::message('Fetching customers...');

        foreach ($apiBrokerConnection->query($sql) as $customer) {
            try {
                if (!empty($customer['DB_Host']) && !empty($customer['DB_Name']) && !empty($customer['DB_Username']) && !empty($customer['DB_Password'])) {
                    IO::message('Connecting to host:' . $customer['DB_Host'] . ', database:' . $customer['DB_Name'] . '...');
                    $customerConnection = new \PDO('mysql:host=' . $customer['DB_Host'] . ';dbname=' . $customer['DB_Name'],
                        $customer['DB_Username'], $customer['DB_Password'],
                        [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);

                    $customerConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
                    $customerConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
                    $customerConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                    IO::message('Processing {' . $customer['Name'] . '}...');

                    $users = $customerConnection->query("SELECT COUNT(1) AS `count` FROM `PMaro`")->fetch()['count'];
                    $cases = $customerConnection->query("SELECT COUNT(1) As `count` FROM `case_cases`")->fetch()['count'];

                    // Insert into `customer_cache` table
                    IO::message('Inserting {' . $customer['Name'] . '} into customer_cache table...');

                    $sql = "INSERT INTO `customer_cache` (`customer_name`, `environment`, `db_host`, `users`, `cases`) 
                          VALUES (:customer_name, :environment, :db_host, :users, :cases)";

                    $stmt = $pdo->prepare($sql);

                    $stmt->execute([
                        'customer_name' => $customer['Name'],
                        'environment'   => $environment,
                        'db_host'       => $customer['DB_Host'],
                        'users'         => $users,
                        'cases'         => $cases
                    ]);

                    IO::message('Customer {' . $customer['Name'] . '} inserted successfully' . PHP_EOL);
                } else {
                    IO::message('Customer {' . $customer['Name'] . '} does not have valid DB connection settings.');
                }
            } catch (Exception $e) {
                IO::message('Error: ' . $e->getMessage());
            }
        }
    } catch (Exception $e) {
        IO::message('Error: ' . $e->getMessage());
    }
}

IO::message('Finished customer cache script');


