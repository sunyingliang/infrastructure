<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not enabled, please check configuration in php.ini');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'log', ['dns', 'username', 'password'])) {
    die('Connection information for {LOG_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

try {
    $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['log']);

    if ($pdo === null) {
        die('Error: Cannot connect to database');
    }

    if (count($argv) != 2) {
        die('Error: command format is incorrect. e.g. php path/to/scripts/truncate_componentslog.php execute-frequency.');
    }

    // Truncate components logs to 6 months (daily script)
    if (isset($argv[1]) && strtolower($argv[1]) == 'daily') {
       $pdo->exec("DELETE FROM `Components` WHERE `LogTime` < DATE_SUB(NOW(), INTERVAL 6 MONTH)");
    }
    
    // Truncate records of target='/iisstart.htm' and target='/build.txt' (hourly script)
    if (isset($argv[1]) && strtolower($argv[1]) == 'hourly') {
        $stmt = $pdo->prepare("DELETE FROM `Components` WHERE `target`=:start OR `target`=:build");
        if ($stmt->execute(['start' => '/iisstart.htm', 'build' => '/build.txt']) === false) {
            die('PDOStatement execution failed');
        }
    }
}catch (Exception $e) {
    die($e->getMessage());
}
