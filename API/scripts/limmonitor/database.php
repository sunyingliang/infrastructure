<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

// Create table 'lim_monitoring' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `lim_monitoring` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `LIMName` varchar(40) NOT NULL,
  `URL` varchar(500) NOT NULL,
  `CustomerName` varchar(200) NOT NULL,
  `Result` tinyint(1) NOT NULL DEFAULT '0',
  `Version` varchar(64) DEFAULT NULL,
  `BuildDate` datetime DEFAULT NULL,
  `Framework` varchar(20) DEFAULT NULL,
  `Software` varchar(40) DEFAULT NULL,
  `Runtime` varchar(20) DEFAULT NULL,
  `Form` varchar(20) DEFAULT NULL,
  `as_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if Version column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Version'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Version` varchar(64) DEFAULT NULL;");
}

// Check if BuildDate column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'BuildDate'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `BuildDate` datetime DEFAULT NULL;");
}

// Check if Framework column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Framework'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Framework` varchar(20) DEFAULT NULL;");
}

// Check if Software column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Software'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Software` varchar(40) DEFAULT NULL;");
}

// Check if Runtime column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Runtime'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Runtime` varchar(20) DEFAULT NULL;");
}

// Check if Form column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Form'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Form` varchar(20) DEFAULT NULL;");
}

// remove columns
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Key'")->fetch(PDO::FETCH_ASSOC);

if ($result !== false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` DROP COLUMN `Key`;");
    $pdo->exec("ALTER TABLE `lim_monitoring` DROP COLUMN `IV`;");
    $pdo->exec("ALTER TABLE `lim_monitoring` DROP COLUMN `Encryption`;");
}

// Check if Index exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND index_name = 'INDEX'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring`
	ADD INDEX `INDEX` (`id`, `LIMName`, `CustomerName`, `Result`, `Version`, `Form`);");
}

// Check if Response column exists, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'lim_monitoring' AND column_name = 'Response'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `lim_monitoring` ADD `Response` VARCHAR(255) DEFAULT NULL AFTER `as_at`");
}
