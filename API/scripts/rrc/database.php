<?php
//Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
};

require __DIR__ . '/../../config/common.php';
require __DIR__ . '/../../classes/Common/autoload.php';

// Check connection is working before processing (DNA might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);
        $counter++;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

// Create rrc_statistics table
$sql = "CREATE TABLE IF NOT EXISTS `rrc_statistics`(
            `id` INT unsigned NOT NULL AUTO_INCREMENT,
            `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
            `total_hits` INT unsigned NOT NULL DEFAULT 0,
            `naf_hits` INT unsigned NOT NULL DEFAULT 0,
            `total_hits_by_site` VARCHAR(4000) DEFAULT '',
            `naf_hits_by_site` VARCHAR(4000) DEFAULT '',
            `total_form_submission_by_site` VARCHAR(4000) DEFAULT '',
            `clients_in_use` VARCHAR(4000) DEFAULT '',
            PRIMARY KEY (`id`),
            INDEX `date` (`date`),
            INDEX `total_hits_s` (`total_hits_by_site`),
            INDEX `naf_hits_s` (`naf_hits_by_site`),
            INDEX `form_submission_s` (`total_form_submission_by_site`),
            INDEX `clients_in_use` (`clients_in_use`)
        )";
if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Create rrc_versions table
$sql = "CREATE TABLE IF NOT EXISTS `rrc_versions`(
            `id` INT unsigned NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(64) NOT NULL DEFAULT '',
            `url` VARCHAR(512) NOT NULL DEFAULT '',
            `version` VARCHAR(10) NOT NULL DEFAULT '',
            `enabled` TINYINT(1) DEFAULT 1,
            `last_checked` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            INDEX `name` (`name`),
            INDEX `url` (`url`),
            INDEX `version` (`version`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

// Check if column `enabled` exist, if not create it
$result = $pdo->query("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = DATABASE() AND table_name = 'rrc_versions' AND column_name = 'enabled'")->fetch(PDO::FETCH_ASSOC);

if ($result === false) {
    $pdo->exec("ALTER TABLE `rrc_versions` ADD `enabled` TINYINT(1) DEFAULT 1 AFTER `version`");
}