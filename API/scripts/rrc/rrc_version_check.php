<?php

require __DIR__ . '/../../classes/Common/autoload.php';
require __DIR__ . '/../../config/common.php';
use FS\Common\IO;

$slackMessage = 'RRC Version check [' . COMMON['environment'] . ']: ';

try {
    $version = new FS\RRC\Handler\Versions();

    $response = $version->check(true);

    if ($response) {
        $slackMessage .= $response['status'] == 'error' ? 'Error: ' . $response['response']['message'] : 'Success, all RRC Versions up to date.';
    } else {
        throw new Exception('Failed to check RRC versions');
    }
} catch (Exception $ex) {
    $slackMessage .= 'Error: ' . $ex->getMessage();
}
IO::message($slackMessage);
IO::slack(SLACK['url'], SLACK['channel'], SLACK['username_proxy'], $slackMessage);
