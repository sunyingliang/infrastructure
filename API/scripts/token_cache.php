<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../classes/Common/autoload.php';
require __DIR__ . '/../config/common.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

try {
    $cachedTokens = '';

    $sql = 'SELECT `key`, `read`, `write`, `allow`, `expire` FROM `token`';

    if (($stmt = $pdo->query($sql)) === false) {
        throw new \FS\Common\Exception\PDOQueryException('PDO query failed');
    }

    while ($row = $stmt->fetch()) {
        $cachedTokens .= $row['key'] . '|' . $row['read'] . '|' . $row['write'] . '|' . $row['allow'] . '|' . (isset($row['expire']) ? $row['expire'] : '') . ';';
    }

    $cachedTokens = substr($cachedTokens, 0, -1);

    $wroteChars = file_put_contents(COMMON['token_cached_file'], $cachedTokens, LOCK_EX);

    if ($wroteChars != strlen($cachedTokens)) {
        throw new \FS\Common\Exception\FileWriteException('Failed to cache tokens');
    }
} catch (\Exception $e) {
    die($e->getMessage());
}

exit('Tokens have been cached successfully.');
