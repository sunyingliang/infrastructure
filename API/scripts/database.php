<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/autoload.php';

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
if (!$validation = \FS\Common\IO::checkDBConstant('DATABASE_CONNECTION', 'infrastructure', ['dns', 'username', 'password'])) {
    die('Connection information for {INFRASTRUCTURE_CONNECTION} is not setup correctly' . PHP_EOL . $validation['message']);
}

$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        $pdo = \FS\Common\IO::getPDOConnection(DATABASE_CONNECTION['infrastructure']);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database');
}

#region Create tables for PHP-5
// Create table 'token' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `token` (
            `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NOT NULL DEFAULT '',
            `key` VARCHAR(36) NOT NULL DEFAULT '',
            `read` TINYINT(1) NULL DEFAULT '0',
            `write` TINYINT(1) NULL DEFAULT '0',
            `allow` VARCHAR(255) NOT NULL DEFAULT '',
            `expire` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `name` (`name`),
            UNIQUE INDEX `key` (`key`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion

#region Download
$sql = "CREATE TABLE IF NOT EXISTS `download_component` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `release_notes_url` varchar(255) NOT NULL,
    `notes` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `IX_name` (`name`)
);";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}

$sql = "CREATE TABLE IF NOT EXISTS `download_version` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `component_id` int(10) unsigned NOT NULL,
    `version` varchar(10) NOT NULL,
    `release_date` datetime NOT NULL,
    `url` varchar(255) NOT NULL,
    `public` tinyint(4) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `FK_download_version_download_component` (`component_id`),
    KEY `IX_component_id_release_date` (`component_id`,`release_date`),
    KEY `IX_version` (`version`),
    CONSTRAINT `FK_download_version_download_component` FOREIGN KEY (`component_id`) REFERENCES `download_component` (`id`)
);";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion

#region Create API Broker Customer Detail Table. FOR PHP-376
// Create table `customer_cache` if not exists
$sql = "CREATE TABLE IF NOT EXISTS `customer_cache` (
            `customer_name` VARCHAR(64) NOT NULL DEFAULT '',
            `environment` VARCHAR(64) NOT NULL DEFAULT '',
            `db_host` VARCHAR(255) NOT NULL DEFAULT '',
            `users` INT(10) NOT NULL DEFAULT '0',
            `cases` INT(10) NOT NULL DEFAULT '0',
            KEY `customer_name` (`customer_name`),
            KEY `environment` (`environment`),
            KEY `db_host` (`db_host`)
)";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion

