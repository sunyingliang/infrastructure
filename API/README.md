###Notice:
First, run script `token_cache.php` to generate token_cached.txt.
Then find out the token you need from Infrastructure token table or token_cached.txt.
Tokens are passed with url.

###Crontab API
Crontab API is comprised of cron-job api and cron-log api.

####Cron-job API
1. Get the list of all jobs
    * URI: /api/cronjob/list
    * Method: POST
    * Params: token=tokenParm[&enabled=enabledParam[&...]]
    * Return: 
    ~~~
      {
        "response": {
          "status": "success",
          "data": [
            {
              "id": "1",
              "name": "test-update",
              "frequency": "* * * * *",
              "command": "test-update",
              "comment": "test-update",
              "concurrent": "1",
              "execution_time": "30",
              "enabled": "1"
            }
          ]
        }
      }
    ~~~            
    * Notes: Parameters in square brackets are optional
              
2. Get a specific job by Id
    * URI: /api/cronjob/get
    * Method: POST
    * Params: id=idParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success",
          "data": {
            "id": "1",
            "name": "test-update",
            "frequency": "* * * * *",
            "command": "test-update",
            "comment": "test-update",
            "concurrent": "1",
            "execution_time": "30",
            "enabled": "1"
          }
        }
      }
    ~~~
              
3. Add a job
    * URI: /api/cronjob/create
    * Method: POST
    * Params: name=nameParam&frequency=frequencyParam&command=commandParam&comment=commentParam&
              concurrent=concurrentParam&execution_time=execution_timeParam&enabled=enabledParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success"
        }
      }
    ~~~
 
4. Update a job with specific columns
    * URI: /api/cronjob/update
    * Method: POST
    * Params: id=idParam&name=nameParam&frequency=frequencyParam&command=commandParam&comment=commentParam&
              concurrent=concurrentParam&started=startedParam&execution_time=execution_timeParam&enabled=enabledParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success"
        }
      }
    ~~~
 
5. Delete a job by Id
    * URI: /api/cronjob/delete
    * Method: POST
    * Params: id=idParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success"
        }
      }
    ~~~
 
6. Update the status of a job
    * URI: /api/cronjob/update-status
    * Method: POST
    * Params: id=idParam&started=startedParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success"
        }
      }
    ~~~

####Cron-log API
1. Get the list of logging dates basing on a specific job id
    * URI: /api/cronlog/period
    * Method: POST
    * Params: job_id=jobIdParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success",
          "data": [
            {
              "start_time": "2016-09-14"
            }
          ]
        }
      }
    ~~~
              
2. Get the list of logs basing on a specific job id and date
    * URI: /api/cronlog/list
    * Method: POST
    * Params: job_id=jobIdParam&date=dateParam&offset=offsetParam&limit=limitParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success",
          "data": {
            "totalRowCount": 1,
            "data": [
              {
                "id": "1",
                "startTime": "2016-09-14 06:17:57",
                "response": ""
              }
            ]
          }
        }
      }
    ~~~
              
3. Add a log
    * URI: /api/cronlog/create
    * Method: POST
    * Params: job_id=job_idParam&response=responseParam&token=token
    * Return: 
    ~~~
      {
        "response": {
          "status": "success"
        }
      }
    ~~~
              
###Proxy API  
1. Get the Proxy server apache2 configure file string
    * URI: /proxy/config
    * Method: POST
    * Params: token=tokenParam
    * Return: 
    ~~~
        <VirtualHost *:80>
            DocumentRoot /var/www/Proxy
            SSLProxyEngine On
            SSLProxyCheckPeerName off
            SSLProxyVerify none
            SSLProxyCheckPeerCN off
            SSLProxyCheckPeerExpire off
          
            #Auckland Office LIM
            
              <Location /aucklandoffice>
                ProxyPass http://auckland-office.firmstep.com:32008/LIM retry=0 disablereuse=On keepalive=Off
                SetEnv proxy-initial-not-pooled 1
            </Location>
          
            #Paylink v3 payment connector
              <Location /paylink3>
                ProxyPass https://secure.citypay.com/paylink3/create retry=0 disablereuse=On keepalive=Off
                SetEnv proxy-initial-not-pooled 1
            </Location>
          
            #This URL should fail so that we get at least one entry in the daily test report
              <Location /invalid>
                ProxyPass http://www.thisdoesnotreallyexist.com/ retry=0 disablereuse=On keepalive=Off
                SetEnv proxy-initial-not-pooled 1
            </Location>
          </VirtualHost>
    ~~~      
    
2. Get proxy mapping records
    * URI: /proxy/mapping/get
    * Method: POST
    * Params: token=tokenParam
    * Return: 
    ~~~
        {
          "response": {
            "status": "success",
            "data": [
              {
                "id": 2,
                "path": "paylink3",
                "url": "https://secure.citypay.com/paylink3/create",
                "httpDowngrade": 0,
                "keepAlive": 0,
                "pooled": 1,
                "comment": "Paylink v3 payment connector",
                "testUrl": ""
              }
            ]
          }
        }
    ~~~
    
3. List proxy mapping records with selecting conditions
    * URI: /proxy/mapping/list
    * Method: POST
    * Params: token=token[&path=pathParam&http_downgrade=http_downgradeParam&keep_alive=keep_aliveParam&pooled=pooledParam&offset=offsetParam&limit=limitParam]
    * Return: 
    ~~~
        {
          "response": {
            "status": "success",
            "data": {
              "totalRowCount": 1,
              "rows": [
                {
                  "id": 1,
                  "path": "aucklandoffice",
                  "httpDowngrade": 0,
                  "keepAlive": 0,
                  "pooled": 1,
                  "url": "http://auckland-office.firmstep.com:32008/LIM",
                  "comment": "Auckland Office LIM",
                  "testUrl": "keygen.aspx"
                }
              ]
            }
          }
        }
    ~~~
    * Notes: all params are optional, but offset and limit parameters are binded together.
    
4. Search proxy mapping records with selecting conditions
    * URI: /proxy/mapping/search
    * Method: POST
    * Params: path=pathParam&token=token[&offset=offsetParam&limit=limitParam]
    * Return: 
    ~~~
        {
          "response": {
            "status": "success",
            "data": {
              "totalRowCount": 1,
              "rows": [
                {
                  "id": 1,
                  "path": "aucklandoffice",
                  "httpDowngrade": 0,
                  "keepAlive": 0,
                  "pooled": 1,
                  "url": "http://auckland-office.firmstep.com:32008/LIM",
                  "comment": "Auckland Office LIM",
                  "testUrl": "keygen.aspx"
                }
              ]
            }
          }
        }
    ~~~
    * Notes: params in square brackets are optional, but offset and limit parameters are binded together.
    
5. Create proxy mapping record
    * URI: /proxy/mapping/create
    * Method: POST
    * Params: path=pathParam&url=urlParam&http_downgrade=http_downgradeParam&token=token[&keep_alive=keep_aliveParam&pooled=pooledParam]
    * Return: 
    ~~~
        {
          "response": {
            "status": "success"
          }
        }
    ~~~
    * Notes: params in square brackets are optional.
    
6. Update proxy mapping record
    * URI: /proxy/mapping/update
    * Method: POST
    * Params: path=pathParam&url=urlParam&http_downgrade=http_downgradeParam&token=token[&keep_alive=keep_aliveParam&pooled=pooledParam]
    * Return: 
    ~~~
        {
          "response": {
            "status": "success"
          }
        }
    ~~~
    * Notes: params in square brackets are optional.
    
7. Delete proxy mapping record
    * URI: /proxy/mapping/delete
    * Method: POST
    * Params: path=pathParam&token=token
    * Return: 
    ~~~
        {
          "response": {
            "status": "success"
          }
        }
    ~~~
	
###Logging API  

1. Get the list of Collections Logs 
	* URI: /api/log/collections/list
    * Method: POST
	* Params: token, fromDate, toDate, log, hostname, sitename, message, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows": [
					{
					  "timestamp": 
					  "logger":
					  "level":
					  "message":
					  "exception":
					  "thread":
					  "file":
					  "line":
					  "hostname":
					  "sitename": 
					  }
					]
		}
    ~~~

2. Get the list of Componnets Logs 
	* URI: /api/log/components/list
    * Method: POST
	* Params: token, fromDate, toDate, query, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows": [
					{
					  "ClientHost": 
					  "username":
					  "LogTime":
					  "service":
					  "machine":
					  "serverip":
					  "processingtime":
					  "bytesrecvd":
					  "bytessent": 
					  "servicestatus":
					  "win32status": 
					  "operation":
					  "target":
					  "parameters":
					  }
					]
			}
		}
    ~~~

3. Get the status of Componnets 
	* URI: /api/log/components/status
    * Method: POST
	* Params: token
    * Return: 
    ~~~
		{
		    "response":{
		        "status":"success",
		        "data":[
		            "ALL",200,301,302,304,400,403,404,405,500
		        ]
		    }
		}
    ~~~           
           

4. Get the list of FillTask Logs 
	* URI: /api/log/fillTask/list
    * Method: POST
	* Params: token, fromDate, toDate, message, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows":[
					{
					  "Date": 
					  "Message":
					  }
					]
			}
		}
    ~~~   

5. Get the list of LIM Logs 
	* URI: /api/log/lim/list
    * Method: POST
	* Params: token, fromDate, toDate, message, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows": [
					{
					  "Date": 
					  "Message":
					  } 
					]
			}
		}
    ~~~  
	
6. Get the list of PaymentConnectors Logs 
	* URI: /api/log/paymentConnectors/list
    * Method: POST
	* Params: token, fromDate, toDate, timeFrom, timeTo, reference, level, log, message, exception, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows": [
					{
					  "Reference": 
					  "Date":
					  "Level":
					  "Logger":
					  "Exception":
					  "Message":
					  }
					]
			}
		}
    ~~~  
	
7. Get the list of PaymentConnectors Source 
	* URI: /api/log/paymentConnectors/source
    * Method: POST
	* Params: token=tokenParm
    * Return: 
    ~~~
		[
			"All", "Logger"
		]
    ~~~  

8. Get the list of RRC Logs 
	* URI: /api/log/rrc/list
    * Method: POST
	* Params: token, fromDate, toDate, message, site, offset, limit
    * Return: 
    ~~~
		{
			"TotalRowCount": $rowCount,
					"Rows": [
					{
					  "Date":
					  "Message":
					  "Site":
					  }
					]			
		}
    ~~~  
    
9. Get the list of Email logs
    * URI: /api/log/email/list
    * Method: POST
    * Params: token=tokenParam&status={sent|failed}[&start=startParam[&end=endParam[&subject=subjectParam[&from=fromParam[&to=toParam]]]]]
    * Return:
    ~~~
    {
        "status":"sent",
        "TotalRowCount":2,
        "Rows":[
            {
                "id":3,
                "time_received":"2017-03-27 14:20:10",
                "from_address":"sample1@gmail.com",
                "to_address":"sample2@gmail.com",
                "subject":"example"
            },
            {
                "id":2,
                "time_received":"2017-03-27 14:20:10",
                "from_address":"sample1@gmail.com",
                "to_address":"sample2@gmail.com",
                "subject":"example"
            }
        ]
    }
    ~~~
	
10. Insert an record of email log into database
	* URI: /api/log/email/create
	* Method: POST
	* Params: token=tokenParam&status={sent|failed}
	    1. when the value of status is sent, then the mandatory params also includes: time_received, source_ip,
	       from_address, to_address, cc_address, subject;
	    2. when the value of status is failed, then the mandatory params also includes: 'message_id', 
	       from_address, to_address, subject, time_received, reason;
	* Return:
	~~~
	{
	    "response": {
	        "status":"success",
            "data":{
                "id": 5
            }
	    }
	}

	
11. Remove an record of email log from database
	* URI: /api/log/email/delete
	* Method: POST
	* Params: token=tokenParam&status={sent|failed}&id=idParam
	* Return:
	~~~
	{
	    "status":"success"
	}
	~~~
	
12. Get background log
* URI: /api/oaf/background/list
	* Method: POST
	* Params: token=tokenParams
	* Return:
	~~~
	{
      "response": {
        "status": "success",
        "data": {
          "totalRowCount": 9079,
          "rows": [
            {
              "timestamp": "2017-05-05 04:58:56",
              "server_id": "firmstepdev",
              "site": "firmstepdev.local.firmstep.com",
              "operation": "logBackgroundEnd"
            },
          ]
        }
      }
    }        
	~~~
	
13. Get background site
* URI: /api/oaf/background/site
	* Method: POST
	* Params: token=tokenParams
	* Return:
	~~~
	{
      "response": {
        "status": "success",
        "data": [
          "All",
          "firmstepdev.local.firmstep.com",
          "form",
          "oaf.devops.firmstep.com",
          "test",
          "testing[.-]"
        ]
      }
    }
	~~~
	
14. Get background server
* URI: /api/oaf/background/server
	* Method: POST
	* Params: token=tokenParams
	* Return:
	~~~
	{
      "response": {
        "status": "success",
        "data": [
          "All",
          "",
          "background",
          "background-01",
          "firmstepdev",
          "oaf"
        ]
      }
    }
	~~~
	
15. Get background operation
* URI: /api/oaf/background/operation
	* Method: POST
	* Params: token=tokenParams
	* Return:
	~~~
	{
      "response": {
        "status": "success",
        "data": [
          "All",
          "logBackgroundEnd",
          "logBackgroundStart"
        ]
      }
    }
	~~~
	
16. Get the list of Agilisys Logs		
	* URI: /api/log/agilisys/list		
    * Method: POST		
	* Params: token, fromDate, toDate, environment, log, message, offset, limit		
    * Return: 		
    ~~~		
 	{		
 		"TotalRowCount": $rowCount,		
 		"Rows": [		
		    {		
				"Date": 		
				"Environment":		
				"Logger":		
				"Message":		
			}		
		]		
	}				
    ~~~            		
			
17. Get the list of Agilisys Environments		
	* URI: /api/log/agilisys/environment		
    * Method: POST		
	* Params: token=tokenParm		
    * Return: 		
    ~~~		
		[		
			"All", "Environment"		
		]		
    ~~~		
		
18. Get the list of Agilisys sites		
	* URI: /api/log/agilisys/site		
    * Method: POST		
	* Params: token=tokenParm		
    * Return: 		
    ~~~		
			[		
				"All", "Logger"		
			]		
    ~~~	
	
19. Insert an record of ftp log into database
	* URI: /api/log/ftp/create
	* Method: POST
	* Params: token=tokenParam&customerId=customerIdParam&fileName=fileNameParam
	* Return:
	~~~
	{
        "response": {
            "status": "success",
            "data": {
                "id": "1"
            }
        }
    }

	
20. Update an record of ftp log in database
	* URI: /api/log/log/update
	* Method: POST
	* Params: token=tokenParam&id=idParam&response=responseParam
	* Return:
	~~~
	{
	    "status":"success"
	}
	~~~	
  
	
###Site Master API  
1. Get the list of collections
	* URI: /api/sitemaster/collections/list
    * Method: POST
	* Params: token=tokenParm
    * Return: 
    ~~~
		[
		  {
			"name": 
			"host":
			"dbserver":
			"dbname":
			"dbuser":
			"dbpassword":
			"additionalips":
			"authsitehosts":
			"anonsitehosts": 
			"authusername":
			"authpassword": 
		  }
		]
    ~~~  

2.	Add record to the collection_master site table
	* URI: /api/sitemaster/collections/create
    * Method: POST
	* Params: token=tokenParm&name=nameParam
    * Return: 
    ~~~
		  "SUCCESS"
    ~~~  
	
3.	Update collection_master site table 
	* URI: /api/sitemaster/collections/update
    * Method: POST
	* Params: token, siteName, additionalIPs, authSiteHosts, anonSiteHosts
    * Return: 
    ~~~
		  "SUCCESS"
    ~~~  
4. Get the list of admin sites
	* URI: /api/sitemaster/globaladmin/list
    * Method: POST
	* Params: token=tokenParm
    * Return: 
    ~~~
    [
      {
        "host":
      }
    ]
    ~~~  	

5. Create golbal admin account 
	* URI: /api/sitemaster/globaladmin/create
    * Method: POST
	* Params: token, siteName, fullName, email
    * Return: 
    ~~~
		"resetLink"
    ~~~   

###LIMMonitoring API
1. Get result codes
	* URI: /api/lim/monitoring/resultcodes
    * Method: POST
	* Params: token
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": [
          "Fail",
          "Success",
          "Fail/Fail",
          "Fail/Success",
          "Success/Fail",
          "Success/Success"
        ]
      }
    }
    ~~~

2. List lim monitoring records
  * URI: /api/lim/monitoring/list
  * Method: POST
  * Params: token
  * Return:
  ~~~
  {
    "response": {
      "status": "success",
      "data": {
        "totalRowCount": 19,
        "rows": [
          {
            "LIMName": "Auckland Office",
            "URL": "http://proxy.firmsteptest.com/aucklandoffice/directlim.ashx",
            "CustomerName": "devtest",
            "Result": 0,
            "Version": null,
            "BuildDate": null,
            "Framework": null,
            "Software": null,
            "Runtime": null,
            "Form": "Forms",
            "Response": null
          },
          ...
        ]
      }
    }
  }
  ~~~

###RRC API
1. Insert RRC log record
	* URI: /api/rrc/log
  * Method: POST
	* Params: token, ipAddress, controlType, controlVersion, siteUrl, method, pageType, serverid
  * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "message": "RRC log succeed",
        "data": {
          "id": "6447"
        }
      }
    }
    ~~~

###API Broker API
1.	Get database list
	* URI: /api/apibroker/list
    * Method: POST
	* Params: token=tokenParm
    * Return:
    ~~~
    [
      {
        "ServerName":
      }
    ]
    ~~~

2.	Get host name
	* URI: /api/apibroker/host
    * Method: POST
	* Params: token=tokenParm
    * Return:
    ~~~
    [
      {
        "host":
      }
    ]
    ~~~

3.  Process request and get matched ID and Value from tokens
	* URI: /api/apibroker/process
    * Method: POST
	* Params: token, database, host
    * Return:
    ~~~
		[
		  {
  			"ID":
  			"Value":
		  }
		]
    ~~~

4. Create golbal admin account
	* URI: /api/apibroker/update
    * Method: POST
	* Params: token, newServer, result, host
    * Return:
    ~~~
		"Updated"
    ~~~

###OAF API
1. Insert tracking feature
	* URI: /api/oaf/feature
    * Method: POST
	* Params: token, host, feature
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "id": "180"
        }
      }
    }
    ~~~    

2. Insert tracking submission
	* URI: /api/oaf/submission
    * Method: POST
	* Params: token, formName, formGroupName, status, impersonated, userType, host
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "id": "263"
        }
      }
    }
    ~~~      

3. Insert tracking error
	* URI: /api/oaf/error
    * Method: POST
	* Params: token, host, server, requestFolders, requestParams, error, location
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "id": "2114"
        }
      }
    }
    ~~~       

4. Tracking statistics list
	* URI: /api/oaf/statistics/list
    * Method: POST
	* Params: token
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "json": {
            "month": [
              {
                "id": 33,
                "date": "2017-03-01 00:00:00"
              },
              {
                "id": 28,
                "date": "2017-02-01 00:00:00"
              }
            ],
            "week": [
              {
                "id": 38,
                "date": "2017-04-17 00:00:00"
              },
              {
                "id": 37,
                "date": "2017-04-10 00:00:00"
              }
            ]
          }
        }
      }
    }
    ~~~        

5. Tracking statistics report
	* URI: /api/oaf/statistics/report
    * Method: POST
	* Params: token, id
    * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "hosts": [
            {
              "name": "firmstepdev",
              "CSA": 0,
              "Ext": 0,
              "Int": 4,
              "Anon": 0,
              "Unknown": 0,
              "Total": 4,
              "forms": [
                {
                  "name": "RRC Demo Process",
                  "CSA": 0,
                  "Ext": 0,
                  "Int": 3,
                  "Anon": 0,
                  "Unknown": 0,
                  "Total": 3
                },
                {
                  "name": "User ID etc",
                  "CSA": 0,
                  "Ext": 0,
                  "Int": 1,
                  "Anon": 0,
                  "Unknown": 0,
                  "Total": 1
                }
              ]
            },
            {
              "name": "forms.firmsteptest.com",
              "CSA": 0,
              "Ext": 0,
              "Int": 0,
              "Anon": 11,
              "Unknown": 0,
              "Total": 11,
              "forms": [
                {
                  "name": "First inst",
                  "CSA": 0,
                  "Ext": 0,
                  "Int": 0,
                  "Anon": 11,
                  "Unknown": 0,
                  "Total": 11
                }
              ]
            }
          ]
        }
      }
    }
    ~~~ 
    
6. Get page views
	* URI: /api/oaf/analytics/page-views
    * Method: POST
	* Params: path,
    * Return:
    ~~~
    {
      "response": {
        "visitor-id": 1718406360,
        "session-id": 1186893181,
        "first-visit-time": "2017-05-09 01:12:09",
        "previous-visit-time": "2017-05-09 01:12:09",
        "current-visit-time": "2017-05-09 01:12:09",
        "visit-count": 1
      }
    }
    ~~~
    
7. Get events
	* URI: /api/oaf/analytics/events
    * Method: POST
	* Params: category, action, label, value
    * Return:
    ~~~
    {
      "response": {
        "visitor-id": 1648084086,
        "session-id": 875915370,
        "first-visit-time": "2017-05-09 01:13:56",
        "previous-visit-time": "2017-05-09 01:13:56",
        "current-visit-time": "2017-05-09 01:13:56",
        "visit-count": 1
      }
    }
    ~~~  
    
8. Insert background log record
	* URI: /api/oaf/background/log
  * Method: POST
	* Params: op=opParam&{others}={othersParam}
  * note: the param name and numbers of 'others' depends on the value of 'op'.
  * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "id": "2114"
        }
      }
    }
    ~~~   

###Background API
1. Get background log records
  * URI: /api/oaf/background/list
  * Method: POST
  * Params: token=tokenParam&[fromDate, toDate, server, site, operation, offset, limit, sort, sortOrder]
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "data": {
        "totalRowCount": 9891,
        "rows": [
          {
            "timestamp": "2017-05-19 05:48:38",
            "server_id": "firmstepdev",
            "site": "firmstepdev.local.firmstep.com",
            "operation": "logBackgroundEnd"
          },
          ...
        ]
      }
    }
  }
  ~~~

2. Get background site list
  * URI: /api/oaf/background/site
  * Method: POST
  * Params: token=tokenParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "data": [
        "All",
        "firmstepdev.local.firmstep.com",
        "form",
        "forms.firmsteptest.com",
        "migration.firmsteptest.com",
        "oaf.devops.firmstep.com",
        "test",
        "testing.firmsteptest.com",
        "testing[.-]"
      ]
    }
  }
  ~~~

3. Get background server list
  * URI: /api/oaf/background/server
  * Method: POST
  * Params: token=tokenParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "data": [
        "All",
        "",
        "background",
        "background-01",
        "firmstepdev",
        "oaf"
      ]
    }
  }
  ~~~

4. Get background operation list
  * URI: /api/oaf/background/operation
  * Method: POST
  * Params: token=tokenParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "data": [
        "All",
        "logBackgroundEnd",
        "logBackgroundStart"
      ]
    }
  }
  ~~~

###MONITORING API
1. Update monitoring state field in monitoring table
  * URI: /api/monitoring/switch
  * Method: POST
  * Params: token=tokenParam&guid=guidParam&state=stateParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "message": "updated!"
    }
  }
  ~~~

2. Update group field in monitor_server for 'SQLDump', 'CouchDB', 'CouchDB2'
  * URI: /api/monitoring/toggle
  * Method: POST
  * Params: token=tokenParam&group=groupParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "message": "updated!"
    }
  }
  ~~~

3. Update group field in monitor_server for 'SQLDump', 'CouchDB', 'CouchDB2'
  * URI: /api/monitoring/toggle
  * Method: POST
  * Params: token=tokenParam&group=groupParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "message": "updated!"
    }
  }
  ~~~ 
    
4. Insert monitoring log record
	* URI: /api/monitoring/log
  * Method: POST
	* Params: op=opParam&{others}={othersParam}
  * note: the param name and numbers of 'others' depends on the value of 'op'.
  * Return:
    ~~~
    {
      "response": {
        "status": "success",
        "data": {
          "id": "2114"
        }
      }
    }
    ~~~ 

5. get client list
  * URI: /api/monitoring/client/list
  * Method: POST
  * Params: token=tokenParam
  * Return: 
  ~~~
  {
    "response": {
      "status": "success",
      "data": [
        {
          "id": 1,
          "url": "http://www.firmsteptest.com/",
          "successcriteria": "href"
        },
        ...
      ]
    }
  }
  ~~~ 

6. update client report
  * URI: /api/monitoring/client/report
  * Method: POST
  * Params: 
  ~~~
  {
    "token":"46F3A4C9-0290-40EB-B7E9-4A5495785E0C",	
    "id":"16",
    "host":"Dallas",
    "delay":"1.000000",
    "status":"1"
  }
  ~~~
  * Return: 
  ~~~
  {
    "response": {
      "status": "success"
    }
  }
  ~~~ 
