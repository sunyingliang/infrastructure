<?php

// Common configuration settings
define('COMMON', [
    'token_cached_file' => __DIR__ . '/example.txt',
    'environment'       => 'example'
]);

// Define proxy test endpoint
define('PROXY_TEST_ENDPOINT', 'http://example.example.com/test/');

// Define Collection host suffix
define('HOST_SUFFIX', [
    'oaf_global_admin' => '.example.com',
    'collection'       => '.example.com'
]);

// Define mail service
define('MAIL_SERVICE', [
    'server'             => 'example',
    'port'               => 0,
    'from'               => 'Example',
    'from_rrc'           => 'Example',
    'from_monitoring'    => 'Example',
    'to_tracking'        => 'Example',
    'to_error'           => 'Example',
    'to_rrc'             => 'Example',
    'to_monitoring'      => 'Example',
    'subject_tracking'   => 'example_subject',
    'subject_error'      => 'example_subject',
    'subject_rrc'        => 'example_subject',
    'subject_monitoring' => 'example_subject',
    'message'            => '',
    'message_monitoring' => 'Sample message for testing'
]);

// Define google analytics
define('GOOGLE_ANALYTICS', [
    'ga_id'     => 'example id',
    'ga_domain' => 'example'
]);

// Define monitoring constants
define('PINGDOM', [
    'url'  => 'http://example',
    'auth' => '',
    'key'  => ''
]);

// Define simple queue service
define('SIMPLE_QUEUE_SERVICE', [
    'mode'    => 'example',
    'warning' => 0,
    'error'   => 0
]);

// Define slack constants
define('SLACK', [
    'url'            => 'https://example',
    'channel'        => 'example-bot',
    'username'       => 'example',
    'username_proxy' => 'example [example]'
]);

// Database configuration
define('DATABASE_CONNECTION', [
    'infrastructure' => [
        'dns'      => 'mysql:host=example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'log'            => [
        'dns'      => 'mysql:host=mysql.example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'oaf_master'     => [
        'dns'      => 'sqlsrv:Server=example.com;Database=example;ConnectionPooling=0',
        'username' => 'example',
        'password' => 'example'
    ],
    'collections'    => [
        'dns'      => 'mysql:host=mysql.example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'tracking'       => [
        'dns'      => 'mysql:host=mysql.example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'stats'          => [
        'dns'      => 'mysql:host=mysql.example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'monitoring'     => [
        'dns'      => 'mysql:host=mysql.example.com;dbname=example',
        'username' => 'example',
        'password' => 'example'
    ],
    'api_broker'     => [
        [
            'dns'      => 'mysql:host=mysql.example.com;dbname=example;',
            'username' => 'example',
            'password' => 'example',
            'name'     => 'example'
        ]
    ]
]);
