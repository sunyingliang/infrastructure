<?php

class CrontabJob
{
    /**
     * Tell whether the cron job is enabled or not
     * This will add or not a # at the beginning of the cron line
     *
     * @var boolean
     */
    public $enabled = true;

    /**
     * Min (0 - 59)
     *
     * @var String/int
     *
     */
    public $minutes;
    
    /**
     * Hour (0 - 23)
     *
     * @var String/int
     */
    public $hours;
    
    /**
     * Day of month (1 - 31)
     *
     * @var String/int
     */
    public $dayOfMonth;
    
    /**
     * Month (1 - 12)
     *
     * @var String/int
     */
    public $months;
    
    /**
     * Day of week (0 - 6) (0 or 6 are Sunday to Saturday, or use names)
     *
     * @var String/int
     */
    public $dayOfWeek;

    /**
     * The task command line to be executed 
     *
     * @var String
     */
    public $taskCommandLine;
    
    /**
     * Optional comment that will be placed at the end of the crontab line 
     * and preceded by a #
     *
     * @var String
     */
    public $comments;
    
    /**
     * Predefined scheduling definition
     * Shorcut définition that replace standard définition (preceded by @)
     * possibles values : yearly, monthly, weekly, daily, hourly, reboot
     * When a shortcut is defined, it overwrite stantard définition
     *
     * @var String
     */
    public $shortCut;
    
    /**
     * Factory method to create a CrontabJob from a crontab line.
     *
     * @param String $crontabLine
     * @throws InvalidArgumentException
     * @return CrontabJob
     */	 
    public static function createFromCrontabLine($crontabLine)
	{
		// Create the job from parsed crontab line values
        $crontabJob = new self();
		
		$crontabJob->setFromCrontabLine($crontabLine);
		return $crontabJob;
	}
	
	public function setFromCrontabLine($crontabLine)
    {
        // Check crontab line format validity
		$matches = CrontabJob::CheckCrontabLine($crontabLine);
      
        if (!empty($matches[1])) {
            $this->enabled = false;
        }

        if (!empty($matches)) {
            $this->minutes = $matches[3];
            $this->hours = $matches[4];
            $this->dayOfMonth = $matches[5];
            $this->months = $matches[6];
            $this->dayOfWeek = $matches[7];
        }
        
        if (!empty($matches[8])) {
            $this->shortCut = $matches[9];
        }
        
        $this->taskCommandLine = $matches[10];
        if (!empty($matches[12])) {
            $this->comments = $matches[12];
        }
        
        return $this;
    }
	
	/**
     * Check if a crontab line is valid.
     *
     * @param String $crontabLine
     * @throws InvalidArgumentException
     * @return matches
     */	 
	public static function CheckCrontabLine($crontabLine)
	{
		// Check crontab line format validity
        $crontabLineRegex = '/^[\s\t]*(#)?[\s\t]*(([*0-9,-\/]+)[\s\t]+([*0-9,-\/]+)'
            . '[\s\t]+([*0-9,-\/]+)[\s\t]+([*a-z0-9,-\/]+)[\s\t]+([*a-z0-9,-\/]+)|'
            . '(@(reboot|yearly|annually|monthly|weekly|daily|midnight|hourly)))'
            . '[\s\t]+([^#]+)([\s\t]+#(.+))?$/';

        if (!preg_match($crontabLineRegex, $crontabLine, $matches)) {
            throw new \InvalidArgumentException(
                'Crontab line not well formated then can\'t be parsed'
            );
        }
		return $matches;
	}
    
    /**
     * Format the CrontabJob to a crontab line 
     *
     * @throws InvalidArgumentException
     * @return String
     */
    public function formatCrontabLine()
    {
        
        // Check if job has a task command line
        if (!isset($this->taskCommandLine) || empty($this->taskCommandLine)) {
            throw new \InvalidArgumentException(
                'CrontabJob contain\'s no task command line'
            );
        }
        
        $taskPlanningNotation = (isset($this->shortCut) && !empty($this->shortCut))
            ? sprintf('@%s', $this->shortCut)
            : sprintf(
                '%s %s %s %s %s',
                (isset($this->minutes) ? $this->minutes : '*'),
                (isset($this->hours) ? $this->hours : '*'),
                (isset($this->dayOfMonth) ? $this->dayOfMonth : '*'),
                (isset($this->months) ? $this->months : '*'),
                (isset($this->dayOfWeek) ? $this->dayOfWeek : '*')
            )
        ;
        
        return sprintf(
            '%s%s %s%s',
            ($this->enabled ? '' : '#'),
            $taskPlanningNotation,
            $this->taskCommandLine,
            (isset($this->comments) ? (' #' . $this->comments) : '')
        );
    }
	
}
