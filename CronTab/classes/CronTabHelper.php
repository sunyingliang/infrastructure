<?php

require __DIR__ . '/Common/CURL.php';
require __DIR__ . '/Common/IO.php';

use FS\Common\CURL;
use FS\Common\IO;

class CronTabHelper
{
    public static function updateJobStatus($url, $id, $started)
    {
        try {
            $options = [
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => [
                    'id'      => $id,
                    'started' => $started,
                    'token'   => API_TOKEN
                ],
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true
            ];

            $curl = CURL::init($options);

            $response = CURL::exec($curl);

            if ($response === false) {
                CURL::close($curl);
                $message = 'Failed to update status for job {' . $id . '} via API';
                IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, $message);
                throw new Exception($message . '. CURL Error Number: "' . curl_errno($curl) . '". CURL Error: "' . curl_error($curl) . '"');
            }

            CURL::close($curl);

            $responseArr = json_decode($response, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new Exception('Could not parse data returned from Infrastructure. Response: "' . $response . '"');
            }

            if ($responseArr['response']['status'] === 'error') {
                throw new Exception('Error response from ' . $curl . '. Response: "' . $response['response']['message']);
            }
        } catch (Exception $ex) {
            IO::logError($ex->getMessage());
            return false;
        }

        return true;
    }

    public static function executeJob(&$job)
    {
        if (!is_array($job)) {
            throw new Exception('Error: Passed in parameter {job} must be a array');
        }

        // Update job status
        $job['start_time'] = (new \DateTime())->format('Y-m-d H:i:s');
        if (!self::updateJobStatus($job['apiJobUpdate'], $job['id'], 1)) {
            throw new Exception('Error: Failed to update job status via API');
        }

        // Catch maximum execution time out
        register_shutdown_function(function ($jobId, $startTime, $executionTime, $apiJobUpdate) {
            CronTabHelper::logTimeout($jobId, $startTime, $executionTime, $apiJobUpdate);
        }, $job['id'], $job['start_time'], $job['execution_time'], $job['apiJobUpdate']);

        // Execute job command by php curl(get)
        $options = [
            CURLOPT_URL            => $job['command'],
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true
        ];

        $curl = CURL::init($options);

        $response = CURL::exec($curl);

        CURL::close($curl);

        if ($response === false) {
            throw new Exception('Error: Failed to list URLs via API. CURL Error Number: "' . curl_errno($curl) . '". CURL Error: "' . curl_error($curl) . '"');
        }

        // Check maximum execution time
        if (self::expired($job['start_time'], $job['execution_time'])) {
            exit();
        }

        // Add log and update job status
        self::logAndUpdateStatus($job['id'], $response, $job['apiJobUpdate'], 0);
    }

    public static function findCommandByComment($comment, $commandArr)
    {
        $retValue = -1;
        $length   = count($commandArr);

        for ($i = 0; $i < $length; $i++) {
            if (trim($commandArr[$i][2]) == trim($comment)) {
                $retValue = $i;
                break;
            }
        }

        return $retValue;
    }

    public static function logTimeout($jobId, $startTime, $executionTime, $apiJobUpdate)
    {
        if (self::expired($startTime, $executionTime)) {
            $message = 'Fatal error: Maximum execution time of ' . $executionTime . ' seconds exceeded';
            self::logAndUpdateStatus($jobId, $message, $apiJobUpdate, 0);
        }
    }

    public static function expired($startTime, $executionTime)
    {
        if ($executionTime == 0) {
            return false;
        }

        $expired_timestamp = (new \DateTime($startTime))->getTimestamp() + intval($executionTime);
        $current_timestamp = time();

        return $current_timestamp >= $expired_timestamp;
    }

    public static function logAndUpdateStatus($jobId, $message, $apiJobUpdate, $updateStatus)
    {
        try {
            // Add log
            $options = [
                CURLOPT_URL            => API_LOG_ADD,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => [
                    'job_id'   => $jobId,
                    'response' => $message,
                    'token'    => API_TOKEN
                ],
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true
            ];

            $curl = CURL::init($options);
            $limit = 3;

            for ($i = 0; $i < $limit; $i++) {
                $response = CURL::exec($curl);

                if ($response !== false) {
                    break;
                }

                sleep(1);
            }

            CURL::close($curl);

            if ($response === false) {
                $message = 'Failed to add log for job {' . $jobId . '} via API';
                $slackMessage = 'Failed to log entry to Infrastructure multiple times, falling back to slack: \"' . $message .
                    '\", for additional information please check the log on disk.';
                IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, $slackMessage);
                throw new Exception($message . ' CURL Error Number: "' . curl_errno($curl) . '". CURL Error: "' . curl_error($curl) . '"');
            }
        } catch (Exception $ex) {
            IO::logError($ex->getMessage());
            return false;
        }
        // Update job status
        self::updateJobStatus($apiJobUpdate, $jobId, $updateStatus);
    }
}
