<?php

// Define environment variables
define('API_JOB_LIST', 'example_api_uri');
define('API_JOB_GET', 'example_api_uri');
define('API_JOB_UPDATE', 'example_api_uri');
define('API_LOG_ADD', 'example_api_uri');
define('API_TOKEN', 'example_token');

// Define slack constants
define('SLACK_URL', 'example');
define('SLACK_CHANNEL', 'example');
define('SLACK_USERNAME', 'example');

// Define environment constants
define('RECONCILIATION-HOSTS', '');

// Define APIBROKER connections
define('API_BROKER_CONNECTIONS',[
    [
        'dns'      => '',
        'username' => '',
        'password' => '',
        'name'     => ''
    ]
]);
