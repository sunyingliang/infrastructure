<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

if (is_readable(__DIR__ . '/config/common.php')) {
    require(__DIR__ . '/config/common.php');
} else {
    die('Error: Configuration file is missing');
}

if (is_readable(__DIR__ . '/classes/CronTabHelper.php')) {
    require(__DIR__ . '/classes/CronTabHelper.php');
} else {
    die('Error: CronTabHelper file is missing');
}

echo PHP_EOL . 'Starting auto-reconciliation script...' . PHP_EOL;

if (empty(API_BROKER_CONNECTIONS) || !is_array(API_BROKER_CONNECTIONS[0])) {
    die('Error: No APIBROKER databases configured to process');
}

if (empty(RECONCILIATION_HOSTS)) {
    die('Error: No hosts configured to process');
}

foreach (API_BROKER_CONNECTIONS as $connection) {

    echo 'Connecting to APIBROKER ' . $connection['dns'] . '... ';
    $pdo = connectAPIBroker($connection);
    echo 'OK' . PHP_EOL;

    echo 'Fetching customers... ';
    $customers   = getCustomers($pdo, RECONCILIATION_HOSTS);
    echo 'OK' . PHP_EOL;

    echo 'Fetching cryptography... ';
    $routingSalt = getRoutingSalt($pdo);
    echo 'OK' . PHP_EOL;

    echo 'Sending requests...' . PHP_EOL;
    sendApi($customers, $routingSalt);
}

die('Finished auto-reconciliation script.' . PHP_EOL);

function connectAPIBroker($config)
{
    return new PDO($config['dns'], $config['username'], $config['password']);
}

function getCustomers(\PDO $pdo, $reconciliationHost)
{
    $hosts      = explode(',', $reconciliationHost);
    $params     = [];
    $hostString = '';

    foreach ($hosts as $key => $value) {
        $params[':' . $key] = '%' . trim($value);
        $hostString .= ' OR `domains`.`Host` LIKE ' . ':' . $key;
    }

    $hostString = ltrim($hostString, ' OR');

    $sql  = "SELECT `customers`.`ID` AS id,
                    `customers`.`Name` AS name,
                    `customers`.`salt`,
                    `domains`.`Host` AS host
            FROM `domains`
                INNER JOIN `customers` ON `customers`.`ID` = `domains`.`Customer_ID`
            WHERE " . $hostString;

    $query = $pdo->prepare($sql);
    $query->execute($params);

    return $query->fetchAll(\PDO::FETCH_ASSOC);
}

function getRoutingSalt(\PDO $pdo)
{
    $sql = "SELECT `salt`
            FROM `routing` 
            WHERE `handler` = 'handler_payment_reconciliation' 
                AND `endpoint` = 1";

    $stmt = $pdo->query($sql);

    $result = $stmt->fetch();

    return $result['salt'];
}

function generateHash($customerSalt, $routingFragmentSalt)
{
    $path = 'cdb/payment/reconciliation';

    return hash('sha512', $customerSalt . $routingFragmentSalt . $path);
}

function sendApi($customers, $routingFragmentSalt)
{
    if (!is_array($customers) || empty($customers)) {
       return false;
    }

    foreach ($customers as $key => $customer)
    {
        $sslSocket = fsockopen('ssl://'. $customer['host'], 443, $errorNumber, $errorString, 30);

        if (!$sslSocket) {
            echo $errorString . ' (' . $errorNumber . ')' . PHP_EOL;
        } else {
            $out = 'GET /api/cdb/payment/reconciliation/runAll?apiKey=' . generateHash($customer['salt'], $routingFragmentSalt) . ' HTTP/1.1' . PHP_EOL;
            $out .= 'Host: ' . $customer['host'] . PHP_EOL;
            $out .= 'Connection: Close' . PHP_EOL . PHP_EOL;

            try {
                fwrite($sslSocket, $out);

                echo 'Successfully sent command to ' . $customer['host'] . PHP_EOL;
            } catch (Exception $e) {
                echo 'Failed sending command to ' . $customer['host'] . PHP_EOL;
            }

            fclose($sslSocket);
            session_write_close();
        }

        // Process five per second~ spam avoidance measure
        usleep(200);
    }
}
