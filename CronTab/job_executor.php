<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

if (is_readable(__DIR__ . '/config/common.php')) {
    require(__DIR__ . '/config/common.php');
} else {
    die('Error: Configuration file is missing');
}

if (is_readable(__DIR__ . '/classes/CronTabHelper.php')) {
    require(__DIR__ . '/classes/CronTabHelper.php');
} else {
    die('Error: CronTabHelper file is missing');
}

use FS\Common\CURL;

if (empty($argv[1]) || !is_numeric($argv[1])) {
    die('Error: Passed in parameter {jobId} is missing or illegal');
}

$id = $argv[1];

try {
    // Get job details by id
    $options = [
        CURLOPT_URL            => API_JOB_GET,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => [
            'id'    => $id,
            'token' => API_TOKEN
        ],
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true
    ];

    $curl = CURL::init($options);

    $response = CURL::exec($curl);

    CURL::close($curl);

    if ($response === false) {
        throw new Exception('Failed to get details of job {' . $id . '} via API. CURL Error Number: "' . curl_errno($curl) . '". CURL Error: "' . curl_error($curl) . '"');
    }

    $responseArr = json_decode($response, true);

    if (json_last_error() !== JSON_ERROR_NONE) {
        throw new Exception('Could not parse data returned from Infrastructure. Data: "' . $response . '"');
    }

    if ($responseArr['response']['status'] === 'error') {
        throw new Exception($responseArr['response']['message']);
    }

    $job                 = $responseArr['response']['data'];
    $job['apiJobUpdate'] = API_JOB_UPDATE;
    $job['apiLogAdd']    = API_LOG_ADD;

    // Check if execution delay is enabled
    $delayTime        = $job['delay'] == 1 ? rand(0, 30) : 0;
    $jobExecutionTime = $job['execution_time'] == 0 ? $job['execution_time'] : $job['execution_time'] + $delayTime;

    set_time_limit($jobExecutionTime);
    sleep($delayTime);

    // Check execution condition and execute the job if conditions pass checking
    if ($job['concurrent'] == 1) {
        CronTabHelper::executeJob($job);
    } else {
        if ($job['started'] == 0) {
            CronTabHelper::executeJob($job);
        } else {
            $max_execute_timestamp = (new \DateTime($job['start_time']))->getTimestamp() + intval($job['execution_time']);
            $current_timestamp     = time();

            if ($current_timestamp > $max_execute_timestamp) {
                CronTabHelper::executeJob($job);
            }
        }
    }
} catch (Exception $e) {
    CronTabHelper::logAndUpdateStatus($id, $e->getMessage(), API_JOB_UPDATE, 0);
}
