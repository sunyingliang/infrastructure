<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

if (is_readable(__DIR__ . '/config/common.php')) {
    require(__DIR__ . '/config/common.php');
} else {
    die('Error: Configuration file is missing');
}

if (is_readable(__DIR__ . '/classes/CronTabHelper.php')) {
    require(__DIR__ . '/classes/CronTabHelper.php');
} else {
    die('Error: CronTabHelper file is missing');
}

if (is_readable(__DIR__ . '/classes/CrontabManager/CrontabRepository.php')) {
    require(__DIR__ . '/classes/CrontabManager/CrontabRepository.php');
} else {
    die('Error: Configuration file is missing');
}

use FS\Common\CURL;
use FS\Common\IO;

set_time_limit(600);

try {
    // Call list api to list all enabled jobs from db
    $options = [
        CURLOPT_URL            => API_JOB_LIST,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => 'token=' . API_TOKEN . '&enabled=1',
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true
    ];

    $curl = CURL::init($options);

    $response = CURL::exec($curl);

    CURL::close($curl);

    if ($response === false) {
        throw new Exception('Failed to list urls via api. CURL Error Number: "' . curl_errno($curl) . '". CURL Error: "' . curl_error($curl) . '"');
    }

    $responseArr = json_decode($response, true);

    if ((json_last_error() !== JSON_ERROR_NONE)) {
        throw new Exception('Could not parse JSON');
    }

    if ($responseArr['response']['status'] === 'error' || empty($responseArr['response']['data'])) {
        throw new Exception($responseArr['response']['message']);
    }

    // Write listed jobs into OS crontab
    $cronCommands = [];

    foreach ($responseArr['response']['data'] as $row) {
        $cronCommands[] = [$row['frequency'], 'php /home/ubuntu/bin/CronTab/job_executor.php ' . $row['id'], $row['name']];
    }

    $crontabRepository = new CrontabRepository(new CrontabAdapter());

    // Delete cron jobs which are removed from db
    foreach ($crontabRepository->getJobs() as $cronJob) {
        if (CronTabHelper::findCommandByComment($cronJob->comments, $cronCommands) === -1) {
            $crontabRepository->removeJob($cronJob);
        }
    }

    foreach ($cronCommands as $crontabArgs) {
        $crontabLine = sprintf('%s %s #%s', $crontabArgs[0], str_replace('{DIR}', __DIR__, $crontabArgs[1]), $crontabArgs[2]);

        $taskSearchResults = $crontabRepository->findJobByComment($crontabArgs[2]);

        // Update or flush insert
        if (count($taskSearchResults) == 1) {
            $crontabJob = $taskSearchResults[0];

            if ($crontabJob->formatCrontabLine() != $crontabLine) {
                $crontabJob->setFromCrontabLine($crontabLine);
            }
        } else {
            if (count($taskSearchResults) > 1) {
                foreach ($taskSearchResults as $taskResult) {
                    $crontabRepository->removeJob($taskResult);
                }
            }

            $crontabJob = CrontabJob::createFromCrontabLine($crontabLine);
            $crontabRepository->addJob($crontabJob);
        }
    }

    $crontabRepository->persist();
} catch (Exception $e) {
    IO::logError($e->getMessage());
    die($e->getMessage());
}
