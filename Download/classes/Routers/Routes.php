<?php

namespace FS\Routers;

class Routes
{
    private $routes = [];

    public function get($pattern, $callback)
    {
        $this->route($pattern, 'GET', $callback);
    }

    public function post($pattern, $callback)
    {
        $this->route($pattern, 'POST', $callback);
    }

    public function put($pattern, $callback)
    {
        $this->route($pattern, 'PUT', $callback);
    }

    public function route($pattern, $method, $callback)
    {
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

        if (is_array($method)) {
            foreach ($method as $m) {
                $this->routes[$pattern][$m] = $callback;
            }
        } else if (is_string($method)) {
            $this->routes[$pattern][$method] = $callback;
        }
    }

    public function execute()
    {
        $url    = $_SERVER['PATH_INFO'] ?? $_SERVER['REDIRECT_URL'];
        $method = $_SERVER['REQUEST_METHOD'];
        foreach ($this->routes as $pattern => $methods) {
            if (preg_match($pattern, $url, $params)) {
                if (isset($methods[$method])) {
                    array_shift($params);

                    try {
                        $response = call_user_func_array($methods[$method], array_values($params));

                        if (isset($response)) {
                            if (is_array($response) || is_object($response)) {
                                // convert array to json string
                                header('content-type: application/json');
                                $response = json_encode($response);
                            }
                            exit($response);
                        }
                        // Not Found
                        http_response_code(404);
                        exit();
                    } catch (\Exception $e) {
                        // Internal Server Error
                        http_response_code(500);
                        exit($e->getMessage());
                    }
                } else {
                    // Method Not Allowed
                    http_response_code(405);
                    exit();
                }
            }
        }
        // Bad Request, no matching path found
        http_response_code(400);
        exit();
    }
}