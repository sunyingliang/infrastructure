<?php

namespace FS\Access;

class Components extends ServiceBase
{
    public function __construct()
    {
        $this->token    = INFRASTRUCTURE_SERVICE_TOKEN;
        $this->location = INFRASTRUCTURE_SERVICE_LOCATION;
    }

    public function getDownload($path)
    {
        $url = $this->parseURL('download/customer-' . $path);

        if ($remoteAddress = $_SERVER['REMOTE_ADDR'] ?? '') {
            $url .= '&remoteAddress=' . urlencode($remoteAddress);
        }

        $input = $this->getJsonBody();

        $this->convertParams2Json($url, $input);

        $response = $this->parseCURLRequest('POST', $url, $this->convertJSONToForm($input));

        header('Location: ' . $response->response->data);
        exit();
    }

    public function getResult($path)
    {
        $url = $this->parseURL('download/customer-' . $path);

        $input = $this->getJsonBody();

        $this->convertParams2Json($url, $input);

        $response = $this->parseCURLRequest('POST', $url, $this->convertJSONToForm($input));

        return $response;
    }

    private function convertParams2Json(&$url, &$json)
    {
        $urlArr = explode('?', $url);
        $url    = $urlArr[0];

        if (isset($urlArr[1])) {
            $itemArr = explode('&', $urlArr[1]);

            foreach ($itemArr as $item) {
                $paramItem             = explode('=', $item);
                $json->{$paramItem[0]} = $paramItem[1];
            }
        }
    }
}
