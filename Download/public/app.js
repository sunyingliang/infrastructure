angular.module('app', []).controller('AppCtrl', ['$http', function ($http) {
    var vm = this;
    vm.loading = 1;
    vm.year = (new Date).getFullYear();
    $http.get('api/components').then(function (response) {
        vm.components = response.data.response.data;
        vm.loading = 0;
    });
}]);