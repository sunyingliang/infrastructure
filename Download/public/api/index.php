<?php

require '../../autoload.php';
require '../../config/common.php';

use FS\Access\Components;
use FS\Routers\Routes;

$router = new Routes();

$router->get('/(download/\w+)', function ($path) {
    (new Components())->getDownload($path);
});

$router->get('/(version/\w+)', function ($path) {
    return (new Components())->getResult($path);
});

$router->get('/(components)', function ($path) {
    return (new Components())->getResult($path);
});

$router->execute();
