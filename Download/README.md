# FS-Download

This provides a UI for customers to download locally-installed components from us

This also provide an API to return the current version based on the latest date for each component available at:

GET: /api/version/{component name}

eg request:
GET: /api/version/LIM
response:
"1.1.0"

The url to download the latest version of a file (logs the download and redirect to actual file url)

GET: /api/download/{component name}

Or get a specific version of a file

GET: /api/download/{component name}?version={version}