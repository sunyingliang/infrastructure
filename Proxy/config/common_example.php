<?php

// Define api constants
define('API_ENDPOINT', 'example_infrastructure_endpoint');
define('API_ENDPOINT_LIST', 'example_api_proxy_mapping_get');
define('API_ENDPOINT_CONFIG', 'example_api_proxy_config');
define('API_ENDPOINT_BUILD', 'example_api_infrastructure_build_config');
define('API_TOKEN', 'example_token');

// Define configure path constant
define('CONFIGURE_FILE_PATH', 'example_apache2_Proxy.conf');

// Define mail constants
define('MAIL_SERVER', 'smtp.firmstep.com');
define('MAIL_PORT', 25);
define('MAIL_FROM', 'example_email_from');
define('MAIL_TO', 'example_email_to');
define('MAIL_SUBJECT', 'example_email_subject');
define('MAIL_MESSAGE', 'example_email_message');

// Define slack constants
define('SLACK_URL', 'example');
define('SLACK_CHANNEL', 'example');
define('SLACK_USERNAME', 'example');
