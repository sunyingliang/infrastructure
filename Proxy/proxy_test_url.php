<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

if (is_readable(__DIR__ . '/config/common.php')) {
    require(__DIR__ . '/config/common.php');
} else {
    die('Error: Configuration file is missing');
}

require(__DIR__ . '/classes/Common/CURL.php');
require(__DIR__ . '/classes/Common/IO.php');
require(__DIR__ . '/classes/Common/Mail.php');

use FS\Common\CURL;
use FS\Common\IO;
use FS\Common\Mail;

set_time_limit(3600);

try {
    // Call proxy_mapping list api
    $options = [
        CURLOPT_URL            => API_ENDPOINT . API_ENDPOINT_LIST,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS     => 'token=' . API_TOKEN
    ];

    $curl     = CURL::init($options);
    $response = CURL::exec($curl);

    CURL::close($curl);

    if ($response === false) {
        throw new Exception('Failed to get Proxy mapping from Infrastructure API');
    }

    $responseArr = json_decode($response, true);

    if (json_last_error() !== JSON_ERROR_NONE) {
        throw new Exception('Could not parse data returned from Infrastructure. Data: "' . $response . '"');
    }

    if ($responseArr['response']['status'] === 'error' || empty($responseArr['response']['data'])) {
        throw new Exception($responseArr['response']['message']);
    }

    // Test urls for proxy mapping
    $failedURLs = [];

    foreach ($responseArr['response']['data']['rows'] as $row) {
        if (empty($row['test_url'])) {
            continue;
        }

        $url     = rtrim($row['url'], '/') . '/' . ltrim($row['test_url'], '/');
        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT        => 20,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $curl = CURL::init($options);

        if (curl_exec($curl) === false) {
			IO::message($url . " failed: " . curl_error($curl) . "\r\n");
            $failedURLs[] = '000 - ' . $url;
        } else {
            $info = CURL::getInfo($curl);

            if ($info === false) {
                throw new Exception('Failed to get curl info');
            }

            if ($info['http_code'] != 200) {
				IO::message($url . " error: " . $info['http_code'] . "\r\n");
                $failedURLs[] = $info['http_code'] . ' - ' . $url;
            }
        }

        CURL::close($curl);
    }

    // Send email with error messages
    $config = [
        'server'  => MAIL_SERVER,
        'port'    => MAIL_PORT,
        'from'    => MAIL_FROM,
        'to'      => MAIL_TO,
        'subject' => MAIL_SUBJECT,
        'message' => MAIL_MESSAGE
    ];

    $mail = new Mail($config);

    if (empty($failedURLs)) {
        $mail->setMessage('There are no Failed Proxy URLs');
    } else {
        $message = 'The following is the list of failed Proxy URLs:' . PHP_EOL . PHP_EOL;
        $message .= implode(PHP_EOL, $failedURLs);

        $mail->setMessage($message);
    }

    $mail->send();
} catch (Exception $e) {
    die($e->getMessage() . PHP_EOL);
}
