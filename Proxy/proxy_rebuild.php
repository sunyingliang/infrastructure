<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) {
    die('Error: register_argc_argv is not on, please check configuration in php.ini');
}

if (is_readable(__DIR__ . '/config/common.php')) {
    require(__DIR__ . '/config/common.php');
} else {
    die('Error: Configuration file is missing');
}

require(__DIR__ . '/classes/Common/CURL.php');
require(__DIR__ . '/classes/Common/IO.php');

use FS\Common\CURL;
use FS\Common\IO;

set_time_limit(59);

try {
    if ($argc > 1) {
        $silence = in_array($argv[1], [1, 'true', 'True', 'TRUE']) ? 1 : (in_array($argv[1], [0, 'false', 'False', 'FALSE']) ? 0 : -1);
        if ($silence === -1) {
            IO::message('Error: command line format error. The format must be {php script.php [1|0|true|false]}', 0, true);
        }
    }

    $errorFile = __DIR__ . '/../../log/connection_error.txt';

    // Check internet connection
    $connectionAttemptCount    = 0;
    $connectionAttemptCountMax = 30;

    while (true) {
        // Call Infrastructure build to test connection
        $options = [
            CURLOPT_URL            => API_ENDPOINT . API_ENDPOINT_BUILD,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST'
        ];

        $curl     = Curl::init($options);
        $response = Curl::exec($curl);
        $info     = Curl::getInfo($curl);

        Curl::close($curl);

        if ($info['http_code'] == 200) {
            break;
        } else if ($connectionAttemptCount >= $connectionAttemptCountMax) {
            throw new Exception('Failed to establish connection to Infrastructure');
        }

        $connectionAttemptCount++;
        sleep(1);
    }

    IO::message('Initialising Proxy API VirtualHost daemon');

    // Call proxy configure file api
    $options = [
        CURLOPT_URL            => API_ENDPOINT . API_ENDPOINT_CONFIG,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS     => 'token=' . API_TOKEN
    ];

    $curl     = CURL::init($options);
    $response = CURL::exec($curl);
    $info     = CURL::getInfo($curl);

    CURL::close($curl);

    if ($info === false) {
        throw new Exception('Failed to get curl information');
    }

    if ($info['http_code'] == 0 || $info['http_code'] == 408 || $info['http_code'] == 504) {
        throw new Exception('Infrastructure connection timed out - Could not fetch new configuration. HTTP Code: "' . $info['http_code'] . '"');
    } else if ($info['http_code'] == 500) {
        if (isset($response['message']) && !empty($response['message'])) {
            throw new Exception('Infrastructure returned an error - Could not fetch new configuration: ' . $response['message']);
        } else {
            throw new Exception('Infrastructure returned an unknown error - Could not fetch new configuration. CURL ERROR: "' . curl_error($curl). '"');
        }
    } else if ($info['http_code'] != 200) {
        throw new Exception($info['http_code'] . ', Infrastructure connection failed');
    }

    if ($response === false) {
        throw new Exception('Failed to call curl function. CURL ERROR: "' . curl_error($curl) . '"');
    }

    $response = json_decode($response, true);

    if ($response['response']['status'] != 'success') {
        throw new Exception('Failed to get configure string via API. Response: "' . $response['response']['message'] . '"');
    }

    IO::message('Heartbeat confirmed, fetching data');

    // No connection error, send success message to slack, delete connect_error.txt
    if (file_exists($errorFile)) {
        if (!empty($errorFile)) {
            IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'Infrastructure connection re-established, configuration fetched successfully');
        }

        unlink($errorFile);
    }

    // Compare configure file and update if necessary
    IO::message('Comparing data');

    $currentConfig                = file_get_contents(CONFIGURE_FILE_PATH);
    $response['response']['data'] = html_entity_decode($response['response']['data']);

    if ($response['response']['data'] != $currentConfig) {
        IO::message('Files are different, update needed');
        IO::message('Updating configuration');
        IO::writeFile(CONFIGURE_FILE_PATH, 'w', $response['response']['data']);
        IO::message('Reloading Apache Configuration');
        IO::message(shell_exec("sudo service apache2 reload"));
        IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, 'Configuration updated.');
        IO::message('Complete');
    } else {
        IO::message('Hashes are the same, does not require update', 0, true);
    }
} catch (Exception $e) {
    if (!file_exists($errorFile)) {
        mkdir(dirname($errorFile), 0777, true);
    }

    if (!file_exists($errorFile) || (file_exists($errorFile) && file_get_contents($errorFile) != $e->getMessage())) {
        IO::slack(SLACK_URL, SLACK_CHANNEL, SLACK_USERNAME, $e->getMessage());
        IO::writeFile($errorFile, 'w', $e->getMessage());
    }

    IO::logError($e->getMessage());
    IO::message('Error: ' . $e->getMessage(), 0, true);
}

