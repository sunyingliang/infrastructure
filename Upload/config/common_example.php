<?php

// Constants for DB
define('DB_DRIVER', 'example');
define('DB_HOST', 'example');
define('DB_NAME', 'example');
define('DB_PORT', '3306');
define('DB_USERNAME', 'example');
define('DB_PASSWORD', 'example');

// Constants for aws-sdk-s3
define('S3_REGION', 'example');
define('S3_VERSION', 'latest');
define('S3_CREDENTIAL_KEY', 'example');
define('S3_CREDENTIAL_SECRET', 'example');
define('S3_BUCKET', 'example');

// Constants for mail
define('MAIL_SERVER', 'example');
define('MAIL_PORT', '25');
define('MAIL_SUBJECT', 'File Upload Live');
define('MAIL_FROM', '');
define('MAIL_TO', '');
define('MAIL_DOWNLOAD', 'example');
