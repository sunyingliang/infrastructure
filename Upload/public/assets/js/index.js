$(function () {
    if (typeof FormData !== 'function') {
        alert('Please check your browser is supported. \r\nRequired: Chrome >= 7, Firefox >= 4.0, IE >= 10, Opera >= 12, Safari >= 5');
        return;
    }

    $('#upload_file').change(function () {
        validate(['upload_file']);
    });

    $('#description').keyup(function () {
        validate(['description']);
    });

    $('#btn_upload').click(function () {
        if (!validate(['upload_file', 'description'])) {
            return false;
        }
        $('#btn_upload').prop('disabled', true);
        $('#mask').css('display', 'block');
        // Construct FormData and send by ajax
        var formData = new FormData(), _file = document.getElementById('upload_file'), cb_success = submitSuccess, cb_error = submitError;
        formData.set('UploadedFile', _file.files[0]);
        formData.set('Description', $('#description').val());
        $.ajax({
            url: './src/ajax_upload_file.php',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: cb_success,
            error: cb_error
        });
    });

    $('#ln_go_index').click(function () {
        switchPanal('page_content_thanks', 'page_content_index');
    });
});

/*
 utils
 */
function submitSuccess(data, status, xhr) {
    console.log('return: ', data);
    if (data.response.status == 'error') {
        $('#warning_form').html(data.response.message).addClass('shown');
        $('#btn_upload').prop('disabled', false);
        $('#mask').css('display', 'none');
        return false;
    }
    resetIndex();
    switchPanal('page_content_index', 'page_content_thanks');
}

function submitError(xhr, errStatus, errStr) {
    $('#warning_form').html(errStr).addClass('shown');
    $('#btn_upload').prop('disabled', false);
    $('#mask').css('display', 'none');
    alert(errStr);
}

function validate(arr) {
    var flag = true, length = arr.length;
    for (var i = 0; i < length; i++) {
        if (!$('#' + arr[i]).val().trim()) {
            flag = false;
            $('#warning_' + arr[i]).removeClass('hidden').addClass('shown');
        } else {
            $('#warning_' + arr[i]).removeClass('shown').addClass('hidden');
        }
    }
    return flag;
}

function resetIndex() {
    $('#upload_file, #description').val('');
    $('#warning_upload_file, #warning_description, #warning_form').removeClass('shown').addClass('hidden');
    $('#btn_upload').prop('disabled', false);
    $('#mask').css('display', 'none');
}

function switchPanal(id1, id2) {
    $('#' + id1).css('display', 'none');
    $('#' + id2).css('display', 'block');
}