<?php

if ($_FILES['UploadedFile']['error'] > 0) {
    outputJson('Error occurred when uploading');
}

if (!checkFileSize($_FILES['UploadedFile']['size'])) {
    outputJson('File uploaded exceeds maximum upload size (' . ini_get('upload_max_filesize') . ')');
}

// Save file to s3 using s3 api...
require __DIR__ . '/../../vendor/autoload.php';

try {
    $configPath = __DIR__ . '/../../config/common.php';

    if (!is_readable($configPath)) {
        throw new \Exception('Failed to load configurations from ' . $configPath);
    }

    require_once $configPath;

    if (!is_uploaded_file($_FILES['UploadedFile']['tmp_name'])) {
        throw new Exception('File is not uploaded successfully');
    }

    $s3Helper = new \FS\S3\Helper();
    $uniqueId = \FS\Common\IO::guid();
    $options  = [
        'Bucket'      => S3_BUCKET,
        'Key'         => $uniqueId . '/' . $_FILES['UploadedFile']['name'],
        'SourceFile'  => $_FILES['UploadedFile']['tmp_name'],
        'ContentType' => $_FILES['UploadedFile']['type'],
        'ACL'         => 'public-read'
    ];

    $url = $s3Helper->upload($options);

    if ($url === false) {
        throw new Exception('Failed to putObject with S3SDK');
    }

    $dbHelper   = new \FS\DB\Helper();
    $uploadFile = new \FS\Entity\UploadFile(['fileName' => $_FILES['UploadedFile']['name'], 'uniqueId' => $uniqueId]);

    if ($dbHelper->add($uploadFile) === false) {
        throw new Exception('Failed to insert record into db');
    }

    $mail    = new \FS\Mail\Mail();
    $message = 'A file has been uploaded: ' . $_FILES['UploadedFile']['name'] . PHP_EOL;
    $message .= 'Description: ' . $_POST['Description'] . PHP_EOL;
    $message .= 'Source IP Address: ' . $_SERVER['REMOTE_ADDR'] . PHP_EOL . PHP_EOL;
    $message .= MAIL_DOWNLOAD . $uniqueId . '/' . $_FILES['UploadedFile']['name'] . PHP_EOL;
    $message .= '(This link is valid for one week only, after which the file will be deleted.)';

    $mail->setSubject(MAIL_SUBJECT);
    $mail->setMessage($message);
    $mail->send();
} catch (\Exception $e) {
    outputJson($e->getMessage());
}

outputJson('file uploaded', 'success');

#region Utils
function outputJson($message, $status = 'error')
{
    header('Content-Type: application/json');

    die(json_encode([
        'response' => [
            'status'  => $status,
            'message' => $message
        ]
    ]));
}

function checkFileSize($fileSize)
{
    $uploadMaxFileSize = ini_get('upload_max_filesize');
    $max               = substr($uploadMaxFileSize, 0, -1);
    $unit              = strtolower(substr($uploadMaxFileSize, -1));

    switch ($unit) {
        case 'm':
            $fileSize = ceil($fileSize / (1024 * 1024));
            break;
        case 'k':
            $fileSize = ceil($fileSize / 1024);
            break;
        default:
            $fileSize = ceil($fileSize / (1024 * 1024));
    }

    return $fileSize <= $max;
}
#endregion
