<?php

if (isset($_GET['guid'])) {
    require __DIR__ . '/../../vendor/autoload.php';

    $uploadFile = (new \FS\DB\Helper())->getByUniqueId($_GET['guid']);

    if ($uploadFile->getDeleted() == 0) {
        $configPath = __DIR__ . '/../../config/common.php';

        if (!is_readable($configPath)) {
            throw new \Exception('Failed to load configurations from ' . $configPath);
        }

        require_once $configPath;

        $fullPath = 'https://s3-' . S3_REGION . '.amazonaws.com/' . S3_BUCKET . '/' . $_GET['guid'] . '/' . $uploadFile->getFileName();

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $uploadFile->getFileName() . '"');
        header('Connection: close');
        readfile($fullPath);
        die();
    }
}
