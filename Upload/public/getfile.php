<?php
require './src/inc_get_file.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Firmstep</title>
        <link type="image/png" rel="shortcut icon" href="./assets/img/fav.png">
        <link rel="stylesheet" type="text/css" media="all" href="./assets/css/style.css"/>
    </head>
    <body>
        <div id="pagecontainer">
            <div class="grid_12" id="header" style="width:100%">
                <div class="grid_2 alpha" id="logo">
                    <a href="http://www.firmstep.com/">
                        <img src="/assets/img/firmstep_logo.png" alt="Firmstep logo"/>
                    </a>
                </div>
                <div class="grid_10 omega"></div>
            </div>
            <div class="clear">&nbsp;</div>
            <!-- end of header block -->
            <!-- start of main content block -->
            <div class="grid_8" id="main_content">
                <div id="content" class="nodropshadow">
                    <div id="template-WideContent">
                        <div id="pageContent">
                            <h1>File Upload Form</h1>
                            <p>Sorry, that file could not be found. It may have been deleted.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
