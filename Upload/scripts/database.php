<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../config/common.php';

// Config settings
$config       = [];
$config['db'] = [
    'host'      => DB_HOST,
    'name'      => DB_NAME,
    'username'  => DB_USERNAME,
    'password'  => DB_PASSWORD
];

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        if (!is_array($config)) {
            die('Error: Configuration file is not set properly');
        }

        $pdo = new PDO('mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'], $config['db']['username'], $config['db']['password']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database "' . $config['db']['name'] . '" on "' . $config['db']['host'] . '" server');
}

#region Create tables for NET-225
// Create table 'upload_file' if not exists
$sql = "CREATE TABLE IF NOT EXISTS `upload_file` (
            `file_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
            `filename` VARCHAR(512) NOT NULL DEFAULT '',
            `unique_identifier` VARCHAR(512) NOT NULL DEFAULT '',
            `time_stamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `deleted` TINYINT(4) NOT NULL DEFAULT '0',
            PRIMARY KEY (`file_id`)
        )";

if ($pdo->exec($sql) === false) {
    die('Error: SQL statement error. Check your sql statement {' . $sql . '}');
}
#endregion
