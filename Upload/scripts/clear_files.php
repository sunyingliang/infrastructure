<?php

if (php_sapi_name() != 'cli') {
    die('Must run via cli.');
}

require __DIR__ . '/../vendor/autoload.php';

$configPath = __DIR__ . '/../config/common.php';

if (!is_readable($configPath)) {
    throw new \Exception('Failed to load configurations from ' . $configPath);
}

require_once $configPath;

try {
    $dbHelper = new \FS\DB\Helper();
    $s3Helper = new \FS\S3\Helper();

    $sql = "UPDATE `upload_file` SET `deleted` = 1 WHERE `time_stamp` < DATE_SUB(NOW(), INTERVAL 7 DAY)";

    if ($dbHelper->execute($sql) === false) {
        throw new \Exception('Failed to update upload_file. SQL: ' . $sql);
    }

    // Note: We only soft delete the file after 7 days, we hard delete after 14
    $sql  = "SELECT `file_id`, `filename`, `unique_identifier`
             FROM `upload_file`
             WHERE `time_stamp` < DATE_SUB(NOW(), INTERVAL 14 DAY)";
    $rows = $dbHelper->query($sql);

    foreach ($rows as $row) {
        echo 'Deleting ' . $row['filename'] . ' (' . $row['unique_identifier'] . ')' . PHP_EOL;

        if ($s3Helper->delete(['Bucket' => S3_BUCKET, 'Key' => $row['unique_identifier'] . '/' . $row['filename']]) !== false) {
            if ($dbHelper->deleteById($row['file_id'])) {
                echo 'Deleted ' . $row['filename'] . ' (' . $row['unique_identifier'] . ')' . PHP_EOL;
            }
        }
    }
} catch (\Exception $e) {
    die('Error: ' . $e->getMessage());
}
