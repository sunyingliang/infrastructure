prerequisite
=========================================
1. php version(>=5.4) is installed;
2. composer is installed (Refer to Proxy readme for more detailed composer installation instructions);


configuration  
=========================================
1. deploy all these folders & files in web server;
2. configure 'public' folder as web server's document root, or change 'public' folder name to web server's document root;
3. go to the folder where composer.json is located, and run 'php composer install' (or, 'php composer.phar install' based on composer's name);
4. rename all configuration files under /config folder to the name without '_example', e.g. change common_example.php to common.php;
5. setup configurations under /config, you can get the test config in s3://fs-deploy/configurations if you don't need a localalised installation;
6. run 'php /your-path-to-database.php/database.php' to create essential databases and tables;
