<?php

namespace FS\S3;

class Helper
{
    protected $client;

    public function __construct()
    {
        $this->client = Manager::getInstance();
    }

    public function upload(array $options)
    {
        $mandatory = ['Bucket', 'Key', 'SourceFile', 'ContentType', 'ACL'];

        foreach ($mandatory as $option) {
            if (!isset($options[$option])) {
                throw new \Exception('Parameter {' . $option . '} is not passed in properly');
            }
        }

        try {
            $result = $this->client->putObject($options);
            $flag   = $result['ObjectURL'];
        } catch (\Exception $e) {
            $flag = false;
        }

        return $flag;
    }

    public function delete(array $options)
    {
        $mandatory = ['Bucket', 'Key'];
        
        foreach ($mandatory as $option) {
            if (!isset($options[$option])) {
                throw new \Exception('Parameter {' . $option . '} is not passed in properly');
            }
        }

        try {
            $result = $this->client->deleteObject($options);
            $flag   = var_export($result, true);
        } catch (\Exception $e) {
            $flag = false;
        }

        return $flag;
    }
}
