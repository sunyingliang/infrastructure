<?php

namespace FS\S3;

use Aws\S3\S3Client;
use FS\Common\Exception\InvalidParameterException;

class Manager
{
    private static $client;

    public static function getInstance()
    {
        if (!isset(self::$client) || empty(self::$client)) {
            $config       = self::loadConfig();
            self::$client = new S3Client($config);
        }

        return self::$client;
    }

    private static function loadConfig()
    {
        $configPath = __DIR__ . '/../../config/common.php';

        if (is_readable($configPath)) {
            require_once $configPath;
        } else {
            throw new InvalidParameterException('Failed to load configurations from ' . $configPath);
        }

        $mandatory = ['S3_REGION', 'S3_VERSION', 'S3_CREDENTIAL_KEY', 'S3_CREDENTIAL_SECRET'];
        
        foreach ($mandatory as $item) {
            if (!defined($item)) {
                throw new InvalidParameterException('Constant {' . $item . '} is not configured correctly in ' . $configPath);
            }
        }

        return [
            'version'     => S3_VERSION,
            'region'      => S3_REGION,
            'credentials' => [
                'key'    => S3_CREDENTIAL_KEY,
                'secret' => S3_CREDENTIAL_SECRET
            ]
        ];
    }
}
