<?php

namespace FS\DB;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Entity\UploadFile;

class Helper
{
    protected $db;

    public function __construct()
    {
        $this->db = Manager::getInstance();
    }

    public function add(UploadFile $uploadFile)
    {
        if (!($uploadFile instanceof UploadFile)) {
            throw new InvalidParameterException('Passed in parameter {uploadFile} is not setup correctly');
        }

        $flag = true;
        
        try {
            $sql  = "INSERT INTO `upload_file`(`filename`, `unique_identifier`) VALUES(:filename, :unique_identifier)";
            $stmt = $this->db->prepare($sql);

            if ($stmt === false) {
                throw new PDOCreationException('PDOStatement creation failed.');
            }

            if ($stmt->execute(['filename' => $uploadFile->getFileName(), 'unique_identifier' => $uploadFile->getUniqueId()]) === false) {
                throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
            }
        } catch (\Exception $e) {
            $flag = false;
        }

        return $flag;
    }

    public function getByUniqueId($uniqueId)
    {
        $uploadFile = new UploadFile(['uniqueId' => $uniqueId]);
        $sql        = "SELECT `filename`, `deleted` FROM `upload_file` WHERE `unique_identifier` = :unique_identifier";
        $stmt       = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute(['unique_identifier' => $uniqueId]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        if ($row = $stmt->fetch()) {
            $uploadFile->setFileName($row['filename']);
            $uploadFile->setDeleted($row['deleted']);
        }

        return $uploadFile;
    }

    public function deleteById($id)
    {
        $sql  = "DELETE FROM `upload_file` WHERE `file_id` = :file_id";
        $stmt = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute(['file_id' => $id]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        return true;
    }

    public function query($sql)
    {
        $result = [];
        $stmt   = $this->db->prepare($sql);

        if ($stmt === false) {
            throw new PDOCreationException('PDOStatement creation failed.');
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed. SQL: ' . $sql);
        }

        while ($row = $stmt->fetch()) {
            $result[] = $row;
        }

        return $result;
    }

    public function execute($sql)
    {
        return $this->db->exec($sql);
    }
}
