<?php

namespace FS\DB;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;

class Manager
{
    private static $db;

    public static function getInstance()
    {
        if (!isset(self::$db) || empty(self::$db)) {
            $config = self::loadConfig();

            try {
                $pdo = new \PDO('mysql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['dbname'], $config['username'], $config['password']);
                $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            } catch (\Exception $e) {
                throw new PDOCreationException('Failed to create PDO using the given params');
            }

            self::$db = $pdo;
        }
        return self::$db;
    }

    private static function loadConfig()
    {
        $configPath = __DIR__ . '/../../config/common.php';

        if (is_readable($configPath)) {
            require_once $configPath;
        } else {
            throw new InvalidParameterException('Failed to load configurations from ' . $configPath);
        }

        $mandatory = ['DB_HOST', 'DB_NAME', 'DB_PORT', 'DB_USERNAME', 'DB_PASSWORD'];
        
        foreach ($mandatory as $item) {
            if (!defined($item)) {
                throw new InvalidParameterException('Constant {' . $item . '} is not configured correctly in ' . $configPath);
            }
        }

        return [
            'host'     => DB_HOST,
            'dbname'   => DB_NAME,
            'port'     => DB_PORT,
            'username' => DB_USERNAME,
            'password' => DB_PASSWORD
        ];
    }
}
