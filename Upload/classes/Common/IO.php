<?php

namespace FS\Common;

class IO
{
    public static function guid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        $md5 = strtoupper(md5(uniqid(mt_srand(intval(microtime(true) * 1000)), true)));
        return substr($md5, 0, 8) . '-' . substr($md5, 8, 4) . '-' . substr($md5, 12, 4) . '-' . substr($md5, 16, 4) . '-' . substr($md5, 20, 12);
    }
}
