<?php

namespace FS\Entity;

use FS\Common\Exception\InvalidParameterException;

class UploadFile
{
    protected $fileId;
    protected $fileName;
    protected $uniqueId;
    protected $timeStamp;
    protected $deleted;

    public function __construct($data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        if (isset($data['fileId'])) {
            if (!is_numeric($data['fileId'])) {
                throw new InvalidParameterException('Passed in parameters {fileId} must be an integer');
            } else {
                $this->fileId = $data['fileId'];
            }
        } else {
            $this->fileId = 0;
        }

        if (isset($data['fileName'])) {
            if (strlen($data['fileName']) > 512) {
                $this->fileName = substr($data['fileName'], 0, 511);
            } else {
                $this->fileName = $data['fileName'];
            }
        } else {
            $this->fileName = '';
        }

        if (isset($data['uniqueId'])) {
            if (strlen($data['uniqueId']) > 512) {
                $this->uniqueId = substr($data['uniqueId'], 0, 511);
            } else {
                $this->uniqueId = $data['uniqueId'];
            }
        } else {
            $this->uniqueId = '';
        }

        if (isset($data['deleted'])) {
            if (!is_numeric($data['deleted'])) {
                throw new InvalidParameterException('Passed in parameters {deleted} must be an integer');
            } else {
                $this->deleted = $data['deleted'];
            }
        } else {
            $this->deleted = 0;
        }
    }

    #region Getters & Setters
    public function getFileId()
    {
        return $this->fileId;
    }

    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;
    }

    public function getTimeStamp()
    {
        return $this->timeStamp;
    }

    public function setTimeStamp($timeStamp)
    {
        $this->timeStamp = $timeStamp;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }
    #endregion
}
